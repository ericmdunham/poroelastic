#ifndef SBP2_H
#define SBP2_H

#include "Operator.hpp"

/*
 * 	Class for the first derivative SBP operators of second order
 * 	used for stencil calculations.
 */
class SBP2: public Operator {
	
	public:
		SBP2(double h);
};

#endif