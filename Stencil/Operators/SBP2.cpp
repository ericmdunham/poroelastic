#include "SBP2.hpp"

/* 	
*  	Constructor for the first derivative SBP operators 
* 	of second order
*/
SBP2::SBP2(double h): Operator(1, 2, 2) {
	// Set up boundaries
	leftBoundaryValues[0] = -1/h;
	leftBoundaryValues[1] = 1/h;

	// The right boundary is the left boundary but flipped
	for (int i = 0; i < boundaryRows*boundaryCols; i++) {
		rightBoundaryValues[i] = -leftBoundaryValues[boundaryRows*boundaryCols - 1 - i];
	}

	// Set up interior region
	interiorIndeces[0] = -1;
	interiorIndeces[1] = 1;

	interiorValues[0] = -1/(2*h);
	interiorValues[1] = 1/(2*h);
}
