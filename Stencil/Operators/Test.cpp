#include <iostream>
#include "SBP2.hpp"
#include "SBP4.hpp"

using namespace std;

int main() {
	// Second order operator
	SBP2 op2(1.0);

	cout << "\nInterior Data: \n";
	for (int i = 0; i < op2.interiorLength; i++) {
		cout << op2.interiorIndeces[i] << ": " << op2.interiorValues[i] << "\n";
	}

	cout << "\nLeft Boundary Data:\n";
	for (int i = 0; i < op2.boundaryRows; i++) {
		for (int j = 0; j < op2.boundaryCols; j++) {
			cout << op2.leftBoundaryValues[i*op2.boundaryCols + j] << " ";
		}
		cout << "\n";
	}

	cout << "\nRight Boundary Data:\n";
	for (int i = 0; i < op2.boundaryRows; i++) {
		for (int j = 0; j < op2.boundaryCols; j++) {
			cout << op2.rightBoundaryValues[i*op2.boundaryCols + j] << " ";
		}
		cout << "\n";
	}

	// Forth order operator
	SBP4 op4(0.1);
	cout << "\nForth Order Operator:\n";

	cout << "\nInterior Data: \n";
	for (int i = 0; i < op4.interiorLength; i++) {
		cout << op4.interiorIndeces[i] << ": " << op4.interiorValues[i] << "\n";
	}

	cout << "\nLeft Boundary Data: \n";
	for (int i = 0; i < op4.boundaryRows; i++) {
		for (int j = 0; j < op4.boundaryCols; j++) {
			cout << op4.leftBoundaryValues[i*op4.boundaryCols + j] << " ";
		}
		cout << "\n";
	}

	cout << "\nRight Boundary Data: \n";
	for (int i = 0; i < op4.boundaryRows; i++) {
		for (int j = 0; j < op4.boundaryCols; j++) {
			cout << op4.rightBoundaryValues[i*op4.boundaryCols + j] << " ";
		}
		cout << "\n";
	}

}