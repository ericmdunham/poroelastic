#include "Operator.hpp"

/*
 * 	Constructor for the base class SBP operator used for stencil calculations.
 */
Operator::Operator(int br, int bc, int il) {
	
	// Set up the boundary region
	boundaryRows = br;
	boundaryCols = bc;
	leftBoundaryValues = new double[boundaryRows*boundaryCols];
	rightBoundaryValues = new double[boundaryRows*boundaryCols];

	// Set up the interior region
	interiorLength = il;
	interiorIndeces = new int[interiorLength];
	interiorValues = new double[interiorLength];
}

/*
 * 	Destructor for the base class SBP operator used for stencil calculations.
 */
Operator::~Operator() {
	delete [] leftBoundaryValues;
	delete [] rightBoundaryValues;
	delete [] interiorIndeces;
	delete [] interiorValues;
}
