#include "SBP4.hpp"

/* 	
*  	Constructor for the first derivative SBP operators 
* 	of forth order
*/
SBP4::SBP4(double h): Operator(4, 6, 4) {
	// Set up boundaries

	// First row
	leftBoundaryValues[0] = -24/(17*h);
	leftBoundaryValues[1] = 59/(34*h);
	leftBoundaryValues[2] = -4/(17*h);
	leftBoundaryValues[3] = -3/(34*h);

	// Second row
	leftBoundaryValues[6] = -1/(2*h);
	leftBoundaryValues[8] = 1/(2*h);

	// Third row
	leftBoundaryValues[12] = 4/(43*h);
	leftBoundaryValues[13] = -59/(86*h);
	leftBoundaryValues[15] = 59/(86*h);
	leftBoundaryValues[16] = -4/(43*h);

	//Forth row
	leftBoundaryValues[18] = 3/(98*h);
	leftBoundaryValues[20] = -59/(98*h);
	leftBoundaryValues[22] = 32/(49*h);
	leftBoundaryValues[23] = -4/(49*h);

	// The right boundary is the left boundary but flipped
	for (int i = 0; i < boundaryRows*boundaryCols; i++) {
		rightBoundaryValues[i] = -leftBoundaryValues[boundaryRows*boundaryCols - 1 - i];
	}

	// The right boundary is the left boundary but flipped

	// Set up interior region
	interiorIndeces[0] = -2;
	interiorIndeces[1] = -1;
	interiorIndeces[2] = 1;
	interiorIndeces[3] = 2;

	interiorValues[0] = 1.0/(12*h);
	interiorValues[1] = -2.0/(3*h);
	interiorValues[2] = 2.0/(3*h);
	interiorValues[3] = -1.0/(12*h);
}
