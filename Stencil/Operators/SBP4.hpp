#ifndef SBP4_H
#define SBP4_H

#include "Operator.hpp"

/*
 * 	Class for the first derivative SBP operators of second order
 * 	used for stencil calculations.
 */
class SBP4: public Operator {
	
	public:
		SBP4(double h);
};

#endif