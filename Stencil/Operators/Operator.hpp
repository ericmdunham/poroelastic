#ifndef OPERATOR_H
#define OPERATOR_H

/*
 * 	Base class for a SBP operator used for stencil calculations.
 */
class Operator {
	
	public:
		// Defines the boundary region
		int boundaryRows;
		int boundaryCols;
		double * leftBoundaryValues;
		double * rightBoundaryValues;

		// Defines the interior region
		int interiorLength;
		int * interiorIndeces;
		double * interiorValues;

		Operator(int br, int bc, int il);
		~Operator();
};

#endif