#!/bin/bash
#PBS -N uniformInflux100
#PBS -l nodes=1:ppn=24
#PBS -q tgp
#PBS -V
#PBS -m n
#PBS -k oe
#PBS -e /data/dunham/edunham/poroelastic/MatrixBased/Friction/Injection/uniformInflux100.err
#PBS -o /data/dunham/edunham/poroelastic/MatrixBased/Friction/Injection/uniformInflux100.out
#

EXEC_DIR=/data/dunham/edunham/poroelastic/MatrixBased/Friction/Injection
INIT_DIR=$EXEC_DIR
cd $PBS_O_WORKDIR

date
mpirun $EXEC_DIR/uniformInflowSim direct
date