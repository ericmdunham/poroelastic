#!/bin/bash
#PBS -N pointInjector200
#PBS -l nodes=1:ppn=24
#PBS -q tgp
#PBS -V
#PBS -m n
#PBS -k oe
#PBS -e /data/dunham/cstierns/poroelastic/MatrixBased/Friction/Injection/pointInjector200.err
#PBS -o /data/dunham/cstierns/poroelastic/MatrixBased/Friction/Injection/pointInjector200.out
#

EXEC_DIR=/data/dunham/cstierns/poroelastic/MatrixBased/Friction/Injection
INIT_DIR=$EXEC_DIR
cd $PBS_O_WORKDIR

mpirun $EXEC_DIR/pointInjectorSim direct
