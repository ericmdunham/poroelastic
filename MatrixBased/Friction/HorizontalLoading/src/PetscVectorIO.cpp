/*  Functions for handeling general PETSc vector and matrix I/O 
    Based on code written by Kali Allison
    
    Author: Vidar Stiernstrom, Kim Torberntsson 
*/
#include "PetscVectorIO.hpp"

/*  Function that print out a vector with 15 significant figures.
  
    Input:
        Vec vec  - The vector that is to be printed.

*/
void PrintVec(Vec vec)
{
  PetscInt Ii,Istart,Iend;
  PetscScalar v;
  VecGetOwnershipRange(vec,&Istart,&Iend);
  for (Ii=Istart;Ii<Iend;Ii++)
  {
    VecGetValues(vec,1,&Ii,&v);
    PetscPrintf(PETSC_COMM_WORLD,"%.15e\n",v);
  }
}

/*  Function that writes a PETSc vector to file loc in binary format.
    Note that due to a memory problem in PETSc, looping over this many
    times will result in an error.

    Input:
       Vec vec          - The vector that is to be written
       const char * loc     - file name
  
  Output:
    PetscErrorCode ierr   - Used for error handling in PETSc

*/
PetscErrorCode WriteVec(Vec vec,const char * loc)
{
  PetscErrorCode ierr = 0;
  PetscViewer    viewer;
  PetscViewerBinaryOpen(PETSC_COMM_WORLD,loc,FILE_MODE_WRITE,&viewer);
  ierr = VecView(vec,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  return ierr;
}

/*  Function that loads a PETSc vector from binary file. The vector is
  NOT allowed to be stored in sparse format. (The binary file will 
  then be interpreted as a containing a matrix, causing a crash).
  The vector must be initialized using VecCreate (or duplicate).
  If no preallocation is done using VecSetSize, then VecLoad will
  handle the allocation.

    Input:
       Vec out          - The loaded vector
       const string inputDir  - Path to input directory
       const string fieldName   - Name of binary file
  
  Output:
    PetscErrorCode ierr   - Used for error handling in PETSc

*/
PetscErrorCode LoadVecFromInputFile(Vec out,const string inputDir, const string fieldName)
{
  PetscErrorCode ierr = 0;

  string vecSourceFile = inputDir + fieldName;
  PetscViewer inv;
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&inv);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,vecSourceFile.c_str(),FILE_MODE_READ,&inv);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(inv,PETSC_VIEWER_BINARY_MATLAB);CHKERRQ(ierr);

  ierr = VecLoad(out,inv);CHKERRQ(ierr);
  return ierr;
}

/*  Function that loads a PETSc matrix from binary file. The matrix MUST
  be stored in sparse format. The matrix must be initialized using 
  MatCreate (or duplicate). If no preallocation is done using MatcSetSizes,
  then MatLoad will handle the allocation.

    Input:
       Mat out          - The loaded matrix
       const string inputDir  - Path to input directory
       const string fieldName   - Name of binary file
  
  Output:
    PetscErrorCode ierr   - Used for error handling in PETSc

*/
PetscErrorCode LoadMatFromInputFile(Mat out,const string inputDir, const string fieldName)
{
  PetscErrorCode ierr = 0;

  string matSourceFile = inputDir + fieldName;
  PetscViewer inv;
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&inv);CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,matSourceFile.c_str(),FILE_MODE_READ,&inv);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(inv,PETSC_VIEWER_BINARY_MATLAB);CHKERRQ(ierr);

  ierr = MatLoad(out,inv);CHKERRQ(ierr);
  return ierr;
}

/*  Function that writes a PETSc matrix to file loc in binary format.

    Input:
       Mat mat          - The matrix that is to be written
       const char * loc     - file name
  
  Output:
    PetscErrorCode ierr   - Used for error handling in PETSc

*/
PetscErrorCode WriteMat(Mat mat,const char * loc)
{
  PetscErrorCode ierr = 0;
  PetscViewer    viewer;
  PetscViewerBinaryOpen(PETSC_COMM_WORLD,loc,FILE_MODE_WRITE,&viewer);
  ierr = MatView(mat,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  return ierr;
}