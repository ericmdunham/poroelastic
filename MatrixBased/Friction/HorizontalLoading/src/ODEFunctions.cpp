/* 	Functions used within the time integration procedure of friction code, i.e computing rates,
	solving for slip velocity etc.

	Author: Vidar Stiernstrom, Kim Torberntsson
*/
#include "ODEFunctions.hpp"

/* 	Function that finds the root of an equation using the bisection method algorithm. 
	
	Input:
			std::function<PetscReal (PetscReal)> f 	- The function that is to be evaluated. Here the residual
			PetscReal xL 							- Left bound
			PetscReal xR 							- Right bound
			PetscInt maxIter 						- Maximum number of iterations allowed
			PetscReal xTol 							- Tolerance of root
			PetscReal fTol 							- Tolerance of function value
			PetscReal root							- The found root. Here slip velocity vector V. 

	Output:
			PetscErrorCode ierr 					- Used for error handling in PETSc
*/
PetscErrorCode Bisection(std::function<PetscReal (PetscReal)> f, PetscReal xL, PetscReal xR, PetscInt maxIter, PetscReal xTol, PetscReal fTol, PetscReal& root){

	PetscErrorCode ierr = 0;

	// Find the values of the function at the left and right endpoint
	PetscReal fL = f(xL);
    PetscReal fR = f(xR);
	// Check if the left and right endpoints contain the root
    if (fabs(fL)<fTol){
		root = xL;
		return ierr;
	}
    else if (fabs(fR)<fTol){
		root = xR;
		return ierr;
	}

	// Check that the root is bracketed. If not, throw an error.
	if (fL*fR  >= 0 ){
		ierr = PetscPrintf(PETSC_COMM_SELF, "f(%f) = %f, f(%f) = %f\n",xL, fL, xR, fR);CHKERRQ(ierr);
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Error in Bisection: Invalid bracket, f(a) must have opposite sign of f(b)");
	} 

	// Always split the interval at least one time
	PetscReal xM = (xL + xR)/2;
	PetscReal fM = f(xM);
    PetscReal xErr = fabs(xM - xL);
    PetscReal fErr = fabs(fM);
	PetscReal numIter = 1;

	/* Split the interval until the error tolerances are fulfilled
	(or until maximum iterations are reached) */
	while (numIter <= maxIter && (xErr >= xTol || fErr >= fTol)){
		if (fL*fM <= 0){
			xR = xM;
		}
		else{
			xL = xM;
			fL = fM;
		}
		// Calculate values of the new mid-point
		xM = (xL + xR)/2;
		fM = f(xM);

        xErr = fabs((xM - xL));
        fErr = fabs(fM);
        numIter++;
	}
	root = xM;

	// Check if convergence was achieved. If not, throw an error.
	if (xErr > xTol || fErr > fTol)SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Error in Bisection: No convergence.");
	return ierr;
}

/* 	Function that sets the boundary data of the displacement vector w. In the horizontal loading
	problems, data is only set on the south boundary and north boundary. 
	
	Input:
			PetscInt iLo 			- Lowest index of slip vector u_S for a processor
			PetscInt iHi 	 		- Highest index of slip vector u_S for a processor
			PetscInt Nx 			- Grid points in x-direction	
			Vec u_S 				- Slip vector (displacement u on the south boundary)
			Vec w_S 				- Displacement vector w on the south boundary.
			Mat B_S 				- SBP-matrix acting on w_S
			Vec u_N 				- Loading vector (displacement u on the north boundary)
			Vec w_N 				- Displacement vector w on the north boundary.
			Mat B_N 				- SBP-matrix acting on w_N
			Vec b_m 				- Container vector for boundary data. b_m = B_S*w_S + B_N*w_N 

	Output:
			PetscErrorCode ierr 	- Used for error handling in PETSc
*/
PetscErrorCode ComputeBoundaryData(PetscInt iLo, PetscInt iHi, PetscInt Nx, Vec u_S, Vec w_S, Vec u_N, Vec w_N, Mat B_S, Mat B_N, Vec b_m){
	
	PetscErrorCode ierr = 0;
	PetscReal u_S_i, u_N_i;
	PetscInt i;

	for (i=iLo;i<iHi;i++){
		//Extract boundary data at south boundary
		ierr = VecGetValues(u_S,1,&i,&u_S_i);CHKERRQ(ierr);
		ierr = VecGetValues(u_N,1,&i,&u_N_i);CHKERRQ(ierr);
		//Set boundary vectors at south and north boundary
		ierr = VecSetValue(w_S,i,u_S_i,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValue(w_S,i+Nx,0,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValue(w_N,i,u_N_i,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValue(w_N,i+Nx,0,INSERT_VALUES);CHKERRQ(ierr);
	}
	ierr = VecAssemblyBegin(w_S);CHKERRQ(ierr);
	ierr = VecAssemblyBegin(w_N);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(w_S);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(w_N);CHKERRQ(ierr);
	//Compute boundary vector b_m = B_S*w_s + B_N*w_N
	ierr = MatMult(B_S,w_S,b_m);CHKERRQ(ierr);
	ierr = MatMultAdd(B_N,w_N,b_m,b_m);CHKERRQ(ierr);

	return ierr;

}

/* 	Function that computes the residual tau-f(V,Psi) (pointwise) used for the bisection method 
	when solving for slip velocity.
	
	Input:
			PetscReal sigma_p 		- Effective normal stress on fault
			PetscReal a 	 		- Friction parameter a
			PetscReal V_0 			- Steady sliding velocity	
			PetscReal Psi 			- State
			PetscReal tau_0 		- Background shear on fault
			PetscReal tau_qs		- Quasi-static shear on fault
			PetscReal eta 			- Half the shear-wave impedance
			PetscReal V 			- Slip velocity
	Output:
			PetscErrorCode ierr 	- Used for error handling in PETSc
*/
PetscReal ComputeResidual(PetscReal sigma_p, PetscReal a, PetscReal V_0, PetscReal Psi, PetscReal tau_0, PetscReal tau_qs, PetscReal eta, PetscReal V){
	return sigma_p*a*asinh(V/(2*V_0)*exp(Psi/a)) - tau_0 - tau_qs + eta*V;
}

/* 	Function that computes the slip velocity vector V, using the bisection method. 
	
	Input:
			PetscInt iLo 				- Lowest index of a fault field for a processor
			PetscInt iHi 	 			- Highest index of a fault field for a processor
			PetscInt faultIndexRange[2]	- Array with the index ranges of the fault.
			PetscReal V_0 				- Steady sliding velocity	
			PetscReal tau_0 			- Background shear on fault
			Vec sigma_p 				- Effective normal stress on fault
			Vec a 			 			- Friction parameter a
			Vec Psi 					- State
			Vec tau_qs					- Quasi-static shear on fault
			Vec V 						- Slip velocity

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode ComputeSlipVelocity(PetscInt iLo, PetscInt iHi, PetscInt faultIndexRange[2], PetscReal V_0, PetscReal tau_0, PetscReal eta, Vec sigma_p, Vec a, Vec Psi, Vec tau_qs, Vec V){

	PetscErrorCode ierr = 0;
	
	//Initialize data types used for ComputeResidual
	PetscReal sigma_p_i, a_i, Psi_i, tau_qs_i, V_i;

	//Initialize right boundary and set left bound of V for bisection method
	PetscReal right;
	PetscReal left = 0;

	//Get local loop indices.
	PetscInt i;

	/* Loop over fault boundary and compute slip */
	for (i=iLo; i<iHi; i++){
		//Outside the index range of the fault: V = 0, corresponding to a locked fault.
		if (i < faultIndexRange[0] || i > faultIndexRange[1]) {
			V_i = 0;
		}
		//Inside the index range of the fault: Solve nonlinear equation for V
		else {
			
			//Get value of fiels at index i
			ierr = VecGetValues(sigma_p,1,&i,&sigma_p_i);CHKERRQ(ierr);
			ierr = VecGetValues(a,1,&i,&a_i);CHKERRQ(ierr);
			ierr = VecGetValues(Psi,1,&i,&Psi_i);CHKERRQ(ierr);
			ierr = VecGetValues(tau_qs,1,&i,&tau_qs_i);CHKERRQ(ierr);

			//Set right bound of V for bisection method
			right = (tau_qs_i+tau_0)/eta;

			//Swap bounds if right is negative
			if (right<0) {
				left = right;
				right = 0;
			}

			//Solve for slip velocity using bisection method. Uses lambda expression to pass
			//ComputeResidual to Bisection. Requires the C11.
			ierr = Bisection(
				[sigma_p_i, a_i, V_0, Psi_i, tau_0, tau_qs_i, eta](PetscReal V_i){
					return ComputeResidual(sigma_p_i, a_i, V_0, Psi_i, tau_0, tau_qs_i, eta, V_i);
				}, 
				left, right, 1e6, 1e-10, 1e-10, V_i);CHKERRQ(ierr);
			}
		//Insert result to slip velocity vector
		ierr = VecSetValue(V, i, V_i, INSERT_VALUES);CHKERRQ(ierr);
	}
	//Assemble slip velocity vector
	ierr = VecAssemblyBegin(V);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(V);CHKERRQ(ierr);

	return ierr;
}

/* 	Function that computes stresses sigma_p and tau_qs at the fault 
	
	Input:
			PetscInt iLo 				- Lowest index of fault field for a processor
			PetscInt iHi 	 			- Highest index of fault field for a processor
			PetscInt PetscInt Ny		- Number of grid points in y-direction
			PetscReal sigma_0 			- Background stress	
			PetscReal alpha 			- Biot's coefficient
			Vec sigma_xy 				- Shear stress
			Vec sigma_yy 				- Normal stress in y-direction
			Vec p 						- Pressure
			Vec sigma_p 				- Effective normal stress on fault
			Vec tau_qs					- Quasi-static shear on fault

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode ComputeStressAtFault(PetscInt iLo, PetscInt iHi, PetscInt Ny, PetscReal sigma_0, PetscReal alpha, Vec sigma_xy, Vec sigma_yy, Vec p, Vec sigma_p, Vec tau_qs){

	PetscErrorCode ierr = 0;
	PetscReal sigma_xy_i, sigma_yy_i, sigma_p_i, p_i;
	PetscInt i, iS, iMin, iMax;

	//Get index range of 2D vectors
	ierr = VecGetOwnershipRange(sigma_xy,&iMin,&iMax);CHKERRQ(ierr);

	//First check if processor before you needed any of your values. 
	if((iLo-1)*Ny>iMin-1){
		//If true, loop over the indices, and set the values.
		for(i = (iMin-1)/Ny+1; i<iLo; i++){
			iS = i*Ny;
			//Extract stress at boundary to compute tau_qs and sigma_p.
			ierr = VecGetValues(sigma_xy,1,&iS,&sigma_xy_i);CHKERRQ(ierr);
			ierr = VecGetValues(sigma_yy,1,&iS,&sigma_yy_i);CHKERRQ(ierr);
			ierr = VecGetValues(p,1,&iS,&p_i);CHKERRQ(ierr);
			//Compute sigma_p and set values
			sigma_p_i = sigma_0 - sigma_yy_i - p_i;
			ierr = VecSetValue(tau_qs,i,sigma_xy_i,INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecSetValue(sigma_p,i,sigma_p_i,INSERT_VALUES);CHKERRQ(ierr);
		}
	}
	//Adjust iHi to make sure a processor doesn't try to get values outside its range
	if(Ny*(iHi-1)>iMax-1) iHi = (iMax-1)/Ny+1;
	for (i=iLo;i<iHi;i++){
		iS = Ny*i; //index for element at south boundary
		//Extract stress at boundary to compute tau_qs and sigma_p.
		ierr = VecGetValues(sigma_xy,1,&iS,&sigma_xy_i);CHKERRQ(ierr);
		ierr = VecGetValues(sigma_yy,1,&iS,&sigma_yy_i);CHKERRQ(ierr);
		ierr = VecGetValues(p,1,&iS,&p_i);CHKERRQ(ierr);
		//Compute sigma_p and set values
		sigma_p_i = sigma_0 - sigma_yy_i - p_i;
		ierr = VecSetValue(tau_qs,i,sigma_xy_i,INSERT_VALUES);CHKERRQ(ierr);
		ierr = VecSetValue(sigma_p,i,sigma_p_i,INSERT_VALUES);CHKERRQ(ierr);
	}
	ierr = VecAssemblyBegin(tau_qs);CHKERRQ(ierr);
	ierr = VecAssemblyBegin(sigma_p);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(tau_qs);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(sigma_p);CHKERRQ(ierr);

	return ierr;
}

/* 	Function that computes total shear tau and pore pressure p_S on fault
	
	Input:
			PetscInt iLo 				- Lowest index of fault field for a processor
			PetscInt iHi 	 			- Highest index of fault field for a processor
			PetscInt PetscInt Ny		- Number of grid points in y-direction
			PetscReal tau_0 			- Background shear	
			Vec p 						- Pressure
			Vec tau_qs					- Quasi-static shear on fault
			Vec p_S 					- Pressure at fault
			Vec tau_qs					-Total shear on fault

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode ComputeTotalShearAndPressureAtFault(PetscInt iLo, PetscInt iHi, PetscInt Ny, PetscScalar tau_0, Vec p, Vec tau_qs, Vec p_S, Vec tau){

	PetscErrorCode ierr = 0;
	PetscReal p_i, tau_qs_i, tau_i;
	PetscInt i, iS, iMin, iMax;

	for (i=iLo;i<iHi;i++){
		//Extract pressure at boundary.
		ierr = VecGetValues(tau_qs,1,&i,&tau_qs_i);CHKERRQ(ierr);
		//Compute total shear and set values
		tau_i = tau_qs_i + tau_0;
		ierr = VecSetValue(tau,i,tau_i,INSERT_VALUES);CHKERRQ(ierr);
	}

	//Get index range of 2D vector p
	ierr = VecGetOwnershipRange(p,&iMin,&iMax);CHKERRQ(ierr);

	//First check if processor before you needed any of your values. 
	if((iLo-1)*Ny>iMin-1){
		//If true, loop over the indices, and set the values.
		for(i = (iMin-1)/Ny+1; i<iLo; i++){
			iS = i*Ny;
			//Extract pressure at the boundary.
			ierr = VecGetValues(p,1,&iS,&p_i);CHKERRQ(ierr);
			//Set values
			ierr = VecSetValue(p_S,i,p_i,INSERT_VALUES);CHKERRQ(ierr);
		}
	}
	//Adjust iHi to make sure a processor doesn't try to get values outside its range
	if(Ny*(iHi-1)>iMax-1) iHi = (iMax-1)/Ny+1;
	for (i=iLo;i<iHi;i++){
		iS = Ny*i; //index for element at south boundary
		//Extract pressure at boundary.
		ierr = VecGetValues(p,1,&iS,&p_i);CHKERRQ(ierr);
		//Set values
		ierr = VecSetValue(p_S,i,p_i,INSERT_VALUES);CHKERRQ(ierr);
	}
	ierr = VecAssemblyBegin(p_S);CHKERRQ(ierr);
	ierr = VecAssemblyBegin(tau);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(p_S);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(tau);CHKERRQ(ierr);

	return ierr;
}

/* 	Function that computes the rate of change of deviation of pore pressure dp_p, used for
	time integrating the fluid diffusion equation
	
	Input:
			
			Vec p 						- Pressure
			Vec w 						- Displacements
			Mat F_p						- SBP-matrices acting on p_p
			Vec F_u 					- SBP-matrices acting on w
			Vec dp_p					- Rate of change		

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode FluidDiffusionRate(Vec p_p, Vec w, Mat F_p, Mat F_u, Vec dp_p){
	
	PetscErrorCode ierr = 0;
	//dp_p = F_p*p_p
	ierr = MatMult(F_p,p_p,dp_p);CHKERRQ(ierr); 
	//dp_p = F_u*w_p + dp_p
	ierr = MatMultAdd(F_u,w,dp_p,dp_p);CHKERRQ(ierr); 
	
	return ierr;
}

/* 	Function that solves the mechanical equilibrium equation
	
	Input:
			
			Vec p_p 					- Deviation from pore pressure
			Vec w 						- Displacements
			Mat M_p						- SBP-matrices acting on p_p
			Vec b_m 					- Boundary data vector
			KSP ksp 					- KSP context	

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode MechanicalEquilibrium(Vec p_p, Vec w, Mat M_p, Vec b_m, KSP ksp){
	
	PetscErrorCode ierr = 0;
	//b_m = M_p*p_p + b_m
	ierr = MatMultAdd(M_p,p_p,b_m,b_m);CHKERRQ(ierr);
	//solve M_u*w = b_m
	ierr = KSPSolve(ksp,b_m,w);CHKERRQ(ierr);
	return ierr;

}

/* 	Function that computes the rate of change of state dPsi
	used for time integrating the state variable 
	
	Input:
			PetscInt iLo 				- Lowest index of fault field for a processor
			PetscInt iHi 	 			- Highest index of fault field for a processor
			Vec a 			 			- Friction parameter a
			Vec b 			 			- Friction parameter b
			Vec V 						- Slip velocity
			PetscReal V_0 				- Steady sliding velocity
			Vec Psi 					- State	
			PetscReal L 				- State evolution distance
			PetscReal f_0 				- Reference friction coefficient for sliding at V_0
			Vec dPsi					- Rate of change of state

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode StateRate(PetscInt iLo, PetscInt iHi, Vec a, Vec b, Vec V, PetscReal V_0, Vec Psi, PetscReal L, PetscReal f_0, Vec dPsi){

	PetscErrorCode ierr = 0;

	//Initialize data types used for friction law and state rate
    PetscReal a_i, b_i, Psi_i, V_i, f_i, dPsi_i;

	//Get local loop indices.
	PetscInt i;
	
	/* Loop over fault boundary to compute friction */
	for (i=iLo; i<iHi; i++){
		//Get value of fiels at index i
		ierr = VecGetValues(a,1,&i,&a_i);CHKERRQ(ierr);
		ierr = VecGetValues(b,1,&i,&b_i);CHKERRQ(ierr);
		ierr = VecGetValues(Psi,1,&i,&Psi_i);CHKERRQ(ierr);
		ierr = VecGetValues(V,1,&i,&V_i);CHKERRQ(ierr);

        f_i = a_i*asinh(V_i/(2*V_0)*exp(Psi_i/a_i));
		
		dPsi_i = -V_i/L*(f_i - f_0 + (b_i-a_i)*log(V_i/V_0));
		ierr = VecSetValue(dPsi, i, dPsi_i, INSERT_VALUES);CHKERRQ(ierr);
		//ierr = VecSetValue(dPsi, i, 0, INSERT_VALUES);CHKERRQ(ierr);
	}
	//Assemble slip velocity vector
	ierr = VecAssemblyBegin(dPsi);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(dPsi);CHKERRQ(ierr);

	return ierr;
}