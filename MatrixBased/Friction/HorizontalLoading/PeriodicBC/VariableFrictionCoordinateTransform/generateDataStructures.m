%% Matrix and vector generation for the Horizontal loading problem with
% variable friction

% Script that generates matrices and vectors for the horizontal loading
% problem with variable friction, using a coordinate transform and periodic
% boundary conditions with explicit time stepping. Matrices and
% vectors can then be used in PETSc.
% The script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all

% Add paths for helper functions
addpath '../../../../../Matlab/Helpers' '../../../../../Matlab/Helpers/Friction' ...
    '../../../../../Matlab/Helpers/PeriodicDiscretization'

do_time_integration = 1; % Set to 1 to do the simulation in matlab. Useful for comparing results between PETSc and matlab.
%% Setup

%Set write strides of PETSc code
faultWriteStride = 1;
bodyWriteStride = 1;

% Define type of SBP operators
Order = 2;        	% order of SBP-operator.
DirichletOrder = 2; % order of Dirichlet condition

% Material parameters in self consistent units
Lx = 2;         % m
Ly = 1;         % m
G = 30;         % GPa
kappa = 1e-2;   % 1e-9 m^2/(Pa s)

% Dimensionless material parameters
B = 0.6;
nu = 0.2;
nu_u = 0.33;

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G, kappa, B, nu, nu_u);
lambda_u = lambda + alpha^2*M;

% Present the hydraulic diffusivity
fprintf('c = %f m^2/s\n', c);

% Friction parameters in self consistent units
f_0 = 0.6;                  % 1
a = 0.015;                  % 1
b = 0.02;                   % 1

sigma_0 = 50;               % MPa
L = 1e-2;                   % mm
tau_0 = 0.95*f_0*sigma_0;   % MPa
V_0 = 1e-3;                 % mm/s
c_c = 3.4e3;                % m/s
eta = G/(2*c_c);            % GPa/(m/s)

% Scales that has to be resolved
h_f = G*L*pi/(sigma_0*(b - a));
t_f = h_f/c_c;
h_d = sqrt(kappa*M*t_f);

% Error tolerance for the horisontal displacement at the fault
tol = 1e-5;

%% Spatial discretization
xL = -Lx;                   % West boundary
xR = Lx;                    % East boundary
yL = -Ly;                   % South Boundary
yR = Ly;                    % North Boundary

Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
x = linspace(xL, xR, Nx);     % Spatial grid, x-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction

% The numerical grid xi living on [0, 1]
xi = linspace(0, 1, Nx);
dxi = 1/(Nx - 1);

% Determine the coordinate transformation
hmaxmin = 40;    % Determines h_max/h_min
xf = @(beta) (tanh(beta*xi(end)) - tanh(beta*xi(end - 1)))/...
    (tanh(beta*xi(2)) - tanh(beta*xi(1))) - 1/hmaxmin;
beta = fsolve(xf, 2, optimset('Display','off'));
y = 2*Ly*tanh(beta*xi)/tanh(beta) - Ly;
y = -fliplr(y);
dy = min(diff(y));

% Unitless friction fields
a = a*ones(Nx, 1);
b = b*ones(Nx, 1);
% Specifies which indices that are on the fault
faultIndexRange = [floor(0.25*Nx), floor(0.75*Nx)];

% Slip rate at the north boundary
du_N = 1e5*V_0*ones(Nx, 1)/2;


% Check if the spatial scales are resolved
fprintf('Frictional resolution ratio h_f/dx = %f\n', h_f/dx);
if(h_f/dx < 1)
    disp('Warning: Not resolving frictional resolution');
end
fprintf('h_d/dy = %f\n', h_d/dy);
if(h_d/dy < 1)
    disp('Warning: Not resolving diffusive boundary layers');
end


%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. To specify Dirichlet conditions (displacement, pore pressure)
% set the the corresponding entry in the matrix BC to 1. The parameter
% DirichletOrder determines the order of accuracy of a Dirichlet boundary
% condition. Higher order of accuracy results in a stiffer problem.
% To specify Neumann conditions (Traction/Discharge velocity) set the 
% corresponding entry in BC to 0. Here follows the structure of the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

BC = [1 1;
      0 0;
      1 0];

% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Dirichlet conditions are imposed with the accuracy
% specified by DirichletOrder.
BC = ScalePeriodicBC(lambda, G, kappa, y, Ny, DirichletOrder, BC);
  
% Obtain the stability condition from the boundary conditions and the interior
C = 1; % A numeric constant that is dependent of the problem and discretization
maxdt = C*min(dx^2/(M*kappa), ...  % From interior/Neumann in x-direction
    min(dy^2/(M*kappa), ...     % From interior/Neumann in y-direction
    min(dy/(M*BC(3, 1)), ...    % From Dirichlet at south boundary
    min(dy/(M*BC(3, 2))))));  % From Dirichlet at north boundary

%% Construct the spatial operators

% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dxi, alpha*ones(Ny,Nx), K*ones(Ny,Nx), ...
    G*ones(Ny,Nx), kappa*ones(Ny,Nx), M*ones(Ny,Nx), BC, Order, y');

%% Temporal discritization
startTime = 0;      % s
t(1) = startTime;
dt = min(maxdt, t_f);
endTime = 10*dt;     % s

% Present the number of time steps per characteristic time scale for
% stability
fprintf('t_f/dt = %f\n', t_f/maxdt);

%
% --- Set up inital values ---
%

% Set initial conditions for fields living on whole domain
w = zeros(2*Ny*Nx, 1);
p_p = zeros(Ny*Nx, 1);

% Set initial conditions for fields living on the fault
psi = ones(Nx, 1)*f_0 - 1e-1*f_0*gaussmf(x, [0.2*h_f, 0])';
u_S = zeros(Nx, 1);
u_N = zeros(Nx, 1);

%% Save the relevant data structures to binary files
%Need to add the directory containing PetscBinaryWrite to your path.
%The following line works if you start matlab from bash
petsc_dir = getenv('PETSC_DIR');
addpath(strcat(petsc_dir,'/share/petsc/matlab/'));

%Set input directory for C code
inDir = 'in';
%Create directories if not already present
if ~exist(inDir, 'dir')
    mkdir(inDir);
end
inDir = strcat(inDir,'/');

%Discretization
PetscBinaryWrite(strcat(inDir,'x'),x);
PetscBinaryWrite(strcat(inDir,'y'),y);

%Matrices for fluid-diffusion equation
PetscBinaryWrite(strcat(inDir,'F_p'),F_p);
PetscBinaryWrite(strcat(inDir,'F_u'),F_u);
PetscBinaryWrite(strcat(inDir,'p_p2p'),-P);

%Matrices for mechanical equilibrium equations
PetscBinaryWrite(strcat(inDir,'mM_u'),-M_u);
PetscBinaryWrite(strcat(inDir,'M_p'),M_p);
PetscBinaryWrite(strcat(inDir,'Sigma_xx'),Sigma_xx);
PetscBinaryWrite(strcat(inDir,'Sigma_xy'),Sigma_xy);
PetscBinaryWrite(strcat(inDir,'Sigma_yy'),Sigma_yy);

%Matrices for boundary conditions
PetscBinaryWrite(strcat(inDir,'BC'),sparse(BC));
PetscBinaryWrite(strcat(inDir,'B_S'),BC(1,1)*M_b.S);
PetscBinaryWrite(strcat(inDir,'B_N'),BC(1,2)*M_b.N);
PetscBinaryWrite(strcat(inDir,'u_S'),u_S);
PetscBinaryWrite(strcat(inDir,'u_N'),u_N);
PetscBinaryWrite(strcat(inDir,'du_N'),du_N);

%Initial body fields
PetscBinaryWrite(strcat(inDir,'w'),w);
PetscBinaryWrite(strcat(inDir,'p_p'),p_p);

%Initial fault fields
PetscBinaryWrite(strcat(inDir,'Psi'),psi);
PetscBinaryWrite(strcat(inDir,'a'),a);
PetscBinaryWrite(strcat(inDir,'b'),b);

%I/O parameters
fileID = fopen(strcat(inDir,'problemParameters.in')','w');
fprintf(fileID,'%d\n',faultWriteStride);
fclose(fileID);
fileID = fopen(strcat(inDir,'problemParameters.in')','a');
fprintf(fileID,'%d\n',bodyWriteStride);

%Temporal parameters
fprintf(fileID,'%0.15e\n',startTime);
fprintf(fileID,'%0.15e\n',endTime);
fprintf(fileID,'%0.15e\n',dt);
fprintf(fileID,'%0.15e\n',maxdt);

%Friction parameters
fprintf(fileID,'%0.15e\n',f_0);
fprintf(fileID,'%0.15e\n',sigma_0);
fprintf(fileID,'%0.15e\n',L);
fprintf(fileID,'%0.15e\n',tau_0);
fprintf(fileID,'%0.15e\n',V_0);
fprintf(fileID,'%0.15e\n',eta);
fprintf(fileID,'%0.15e\n',tol);
fprintf(fileID,'%d\n',faultIndexRange(1)-1); %Shift by -1, due to differnece in matlab and C indexing
fprintf(fileID,'%d\n',faultIndexRange(2)-1); %Shift by -1, due to differnece in matlab and C indexing

%Poroelastic parameters
fprintf(fileID,'%0.15e',alpha);
fclose(fileID);

% Save parameters for saveResultToMat.m and plotResults.m
save('setup.mat','Nx','Ny','x','y','dx','dy','G','kappa','B','nu',...
    'nu','nu_u','M','K','alpha','c','a','b','f_0','sigma_0',...
    'tau_0','V_0', 'du_N','L','c_c','eta','h_f','h_d','faultIndexRange',...
    'do_time_integration');

%% Time integration

if do_time_integration
    
    %----------------- Startup, solve for unknown fields-------------

    % --- Set the boundary data in the mechanical equation ---

    B_S = M_b.S*[BC(1,1)*u_S; zeros(Nx, 1)];
    B_N = M_b.N*[BC(1,2)*u_N; zeros(Nx, 1)];

    % --- Solve the mechanical equilibrium equation for u and v ---

    w = -M_u\(M_p*p_p + (B_S + B_N));


    % --- Find slip velocity at each point on the fault ---
    
    % Calculate values of fields in whole domain
    p = reshape(p_p - P*w, Ny, Nx);
    sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
    sigma_yy = reshape(Sigma_yy*w - alpha*p_p, Ny, Nx);
    
    % Find tau_qs and sigma' at the fault
    tau_qs = sigma_xy(1, :)';
    sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';
    
    % Compute the slip velocity at each point
    V = ComputeSlip(Nx, sigma_p, a, V_0, psi, tau_0, tau_qs, eta, faultIndexRange);
    i = 1;
    %% Take time steps
    while t(i) < endTime
        % Perform an adaptive Runge Kutta time step. Retries until a time step
        % with an appropriate size has been taken
        attempt = 1;
        while 1
            
            % Make sure that we don't overshoot past the endtime
            if t(i) + dt > endTime
                dt = endTime - t(i);
            end
            
            %
            % --- RK Stage 1 ---
            %
            
            % --- Calculate the rates of change du, dpsi and dp' ---
            
            du_S1 = V/2;
            dpsi1 = StateRate(a, b, V, V_0, psi, L, f_0);
            dp_p1 = F_p*p_p+ F_u*w;
            
            % --- Integrate fields up to time t + 0.5dt ---
            
            u_S2 = u_S + 0.5*dt*du_S1;
            u_N2 = u_N + 0.5*dt*du_N;
            psi2 = psi + 0.5*dt*dpsi1;
            p_p2 = p_p + 0.5*dt*dp_p1;
            
            % --- Set the boundary conditions in the mechanical equation ---
            
            B_S = M_b.S*[BC(1,1)*u_S2; zeros(Nx, 1)];
            B_N = M_b.N*[BC(1,2)*u_N2; zeros(Nx, 1)];
            
            % --- Solve the mechanical equilibrium equation for u and v ---
            
            w_temp = -M_u\(M_p*p_p2 + (B_N + B_S));
            
            % --- Find slip velocity at each point on the fault ---
            
            % Calculate values of p, sigma_xy and sigma_yy in whole domain
            p = reshape(p_p2 - P*w_temp, Ny, Nx);
            sigma_xy = reshape(Sigma_xy*w_temp, Ny, Nx);
            sigma_yy = reshape(Sigma_yy*w_temp - alpha*p_p2, Ny, Nx);
            
            % Find tau_qs and sigma' at the fault
            tau_qs = sigma_xy(1, :)';
            sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';
            
            % Compute the slip velocity at each point
            V_temp = ComputeSlip(Nx, sigma_p, a, V_0, psi2, tau_0, tau_qs, eta, faultIndexRange);
            
            %
            % --- RK Stage 2 ---
            %
            
            % --- Calculate the rates of change du, dpsi and dp' ---
            
            du_S2 = V_temp/2;
            dpsi2 = StateRate(a, b, V_temp, V_0, psi2, L, f_0);
            dp_p2 = F_p*p_p2 + F_u*w_temp;
            
            % --- Integrate fields up to time t + dt ---
            
            u_S3 = u_S + dt*(-du_S1 + 2*du_S2);
            u_N3 = u_N + dt*du_N;
            psi3 = psi + dt*(-dpsi1 + 2*dpsi2);
            p_p3 = p_p + dt*(-dp_p1 + 2*dp_p2);
            
            
            % --- Set the boundary conditions in the mechanical equation ---
            
            B_S = M_b.S*[BC(1,1)*u_S3; zeros(Nx, 1)];
            B_N = M_b.N*[BC(1,2)*u_N3; zeros(Nx, 1)];
            
            % --- Solve the mechanical equilibrium equation for u and v ---
            
            w_temp = -M_u\(M_p*p_p3 + (B_N + B_S));
            
            % --- Find slip velocity at each point on the fault ---
            
            % Calculate values of p, sigma_xy and sigma_yy in whole domain
            p = reshape(p_p3 - P*w_temp, Ny, Nx);
            sigma_xy = reshape(Sigma_xy*w_temp, Ny, Nx);
            sigma_yy = reshape(Sigma_yy*w_temp - alpha*p_p3, Ny, Nx);
            
            % Find tau_qs and sigma' at the fault
            tau_qs = sigma_xy(1, :)';
            sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';
            
            % Compute the slip velocity at each point
            V_temp = ComputeSlip(Nx, sigma_p, a, V_0, psi3, tau_0, tau_qs, eta, faultIndexRange);
            
            %
            % --- RK Stage 3 ---
            %
            
            % --- Calculate the rates of change du_S, dpsi and dp_p ---
            
            du_S3 = V_temp/2;
            dpsi3 = StateRate(a, b, V_temp, V_0, psi3, L, f_0);
            dp_p3 = F_p*p_p3 + F_u*w_temp;
            
            %
            % --- Error Control on Psi and slip ---
            %
            
            % Perform RK2 and RK3 to obtain u_S at t + dt and find error
            uRK2 = u_S + dt/2*(du_S1 + du_S3);
            uRK3 = u_S + dt/6*(du_S1 + 4*du_S2 + du_S3);
            psiRK2 = psi + dt/2*(dpsi1 + dpsi3);
            psiRK3 = psi + dt/6*(dpsi1 + 4*dpsi2 + dpsi3);
            
            % Calculate max norm of the state and slip rate at fault
            error = max(norm(uRK2 - uRK3, Inf), norm(psiRK2 - psiRK3, Inf));
            
            % --- For acceptable time step ---
            if error < tol
                
                % Update value of u
                u_S = uRK3;
                psi = psiRK3;
                
                % Update value of u_N and p_p
                u_N = u_N + dt*du_N;
                p_p = p_p + dt/6*(dp_p1 + 4*dp_p2 + dp_p3);
                
                % --- Set the boundary data in the mechanical equation ---
                
                B_S = M_b.S*[BC(1,1)*u_S; zeros(Nx, 1)];
                B_N = M_b.N*[BC(1,2)*u_N; zeros(Nx, 1)];
                
                % --- Solve the mechanical equilibrium equation for u and v ---
                
                w = -M_u\(M_p*p_p + (B_S + B_N));
                
                % --- Find slip velocity at each point on the fault ---
                
                % Calculate values of fields in whole domain
                p = reshape(p_p - P*w, Ny, Nx);
                sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
                sigma_yy = reshape(Sigma_yy*w - alpha*p_p, Ny, Nx);
                
                % Find tau_qs and sigma' at the fault
                tau_qs = sigma_xy(1, :)';
                sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';
                
                % Compute the slip velocity at each point
                V = ComputeSlip(Nx, sigma_p, a, V_0, psi, tau_0, tau_qs, eta, faultIndexRange);
                
                % Update the number of time steps performed
                i = i + 1;
                
                % Update the time
                t(i) = t(i - 1) + dt;
                
                % Find suggestion for next time step length
                if error ~= 0
                    dt = min(min(0.9*dt*(tol/error)^(1/3), maxdt), 5*dt);
                end
                
                % Exit while-loop and perform next time step
                break
            end
            
            % --- For unacceptable timestep ---
            
            % Notify user and update timestep
            fprintf('Rejecting attempt %d, error = %0.15e, with dt = %.15e at t = %0.15e\n'...
                ,attempt, error, dt, t(i))
            
            % Find suggestion for next time step length
            dt = min(0.9*dt*(tol/error)^(1/3), maxdt);
            
            attempt = attempt + 1;
        end
        
        
    end
    % --- Save results ---
       
    u = reshape(w(1:Ny*Nx),Ny,Nx);
    v = reshape(w(Nx*Ny + 1:2*Ny*Nx),Ny,Nx);
    sigma_xx = reshape(Sigma_xx*w - alpha*p_p,Ny,Nx);
    save('comparison.mat','u','v','p');
    
end


% Remove paths for helper functions
rmpath '../../../../../Matlab/Helpers' '../../../../../Matlab/Helpers/Friction' ...
    '../../../../../Matlab/Helpers/PeriodicDiscretization'
