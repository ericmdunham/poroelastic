%% Script for reading PETSc solution vectors and saving them in a .mat file
% Requires the PETSc matlab functions PetscBinaryRead. Matlab must be
% started from bash.
%
% resultDir  - Path to PETSc binary solution files
% outputFileName - Name of the .mat file that will be created
%
% ---------- Problem setup parameters -------------
%
% x,y       - Coordinate axes
% Nx,Ny     - Grid points
% dx,dy     - Grid spacing
% G         - Shear modulus
% K         - Bulk modulus (drained)
% kappa     - permeability parameter
% B         - Skempton's parameter
% nu        - Poisson's ratio (drained)
% nu_u      - Poisson's ration (undrained)
% M         - Biot's modulus
% alpha     - Biot's coefficient
% c         - Hydraulic diffusivity
% QR        - Influx/Injection rate
% kappaRel  - Ratio in permeability between the two zones
% kappaSize - Ratio between length of damage zone is compared to the domain
%
% ---------- Friction parameters -------------
%
% a,b       - Friction parameters
% f_0       - Reference friction coefficient for sliding at V_0
% psi_0     - Initial state
% sigma_0   - Background normal stress
% tau_0     - Background shear
% V_0       - Steady sliding slip velocity
% L         - State evolution distance (D_c)
% c_c       - Elastic wave speed
% eta       - Half the shear-wave impedance
% h_f       - Frictional spatial scale (h*)
% h_d       - Diffusive spatial scale
% faultIndexRange - Range of indices of x that spanning the fault
% 
% ------------- Solution vectors ----------------
%
% Solution vectors are stored as 2D matrices, where 
% 1D fields have the dimension Nx-by-Nt and 2D fields
% have the dimensions Ny*Nx-by-Nt. To obtain the 2D field at time index i
% use 2DMatrix = reshape(2Dfield(:,i),Ny,Nx)
%
% Body fields:
% t_body     - Sampling time points for body fields
% w = [u,v]' - Displacement vector
% p          - Pore pressure       
% sigma_xx   - Stress x-direction   
% sigma_xy   - Shear               
% sigma_yy   - Stress y-direction   
% 
% Fault fields:
% t_fault    - Sampling time points for fault fields
% tau        - Total shear        
% sigma_p    - Effective normal stress 
% psi        - State
% V          - Slip velocity    
% u_S        - Slip               
% p_S        - Pore pressure on fault
%
% Author: Kim Torberntsson, Vidar Stiernstrom

%% Setup I/O

resultDir = 'out_dc80/';
outputFileName = 'UniformInflux_dc80.mat'; %.mat file
petscDir = getenv('PETSC_DIR'); %This requires that matlab has been started from bash.
addpath(strcat(petscDir,'/share/petsc/matlab/'));

%% Load the problem setup, i.e. problem parameters, domain, etc.
load('setup.mat'); %Parameters central to the problem.

save_body = false;
if save_body
%% Read body fields
fileID = fopen(strcat(resultDir,'timeBody.txt'),'r');
t_body = fscanf(fileID,'%e');
fclose(fileID);
w = cell2mat(PetscBinaryRead(strcat(resultDir,'w'), 'cell', 150000));
p = cell2mat(PetscBinaryRead(strcat(resultDir,'p'), 'cell', 150000));
sigma_xx = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_xx'), 'cell', 150000));
sigma_xy = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_xy'), 'cell', 150000));
sigma_yy = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_yy'), 'cell', 150000));
end

%% Read fault fields
fileID = fopen(strcat(resultDir,'timeFault.txt'),'r');
t_fault = fscanf(fileID,'%e');
fclose(fileID);
sigma_p = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_p'), 'cell', 150000));
tau = cell2mat(PetscBinaryRead(strcat(resultDir,'tau'), 'cell', 150000));
psi = cell2mat(PetscBinaryRead(strcat(resultDir,'Psi'), 'cell', 150000));
V = cell2mat(PetscBinaryRead(strcat(resultDir,'V'), 'cell', 150000));
u_S = cell2mat(PetscBinaryRead(strcat(resultDir,'u_S'), 'cell', 150000));
p_S = cell2mat(PetscBinaryRead(strcat(resultDir,'p_S'), 'cell', 150000));

%% Save result to .mat file
if save_body
save(outputFileName,'Nx','Ny','x','y','dx','dy','G','kappa','B','nu',...
     'nu','nu_u','M','K','alpha','c','a','b','f_0','psi_0','sigma_0',...
     'tau_0','V_0','L','c_c','eta','h_f','h_d','faultIndexRange',...
     'QR', 'kappaRel', 'kappaSize','w','p','sigma_xx','sigma_xy',...
     'sigma_yy','sigma_p','tau','psi','V','u_S','p_S',...
     't_fault','t_body')
else
save(outputFileName,'Nx','Ny','x','y','dx','dy','G','kappa','B','nu',...
     'nu','nu_u','M','K','alpha','c','a','b','f_0','psi_0','sigma_0',...
     'tau_0','V_0','L','c_c','eta','h_f','h_d','faultIndexRange',...
     'QR', 'kappaRel', 'kappaSize',...
     'sigma_p','tau','psi','V','u_S','p_S',...
     't_fault')
end
