
load UniformInflux_dc10
t10=t_fault;
V10=V*1e-3;
n10=find(max(V10,[],1)>1e-3);
D10=u_S;
N10=sigma_p;
S10=tau;

load UniformInflux_dc40
t40=t_fault;
V40=V*1e-3;
n40=find(max(V40,[],1)>1e-3);
D40=u_S;
N40=sigma_p;
S40=tau;

load UniformInflux_dc80
t80=t_fault;
V80=V*1e-3;
D80=u_S;
N80=sigma_p;
S80=tau;

% plots
set(0,'DefaultTextFontSize',12)
set(0,'DefaultAxesFontSize',12)
set(0,'DefaultTextFontName','Helvetica')
set(0,'DefaultAxesFontName','Helvetica')

saveplots = true;

set(gcf,'paperposition',[0 0 4 3])
semilogy(t80,max(V80,[],1),t40,max(V40,[],1),t10,max(V10,[],1))
xlabel('time, t (s)')
ylabel('slip velocity, V (m/s)')
axis([0 20 1e-7 1e1])
legend('d_c = 80 mm','d_c = 40 mm','d_c = 10 mm') % change to \mum in Illustrator
set(gca,'xtick',[0:2:20])
set(gca,'ytick',10.^[-7:1])
if saveplots, print -depsc2 figures/log10V, end

s=2000;plot(x,D80(:,1:s:end),'k',x,D40(:,end),'r',x,D10(:,end),'b')
axis([-2.5 2.5 0 0.2])
xlabel('distance, x (m)')
ylabel('slip, \delta (mm)')
legend('d_c = 10 mm','d_c = 40 mm','d_c = 80 mm') % change to \mum in Illustrator
if saveplots, print -depsc2 figures/slipcontours, end

s=20;plot(x,D10(:,n10(1:s:end)),'b',x,D40(:,n40(1:s:end)),'r')
axis([-2.5 2.5 0 0.2])
xlabel('distance, x (m)')
ylabel('slip, \delta (mm)')
legend('d_c = 10 mm','d_c = 40 mm') % change to \mum in Illustrator
if saveplots, print -depsc2 figures/slipcontours2, end

pcolored([-2.5 2.5],[4.971 4.9735],[0 0; 0 0])
xlabel('distance, x (m)')
ylabel('time, t (s)')
title('slip velocity, V (m/s)')
cmap(256,0,1)
axis([-2.5 2.5 4.971 4.9735])
caxis([0 2])
if saveplots, print -depsc2 figures/V10spacetime_frame, end

pcolored(x,t10(n10),V10(:,n10))
cmap(256,0,1)
axis([-2.5 2.5 4.971 4.9735])
caxis([0 2])
axis off,colorbar('off')
if saveplots, print -dpng figures/V10spacetime, end

pcolored([-4 4],[0 8],[50 50; 50 50])
axis([-4 4 0 8])
hold on,plot(-4+sqrt(0.9748*linspace(0,8)),linspace(0,8),'k'),hold off
xlabel('distance, x (m)')
ylabel('time, t (s)')
title('effective normal stress, \sigma^{\prime} (MPa)')
caxis([40 55])
cmap(256,-2,1)
if saveplots, print -depsc2 figures/N10spacetime_frame, end

pcolored(x,t10,N10)
caxis([40 55])
cmap(256,-2,1)
axis([-4 4 0 8])
axis off, colorbar('off')
if saveplots, print -dpng figures/N10spacetime, end

set(gcf,'paperposition',[0 0 2.5 1.5])
set(gca,'fontsize',10)
pcolored([-2.5 2.5],[4.971 4.9735],[50 50; 50 50])
xlabel('distance, x (m)')
ylabel('time, t (s)')
caxis([40 55]),colorbar('off')
cmap(256,-2,1)
axis([-2.5 2.5 4.971 4.9735])
set(gca,'xtick',[-2:2])
set(gca,'ytick',[4.971 4.972 4.973])
if saveplots, print -depsc2 figures/N10spacetime_zoom_frame, end

pcolored(x,t10(n10),N10(:,n10))
caxis([40 55]),colorbar('off')
cmap(256,-2,1)
axis([-2.5 2.5 4.971 4.9735])
axis off
if saveplots, print -dpng figures/N10spacetime_zoom, end

set(gcf,'paperposition',[0 0 4 3])
set(gca,'fontsize',12)
pcolored([-4 4],[0 4],[0 0; 0 0])
cmap(256,-1,1)
caxis([-6 6])
axis equal
axis([-4 4 0 4])
hold on, plot([-4 4],[0.5 0.5],'k'), hold off
xlabel('distance along fault, x (m)')
ylabel('distance normal to fault, y (m)')
title('pore pressure change, p (MPa)')
if saveplots, print -depsc2 figures/pressure_frame, end

for n=[355 360 363 370 400]
 pcolored(x,2+y,P(:,:,n)),axis equal,caxis([-6 6])
 axis([-4 4 0 4])
 axis off,colorbar('off')
 drawnow
 if saveplots, print('-dpng',['figures/pressure' num2str(n)]), end
end


pcolored([-4 4],[0 20],[50 50; 50 50])
axis([-4 4 0 20])
hold on,plot(-4+sqrt(0.9748*linspace(0,20)),linspace(0,20),'k'),hold off
xlabel('distance, x (m)')
ylabel('time, t (s)')
title('effective normal stress, \sigma^{\prime} (MPa)')
caxis([40 55])
cmap(256,-2,1)
if saveplots, print -depsc2 figures/N80spacetime_frame, end

pcolored(x,t80,N80)
caxis([40 55])
cmap(256,-2,1)
axis([-4 4 0 20])
axis off, colorbar('off')
if saveplots, print -dpng figures/N80spacetime, end

%plot(x,D10(:,end),'b',x,D40(:,end),'r',x,D80(:,end),'g')

%pcolored(x,t10(n10),log10(V10(:,n10)))

%pcolored(x,t40(n40),log10(V40(:,n40)))

%pcolored(x,t40(n40),V40(:,n40))

