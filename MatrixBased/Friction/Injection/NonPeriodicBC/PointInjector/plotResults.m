%% Script for displaying the simulation of the point injector problem
% The script reads the data computed using the C version of the
% point injector problem and then plots the result, and if
% the flag saveMovie = 1 is set, then generates .avi files of the
% simulation. If generateDataStructures was run with the flag
% do_time_integration = 1, the script also computes the difference between
% the Matlab solution and the C solution based in the L2-norm.
% 
%  
% Important note: Make sure you have the same problem parameters written
% to setup.mat as used in the C code (they are automatically written 
% when runninggenerateDataStructures.m). If they differ, simply run
% generateDataStructres.m again with the correct parameter setup.

% Author: Kim Torberntsson, Vidar Stiernstrom

close all;
clear all;

petscDir = getenv('PETSC_DIR');
addpath(strcat(petscDir,'/share/petsc/matlab/'));
resultDir = 'out/';
saveMovie = 0;
load('setup.mat');

%% Plot body fields

fileID = fopen(strcat(resultDir,'timeBody.txt'),'r');
t_body = fscanf(fileID,'%e');
fclose(fileID);

w = cell2mat(PetscBinaryRead(strcat(resultDir,'w'), 'cell', 100000));
p = cell2mat(PetscBinaryRead(strcat(resultDir,'p'), 'cell', 100000));
sigma_xx = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_xx'), 'cell', 100000));
sigma_xy = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_xy'), 'cell', 100000));
sigma_yy = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_yy'), 'cell', 100000));
figure(1);
frames = struct('cdata', cell(1,length(t_body)), 'colormap', cell(1,length(t_body)));
pause();
for i = 1:length(t_body)
    t_i = t_body(i);
    u_i = reshape(w(1:Nx*Ny,i), Ny, Nx);
    v_i = reshape(w(Nx*Ny+1:2*Nx*Ny,i), Ny, Nx);
    p_i = reshape(p(1:Nx*Ny,i), Ny, Nx);
    sigma_xx_i = reshape(sigma_xx(1:Nx*Ny,i), Ny, Nx);
    sigma_xy_i = reshape(sigma_xy(1:Nx*Ny,i), Ny, Nx);
    sigma_yy_i = reshape(sigma_yy(1:Nx*Ny,i), Ny, Nx);
    
   subplot(2, 3, 1);
    pcolor(x, y, u_i);
    colorbar
    shading flat;
    xlabel('x [m]');
    ylabel('y [m]');
    title('u [mm]');
    
    subplot(2,3,2);
    pcolor(x, y, v_i);
    colorbar
    shading flat;
    xlabel('x [m]');
    ylabel('y [m]');
    title('v [mm]');
    
    subplot(2,3,3);
    pcolor(x, y, p_i);
    colorbar
    shading flat;
    xlabel('x [m]');
    ylabel('y [m]');
    title('p [MPa]');

    subplot(2, 3, 4);
    pcolor(x, y, sigma_xx_i);
    colorbar
    shading flat;
    xlabel('x [m]');
    ylabel('y [m]');
    title('\sigma_{xx} [MPa]');
    
    subplot(2, 3, 5);
    pcolor(x, y, sigma_xy_i);
    colorbar
    shading flat;
    xlabel('x [m]');
    ylabel('y [m]');
    title('\sigma_{xy} [MPa]');
    
    subplot(2, 3, 6);
    pcolor(x, y, sigma_yy_i);
    colorbar
    shading flat;
    xlabel('x [m]');
    ylabel('y [m]');
    title('\sigma_{yy} [MPa]');
    
    suptitle(strcat('t = ', num2str(t_i), ' s'));
    drawnow
    
    if saveMovie
        % Save frame to save as movie
        frames(i) = getframe(gcf);
    end
    
end
% Save movie
if saveMovie
    clear t_i w p sigma_xx sigma_xy sigma_yy u_i v_i p_i sigma_xx_i ...
        sigma_xy_i sigma_yy_i;
    video = VideoWriter('bodyfields.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
    clear frames;
end

%% Plot fault fields

fileID = fopen(strcat(resultDir,'timeFault.txt'),'r');
t_fault = fscanf(fileID,'%e');
fclose(fileID);


sigma_p = cell2mat(PetscBinaryRead(strcat(resultDir,'sigma_p'), 'cell', 100000));
tau = cell2mat(PetscBinaryRead(strcat(resultDir,'tau'), 'cell', 100000));
psi = cell2mat(PetscBinaryRead(strcat(resultDir,'Psi'), 'cell', 100000));
V = cell2mat(PetscBinaryRead(strcat(resultDir,'V'), 'cell', 100000));

figure(2);
plotind = [1:length(t_fault)];
frames = struct('cdata', cell(1,length(plotind)),...
    'colormap', cell(1,length(plotind)));
pause();
j = 0;
%Specify point on fault used for plotting.
faultInd = faultIndexRange(1):faultIndexRange(2);
fLo = faultIndexRange(1); %left most point of fault
for i = plotind
    j = j+1;
    tausig = (tau(fLo,1:i)- eta*V(fLo,1:i))./sigma_p(fLo,1:i);
    VP = V(fLo,1:i);
    fss = f_0 - (b(fLo) - a(fLo))*log(V(fLo,1:i)/V_0);
    
    % Plot slip and friction on fault at specified point.
    subplot(4, 2, 1:2);
    semilogx(VP, tausig, VP, fss);
    xlabel('Slip rate V [mm/s]');
    ylabel('f');
    
    subplot(4, 2, 3);
    semilogy(x(faultInd), V(faultInd', i));
    xlabel('x [m]')
    ylabel('Slip rate V [mm/s]');
    axis([x(faultIndexRange(1)) x(faultIndexRange(2))...
        min(min(V))*0.9 max(max(V))*1.1]);
    
    subplot(4, 2, 4);
    semilogy(x(faultInd), psi(faultInd', i));
    xlabel('x [m]')
    ylabel('State \Psi [mm/s]');
    axis([x(faultIndexRange(1)) x(faultIndexRange(2))....
        min(min(psi))*0.9 max(max(psi))*1.1]);
    
    subplot(4, 2, 5);
    semilogy(t_fault(1:i), VP);
    xlabel('t [s]');
    ylabel('Slip Rate V [mm/s]');
    
    subplot(4, 2, 6);
    plot(t_fault(1:i), tausig);
    xlabel('t [s]')
    ylabel('f');
    
    suptitle(strcat('t = ', num2str(t_fault(i)), ' s'));
    drawnow
    
    if saveMovie
        frames(j) = getframe(gcf);
    end
end

if saveMovie
    clear tausig VP fss psi V;
    video = VideoWriter('slipnfriction.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
    clear frames;
end

u_S = cell2mat(PetscBinaryRead(strcat(resultDir,'u_S'), 'cell', 100000));
p_S = cell2mat(PetscBinaryRead(strcat(resultDir,'p_S'), 'cell', 100000));

figure(3);
frames = struct('cdata', cell(1,length(plotind)),...
    'colormap', cell(1,length(plotind)));
pause();
j = 0;
for i = plotind
    j = j+1;
   % Plot slip, slip rate and stresses on the fault.

    subplot(2, 2, 1);
    plot(x, u_S(:, i));
    xlabel('x [m]');
    ylabel('Slip u [mm]');
    
    subplot(2, 2, 2);
    plot(x, p_S(:, i));
    xlabel('x [m]');
    ylabel('Pore pressure p [MPa]');
    
    subplot(2, 2, 3);
    plot(x, sigma_p(:, i));
    xlabel('x [m]');
    ylabel('\sigma^{\prime} [MPa]');
    
    subplot(2, 2, 4);
    plot(x, tau(:, i));
    xlabel('x [m]');
    ylabel('\tau [MPa]');
   
    suptitle(strcat('t = ', num2str(t_fault(i)), ' s'));
    drawnow
    
    if saveMovie
        frames(j) = getframe(gcf);
    end
end

% Save movie
if saveMovie
    video = VideoWriter('faultfields.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

if do_time_integration
    load('comparison.mat');
    difference_u = sqrt(dy*dx)*norm((u_i - u))
    difference_v = sqrt(dy*dx)*norm((v_i - v))
    difference_p = sqrt(dy*dx)*norm((p_i - p))
end