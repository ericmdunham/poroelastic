/*  Functions for handeling general PETSc vector and matrix I/O 
    Based on code written by Kali Allison
    
    Author: Vidar Stiernstrom, Kim Torberntsson 
*/
#ifndef PETSCVECTORIO_HPP
#define PETSCVECTORIO_HPP

#include <string>
#include <petscts.h>

using std::string;

/*  Function that print out a vector with 15 significant figures.
  
    Input:
        Vec vec  - The vector that is to be printed.

*/
void PrintVec(Vec vec);

/*  Function that writes a PETSc vector to file loc in binary format.
    Note that due to a memory problem in PETSc, looping over this many
    times will result in an error.

    Input:
       Vec vec 					- The vector that is to be written
       const char * loc 		- file name
	
	Output:
		PetscErrorCode ierr 	- Used for error handling in PETSc

*/
PetscErrorCode WriteVec(Vec vec,const char * loc);

/*  Function that loads a PETSc vector from binary file. The vector is
	NOT allowed to be stored in sparse format. (The binary file will 
	then be interpreted as a containing a matrix, causing a crash).
	The vector must be initialized using VecCreate (or duplicate).
	If no preallocation is done using VecSetSize, then VecLoad will
	handle the allocation.

    Input:
       Vec out 					- The loaded vector
       const string inputDir 	- Path to input directory
       const string fieldName 	- Name of binary file
	
	Output:
		PetscErrorCode ierr 	- Used for error handling in PETSc

*/
PetscErrorCode LoadVecFromInputFile(Vec out,const string inputDir, const string fieldName);

/*  Function that loads a PETSc matrix from binary file. The matrix MUST
	be stored in sparse format. The matrix must be initialized using 
	MatCreate (or duplicate). If no preallocation is done using MatcSetSizes,
	then MatLoad will handle the allocation.

    Input:
       Mat out 					- The loaded matrix
       const string inputDir 	- Path to input directory
       const string fieldName 	- Name of binary file
	
	Output:
		PetscErrorCode ierr 	- Used for error handling in PETSc

*/
PetscErrorCode LoadMatFromInputFile(Mat out,const string inputDir, const string fieldName);

/*  Function that writes a PETSc matrix to file loc in binary format.

    Input:
       Mat mat 					- The matrix that is to be written
       const char * loc 		- file name
	
	Output:
		PetscErrorCode ierr 	- Used for error handling in PETSc

*/
PetscErrorCode WriteMat(Mat mat,const char * loc);

#endif