/* 
	Simulation of a poroelastic media in 2D with a fault on the south boundary and fluid injection. The model is based on the explicit u-p' formulations of the linear poroelastic equations and use rate- and state friction to solve for slip velocity. The code makes use of the PETSc library for vector - matrix operations and parallelization.
	
	Info for running:

	Run with mpiexec -np N ./EXECUTABLE SOLVER, where N is the number of processors, EXECUTABLE is the binary file for the main function (currenly pointInjectorSim or uniformInflowSim, see Makefile) and SOLVER is either "iterative" or "direct". Further KSP options can be specified from command line.

	All problem parameters, vectors and matrices are stated and constructed in the Matlab code BUILDDIR/generateDataStructures.m, (which has to be run prior to running this code) and are written to the  directory BUILDDIR/in/, where BUILDDIR is the directory of the problem (Again, see Makefile.)
   
   	The solutions (body fields and fault fields) as well as the sampling times are written to files in the directory BUILDDIR/out/. Sampling frequencies faultWriteStride and bodyWriteStride are set in generateDataStructures. The solutions can then be loaded in to Matlab and simulated using the script readResult.m.

   	Author: Vidar Stiernstrom, Kim Torberntsson

*/
   	
#include <petscts.h>
#include <cmath>
#include "ODEFunctions.hpp"
#include "FrictionIO.hpp"

#define DO_PROFILING 0

using namespace std;

int main(int argc,char *argv[])
{

	PetscErrorCode ierr;
	PetscMPIInt rank, totalSize;			
	string buildDir, inputDir, outputDir, solver;
	KSP ksp;
	PC pc;

#if DO_PROFILING == 1
	double timerStart, timerEnd, timerComputeBoundaryData, timerComputeSlipVelocity, timerComputeStressAtFault, timerKSPSetUp, timerMechanicalEquilibrium, timerTS;
	double timerIntegrateFields = 0;
#endif

	//Initialize PETSc
	PetscInitialize(&argc,&argv,NULL,NULL);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	MPI_Comm_size(PETSC_COMM_WORLD,&totalSize);
	ierr = 0;

	if (argc>1){solver = argv[1];}//Check if solver is defined from command line
    else {solver = "iterative";} //Default is iterative solver


//Set input directory for data structures
#if BUILD == 10
    buildDir = "NonPeriodicBC/PointInjector/";
    inputDir = buildDir + "in/";
    outputDir = buildDir + "out/";
#endif

#if BUILD == 11
    buildDir = "NonPeriodicBC/UniformInflow/";
    inputDir = buildDir + "in/";
    outputDir = buildDir + "out/";
#endif

	//Check if the matrices should be partitioned sequential or in parallel
    MatType matType;
    if (totalSize > 1) {matType = MATMPIAIJ;}
    else matType = MATSEQAIJ;

    FrictionIO io = FrictionIO(inputDir,outputDir,matType);

	/* --------------- Set up parameters --------------- */
    problem_parameters_t parameters;
    ierr = io.LoadParametersFromMatlab(parameters);CHKERRQ(ierr);

    //I/O parameters
    PetscInt faultWriteStride = parameters.faultWriteStride;
    PetscInt bodyWriteStride = parameters.bodyWriteStride;

	//Temporal parameters
    PetscReal currentTime = parameters.startTime;
    PetscReal endTime = parameters.endTime;
    PetscReal dt = parameters.dt;
    PetscReal maxdt = parameters.maxdt;

	//Friction parameters
    PetscReal f_0 = parameters.f_0;
    PetscReal sigma_0 = parameters.sigma_0;
    PetscReal L = parameters.L;
    PetscReal tau_0 = parameters.tau_0;
    PetscReal V_0 = parameters.V_0;
    PetscReal eta = parameters.eta;
    PetscReal tol = parameters.tol;
    PetscInt faultIndexRange[2];
    faultIndexRange[0] = parameters.faultIndexLo;
    faultIndexRange[1] = parameters.faultIndexHi;
	
	//Poroelastic parameters
    PetscReal alpha = parameters.alpha;

	/* --------------- Set up domain --------------- */
    Vec x, y;
    PetscInt Nx, Ny;
	// Set up vectors over processors
    ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&y);CHKERRQ(ierr);
	// Load domain from binary files generated in Matlab
    ierr = io.LoadDomainFromMatlab(x, Nx, y, Ny);

	/* --------------- Construct Mechanical equilibrium discretization --------------- */
    Mat mM_u, M_p, Sigma_xx, Sigma_xy, Sigma_yy;
	// Set up matrices over processors
    ierr = MatCreate(PETSC_COMM_WORLD,&mM_u);CHKERRQ(ierr);
    ierr = MatCreate(PETSC_COMM_WORLD,&M_p);CHKERRQ(ierr);
    ierr = MatCreate(PETSC_COMM_WORLD,&Sigma_xx);CHKERRQ(ierr);
    ierr = MatCreate(PETSC_COMM_WORLD,&Sigma_xy);CHKERRQ(ierr);
    ierr = MatCreate(PETSC_COMM_WORLD,&Sigma_yy);CHKERRQ(ierr);
	// Load Mechanical equilibrium and stress matrices from binary files generated in Matlab
    ierr = io.LoadMechanicalEquilibriumFromMatlab(mM_u, M_p, Sigma_xx, Sigma_xy, Sigma_yy);

	/* ---------------  Construct Fluid-Diffusion discretization --------------- */
    Mat F_p, F_u, p_p2p;
    Vec Q;
	// Set up matrices over processors
    ierr = MatCreate(PETSC_COMM_WORLD,&F_p);CHKERRQ(ierr);
    ierr = MatCreate(PETSC_COMM_WORLD,&F_u);CHKERRQ(ierr);
    ierr = MatCreate(PETSC_COMM_WORLD,&p_p2p);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&Q);CHKERRQ(ierr);
	// Load Fluid-Diffusion matrices from binary files generated in Matlab
    ierr = io.LoadFluidDiffusionFromMatlab(F_p, F_u, p_p2p, Q);

	/* --------------- Construct body fields --------------- */
    Vec w, p_p, p, sigma_xx, sigma_xy, sigma_yy;
    ierr = VecCreate(PETSC_COMM_WORLD,&w);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&p_p);CHKERRQ(ierr);
	// Load initial body field vectors from binary files generated in Matlab
    ierr = io.LoadInitialBodyFieldsFromMatlab(w, p_p);CHKERRQ(ierr);
    ierr = VecDuplicate(p_p,&p);CHKERRQ(ierr);
    ierr = VecDuplicate(p_p,&sigma_xx);CHKERRQ(ierr);
    ierr = VecDuplicate(p_p,&sigma_xy);CHKERRQ(ierr);
    ierr = VecDuplicate(p_p,&sigma_yy);CHKERRQ(ierr);

	/* --------------- Construct fault fields --------------- */
     Vec Psi, sigma_p, tau_qs, tau, a, b, V, p_S;
	// Set up vectors and matrices over processors
    ierr = VecCreate(PETSC_COMM_WORLD,&Psi);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&a);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&b);CHKERRQ(ierr);
	// Load initial fault field vectors from binary files generated in Matlab
    ierr = io.LoadInitialFaultFieldsFromMatlab(Psi, a, b);CHKERRQ(ierr);
	// Create effective stress vector sigma_p,shear on fault vector tau_qs, tau_qs, slip velocity vector V and pressure vector p_S
    ierr = VecDuplicate(Psi,&sigma_p);CHKERRQ(ierr);
    ierr = VecDuplicate(Psi,&tau_qs);CHKERRQ(ierr);
    ierr = VecDuplicate(Psi,&tau);CHKERRQ(ierr);	
    ierr = VecDuplicate(Psi,&V);CHKERRQ(ierr);
    ierr = VecDuplicate(Psi,&p_S);CHKERRQ(ierr);
    
	/* --------------- Construct boundary operators and vectors --------------- */
    Mat B_S;
    Vec b_m, w_S, u_S, du_S;
    ierr = MatCreate(PETSC_COMM_WORLD,&B_S);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&b_m);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&w_S);CHKERRQ(ierr);
    ierr = VecCreate(PETSC_COMM_WORLD,&u_S);CHKERRQ(ierr);
    ierr = io.LoadBoundaryConditionsFromMatlab(u_S, B_S);
	// Create the boundary data vector b_m for mechanical equilibrium equation
    ierr = VecSetSizes(b_m,PETSC_DECIDE,2*Nx*Ny);CHKERRQ(ierr);
    ierr = VecSetFromOptions(b_m);CHKERRQ(ierr);
	// South boundary vectors for mechanical equilibrium equation
    ierr = VecSetSizes(w_S,PETSC_DECIDE,2*Nx);CHKERRQ(ierr);
    ierr = VecSetFromOptions(w_S);CHKERRQ(ierr);

	/* --------------- Containers used for adaptive RK method --------------- */
    Vec w_temp, u_S2, u_S3, du_S2, du_S3, u_RK2, u_RK3, e_uRK, p_p2, p_p3, dp_p, dp_p2, dp_p3, Psi2, Psi3, dPsi, dPsi2, dPsi3, V_temp;
    ierr = VecDuplicate(w,&w_temp);CHKERRQ(ierr);
	// South boundary vector stage variables
    ierr = VecDuplicate(u_S,&u_S2);CHKERRQ(ierr);
    ierr = VecDuplicate(u_S,&u_S3);CHKERRQ(ierr);
    ierr = VecDuplicate(u_S,&du_S);CHKERRQ(ierr);
    ierr = VecDuplicate(u_S,&du_S2);CHKERRQ(ierr);
    ierr = VecDuplicate(u_S,&du_S3);CHKERRQ(ierr);
	// Vectors for computing error of slip on fault
    ierr = VecDuplicate(u_S,&u_RK2);CHKERRQ(ierr);
    ierr = VecDuplicate(u_S,&u_RK3);CHKERRQ(ierr);
    ierr = VecDuplicate(u_S,&e_uRK);CHKERRQ(ierr);
	// North boundary vector stage variables
	// Pressure stage variables p_p2, p_p3
    ierr = VecDuplicate(p_p,&p_p2);CHKERRQ(ierr);
    ierr = VecDuplicate(p_p,&p_p3);CHKERRQ(ierr);
	// Rate of change vector dp_p, and stage variables dp_p2, dp_p3
    ierr = VecDuplicate(p_p,&dp_p);CHKERRQ(ierr);
    ierr = VecDuplicate(p_p,&dp_p2);CHKERRQ(ierr);
    ierr = VecDuplicate(p_p,&dp_p3);CHKERRQ(ierr);
	// State stage variables for Psi2, Psi3
    ierr = VecDuplicate(Psi,&Psi2);CHKERRQ(ierr);
    ierr = VecDuplicate(Psi,&Psi3);CHKERRQ(ierr);
	// Create rate of change vectors dPsi, dPsi2 dPsi3
    ierr = VecDuplicate(Psi,&dPsi);CHKERRQ(ierr);
    ierr = VecDuplicate(Psi,&dPsi2);CHKERRQ(ierr);
    ierr = VecDuplicate(Psi,&dPsi3);CHKERRQ(ierr);
    // Create temporary storage vector V_temp
    ierr = VecDuplicate(V,&V_temp);CHKERRQ(ierr);

	/* --------------- Set up KSP --------------- */

#if DO_PROFILING == 1	
    MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0) timerKSPSetUp = MPI_Wtime();
#endif

	// Set up the Krylov subspace so you can use the different solvers:
	ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

	// Set KSPType for direct solver, using direct LU from MUMPS
	if (solver == "direct"){
		ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
		ierr = KSPSetOperators(ksp,mM_u,mM_u);CHKERRQ(ierr);
		ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE);CHKERRQ(ierr);
		// Set preconditioner for direct solver, using direct LU from MUMPS
		ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
		ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
		ierr = PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS);CHKERRQ(ierr);
		ierr = PCFactorSetUpMatSolverPackage(pc);CHKERRQ(ierr);

	}
	//set KSPType for iterative solver
	else if (solver == "iterative"){
		ierr = KSPSetType(ksp,KSPRICHARDSON);CHKERRQ(ierr);
		ierr = KSPSetOperators(ksp,mM_u,mM_u);CHKERRQ(ierr);
		ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE);CHKERRQ(ierr);
		//Set up preconditioner, using the boomerAMG PC from Hypre
		ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
		ierr = PCSetType(pc,PCHYPRE);CHKERRQ(ierr);
		ierr = PCHYPRESetType(pc,"boomeramg");CHKERRQ(ierr);
		ierr = PCFactorSetLevels(pc,4);CHKERRQ(ierr);
		ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);CHKERRQ(ierr);
	}
	// finish setting up KSP context using options defined above
	ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
	// perform computation of preconditioner now, rather than on first use
	ierr = KSPSetUp(ksp);CHKERRQ(ierr);

#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0){	
		timerKSPSetUp = MPI_Wtime() - timerKSPSetUp;
		timerTS = MPI_Wtime();
	}
#endif

	/* --------------- Time integration -------------------- */	
	PetscInt iLo, iHi;
	PetscReal error;

	//Compute index ranges for vectors living on fault
	ierr = VecGetOwnershipRange(u_S,&iLo,&iHi);CHKERRQ(ierr);

	/* Start up. Solve for unknown fields */

#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0) timerStart = MPI_Wtime();
#endif

	//Compute boundary data vector b_m
	ierr = ComputeBoundaryData(iLo, iHi, Nx, u_S, w_S, B_S, b_m);CHKERRQ(ierr);

#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0){
		timerEnd = MPI_Wtime();
		timerComputeBoundaryData = timerEnd-timerStart;
		timerStart = MPI_Wtime();
	}
#endif

	//Solve mechanical equilibrium equations for u and v.
	ierr = MechanicalEquilibrium(p_p, w, M_p, b_m, ksp);CHKERRQ(ierr);
	
#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0) {
		timerEnd = MPI_Wtime();
		timerMechanicalEquilibrium = timerEnd-timerStart;
	}
#endif

	//Compute pore pressure p = p_p + p_p2p*w
	ierr = MatMultAdd(p_p2p,w,p_p,p);CHKERRQ(ierr);
		//Compute stress sigma_xx = Sigma_xy*w - alpha*p_p
	ierr = MatMult(Sigma_xx,w,sigma_xx);CHKERRQ(ierr);
	ierr = VecAXPY(sigma_xx,-1*alpha,p_p);CHKERRQ(ierr);
	//Compute stress sigma_xy = Sigma_xy*w
	ierr = MatMult(Sigma_xy,w,sigma_xy);CHKERRQ(ierr);
	//Compute stress sigma_yy = Sigma_xy*w - alpha*p_p
	ierr = MatMult(Sigma_yy,w,sigma_yy);CHKERRQ(ierr);
	ierr = VecAXPY(sigma_yy,-1*alpha,p_p);CHKERRQ(ierr);

#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0) timerStart = MPI_Wtime();
#endif

	//Compute quasi-static shear stress tau_qs and effective normal stress sigma_p on fault.
	ierr = ComputeStressAtFault(iLo, iHi, Ny, sigma_0, alpha, sigma_xy, sigma_yy, p, sigma_p, tau_qs);CHKERRQ(ierr);

#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0){
		timerEnd =  MPI_Wtime();
		timerComputeStressAtFault = timerEnd - timerStart;
		timerStart = MPI_Wtime();
	}
#endif
	
	//Solve for slip velocity V
	ierr = ComputeSlipVelocity(iLo, iHi, faultIndexRange, V_0, tau_0, eta, sigma_p, a, Psi, tau_qs, V);CHKERRQ(ierr);

#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0) {
		timerEnd = MPI_Wtime();
		timerComputeSlipVelocity = timerEnd-timerStart;
	}
#endif

#if DO_PROFILING == 0
	//Compute pressure and total shear at fault
	ierr = ComputeTotalShearAndPressureAtFault(iLo, iHi, Ny, tau_0, p, tau_qs, p_S, tau);CHKERRQ(ierr);
	//Initialize input streams and write down initial data.
	ierr = io.InitializeBodyFieldViewers(currentTime, w, p, sigma_xx, sigma_xy, sigma_yy);CHKERRQ(ierr);
	ierr = io.InitializeFaultFieldViewers(currentTime, u_S, V, p_S, tau, sigma_p, Psi);CHKERRQ(ierr);
#endif

	PetscPrintf(PETSC_COMM_WORLD,"Beginning simulation using %s solver running with %d processes:\n",solver.c_str(), totalSize);
	PetscPrintf(PETSC_COMM_WORLD,"Domain size: %d,%d\n",Nx,Ny);


	/* ------------- Start of adaptive RK method ------------- */	
	int attempt;
	int numberOfTimeSteps = 0;
	int writeCounter = 1;

	while (currentTime < endTime){
		attempt = 1;
		while (true){
			if (currentTime + dt > endTime) dt = endTime-currentTime;

			if (numberOfTimeSteps%100 == 0) PetscPrintf(PETSC_COMM_WORLD,"Step number: %d, Current time: %f, End time: %f\n",numberOfTimeSteps,currentTime,endTime);

			/* ------------- RK Stage 1 ------------- */

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0) timerStart = MPI_Wtime();
#endif
			//Compute rates
			ierr = VecCopy(V,du_S);CHKERRQ(ierr);
			ierr = VecScale(du_S, 0.5);CHKERRQ(ierr);
			ierr = StateRate(iLo, iHi, a, b, V, V_0, Psi, L, f_0, dPsi);CHKERRQ(ierr);
			ierr = FluidDiffusionRate(p_p, w, F_p, F_u, Q, dp_p);CHKERRQ(ierr);

			//Integrate fields to t + 0.5*dt
			//Update  fault slip u_S
			ierr = VecWAXPY(u_S2,0.5*dt,du_S,u_S);CHKERRQ(ierr);
			//Update state Psi
			ierr = VecWAXPY(Psi2,0.5*dt,dPsi,Psi);CHKERRQ(ierr);
			//Update pore pressure p
			ierr = VecWAXPY(p_p2,0.5*dt,dp_p,p_p);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerIntegrateFields += timerEnd-timerStart;
				timerStart = MPI_Wtime();	
			} 
#endif
			//Compute boundary data vector b_m
			ierr = ComputeBoundaryData(iLo, iHi, Nx, u_S2, w_S, B_S, b_m);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerComputeBoundaryData += timerEnd-timerStart;
				timerStart = MPI_Wtime();
			}
#endif
			//Solve mechanical equilibrium equations for u and v.
			ierr = MechanicalEquilibrium(p_p2, w_temp, M_p, b_m,ksp);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerMechanicalEquilibrium += timerEnd-timerStart;
			}
#endif	
			//Compute pore pressure p = p_p_2 + p_p2p*w
			ierr = MatMultAdd(p_p2p,w_temp,p_p2,p);CHKERRQ(ierr);
			//Compute stress sigma_xy = Sigma_xy*w
			ierr = MatMult(Sigma_xy,w_temp,sigma_xy);CHKERRQ(ierr);
			//Compute stress sigma_yy = Sigma_xy*w - alpha*p_p
			ierr = MatMult(Sigma_yy,w_temp,sigma_yy);CHKERRQ(ierr);
			ierr = VecAXPY(sigma_yy,-1*alpha,p_p2);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0) timerStart = MPI_Wtime();
#endif
			//Compute quasi-static shear stress tau_qs and effective normal stress sigma_p on fault.
			ierr = ComputeStressAtFault(iLo, iHi, Ny, sigma_0, alpha, sigma_xy, sigma_yy, p, sigma_p, tau_qs);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerComputeStressAtFault += timerEnd-timerStart;
				timerStart = MPI_Wtime();
			}
#endif
			//Solve for slip velocity V
			ierr = ComputeSlipVelocity(iLo, iHi, faultIndexRange, V_0, tau_0, eta, sigma_p, a, Psi2 ,tau_qs,V_temp);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerComputeSlipVelocity += timerEnd-timerStart;
				timerStart = MPI_Wtime();
			}
#endif

			/* ------------- RK Stage 2 ------------- */

			//Compute rates
			ierr = VecCopy(V_temp,du_S2);CHKERRQ(ierr);
			ierr = VecScale(du_S2, 0.5);CHKERRQ(ierr);
			ierr = StateRate(iLo, iHi, a, b, V_temp, V_0, Psi2, L, f_0, dPsi2);CHKERRQ(ierr);
			ierr = FluidDiffusionRate(p_p2, w_temp, F_p, F_u, Q, dp_p2);CHKERRQ(ierr);

			//Integrate fields to t + dt
			//Update  fault slip u_S
			ierr = VecWAXPY(u_S3,-1*dt,du_S,u_S);CHKERRQ(ierr);
			ierr = VecAXPY(u_S3,2*dt,du_S2);CHKERRQ(ierr);
			//Update state Psi
			ierr = VecWAXPY(Psi3,-1*dt,dPsi,Psi);CHKERRQ(ierr);
			ierr = VecAXPY(Psi3,2*dt,dPsi2);CHKERRQ(ierr);
			//Update pore pressure p'
			ierr = VecWAXPY(p_p3,-1*dt,dp_p,p_p);CHKERRQ(ierr);
			ierr = VecAXPY(p_p3,2*dt,dp_p2);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerIntegrateFields += timerEnd-timerStart;
				timerStart = MPI_Wtime();	
			} 
#endif			

			//Compute boundary data vector b_m
			ierr = ComputeBoundaryData(iLo, iHi, Nx, u_S3, w_S, B_S, b_m);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerComputeBoundaryData += timerEnd-timerStart;
				timerStart = MPI_Wtime();
			}
#endif

			//Solve mechanical equilibrium equations for u and v.
			ierr = MechanicalEquilibrium(p_p3, w_temp, M_p, b_m, ksp);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerMechanicalEquilibrium += timerEnd-timerStart;
			}
#endif	
			//Compute pore pressure p = p_p_3 + p_p2p*w
			ierr = MatMultAdd(p_p2p,w_temp,p_p3,p);CHKERRQ(ierr);
			//Compute stress sigma_xy = Sigma_xy*w
			ierr = MatMult(Sigma_xy,w_temp,sigma_xy);CHKERRQ(ierr);
			//Compute stress sigma_yy = Sigma_xy*w - alpha*p_p
			ierr = MatMult(Sigma_yy,w_temp,sigma_yy);CHKERRQ(ierr);
			ierr = VecAXPY(sigma_yy,-1*alpha,p_p3);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0) timerStart = MPI_Wtime();
#endif

			//Compute quasi-static shear stress tau_qs and effective normal stress sigma_p on fault.
			ierr = ComputeStressAtFault(iLo, iHi, Ny, sigma_0, alpha, sigma_xy, sigma_yy, p, sigma_p, tau_qs);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerComputeStressAtFault += timerEnd-timerStart;
				timerStart = MPI_Wtime();
			}
#endif

			//Solve for slip velocity V
			ierr = ComputeSlipVelocity(iLo, iHi, faultIndexRange, V_0, tau_0, eta, sigma_p, a, Psi3, tau_qs, V_temp);CHKERRQ(ierr);

#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerComputeSlipVelocity += timerEnd-timerStart;
				timerStart = MPI_Wtime();
			}
#endif

			/* ------------- RK Stage 3 ------------- */

			//Compute rates
			ierr = VecCopy(V_temp, du_S3);CHKERRQ(ierr);
			ierr = VecScale(du_S3, 0.5);CHKERRQ(ierr);
			ierr = StateRate(iLo, iHi, a, b, V_temp, V_0, Psi3, L, f_0, dPsi3);CHKERRQ(ierr);
			ierr = FluidDiffusionRate(p_p3, w_temp, F_p, F_u, Q, dp_p3);CHKERRQ(ierr);

			/* ------------- Error estimate ------------- */
			//Perform 2nd order Runge-Kutta update to calculate u_S at t + dt
			ierr = VecWAXPY(u_RK2,0.5*dt,du_S,u_S);CHKERRQ(ierr);
			ierr = VecAXPY(u_RK2,0.5*dt,du_S3);CHKERRQ(ierr);
			//Perform 3rd order Runge-Kutta update to calculate u_S at t + dt
			ierr = VecWAXPY(u_RK3,1.0/6*dt,du_S,u_S);CHKERRQ(ierr);
			ierr = VecAXPY(u_RK3,4.0/6*dt,du_S2);CHKERRQ(ierr);
			ierr = VecAXPY(u_RK3,1.0/6*dt,du_S3);CHKERRQ(ierr);


#if DO_PROFILING == 1
			MPI_Barrier(PETSC_COMM_WORLD);
			if (rank == 0){
				timerEnd = MPI_Wtime();
				timerIntegrateFields += timerEnd-timerStart;	
			} 
#endif	

			//Calculate error
			ierr = VecWAXPY(e_uRK,-1,u_RK2,u_RK3);CHKERRQ(ierr);
			ierr = VecNorm(e_uRK,NORM_INFINITY,&error);CHKERRQ(ierr);

			// If step is acceptable: Update fields, write data, and exit loop
			if (error < tol){

#if DO_PROFILING == 1
				MPI_Barrier(PETSC_COMM_WORLD);
				if (rank == 0) timerStart = MPI_Wtime();
#endif

				//Update value of u_S
				ierr = VecSwap(u_S,u_RK3);CHKERRQ(ierr);
				
				//Update value of p_p
				ierr = VecAXPY(p_p,1.0/6*dt,dp_p);CHKERRQ(ierr);
				ierr = VecAXPY(p_p,4.0/6*dt,dp_p2);CHKERRQ(ierr);
				ierr = VecAXPY(p_p,1.0/6*dt,dp_p3);CHKERRQ(ierr);

				//Update value of Psi
				ierr = VecAXPY(Psi,1.0/6*dt,dPsi);CHKERRQ(ierr);
				ierr = VecAXPY(Psi,4.0/6*dt,dPsi2);CHKERRQ(ierr);
				ierr = VecAXPY(Psi,1.0/6*dt,dPsi3);CHKERRQ(ierr);

#if DO_PROFILING == 1
				MPI_Barrier(PETSC_COMM_WORLD);
				if (rank == 0){
					timerEnd = MPI_Wtime();
					timerIntegrateFields += timerEnd-timerStart;
					timerStart = MPI_Wtime();	
				} 
#endif	

				//Compute boundary data vector b_m
				ierr = ComputeBoundaryData(iLo, iHi, Nx, u_S, w_S, B_S, b_m);CHKERRQ(ierr);

#if DO_PROFILING == 1
				MPI_Barrier(PETSC_COMM_WORLD);
				if (rank == 0){
					timerEnd = MPI_Wtime();
					timerComputeBoundaryData += timerEnd-timerStart;
					timerStart = MPI_Wtime();
				}
#endif

				//Solve mechanical equilibrium equations for u and v.
				ierr = MechanicalEquilibrium(p_p, w, M_p, b_m, ksp);CHKERRQ(ierr);


#if DO_PROFILING == 1
				MPI_Barrier(PETSC_COMM_WORLD);
				if (rank == 0){
					timerEnd = MPI_Wtime();
					timerMechanicalEquilibrium += timerEnd-timerStart;
				}
#endif	

				//Compute pore pressure p = p_p + p_p2p*w
				ierr = MatMultAdd(p_p2p,w,p_p,p);CHKERRQ(ierr);
				//Compute stress sigma_xy = Sigma_xy*w
				ierr = MatMult(Sigma_xy,w,sigma_xy);CHKERRQ(ierr);
				//Compute stress sigma_yy = Sigma_xy*w - alpha*p_p
				ierr = MatMult(Sigma_yy,w,sigma_yy);CHKERRQ(ierr);
				ierr = VecAXPY(sigma_yy,-1*alpha,p_p);CHKERRQ(ierr);

#if DO_PROFILING == 1
				MPI_Barrier(PETSC_COMM_WORLD);
				if (rank == 0) timerStart = MPI_Wtime();
#endif

				//Compute quasi-static shear stress tau_qs and effective normal stress sigma_p on fault.
				ierr = ComputeStressAtFault(iLo, iHi, Ny, sigma_0, alpha, sigma_xy, sigma_yy, p, sigma_p, tau_qs);CHKERRQ(ierr);

#if DO_PROFILING == 1
				MPI_Barrier(PETSC_COMM_WORLD);
				if (rank == 0){
					timerEnd = MPI_Wtime();
					timerComputeStressAtFault += timerEnd-timerStart;
					timerStart = MPI_Wtime();
				}
#endif

				//Solve for slip velocity V
				ierr = ComputeSlipVelocity(iLo, iHi, faultIndexRange, V_0, tau_0, eta, sigma_p, a, Psi, tau_qs, V);CHKERRQ(ierr);

#if DO_PROFILING == 1
				MPI_Barrier(PETSC_COMM_WORLD);
				if (rank == 0){
					timerEnd = MPI_Wtime();
					timerComputeSlipVelocity += timerEnd-timerStart;
				}
#endif

				// Update the time
				currentTime+=dt;
				// Update number of time steps:
				numberOfTimeSteps++;

#if DO_PROFILING == 0
				//Write down data of updated fields
				if (writeCounter%faultWriteStride == 0){
					//Compute pressure and total shear at fault
					ierr = ComputeTotalShearAndPressureAtFault(iLo, iHi, Ny, tau_0, p, tau_qs, p_S, tau);CHKERRQ(ierr);
					ierr = io.WriteFaultFields(currentTime, u_S, V, p_S, tau, sigma_p, Psi);CHKERRQ(ierr);
					if (writeCounter%bodyWriteStride == 0) {
						//Compute stress sigma_xx = Sigma_xy*w - alpha*p_p
						ierr = MatMult(Sigma_xx,w,sigma_xx);CHKERRQ(ierr);
						ierr = VecAXPY(sigma_xx,-1*alpha,p_p);CHKERRQ(ierr);
						ierr = io.WriteBodyFields(currentTime, w, p, sigma_xx, sigma_xy, sigma_yy);CHKERRQ(ierr);
					}
				}
#endif
				// Find suggestion for next time step length
				if (error != 0) {
					dt = min(min(0.9*dt*pow(tol/error,1.0/3), maxdt), 5*dt);
				}

				// Exit while-loop and perform next time step
				break;
			}

			/* ------------- For unacceptable time step ------------- */
			ierr = PetscPrintf(PETSC_COMM_WORLD,"Rejecting attempt %d, error = %0.15e, with dt = %.15e at t = %0.15e\n",attempt, error, dt, currentTime);CHKERRQ(ierr);
			// Find suggestion for next time step length
			dt = min(0.9*dt*pow(tol/error,1.0/3), maxdt);
			attempt++;
		}
		writeCounter++;
	}

#if DO_PROFILING == 0
	//Write fault fields for last time step if not already written.
	if ((writeCounter-1)%faultWriteStride !=0) {
		//Compute pressure and total shear at fault
		ierr = ComputeTotalShearAndPressureAtFault(iLo, iHi, Ny, tau_0, p, tau_qs, p_S, tau);CHKERRQ(ierr);
		ierr = io.WriteFaultFields(currentTime, u_S, V, p_S, tau, sigma_p, Psi);CHKERRQ(ierr);
	}
	   //Write body fields for last time step if not already written.
	if ((writeCounter-1)%bodyWriteStride !=0) {
		//Compute stress sigma_xx = Sigma_xy*w - alpha*p_p
		ierr = MatMult(Sigma_xx,w,sigma_xx);CHKERRQ(ierr);
		ierr = VecAXPY(sigma_xx,-1*alpha,p_p);CHKERRQ(ierr);
		ierr = io.WriteBodyFields(currentTime, w, p, sigma_xx, sigma_xy, sigma_yy);CHKERRQ(ierr);
	}
		/* Print to notify user that simulation is complete */
	PetscPrintf(PETSC_COMM_WORLD,"Completed simulation using %s solver running with %d processes:\n",solver.c_str(), totalSize);
	PetscPrintf(PETSC_COMM_WORLD,"Domain size: %d,%d\n",Nx,Ny);
	PetscPrintf(PETSC_COMM_WORLD,"Start time: %f, End time: %f, Number of time steps: %d\n",parameters.startTime,endTime,numberOfTimeSteps);
	PetscPrintf(PETSC_COMM_WORLD,"Results written to file.\n");
#endif

#if DO_PROFILING == 1
	MPI_Barrier(PETSC_COMM_WORLD);
	if (rank == 0) timerTS = MPI_Wtime()-timerTS;

	/* Print runtime info */
	PetscPrintf(PETSC_COMM_WORLD,"Completed simulation using %s solver running with %d processes:\n",solver.c_str(), totalSize);
	PetscPrintf(PETSC_COMM_WORLD,"Domain size: %d,%d\n",Nx,Ny);
	PetscPrintf(PETSC_COMM_WORLD,"Start time: %f, End time: %f, Number of time steps: %d\n",parameters.startTime,endTime,numberOfTimeSteps);
	PetscPrintf(PETSC_COMM_WORLD,"Required time for setting up KSP: %f\n",timerKSPSetUp);
	PetscPrintf(PETSC_COMM_WORLD,"Required time for simulation: %f\n",timerTS);
	PetscPrintf(PETSC_COMM_WORLD,"Required time for computing boundary data: %f\n",timerComputeBoundaryData);
	PetscPrintf(PETSC_COMM_WORLD,"Required time for solving mechanical equilibrium equation: %f\n",timerMechanicalEquilibrium);
	PetscPrintf(PETSC_COMM_WORLD,"Required time for computing stresses at fault: %f\n",timerComputeStressAtFault);
	PetscPrintf(PETSC_COMM_WORLD,"Required time for computing slip velocity: %f\n",timerComputeSlipVelocity);
	PetscPrintf(PETSC_COMM_WORLD,"Required time for integrating fields: %f\n",timerIntegrateFields);
#endif

	/* --------------- Clean up memory --------------- */
	//KSP
	ierr = KSPDestroy(&ksp);CHKERRQ(ierr);

	//Domain
	ierr = VecDestroy(&x);CHKERRQ(ierr);
	ierr = VecDestroy(&y);CHKERRQ(ierr);

	//Mechanical equilibrium
	ierr = MatDestroy(&M_p);CHKERRQ(ierr);
	ierr = MatDestroy(&mM_u);CHKERRQ(ierr);
	ierr = MatDestroy(&Sigma_xx);CHKERRQ(ierr);
	ierr = MatDestroy(&Sigma_xy);CHKERRQ(ierr);
	ierr = MatDestroy(&Sigma_yy);CHKERRQ(ierr);


	//Fluid diffusion
	ierr = MatDestroy(&F_p);CHKERRQ(ierr);
	ierr = MatDestroy(&F_u);CHKERRQ(ierr);
	ierr = MatDestroy(&p_p2p);CHKERRQ(ierr);
	ierr = VecDestroy(&Q);CHKERRQ(ierr);

	//Body fields
	ierr = VecDestroy(&w);CHKERRQ(ierr);
	ierr = VecDestroy(&sigma_xx);CHKERRQ(ierr);
	ierr = VecDestroy(&sigma_xy);CHKERRQ(ierr);
	ierr = VecDestroy(&sigma_yy);CHKERRQ(ierr);
	ierr = VecDestroy(&p_p);CHKERRQ(ierr);
	ierr = VecDestroy(&p);CHKERRQ(ierr);

	//Fault
	ierr = VecDestroy(&tau_qs);CHKERRQ(ierr);
	ierr = VecDestroy(&tau);CHKERRQ(ierr);
	ierr = VecDestroy(&sigma_p);CHKERRQ(ierr);
	ierr = VecDestroy(&Psi);CHKERRQ(ierr);
	ierr = VecDestroy(&a);CHKERRQ(ierr);
	ierr = VecDestroy(&b);CHKERRQ(ierr);
	ierr = VecDestroy(&V);CHKERRQ(ierr);
	ierr = VecDestroy(&p_S);CHKERRQ(ierr);

	//Boundary
	ierr = MatDestroy(&B_S);CHKERRQ(ierr);
	ierr = VecDestroy(&b_m);CHKERRQ(ierr);
	ierr = VecDestroy(&w_S);CHKERRQ(ierr);
	ierr = VecDestroy(&u_S);CHKERRQ(ierr);
	ierr = VecDestroy(&du_S);CHKERRQ(ierr);

	//RK stage variables
	ierr = VecDestroy(&w_temp);CHKERRQ(ierr);
	ierr = VecDestroy(&u_S2);CHKERRQ(ierr);
	ierr = VecDestroy(&u_S3);CHKERRQ(ierr);
	ierr = VecDestroy(&du_S2);CHKERRQ(ierr);
	ierr = VecDestroy(&du_S3);CHKERRQ(ierr);
	ierr = VecDestroy(&u_RK2);CHKERRQ(ierr);
	ierr = VecDestroy(&u_RK3);CHKERRQ(ierr);
	ierr = VecDestroy(&e_uRK);CHKERRQ(ierr);
	ierr = VecDestroy(&p_p2);CHKERRQ(ierr);
	ierr = VecDestroy(&p_p3);CHKERRQ(ierr);
	ierr = VecDestroy(&dp_p);CHKERRQ(ierr);
	ierr = VecDestroy(&dp_p2);CHKERRQ(ierr);
	ierr = VecDestroy(&dp_p3);CHKERRQ(ierr);
	ierr = VecDestroy(&Psi2);CHKERRQ(ierr);
	ierr = VecDestroy(&Psi3);CHKERRQ(ierr);
	ierr = VecDestroy(&dPsi);CHKERRQ(ierr);
	ierr = VecDestroy(&dPsi2);CHKERRQ(ierr);
	ierr = VecDestroy(&dPsi3);CHKERRQ(ierr);
	ierr = VecDestroy(&V_temp);CHKERRQ(ierr);

	// Viewer contexts
	ierr = io.DestroyViewerContexts();CHKERRQ(ierr);


	PetscFinalize();
	return ierr;
}
