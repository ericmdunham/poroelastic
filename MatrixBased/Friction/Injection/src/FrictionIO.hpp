/*Class for handling I/O operations for the Injection problem setups.
  
	Author: Vidar Stiernstrom, Kim Torberntsson
*/
#ifndef FRICTIONIO_H
#define FRICTIONIO_H

#include <string>
#include "PetscVectorIO.hpp"
#include <iostream>
#include <fstream>


/* Struct used for reading problem parameters. 
	faultWriteStride 	- how often fault fields are written to file
	bodyWriteStride 	- how often body fields are written to file
	startTime			- startTime of simulation
	endTime				- endTime of simulation
	dt 					- Initial time step dt
	maxdt 				- Maximum timestep allowed for stability
	f_0					- Reference friction coefficient for sliding at V_0
	sigma_0 			- Background effective normal stress on fault
	L 					- State evolution distance (Dc)
	tau_0 				- Background shear stress on fault
	V_0 				- Steady sliding slip velocity
	eta 				- Half the shear-wave impedance
	tol 				- Tolerance for error of adaptive time step
	faultIndexLo		- Lower index of x-vector where fault begins
	faultIndexHi		- Higher index of x-vector where fault ends
	alpha 				- Biot's coefficient
*/
typedef struct problem_parameters {
	
	//I/O parameters
	PetscInt faultWriteStride;
	PetscInt bodyWriteStride;

	//temporal parameters
	PetscReal startTime;
	PetscReal endTime;
	PetscReal dt;
	PetscReal maxdt;

	//friction parameters
	PetscReal f_0;
	PetscReal sigma_0;
	PetscReal L;
	PetscReal tau_0;
	PetscReal V_0;
	PetscReal eta;
	PetscReal tol;
	PetscInt faultIndexLo;
	PetscInt faultIndexHi;

	//poroelastic parameters
	PetscReal alpha;

}problem_parameters_t;

using std::string;

class FrictionIO {
	private:
		//I/O directories
		string inputDir;
		string outputDir;
		//Viewer contexts
		PetscViewer timeFaultViewer;
		PetscViewer timeBodyViewer;
		PetscViewer wViewer;
		PetscViewer pViewer;
		PetscViewer sigma_xxViewer;
		PetscViewer sigma_xyViewer;
		PetscViewer sigma_yyViewer;
		PetscViewer u_SViewer;
		PetscViewer VViewer;
		PetscViewer p_SViewer;
		PetscViewer tauViewer;
		PetscViewer sigma_pViewer;
		PetscViewer PsiViewer;
		//Number of points in x and y direction. Used when loading matrices.
		PetscInt Nx, Ny;
		//The type of matrices to be loaded and initialized.
		MatType matType;
		
	public:
		PetscErrorCode ierr;
		//Constructor and destructor
		FrictionIO(const string inputDir,const string outputDir, MatType matType);
		~FrictionIO();

		/* ------- Member functions -------*/

		/* 	Function for initializing viewers used for writing body fields to file. Also writes the input to file 
			
			Input:	
					PetscReal t 		- Current time
					Vec w 				- Stacked displacement
					Vec p 				- Pore pressure
					Vec sigma_xx 		- Normal stress in x-direction
					Vec sigma_xy 		- Shear stress
					Vec sigma_yy 		- Normal stress in y-direction

			Output:
					PetscErrorCode ierr - Used for error handling in PETSc.
		*/
		PetscErrorCode InitializeBodyFieldViewers(PetscReal t, Vec w, Vec p, Vec sigma_xx, Vec sigma_xy, Vec sigma_yy);

		/* 	Function for initializing viewers used for writing  living on fault to file. 
			Also writes the input to file in directory specified by outputDir
			
			Input:	
					PetscReal t 		- Current time
					Vec u_S				- Slip on fault
					Vec V 				- Slip velocity
					Vec p_S 			- Pore pressure on fault
					Vec tau				- Total shear on fault
					Vec sigma_p 		- Effective normal stress on fault			
					Vec Psi 			- State variable

			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode InitializeFaultFieldViewers(PetscReal t, Vec u_S, Vec V, Vec p_S, Vec tau, Vec sigma_p, Vec Psi);
		
		/* 	Function for writing body fields to file in directory specified by outputDir
			
			Input:	
					PetscReal t 		- Current time
					Vec w 				- Stacked displacement
					Vec p 				- Pore pressure
					Vec sigma_xx 		- Normal stress in x-direction
					Vec sigma_xy 		- Shear stress
					Vec sigma_yy 		- Normal stress in y-direction

			Output:
					PetscErrorCode ierr - Used for error handling in PETSc.
		*/
		PetscErrorCode WriteBodyFields(PetscReal t, Vec w, Vec p, Vec sigma_xx, Vec sigma_xy, Vec sigma_yy);

		/*  Function for writing fault fields to file in directory specified by outputDir
	
			Input:	
					PetscReal t 		- Current time
					Vec u_S				- Slip on fault
					Vec V 				- Slip velocity
					Vec p_S 			- Pore pressure on fault
					Vec tau				- Total shear on fault
					Vec sigma_p 		- Effective normal stress on fault			
					Vec Psi 			- State variable

			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode WriteFaultFields(PetscReal t, Vec u_S, Vec V, Vec p_S, Vec tau, Vec sigma_p, Vec Psi);

		/* 	Function for cleaning up the viewer contexts. Should be called before PetscFinalize() is called
			
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode DestroyViewerContexts();

		/* 	Function for loading boundary vectors and matrices from Matlab. For the
			injection problems, the only non-zero boundary condition is the slip
			vector. (For the inflow problem the influx boundary condition is 
			treated as a source term.)
			
			Input:
					Vec u_S - Displacement vector u on south boundary (Slip vector)
					Mat B_S - The SAT-matrices that operate on boundary data at south boundary
			
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode LoadBoundaryConditionsFromMatlab(Vec u_S, Mat B_S);

		/* 	Function for loading the x and y coordinate vectors from Matlab.
			Extracts Nx and Ny from x and y. 
			
			Input:
					Vec x 		- x-coordinate axis vector
					PetscInt Nx - Number of grid points in x-direction
					Vec y 		- y-coordinate axis vector
					PetscInt Ny - Number of grid points in y-direction
			
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode LoadDomainFromMatlab(Vec x, PetscInt& Nx, Vec y, PetscInt& Ny);

		/* 	Function for loading matrices and vectors from Matlab used when 
			time integrating the fluid diffusion equation.

			Input:
					Mat F_p 	- SBP-matrices acting on pore pressure p'
					Mat F_u 	- SBP-matrices acting on displacement vector w = [u, v]^T
					Mat p_p2p   - Matrix used for converting between p' and p. p = p' + p_p2p*w
					Vec Q		- Source term in fluid diffusion equation
			
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode LoadInitialBodyFieldsFromMatlab(Vec w, Vec p_p);

		/* 	Function for loading the initial conditions for body fields from Matlab.

			Input:
					Vec w 	- Initial displacement vector w = [u, v]^T
					Vec p_p - Initial deviation from pore pressure vector p_p 
					
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode LoadInitialFaultFieldsFromMatlab(Vec Psi, Vec a, Vec b);

		/* 	Function for loading the initial conditions for fault fields from Matlab.

			Input:
					Vec Psi - Initial state
					Vec a 	- Friction parameter a
					Vec b 	- Friction parameter b
					
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode LoadFluidDiffusionFromMatlab(Mat F_p, Mat F_u, Mat p2p_p, Vec Q);

		/* 	Function for loading matrices and vectors from Matlab used when 
			solving the mechanical equilibrium equation. Also loads the matrices
			used for obtaining stresses.

			Input:
					Mat mMu 		- The left-hand-side SBP-matrices, used for solving mM_u*w = M_p * p_p + b_m
					Mat F_u 		- SBP-matrices acting on deviation of pore pressure p'
					Mat Sigma_xx   	- Matrix used for computing normal stress in x-direction.
									  sigma_xx = Sigma_xx * w - alpha*p_p
					Mat Sigma_xy   	- Matrix used for computing shear stress.
									  sigma_xy = Sigma_xy * w
					Mat Sigma_yy   	- Matrix used for computing normal stress in y-direction.
									  sigma_yy = Sigma_yy * w - alpha*p_p				  
					
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode LoadMechanicalEquilibriumFromMatlab(Mat mM_u, Mat M_p, Mat Sigma_xx, Mat Sigma_xy, Mat Sigma_yy);

		/* 	Function for loading problem setup parameters from Matlab.

			Input:
					problem_parameters_t parameters	- Struct containing the following problem parameters
					
			Output:
					PetscErrorCode ierr - Used for error handling in PETSc
		*/
		PetscErrorCode LoadParametersFromMatlab(problem_parameters_t& parameters);
};

#endif