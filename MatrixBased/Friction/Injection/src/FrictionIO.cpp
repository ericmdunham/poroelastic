/*Class for handling I/O operations for the Injection problem setups.
  
	Author: Vidar Stiernstrom, Kim Torberntsson
 */
#include "FrictionIO.hpp"

/* Constructor. Creates viewer contexts, sets I/O directories and initializes the Mat type */
FrictionIO::FrictionIO(const string inputDir, const string outputDir, MatType matType){
	
	//Set input and output directories used for reading and writing
	FrictionIO::inputDir = inputDir;
	FrictionIO::outputDir = outputDir;
	
	//Create viewer context for all relevant fields
	PetscViewerCreate(PETSC_COMM_WORLD,&timeFaultViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&timeBodyViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&wViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&pViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&sigma_xxViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&sigma_xyViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&sigma_yyViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&u_SViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&VViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&p_SViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&tauViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&sigma_pViewer);
	PetscViewerCreate(PETSC_COMM_WORLD,&PsiViewer);

	//Set the matrix type used by PETSc for initializing the matrices.
	FrictionIO::matType = matType;
}
/* Destructor.*/
FrictionIO::~FrictionIO(){
}
/* ------- Member functions -------*/

/* 	Function for initializing viewers used for writing body fields to file. Also writes the input to file 
	
	Input:	
			PetscReal t 		- Current time
			Vec w 				- Stacked displacement
			Vec p 				- Pore pressure
			Vec sigma_xx 		- Normal stress in x-direction
			Vec sigma_xy 		- Shear stress
			Vec sigma_yy 		- Normal stress in y-direction

	Output:
			PetscErrorCode ierr - Used for error handling in PETSc.
*/
PetscErrorCode FrictionIO::InitializeBodyFieldViewers(PetscReal t, Vec w, Vec p, Vec sigma_xx, Vec sigma_xy, Vec sigma_yy){
	ierr = 0;

	ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,(outputDir+"timeBody.txt").c_str(),&timeBodyViewer);CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(timeBodyViewer, "%.15e\n",t);CHKERRQ(ierr);

	//Initialize displacement w = [u ,v]^T viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"w").c_str(),FILE_MODE_WRITE, &wViewer);CHKERRQ(ierr);
	ierr = VecView(w,wViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&wViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"w").c_str(),FILE_MODE_APPEND,&wViewer);CHKERRQ(ierr);

	//Initialize pore pressure viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"p").c_str(),FILE_MODE_WRITE, &pViewer);CHKERRQ(ierr);
	ierr = VecView(p,pViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&pViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"p").c_str(),FILE_MODE_APPEND,&pViewer);CHKERRQ(ierr);

	//Initialize horizontal stress sigma_xx viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_xx").c_str(),FILE_MODE_WRITE, &sigma_xxViewer);CHKERRQ(ierr);
	ierr = VecView(sigma_xx,sigma_xxViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_xxViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_xx").c_str(),FILE_MODE_APPEND,&sigma_xxViewer);CHKERRQ(ierr);

	//Initialize shear stress sigma_xy viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_xy").c_str(),FILE_MODE_WRITE, &sigma_xyViewer);CHKERRQ(ierr);
	ierr = VecView(sigma_xy,sigma_xyViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_xyViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_xy").c_str(),FILE_MODE_APPEND,&sigma_xyViewer);CHKERRQ(ierr);

	//Initialize vertical stress sigma_yy viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_yy").c_str(),FILE_MODE_WRITE, &sigma_yyViewer);CHKERRQ(ierr);
	ierr = VecView(sigma_yy,sigma_yyViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_yyViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_yy").c_str(),FILE_MODE_APPEND,&sigma_yyViewer);CHKERRQ(ierr);

	return ierr;
}

/* 	Function for initializing viewers used for writing  living on fault to file. 
	Also writes the input to file in directory specified by outputDir
	
	Input:	
			PetscReal t 		- Current time
			Vec u_S				- Slip on fault
			Vec V 				- Slip velocity
			Vec p_S 			- Pore pressure on fault
			Vec tau				- Total shear on fault
			Vec sigma_p 		- Effective normal stress on fault			
			Vec Psi 			- State variable

	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::InitializeFaultFieldViewers(PetscReal t, Vec u_S, Vec V, Vec p_S, Vec tau, Vec sigma_p, Vec Psi){
	ierr = 0;
	//Initialize time viewer for fields living on fault and write result to file.
	ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,(outputDir+"timeFault.txt").c_str(),&timeFaultViewer);CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(timeFaultViewer, "%.15e\n",t);CHKERRQ(ierr);

	//Initialize slip u_S viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"u_S").c_str(),FILE_MODE_WRITE, &u_SViewer);CHKERRQ(ierr);
	ierr = VecView(u_S,u_SViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&u_SViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"u_S").c_str(),FILE_MODE_APPEND,&u_SViewer);CHKERRQ(ierr);

	//Initialize slip velocity V viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"V").c_str(),FILE_MODE_WRITE, &VViewer);CHKERRQ(ierr);
	ierr = VecView(V,VViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&VViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"V").c_str(),FILE_MODE_APPEND,&VViewer);CHKERRQ(ierr);

	//Initialize pressure p_S viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"p_S").c_str(),FILE_MODE_WRITE, &p_SViewer);CHKERRQ(ierr);
	ierr = VecView(p_S,p_SViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&p_SViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"p_S").c_str(),FILE_MODE_APPEND,&p_SViewer);CHKERRQ(ierr);

	//Initialize shear tau viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"tau").c_str(),FILE_MODE_WRITE, &tauViewer);CHKERRQ(ierr);
	ierr = VecView(tau,tauViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&tauViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"tau").c_str(),FILE_MODE_APPEND,&tauViewer);CHKERRQ(ierr);

	//Initialize effective normal stress sigma_p viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_p").c_str(),FILE_MODE_WRITE, &sigma_pViewer);CHKERRQ(ierr);
	ierr = VecView(sigma_p,sigma_pViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_pViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"sigma_p").c_str(),FILE_MODE_APPEND,&sigma_pViewer);CHKERRQ(ierr);

	//Initialize state Psi viewer, and write result to file.
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"Psi").c_str(),FILE_MODE_WRITE, &PsiViewer);CHKERRQ(ierr);
	ierr = VecView(Psi,PsiViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&PsiViewer);CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,(outputDir+"Psi").c_str(),FILE_MODE_APPEND,&PsiViewer);CHKERRQ(ierr);

	return ierr;
}

/* 	Function for writing body fields to file in directory specified by outputDir
	
	Input:	
			PetscReal t 		- Current time
			Vec w 				- Stacked displacement
			Vec p 				- Pore pressure
			Vec sigma_xx 		- Normal stress in x-direction
			Vec sigma_xy 		- Shear stress
			Vec sigma_yy 		- Normal stress in y-direction

	Output:
			PetscErrorCode ierr - Used for error handling in PETSc.
*/
PetscErrorCode FrictionIO::WriteBodyFields(PetscReal t, Vec w, Vec p, Vec sigma_xx, Vec sigma_xy, Vec sigma_yy){
	ierr = 0;
	//Write current time to file
	ierr = PetscViewerASCIIPrintf(timeBodyViewer, "%.15e\n",t);CHKERRQ(ierr);
	//Write displacements w = [u ,v]^T to file
	ierr = VecView(w,wViewer);CHKERRQ(ierr);
	//Write pore pressure p to file
	ierr = VecView(p,pViewer);CHKERRQ(ierr);
	//Write horizontal stress sigma_xx to file
	ierr = VecView(sigma_xx,sigma_xxViewer);CHKERRQ(ierr);
	//Write shear stress sigma_xy to file
	ierr = VecView(sigma_xy,sigma_xyViewer);CHKERRQ(ierr);
	//Write vertical stress sigma_yy to file
	ierr = VecView(sigma_yy,sigma_yyViewer);CHKERRQ(ierr);
	return ierr;
}

/*  Function for writing fault fields to file in directory specified by outputDir
	
	Input:	
			PetscReal t 		- Current time
			Vec u_S				- Slip on fault
			Vec V 				- Slip velocity
			Vec p_S 			- Pore pressure on fault
			Vec tau				- Total shear on fault
			Vec sigma_p 		- Effective normal stress on fault			
			Vec Psi 			- State variable

	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::WriteFaultFields(PetscReal t, Vec u_S, Vec V, Vec p_S, Vec tau, Vec sigma_p, Vec Psi){
	ierr = 0;
	//Write current time to file
	ierr = PetscViewerASCIIPrintf(timeFaultViewer, "%.15e\n",t);CHKERRQ(ierr);
	//Write slip u_S to file
	ierr = VecView(u_S,u_SViewer);CHKERRQ(ierr);
	//Write slip velocity V to file
	ierr = VecView(V,VViewer);CHKERRQ(ierr);
	//Write pressure on fault p_S V to file
	ierr = VecView(p_S,p_SViewer);CHKERRQ(ierr);
	//Write shear tau to file
	ierr = VecView(tau,tauViewer);CHKERRQ(ierr);
	//Write effective normal stress sigma_p to file
	ierr = VecView(sigma_p,sigma_pViewer);CHKERRQ(ierr);
	//Write state Psi to file
	ierr = VecView(Psi,PsiViewer);CHKERRQ(ierr);
	return ierr;
}

/* 	Function for cleaning up the viewer contexts. Should be called before PetscFinalize() is called
	
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::DestroyViewerContexts(){
	
	ierr = 0;

	ierr = PetscViewerDestroy(&timeFaultViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&timeBodyViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&wViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&pViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_xxViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_xyViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_yyViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&u_SViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&VViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&p_SViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&tauViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&sigma_pViewer);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&PsiViewer);CHKERRQ(ierr);
	
	return ierr;
}

/* 	Function for loading boundary vectors and matrices from Matlab. For the
	injection problems, the only non-zero boundary condition is the slip
	vector. (For the inflow problem the influx boundary condition is 
	treated as a source term.)
	
	Input:
			Vec u_S - Displacement vector u on south boundary (Slip vector)
			Mat B_S - The SAT-matrices that operate on boundary data at south boundary
	
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::LoadBoundaryConditionsFromMatlab(Vec u_S, Mat B_S){
	
	ierr = 0;

	//Load initial fault slip u_S
	ierr = VecSetSizes(u_S,PETSC_DECIDE,Nx);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(u_S,inputDir,"u_S");CHKERRQ(ierr);
	ierr = VecSetFromOptions(u_S);CHKERRQ(ierr);

	// Load the south boundary matrix B_S
	ierr = MatSetType(B_S,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(B_S,PETSC_DECIDE,PETSC_DECIDE,2*Ny*Nx,2*Nx);
	ierr = LoadMatFromInputFile(B_S,inputDir,"B_S");CHKERRQ(ierr);
	ierr = MatSetFromOptions(B_S);CHKERRQ(ierr);

	return ierr;
}


/* 	Function for loading the x and y coordinate vectors from Matlab.
	Extracts Nx and Ny from x and y. 
	
	Input:
			Vec x 		- x-coordinate axis vector
			PetscInt Nx - Number of grid points in x-direction
			Vec y 		- y-coordinate axis vector
			PetscInt Ny - Number of grid points in y-direction
	
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::LoadDomainFromMatlab(Vec x, PetscInt& Nx, Vec y, PetscInt& Ny){
	
	ierr = 0;

	// Load x, extract Nx
	ierr = LoadVecFromInputFile(x,inputDir,"x");CHKERRQ(ierr);
	ierr = VecSetFromOptions(x);CHKERRQ(ierr);
	ierr = VecGetSize(x,&Nx);CHKERRQ(ierr);
    
    // Load y, extract Ny 
	ierr = LoadVecFromInputFile(y,inputDir,"y");CHKERRQ(ierr);
	ierr = VecSetFromOptions(y);CHKERRQ(ierr);
	ierr = VecGetSize(y,&Ny);CHKERRQ(ierr);
	
	//Set class parameters Nx and Ny used when loading from Matlab.
	FrictionIO::Nx = Nx;
	FrictionIO::Ny = Ny;


	return ierr;

}

/* 	Function for loading matrices and vectors from Matlab used when 
	time integrating the fluid diffusion equation.

	Input:
			Mat F_p 	- SBP-matrices acting on pore pressure p'
			Mat F_u 	- SBP-matrices acting on displacement vector w = [u, v]^T
			Mat p_p2p   - Matrix used for converting between p' and p. p = p' + p_p2p*w
			Vec Q		- Source term in fluid diffusion equation
	
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/		
PetscErrorCode FrictionIO::LoadFluidDiffusionFromMatlab(Mat F_p, Mat F_u, Mat p_p2p, Vec Q){
	
	ierr = 0;
    
    // Load the matrix F_p acting on p
	ierr = MatSetType(F_p,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(F_p,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,Ny*Nx);
	ierr = LoadMatFromInputFile(F_p,inputDir,"F_p");CHKERRQ(ierr);
	ierr = MatSetFromOptions(F_p);CHKERRQ(ierr);


	// Load the matrix F_u acting on w
	ierr = MatSetType(F_u,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(F_u,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(F_u,inputDir,"F_u");CHKERRQ(ierr);
	ierr = MatSetFromOptions(F_u);CHKERRQ(ierr);

	// Load the matrix P, for transforming p_p to p.
	ierr = MatSetType(p_p2p,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(p_p2p,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(p_p2p,inputDir,"p_p2p");CHKERRQ(ierr);
	ierr = MatSetFromOptions(p_p2p);CHKERRQ(ierr);

	//Load the source term Q
	ierr = VecSetSizes(Q,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(Q,inputDir,"Q");CHKERRQ(ierr);
	ierr = VecSetFromOptions(Q);CHKERRQ(ierr);

	return ierr;
}

/* 	Function for loading the initial conditions for body fields from Matlab.

	Input:
			Vec w 	- Initial displacement vector w = [u, v]^T
			Vec p_p - Initial deviation from pore pressure vector p_p 
			
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::LoadInitialBodyFieldsFromMatlab(Vec w, Vec p_p){
	
	ierr = 0;

	//Load initial displacement vector w = [u, v]^T
	ierr = VecSetSizes(w,PETSC_DECIDE,2*Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(w,inputDir,"w");CHKERRQ(ierr);
	ierr = VecSetFromOptions(w);CHKERRQ(ierr);

	//Load initial pore pressure perturbation vector p_p
	ierr = VecSetSizes(p_p,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(p_p,inputDir,"p_p");CHKERRQ(ierr);
	ierr = VecSetFromOptions(p_p);CHKERRQ(ierr);

	return ierr;
}


/* 	Function for loading the initial conditions for fault fields from Matlab.

	Input:
			Vec Psi - Initial state
			Vec a 	- Friction parameter a
			Vec b 	- Friction parameter b
			
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::LoadInitialFaultFieldsFromMatlab(Vec Psi, Vec a, Vec b){

	ierr = 0;

	//Load initial state Psi
	ierr = VecSetSizes(Psi,PETSC_DECIDE,Nx);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(Psi,inputDir,"Psi");CHKERRQ(ierr);
	ierr = VecSetFromOptions(Psi);CHKERRQ(ierr);

	//Load friction parameter a
	ierr = VecSetSizes(a,PETSC_DECIDE,Nx);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(a,inputDir,"a");CHKERRQ(ierr);
	ierr = VecSetFromOptions(a);CHKERRQ(ierr);

	//Load friction parameter b
	ierr = VecSetSizes(b,PETSC_DECIDE,Nx);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(b,inputDir,"b");CHKERRQ(ierr);
	ierr = VecSetFromOptions(b);CHKERRQ(ierr);

	return ierr;
}

/* 	Function for loading matrices and vectors from Matlab used when 
	solving the mechanical equilibrium equation. Also loads the matrices
	used for obtaining stresses.

	Input:
			Mat mMu 		- The left-hand-side SBP-matrices, used for solving mM_u*w = M_p * p_p + b_m
			Mat F_u 		- SBP-matrices acting on deviation of pore pressure p'
			Mat Sigma_xx   	- Matrix used for computing normal stress in x-direction.
							  sigma_xx = Sigma_xx * w - alpha*p_p
			Mat Sigma_xy   	- Matrix used for computing shear stress.
							  sigma_xy = Sigma_xy * w
			Mat Sigma_yy   	- Matrix used for computing normal stress in y-direction.
							  sigma_yy = Sigma_yy * w - alpha*p_p				  
			
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::LoadMechanicalEquilibriumFromMatlab(Mat mM_u, Mat M_p, Mat Sigma_xx, Mat Sigma_xy, Mat Sigma_yy){
	
	ierr = 0;

	// Load the LHS matrix M_u, acting on w.
	ierr = MatSetType(mM_u,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(mM_u,PETSC_DECIDE,PETSC_DECIDE,2*Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(mM_u,inputDir,"mM_u");CHKERRQ(ierr);
	ierr = MatSetFromOptions(mM_u);CHKERRQ(ierr);

	// Load the RHS matrix M_p, acting on p.
	ierr = MatSetType(M_p,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(M_p,PETSC_DECIDE,PETSC_DECIDE,2*Ny*Nx,Ny*Nx);
	ierr = LoadMatFromInputFile(M_p,inputDir,"M_p");CHKERRQ(ierr);
	ierr = MatSetFromOptions(M_p);CHKERRQ(ierr);

	//Load the horizontal stress matrix Sigma_xx
	ierr = MatSetType(Sigma_xx,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(Sigma_xx,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(Sigma_xx,inputDir,"Sigma_xx");CHKERRQ(ierr);
	ierr = MatSetFromOptions(Sigma_xx);CHKERRQ(ierr);

	//Load the shear stress matrix Sigma_xy
	ierr = MatSetType(Sigma_xy,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(Sigma_xy,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(Sigma_xy,inputDir,"Sigma_xy");CHKERRQ(ierr);
	ierr = MatSetFromOptions(Sigma_xy);CHKERRQ(ierr);

	//Load the vertical stress matrix Sigma_yy
	ierr = MatSetType(Sigma_yy,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(Sigma_yy,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(Sigma_yy,inputDir,"Sigma_yy");CHKERRQ(ierr);
	ierr = MatSetFromOptions(Sigma_yy);CHKERRQ(ierr);

	return ierr;
}

/* 	Function for loading problem setup parameters from Matlab.

	Input:
			problem_parameters_t parameters	- Struct containing the following problem parameters

			faultWriteStride 	- how often fault fields are written to file
			bodyWriteStride 	- how often body fields are written to file
			startTime			- startTime of simulation
			endTime				- endTime of simulation
			dt 					- Initial time step dt
			maxdt 				- Maximum timestep allowed for stability
			f_0					- Reference friction coefficient for sliding at V_0
			sigma_0 			- Background effective normal stress on fault
			L 					- State evolution distance (Dc)
			tau_0 				- Background shear stress on fault
			V_0 				- Steady sliding slip velocity
			eta 				- Half the shear-wave impedance
			tol 				- Tolerance for error of adaptive time step
			faultIndexLo		- Lower index of x-vector where fault begins
			faultIndexHi		- Higher index of x-vector where fault ends
			alpha 				- Biot's coefficient
			
	Output:
			PetscErrorCode ierr - Used for error handling in PETSc
*/
PetscErrorCode FrictionIO::LoadParametersFromMatlab(problem_parameters_t& parameters){

	ierr = 0;
	std::ifstream inputFile;

	inputFile.open((inputDir+"problemParameters.in").c_str());
	//Read temporal parameters
	if (inputFile.is_open()) {
		
		//I/O parameters
		inputFile >> parameters.faultWriteStride;
		inputFile >> parameters.bodyWriteStride;
		
		//Temporal parameters
		inputFile >> parameters.startTime;
		inputFile >> parameters.endTime;
		inputFile >> parameters.dt;
		inputFile >> parameters.maxdt;
		
		//Friction parameters
		inputFile >> parameters.f_0;
		inputFile >> parameters.sigma_0;
		inputFile >> parameters.L;
		inputFile >> parameters.tau_0;
		inputFile >> parameters.V_0;
		inputFile >> parameters.eta;
		inputFile >> parameters.tol;
		inputFile >> parameters.faultIndexLo;
		inputFile >> parameters.faultIndexHi;

		//Poroelastic parameters
		inputFile >> parameters.alpha;
		inputFile.close();

	} else SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Error LoadParametersFromMatlab: Unable to open file problemParameters.in");

	return ierr;
}