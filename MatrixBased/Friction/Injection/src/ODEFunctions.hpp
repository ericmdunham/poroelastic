/* 	Functions used within the time integration procedure of friction code, i.e computing rates,
	solving for slip velocity etc.

	Author: Vidar Stiernstrom, Kim Torberntsson
*/
#ifndef ODEFUNCTIONS_HPP
#define ODEFUNCTIONS_HPP

#include <cmath>
#include <functional>
#include <petscts.h>

/* 	Function that finds the root of an equation using the bisection method algorithm. 
	
	Input:
			std::function<PetscReal (PetscReal)> f 	- The function that is to be evaluated. Here the residual
			PetscReal xL 							- Left bound
			PetscReal xR 							- Right bound
			PetscInt maxIter 						- Maximum number of iterations allowed
			PetscReal xTol 							- Tolerance of root
			PetscReal fTol 							- Tolerance of function value
			PetscReal root							- The found root. Here slip velocity vector V. 

	Output:
			PetscErrorCode ierr 					- Used for error handling in PETSc
*/
PetscErrorCode Bisection(std::function<PetscReal (PetscReal)> f, PetscReal xL, PetscReal xR, PetscInt maxIter, PetscReal xTol, PetscReal fTol, PetscReal& root);

/* 	Function that sets the boundary data of the displacement vector w. In the injection problems, data is only set on
	the south boundary. 
	
	Input:
			PetscInt iLo 			- Lowest index of slip vector u_S for a processor
			PetscInt iHi 	 		- Highest index of slip vector u_S for a processor
			PetscInt Nx 			- Grid points in x-direction	
			Vec u_S 				- Slip vector
			Vec w_S 				- Displacement vector w on south boundary.
			Mat B_S 				- SBP-matrix acting on w_S
			Vec b_m 				- Container vector for boundary data. b_m = B_S*w_S 

	Output:
			PetscErrorCode ierr 	- Used for error handling in PETSc
*/
PetscErrorCode ComputeBoundaryData(PetscInt iLo, PetscInt iHi, PetscInt Nx, Vec u_S, Vec w_S, Mat B_S, Vec b_m);

/* 	Function that computes the residual tau-f(V,Psi) (pointwise) used for the bisection method 
	when solving for slip velocity.
	
	Input:
			PetscReal sigma_p 		- Effective normal stress on fault
			PetscReal a 	 		- Friction parameter a
			PetscReal V_0 			- Steady sliding velocity	
			PetscReal Psi 			- State
			PetscReal tau_0 		- Background shear on fault
			PetscReal tau_qs		- Quasi-static shear on fault
			PetscReal eta 			- Half the shear-wave impedance
			PetscReal V 			- Slip velocity
	Output:
			PetscErrorCode ierr 	- Used for error handling in PETSc
*/
PetscReal ComputeResidual(PetscReal sigma_p, PetscReal a, PetscReal V_0, PetscReal Psi, PetscReal tau_0, PetscReal tau_qs, PetscReal eta, PetscReal V);

/* 	Function that computes the slip velocity vector V, using the bisection method. 
	
	Input:
			PetscInt iLo 				- Lowest index of a fault field for a processor
			PetscInt iHi 	 			- Highest index of a fault field for a processor
			PetscInt faultIndexRange[2]	- Array with the index ranges of the fault.
			PetscReal V_0 				- Steady sliding velocity	
			PetscReal tau_0 			- Background shear on fault
			Vec sigma_p 				- Effective normal stress on fault
			Vec a 			 			- Friction parameter a
			Vec Psi 					- State
			Vec tau_qs					- Quasi-static shear on fault
			Vec V 						- Slip velocity

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode ComputeSlipVelocity(PetscInt iLo, PetscInt iHi, PetscInt faultIndexRange[2], PetscReal V_0, PetscReal tau_0, PetscReal eta, Vec sigma_p, Vec a,  Vec Psi, Vec tau_qs, Vec V);

/* 	Function that computes stresses sigma_p and tau_qs at the fault 
	
	Input:
			PetscInt iLo 				- Lowest index of fault field for a processor
			PetscInt iHi 	 			- Highest index of fault field for a processor
			PetscInt PetscInt Ny		- Number of grid points in y-direction
			PetscReal sigma_0 			- Background stress	
			PetscReal alpha 			- Biot's coefficient
			Vec sigma_xy 				- Shear stress
			Vec sigma_yy 				- Normal stress in y-direction
			Vec p 						- Pressure
			Vec sigma_p 				- Effective normal stress on fault
			Vec tau_qs					- Quasi-static shear on fault

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode ComputeStressAtFault(PetscInt iLo, PetscInt iHi, PetscInt Ny, PetscReal sigma_0, PetscReal alpha, Vec sigma_xy, Vec sigma_yy, Vec p, Vec sigma_p, Vec tau_qs);

/* 	Function that computes total shear tau and pore pressure p_S on fault
	
	Input:
			PetscInt iLo 				- Lowest index of fault field for a processor
			PetscInt iHi 	 			- Highest index of fault field for a processor
			PetscInt PetscInt Ny		- Number of grid points in y-direction
			PetscReal tau_0 			- Background shear	
			Vec p 						- Pressure
			Vec tau_qs					- Quasi-static shear on fault
			Vec p_S 					- Pressure at fault
			Vec tau_qs					-Total shear on fault

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode ComputeTotalShearAndPressureAtFault(PetscInt iLo, PetscInt iHi, PetscInt Ny, PetscReal tau_0, Vec p, Vec tau_qs, Vec p_S, Vec tau);

/* 	Function that computes the rate of change of deviation of pore pressure dp_p, used for
	time integrating the fluid diffusion equation
	
	Input:
			
			Vec p 						- Pressure
			Vec w 						- Displacements
			Mat F_p						- SBP-matrices acting on p_p
			Vec F_u 					- SBP-matrices acting on w
			Vec Q						- Source term
			Vec dp_p					- Rate of change		

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode FluidDiffusionRate(Vec p_p, Vec w, Mat F_p, Mat F_u, Vec Q, Vec dp_p);

/* 	Function that solves the mechanical equilibrium equation
	
	Input:
			
			Vec p_p 					- Deviation from pore pressure
			Vec w 						- Displacements
			Mat M_p						- SBP-matrices acting on p_p
			Vec b_m 					- Boundary data vector
			KSP ksp 					- KSP context	

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode MechanicalEquilibrium(Vec p_p, Vec w, Mat mM_p, Vec b_m, KSP ksp);

/* 	Function that computes the rate of change of state dPsi
	used for time integrating the state variable 
	
	Input:
			PetscInt iLo 				- Lowest index of fault field for a processor
			PetscInt iHi 	 			- Highest index of fault field for a processor
			Vec a 			 			- Friction parameter a
			Vec b 			 			- Friction parameter b
			Vec V 						- Slip velocity
			PetscReal V_0 				- Steady sliding velocity
			Vec Psi 					- State	
			PetscReal L 				- State evolution distance
			PetscReal f_0 				- Reference friction coefficient for sliding at V_0
			Vec dPsi					- Rate of change of state

	Output:
			PetscErrorCode ierr 		- Used for error handling in PETSc
*/
PetscErrorCode StateRate(PetscInt iLo, PetscInt iHi, Vec a, Vec b, Vec V, PetscReal V_0, Vec Psi, PetscReal L, PetscReal f_0, Vec dPsi);

#endif
