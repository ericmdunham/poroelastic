#include <petscts.h>
#include <cmath>
#include "PetscVectorIO.hpp"
int main(int argc,char *argv[])
{

	PetscErrorCode ierr;
	PetscMPIInt localRank, totalSize;
	PetscInt i, Nu, Nt;
	PetscReal norm;
	double timerTS, timerKSPSetUp;		
	Vec v1, v2, va, b;
	Mat L, M, B;
	MatType matType;
	string inputDir, fieldName, solver;
	KSP ksp;
	PC pc;

	
  	//Initialize PETSc
	PetscInitialize(&argc,&argv,NULL,NULL);
	MPI_Comm_rank(PETSC_COMM_WORLD,&localRank);
	MPI_Comm_size(PETSC_COMM_WORLD,&totalSize);
	ierr = 0;

	if (argc>1) //Check if solver is defined from command line
	{
		solver = argv[1];
	}
	else //Default is iterative solver
	{
		solver = "iterative";
	}
	/* Loads vectors and matrices from binary files */
	inputDir = "../GenerateMatrices/DataStructs/";

  	//Load initial solution vector from binary files
	fieldName = "v1";
	ierr = VecCreate(PETSC_COMM_WORLD,&v1);CHKERRQ(ierr);
	ierr = VecSetFromOptions(v1);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(v1,inputDir,fieldName);CHKERRQ(ierr);
    //Duplicate structure of vector. Need two containers for the solver.
	ierr = VecDuplicate(v1,&v2);CHKERRQ(ierr);
	//Duplicate structure of vector, to use for analytical solution
	ierr = VecDuplicate(v1,&va);CHKERRQ(ierr);
	ierr = VecSetFromOptions(va);CHKERRQ(ierr);
	fieldName = "va";
	ierr = LoadVecFromInputFile(va,inputDir,fieldName);CHKERRQ(ierr);

  	//Check if the matrices should be partitioned sequential or in parallel
	if (totalSize > 1) {matType = MATMPIAIJ;}
	else {matType = MATSEQAIJ;}

  	//Load the LHS matrix L
	fieldName = "L";
	ierr = MatCreate(PETSC_COMM_WORLD,&L);CHKERRQ(ierr);
	ierr = MatSetType(L,matType);CHKERRQ(ierr);
	ierr = LoadMatFromInputFile(L,inputDir,fieldName);CHKERRQ(ierr);

   	//Load the RHS matrix M
	fieldName = "M";
	ierr = MatCreate(PETSC_COMM_WORLD,&M);CHKERRQ(ierr);
	ierr = MatSetType(M,matType);CHKERRQ(ierr);
	ierr = LoadMatFromInputFile(M,inputDir,fieldName);CHKERRQ(ierr);

  	//Load the boundary data matrix B
	fieldName = "B";
	ierr = MatCreate(PETSC_COMM_WORLD,&B);CHKERRQ(ierr);
	ierr = MatSetType(B,matType);CHKERRQ(ierr);
	ierr = LoadMatFromInputFile(B,inputDir,fieldName);CHKERRQ(ierr);

    //Get dimensions of B matrix.
	ierr = MatGetSize(B,&Nu,&Nt);CHKERRQ(ierr);
    //Construct vector for storing boundary data
	ierr = VecCreate(PETSC_COMM_WORLD,&b);CHKERRQ(ierr);
	ierr = VecSetSizes(b,PETSC_DECIDE,Nu);CHKERRQ(ierr);
	ierr = VecSetFromOptions(b);CHKERRQ(ierr);

	MPI_Barrier(PETSC_COMM_WORLD);
	if (localRank == 0) timerKSPSetUp = MPI_Wtime(); //Start clock for KSP set up

	// set up the Krylov subspace so you can use the different solvers:
	ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

	// Set KSPType for direct solver, using direct LU from MUMPS
	if (solver == "direct")
	{
		ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
	}
	//set KSPType for iterative solver
	else if (solver == "iterative")
	{	//Default is GMRES. Can be overwritten using command line option
		ierr = KSPSetType(ksp,KSPGMRES);CHKERRQ(ierr);
		ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);CHKERRQ(ierr);
		PetscInt restart = 10;
		ierr =  KSPGMRESSetRestart(ksp, restart);
	}

	ierr = KSPSetOperators(ksp,L,L);CHKERRQ(ierr);
	ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE);CHKERRQ(ierr);
	ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);

// Set KSPPC for direct solver, using direct LU from MUMPS
	if (solver == "direct")
	{
		ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
		ierr = PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS);CHKERRQ(ierr);
		ierr = PCFactorSetUpMatSolverPackage(pc);CHKERRQ(ierr);
		/* for Cholesky factorization from MUMPS
		ierr = PCSetType(pc,PCCHOLESKY);CHKERRQ(ierr);
  		*/
	}
	// Set KSPPC for iterative solver
	else if (solver == "iterative") 
	{ 
		//Default is PCASM. Can be overwritten using command line option
		ierr = PCSetType(pc,PCASM);CHKERRQ(ierr);
	}

	// finish setting up KSP context using options defined above
	ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  	// perform computation of preconditioners now, rather than on first use
	ierr = KSPSetUp(ksp);CHKERRQ(ierr);

	MPI_Barrier(PETSC_COMM_WORLD);
   	   if (localRank == 0)
   	    {	timerKSPSetUp = MPI_Wtime()-timerKSPSetUp; //Stop clock for KSP set uP
   	   		timerTS = MPI_Wtime(); //Start clock for time stepping
   	   	}

  	// Time stepping
	for (i = 0; i<Nt-1; i++)
	{
  		/* Get boundary data at time i+1. 
   	   	Should probably be changed. Not sure if it's smart to store in matrix.
   	   	Better to compute? */
   	   	ierr = MatGetColumnVector(B,b,i+1);CHKERRQ(ierr);
  		//Construct RHS: M*v1+b. Current time step in v1. Stores result in v2.
   	   	ierr = MatMultAdd(M,v1,b,v2);CHKERRQ(ierr); 
    	// solve E*v1 = v2 (Writes over previous time step, no need to swap pointers)
   	   	ierr = KSPSolve(ksp,v2,v1);CHKERRQ(ierr);
   	   }
   	   MPI_Barrier(PETSC_COMM_WORLD);
   	   if (localRank == 0) timerTS = MPI_Wtime()-timerTS; //Stop clock for time stepping
   	   
	//Calculate pointwise difference of numerical and analytical solution. Store in v2
   	   ierr =  VecWAXPY(v2,-1,v1,va);CHKERRQ(ierr);
	//Calculate max norm of difference.
   	   ierr =  VecNorm(v2,NORM_INFINITY,&norm);CHKERRQ(ierr);
	/* Print results */
   	   PetscPrintf(PETSC_COMM_WORLD,"Results using %s solver running with %d processes:\n",solver.c_str(), totalSize);
   	   PetscPrintf(PETSC_COMM_WORLD,"Required time for setting up KSP: %f\n",timerKSPSetUp);
   	   PetscPrintf(PETSC_COMM_WORLD,"Required time for time stepping: %f\n",timerTS);
   	   PetscPrintf(PETSC_COMM_WORLD,"Maximum error: %f\n",norm);

   	   
     //Write result to file
   	   ierr = WriteVec(v1,"../GenerateMatrices/DataStructs/v_petsc");CHKERRQ(ierr);

 	// clean up memory before closing. you can call these on NULL pointers.
   	   KSPDestroy(&ksp);
   	   VecDestroy(&v1);
   	   VecDestroy(&v2);
   	   VecDestroy(&va);
   	   VecDestroy(&b);
   	   MatDestroy(&L);
   	   MatDestroy(&M);
   	   MatDestroy(&B);

   	   PetscFinalize();
   	   return ierr;
   	}


  /* 
  // for Cholesky factorization from MUMPS
  ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,sbp._A,sbp._A);CHKERRQ(ierr);
  ierr = KPSSetReusePreconditioner(ksp,PETSC_TRUE);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  PCSetType(pc,PCCHOLESKY);
  PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS);
  PCFactorSetUpMatSolverPackage(pc);
  */

   /*
  // for conjugate gradient (requires your matrix to be symmetric)
  ierr = KSPSetType(ksp,KSPCG);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,_A,_A);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  PCSetType(pc,PCGAMG); // this seems to be the suggested default option
  */
