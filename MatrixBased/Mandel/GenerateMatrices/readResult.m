% Script for reading results computed using PETSc and then comparing them
% to analytic solution, by computing the l2-norm. Assumes that 
% generateMatrices.m has been executed. If generateMatrices.m is run with 
% do_verification = 1, then also computes the l2-norm using for the Matlab
% computation, for comparison.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

petscDir = getenv('PETSC_DIR');
addpath(strcat(petscDir,'/share/petsc/matlab/'));
inDir = './data_structs/';

v_petsc = PetscBinaryRead(strcat(inDir,'v_petsc'),'cell',100000);
v1 = cell2mat(v_petsc);

u = reshape(v1(1:Ny*Nx), Ny, Nx);
u_ref = ones(Ny, 1)*u_a(:, end)';
norms_u_petsc = sqrt(dy*dx)*norm(u - u_ref)
    
v= reshape(v1(Ny*Nx+1:2*Ny*Nx), Ny, Nx);
v_ref = v_a(:, end)*ones(1, Nx);
norms_v_petsc = sqrt(dy*dx)*norm(v - v_ref)
    
p = reshape(v1(2*Ny*Nx+1:3*Ny*Nx), Ny, Nx);
p_ref = ones(Ny, 1)*p_a(:, end)';
norms_p_petsc = sqrt(dy*dx)*norm(p - p_ref)

if do_verification
    u = reshape(V(1:Ny*Nx), Ny, Nx);
    norms_u_matlab = sqrt(dy*dx)*norm(u - u_ref)
    
    v = reshape(V(Ny*Nx+1:2*Ny*Nx), Ny, Nx);
    norms_v_matlab = sqrt(dy*dx)*norm(v - v_ref)
   
    p = reshape(V(2*Ny*Nx+1:3*Ny*Nx), Ny, Nx);
    norms_p_matlab = sqrt(dy*dx)*norm(p - p_ref)
end

%Check if the matrices has been loaded correctly.
% 
%  A_loaded = PetscBinaryRead(strcat(inDir,'A_loaded'),'cell',100000);
%  A_loadedMat = cell2mat(A_loaded);
%  norm_A = norm(A-A_loadedMat,Inf)
%  
%  B_loaded = PetscBinaryRead(strcat(inDir,'B_loaded'),'cell',100000);
%  B_loadedMat = cell2mat(B_loaded);
%  norm_B = norm(B-B_loadedMat,Inf)
%  
%  
%  C_loaded = PetscBinaryRead(strcat(inDir,'C_loaded'),'cell',100000);
%  C_loadedMat = cell2mat(C_loaded);
%  norm_C = norm(C-C_loadedMat,Inf)