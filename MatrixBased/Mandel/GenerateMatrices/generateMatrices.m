clear all;

% Add paths for helper functions
addpath '../../../Matlab/Helpers/Mandel' ...
    '../../../Matlab/Helpers/ImplicitDiscretization'

%% Define type of SBP operators
SBPStencil = 'r';          % Type of SBP-stencil
                            % w - wide, n - narrow, r - russian
order = 3;                  % order of SBP-operator. Must be consistent
                            % with stencil type
doVerification = 0;        % If Matlab should compute the solution

%% Material parameters

% Parameters in dimensionless scales
a = 1;                  % Length in x-direction
b = 1;                  % Length in y-direction
G = 1;                  % Shear modulus
kappa = 1;              % k/my, k-permiability, my fluid viscoscity
F = 1;                  % Compressive force

% Dimensionless parameters
B = 0.8;                % Skemptons paramater
nu = 0.2;               % Poissions ratio (drained)
nu_u = 0.4;             % Poissons ratio (undrained)

% Compute other poroelastic material parameters;
[K, alpha, M, c] = MaterialParameters(G,kappa,B,nu,nu_u);

%% Spatial discretization

Nx = 100;                    % Resolution in x-direction
Ny = 100;                    % Resolution in y-direction
xL = 0;                     % West boundary
xR = a;                     % East boundary
yL = 0;                     % South Boundary
yR = b;                     % North Boundary
x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dx = (xR-xL)/(Nx-1);
dy = (yR-yL)/(Ny-1);

%% Temporal discretization

startTime = 0.01*a^2/c;  % Start of simulation
endTime = 0.02*a^2/c;    % End of simulation
Nt = 10;                % Temporal resolution
t = linspace(startTime, endTime, Nt);   % Temporal grid
dt = (endTime - startTime)/(Nt - 1);

%% Initial condition
% Impose analytical solutions at t=startTime as initial conditions.

%Compute analytical solutions
r_max = 50; % Determines quality of analytic solution. 1 < r_max < Inf
[u_a, v_a, p_a] = ...
    AnalyticalSolutions(x, y, t, G, F, c, nu, nu_u, B, r_max);

V_init = [reshape(ones(Ny, 1)*u_a(:, 1)', Nx*Ny, 1);...
    reshape(v_a(:, 1)*ones(1, Nx), Nx*Ny, 1);...
    reshape(ones(Ny, 1)*p_a(:, 1)', Nx*Ny, 1)];


%% Boundary conditions and boundary data for Mandel's problem

% West boundary
% u = 0, sigma_yx = 0, q_x = 0
%
% East boundary
% sigma_xx = 0, sigma_yx = 0, p = 0
%
% South boundary
% sigma_xy = 0, v = 0, q_y = 0
%
% North boundary
% sigma_xy = 0, v = v_a, q_y = 0

% Boundary conditions are specified in the 3x4 matrix BC of the following
% structure
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
% 0 - condition on traction, or discharge velocity
% 1 - condition on displacement or pore pressure.
% Example: BC(1,1) = 1 <--> Condition on x-component of displacement on
%                           West boundary
%          BC(2,3) = 0 <--> Condition on discharge velocity on East
%                           boundary
%

BC = [1 0 0 0;
      0 0 1 1;
      0 1 0 0];

% Boundary data
%
% Data is specified component-wise for all times in a M-Nt matrix, where M
% is the resolution in the corresponding dimension.
% Example: gWu = zeros(Ny,Nt) <--> u=0 at West boundary for all times,
%          gWv = ones(Ny,Nt)   <--> v=1 at West boundary for all times.
% Data is then stored in boundary data matrices (eg. g), later used in the
% SBP-SAT discretization. Data must be stored as follows:
% g = [gW; gE; gS; gN], where e.g. gW = [gWu; gWv]. Similar for f.

% Boundary data for mechanical equilibrium equations
gWu = spalloc(Ny, Nt, 0);
gWv = spalloc(Ny, Nt, 0);
gW = [gWu; gWv];

gEu = spalloc(Ny, Nt, 0);
gEv = spalloc(Ny, Nt, 0);
gE = [gEu; gEv];

gSu = spalloc(Nx, Nt, 0);
gSv = spalloc(Nx, Nt, 0);
gS = [gSu; gSv];

gNu = spalloc(Nx, Nt, 0);
gNv = ones(Nx, 1)*v_a(Ny, :);
gN = [gNu; gNv];

g = [gW; gE; gS; gN];

% Boundary data for fluid diffusion equation
fW = spalloc(Ny, Nt, 0);
fE = spalloc(Ny, Nt, 0);
fS = spalloc(Nx, Nt, 0);
fN = spalloc(Nx, Nt, 0);

f = [fW; fE; fS; fN];

%% Construct the spatial operators

% Expand the material coefficients into matrix form
alpha = alpha*ones(Ny,Nx);
K = K*ones(Ny,Nx);
G = G*ones(Ny,Nx);
kappa = kappa*ones(Ny,Nx);
M = M*ones(Ny,Nx);

% Build the matrices used for the discretization of the system
[M, A, B] = SpatialOperators(Nx, dx, Ny, dy, alpha, K, G, kappa, M, ...
    BC, g, f, SBPStencil, order);

% Time stepping matrices
L = M - dt*A;

% Construct analytical solution at t = endTime
V_a = [reshape(ones(Ny, 1)*u_a(:, end)',Nx*Ny,1);
       reshape(v_a(:, end)*ones(1, Nx),Nx*Ny,1);
       reshape(ones(Ny, 1)*p_a(:, end)',Nx*Ny,1)];

%% Save the relevant data structures to binary files
%Need to add the directory containing PetscBinaryWrite to your path.
%The following line works if you start matlab from bash
petscDir = getenv('PETSC_DIR');
addpath(strcat(petscDir,'/share/petsc/matlab/'));

%Create directory if not already present
if ~exist('DataStructs', 'dir')
    mkdir('DataStructs'); 
end
outDir = 'DataStructs/';    %Set output directory

%Save spatial matrices and solution vector
PetscBinaryWrite(strcat(outDir,'L'),L);         %Matrix on LHS
PetscBinaryWrite(strcat(outDir,'M'),M);         %Matrix on RHS
PetscBinaryWrite(strcat(outDir,'B'),dt*B);      %Boundary data vector
PetscBinaryWrite(strcat(outDir,'v1'),V_init);   %Initial solution vector
PetscBinaryWrite(strcat(outDir,'va'),V_a);     %Analytical solution vector

%% Time integrate system
if doVerification
    V = v_init;
    for i = 1 : Nt - 1
        % Solve using Euler Backward
        V = L\(M*V + dt*B(:,i+1));
    end
end

% Remove paths for helper functions
rmpath '../../../Matlab/Helpers/Mandel' ...
    '../../../Matlab/Helpers/ImplicitDiscretization'
