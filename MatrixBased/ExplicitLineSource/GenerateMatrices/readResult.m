% Script for reading results computed using PETSc and then comparing them
% to analytic solution, by computing the l2-norm. Assumes that 
% generateMatrices.m has been executed. If generateMatrices.m is run with 
% do_verification = 1, then also computes the l2-norm using for the Matlab
% computation, for comparison.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

petscDir = getenv('PETSC_DIR');
addpath(strcat(petscDir,'/share/petsc/matlab/'));
inDir = './DataStructs/';

u_petsc = PetscBinaryRead(strcat(inDir,'u'), 'cell', 100000);
u = cell2mat(u_petsc);
u = reshape(u, Ny, Nx);
u_ref = ones(Ny, 1)*u_a(:, end)';
norms_u_petsc = sqrt(dy*dx)*norm(u - u_ref)

v_petsc = PetscBinaryRead(strcat(inDir,'v'), 'cell', 100000);
v = cell2mat(v_petsc);
v = reshape(v, Ny, Nx);
v_ref = v_a(:, end)*ones(1, Nx);
norms_v_petsc = sqrt(dy*dx)*norm(v - v_ref)


p_petsc = PetscBinaryRead(strcat(inDir,'p'), 'cell', 100000);
p = cell2mat(p_petsc);
p = reshape(p, Ny, Nx);
p_ref = ones(Ny, 1)*p_a(:, end)';
norms_p_petsc = sqrt(dy*dx)*norm(p - p_ref)

if do_time_integration
    load('results.mat');
    u = reshape(u, Ny, Nx);
    norms_u_matlab = sqrt(dy*dx)*norm(u - u_ref)
    
    v = reshape(v, Ny, Nx);
    norms_v_matlab = sqrt(dy*dx)*norm(v - v_ref)
   
    p = reshape(p, Ny, Nx);
    norms_p_matlab = sqrt(dy*dx)*norm(p - p_ref)
end

%Check if the matrices has been loaded correctly.
% 
%  A_loaded = PetscBinaryRead(strcat(inDir,'A_loaded'),'cell',100000);
%  A_loadedMat = cell2mat(A_loaded);
%  norm_A = norm(A-A_loadedMat,Inf)
%  
%  B_loaded = PetscBinaryRead(strcat(inDir,'B_loaded'),'cell',100000);
%  B_loadedMat = cell2mat(B_loaded);
%  norm_B = norm(B-B_loadedMat,Inf)
%  
%  
%  C_loaded = PetscBinaryRead(strcat(inDir,'C_loaded'),'cell',100000);
%  C_loadedMat = cell2mat(C_loaded);
%  norm_C = norm(C-C_loadedMat,Inf)