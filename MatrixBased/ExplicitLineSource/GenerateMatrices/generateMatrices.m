%% Matrix and vector generation for the line source problem with explicit 
%  time stepping.
% Script that generates matrices and vectors for the line source problem 
% with explicit time stepping using the SBP-SAT technique. Matrices and
% vectors can then be used in PETSc.
% The script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all

% Add paths for helper functions
addpath '../../../Matlab/Helpers/LineSource' '../../../Matlab/Helpers/'...
    '../../../Matlab/Helpers/ExplicitDiscretization'

do_time_integration = 1; %Set to one if you want MATLAB to compute

                         %numerical solution

%% Define type of SBP operators
SBPStencil = 'r';	% Type of SBP-stencil. w - wide, n - narrow, r - russian
order = 3;        	% order of SBP-operator.

% Dirac Delta parameters
ooa = 2;    % Order of accuracy
sc = 1;     % Smoothness conditions

%% Material parameters
a = 1;      % 10^3 m
b = 1;      % 10^3 m
G = 1;      % 10^9 Pa
kappa = 1;  % 10^-9 m^2/(Pa s)

q = 1;
rho = 1;

% Dimensionless parameters
B = 0.6;
nu = 0.2;
nu_u = 0.33;

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G, kappa, B, nu, nu_u);

%% Spatial discretization
xL = -a;                    % West boundary
xR = a;                     % East boundary
yL = -b;                    % South Boundary
yR = b;                     % North Boundary
% Use even points in line source
Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction
dy = (yR - yL)/(Ny - 1);    % Spatial step size, y-direction

%% Temporal discritization

startTime = 0.001;                     % Start of simulation
endTime = 0.0015;                      % End of simulation
Nt = 400;                              % Temporal resolution
t = linspace(startTime, endTime, Nt);  % Temporal grid
dt = (endTime - startTime)/(Nt - 1);   % Temporal step size

%% Initial condition

% Compute analytical solutions
[u_a, v_a, p_a] = ...
    AnalyticalSolutions(x, y, t, q, rho, G, lambda, kappa, c, alpha);

% Impose analytical solutions at t=startTime as initial conditions.
w_init = [reshape(u_a(:, :, 1), Ny*Nx, 1);
    reshape(v_a(:, :, 1), Ny*Nx, 1)];
p_init = reshape(p_a(:, :, 1), Ny*Nx, 1);

%% Boundary conditions and boundary data for Mandel's problem

% West boundary
% u = 0, sigma_yx = 0, q_x = 0
%
% East boundary
% sigma_xx = 0, sigma_yx = 0, p = 0
%
% South boundary
% sigma_xy = 0, v = 0, q_y = 0
%
% North boundary
% sigma_xy = 0, v = v_a, q_y = 0

% Boundary conditions are specified in the 3x4 matrix BC of the following
% structure
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
% 0 - condition on traction, or discharge velocity
% 1 - condition on displacement or pore pressure.
% Example: BC(1,1) = 1 <--> Condition on x-component of displacement on
%                           West boundary
%          BC(2,3) = 0 <--> Condition on discharge velocity on East
%                           boundary
%

BC = [1 1 1 1
    1 1 1 1
    1 1 1 1];

% How "hard" the Dirichlet boundary conditions are imposed
weight = 1/(dx*dy)^1.5;

%% Construct the spatial operators

% Expand the material coefficients into matrix form
alpha = alpha*ones(Ny,Nx);
K = K*ones(Ny,Nx);
G = G*ones(Ny,Nx);
kappa = kappa*ones(Ny,Nx);
M = M*ones(Ny,Nx);

% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, M, P] = SpatialOperators(Nx, dx, Ny, dy, ...
    alpha, K, G, kappa, M, BC, SBPStencil, weight, order);

% Matrices used by the time integrator.
dtM = dt*M;
F_p = M*F_p;
F_u = M*F_u;

% Create forcing function
Q = q*point_source(Nx, dx, Ny, dy, ooa, sc);
Q = full(M*Q);

% Boundary data vector for the mechanical equilibrium equations
gWu = squeeze(u_a(:, 1, :));
gWv = squeeze(v_a(:, 1, :));
gW = [gWu; gWv];

gEu = squeeze(u_a(:, Nx, :));
gEv = squeeze(v_a(:, Nx, :));
gE = [gEu; gEv];

gSu = squeeze(u_a(1, :, :));
gSv = squeeze(v_a(1, :, :));
gS = [gSu; gSv];

gNu = squeeze(u_a(Ny, :, :));
gNv = squeeze(v_a(Ny, :, :));
gN = [gNu; gNv];

% Boundary data matrix for the mechanical equilibrium equations
B_m = M_b.W*gW + M_b.E*gE + M_b.S*gS + M_b.N*gN;

% Boundary data for the fluid diffusion equation
fW = squeeze(p_a(:, 1, :));
fE = squeeze(p_a(:, Nx, :));
fS = squeeze(p_a(1, :, :));
fN = squeeze(p_a(Ny, :, :));

% Boundary data vector for the fluid diffusion equation
B_f = F_b.W*fW + F_b.E*fE + F_b.S*fS + F_b.N*fN;
B_f = M*B_f;


%% Save the relevant data structures to binary files
%Need to add the directory containing PetscBinaryWrite to your path.
%The following line works if you start matlab from bash
petsc_dir = getenv('PETSC_DIR');
addpath(strcat(petsc_dir,'/share/petsc/matlab/'));

%Create directory if not already present
if ~exist('DataStructs', 'dir')
    mkdir('DataStructs'); 
end
%Set output directory
outDir = './DataStructs/';

%Discretization
PetscBinaryWrite(strcat(outDir,'x'),x);
PetscBinaryWrite(strcat(outDir,'y'),y);
PetscBinaryWrite(strcat(outDir,'t'),t);

%Matrices for fluid-diffusion equation
PetscBinaryWrite(strcat(outDir,'F_p'),F_p); 
PetscBinaryWrite(strcat(outDir,'F_u'),F_u);     
PetscBinaryWrite(strcat(outDir,'Q'),Q);
PetscBinaryWrite(strcat(outDir,'B_f'),B_f);
PetscBinaryWrite(strcat(outDir,'p2p_p'),P);

%Matrices for mechanical equilibrium equations
PetscBinaryWrite(strcat(outDir,'M_u'),M_u);
PetscBinaryWrite(strcat(outDir,'mM_p'),-M_p);     
PetscBinaryWrite(strcat(outDir,'mB_m'),-B_m);

%Initial solution vectors
PetscBinaryWrite(strcat(outDir,'w_init'),w_init);  
PetscBinaryWrite(strcat(outDir,'p_p_init'),p_init+P*w_init);

%Analytical solution vector
PetscBinaryWrite(strcat(outDir,'u_a'),reshape(u_a(:,:,end),Nx*Ny,1));         
PetscBinaryWrite(strcat(outDir,'v_a'),reshape(v_a(:,:,end),Nx*Ny,1));
PetscBinaryWrite(strcat(outDir,'p_a'),reshape(p_a(:,:,end),Nx*Ny,1));

%% Time integrate system
% Set initial conditions
if doTimeIntegration
    w = w_init;
    p_p = p_init + P*w;
    
    for i = 1 : Nt - 1

        % Time time step in the fluid diffusion equation using Euler Forward
        p_p = p_p + dt*(F_p*p_p + F_u*w + B_f(:,i) + Q);
        
        % Solve the mechanical equilibrium equation for u and v.
        w = M_u\(-M_p*p_p - B_m(:,i+1));
        
    end
    % Obtain the numerical solution
    p = p_p - P*w;
    u = w(1:Ny*Nx);
    v = w(Nx*Ny + 1:2*Ny*Nx);
    
    save('results.mat','u','v','p');

end


% Remove paths for helper functions
rmpath '../../../Matlab/Helpers/LineSource' '../../../Matlab/Helpers/' ...
    '../../../Matlab/Helpers/ExplicitDiscretization'
