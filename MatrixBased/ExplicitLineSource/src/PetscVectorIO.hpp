#ifndef PETSCVECTORIO_HPP
#define PETSCVECTORIO_HPP

#include <string>
#include <petscts.h>

using std::string;
// Print out a vector with 15 significant figures.
void PrintVec(Vec vec);

// Write vec to the file loc in binary format.
// Note that due to a memory problem in PETSc, looping over this many
// times will result in an error.
PetscErrorCode WriteVec(Vec vec,const char * loc);

// loads a PETSc Vec from a binary file
PetscErrorCode LoadVecFromInputFile(Vec out,const string inputDir, const string fieldName);
// loads a PETSc Mat from a binary file
PetscErrorCode LoadMatFromInputFile(Mat out,const string inputDir, const string fieldName);
//Write Mat to the file loc in binary format.
PetscErrorCode WriteMat(Mat mat,const char * loc);

#endif