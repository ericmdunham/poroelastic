#ifndef LOADFROMMATLAB_HPP
#define LOADFROMMATLAB_HPP

#include "PetscVectorIO.hpp"


PetscErrorCode LoadDomainFromMatlab(Vec x, PetscInt& Nx, PetscReal& dx, Vec y, PetscInt& Ny, PetscReal& dy, Vec t, PetscInt& Nt, PetscReal& dt, const string inputDir);

PetscErrorCode LoadInitialConditionsFromMatlab(PetscInt Nx, PetscInt Ny, Vec p_p_init, Vec w_init, const string inputDir);

PetscErrorCode LoadFluidDiffusionFromMatlab(PetscInt Nx, PetscInt Ny, PetscInt Nt, MatType matType, Mat F_p, Mat F_u, Mat B_f, Mat p2p_p, Vec Q, const string inputDir);

PetscErrorCode LoadMechanicalEquilibriumFromMatlab(PetscInt Nx, PetscInt Ny, PetscInt Nt, MatType matType, Mat M_u, Mat mM_p, Mat mB_m, const string inputDir);

PetscErrorCode LoadAnalyticSolutionsFromMatlab(PetscInt Nx, PetscInt Ny, PetscInt Nt, Vec p_a, Vec u_a, Vec v_a, const string inputDir);

#endif