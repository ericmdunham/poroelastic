#include "LoadFromMatlab.hpp"

PetscErrorCode LoadDomainFromMatlab(Vec x, PetscInt& Nx, PetscReal& dx, Vec y, PetscInt& Ny, PetscReal& dy, Vec t, PetscInt& Nt, PetscReal& dt, const string inputDir){
	
	PetscErrorCode ierr = 0;
	PetscInt iLo, iHi, ind[2];
	PetscReal xVals[2], yVals[2], tVals[2];

	/* Load x, extract Nx, dx */
	ierr = LoadVecFromInputFile(x,inputDir,"x");CHKERRQ(ierr);
	ierr = VecSetFromOptions(x);CHKERRQ(ierr);
	ierr = VecGetSize(x,&Nx);CHKERRQ(ierr);
	//Calculate dx by extracting first two local elements of x. 
	ierr = VecGetOwnershipRange(x,&iLo,&iHi);
	ind[0] = iLo;
	ind[1] = iLo+1;
	ierr = VecGetValues(x,2,ind,xVals);CHKERRQ(ierr);
	dx = xVals[1] - xVals[0];
    
    /* Load y, extract Ny, dy */
	ierr = LoadVecFromInputFile(y,inputDir,"y");CHKERRQ(ierr);
	ierr = VecSetFromOptions(y);CHKERRQ(ierr);
	ierr = VecGetSize(y,&Ny);CHKERRQ(ierr);
	//Calculate dy by extracting first two local elements of y.   
	ierr = VecGetOwnershipRange(y,&iLo,&iHi);
	ind[0] = iLo;
	ind[1] = iLo+1;
	ierr = VecGetValues(y,2,ind,yVals);CHKERRQ(ierr);
	dy = yVals[1] - yVals[0];
	
	/* Load t, extract Nt, dt */
	ierr = LoadVecFromInputFile(t,inputDir,"t");CHKERRQ(ierr);
	ierr = VecSetFromOptions(t);CHKERRQ(ierr);
    ierr = VecGetSize(t,&Nt);CHKERRQ(ierr);
    //Calculate dt by extracting first two local elements of t.
	ierr = VecGetOwnershipRange(t,&iLo,&iHi);
	ind[0] = iLo;
	ind[1] = iLo+1;
	ierr = VecGetValues(t,2,ind,tVals);CHKERRQ(ierr);
	dt = tVals[1] - tVals[0];

	return ierr;
}


PetscErrorCode LoadInitialConditionsFromMatlab(PetscInt Nx, PetscInt Ny, Vec p_p_init, Vec w_init, const string inputDir){
	
	PetscErrorCode ierr = 0;

	//Load initial pore pressure perturbation vector p_p_init
	ierr = VecSetSizes(p_p_init,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(p_p_init,inputDir,"p_p_init");CHKERRQ(ierr);
	ierr = VecSetFromOptions(p_p_init);CHKERRQ(ierr);

	//Load initial displacement vector w_init = [u_init, v_init]^T
	ierr = VecSetSizes(w_init,PETSC_DECIDE,2*Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(w_init,inputDir,"w_init");CHKERRQ(ierr);
	ierr = VecSetFromOptions(w_init);CHKERRQ(ierr);

	return ierr;

}

PetscErrorCode LoadFluidDiffusionFromMatlab(PetscInt Nx, PetscInt Ny, PetscInt Nt, MatType matType, Mat F_p, Mat F_u, Mat B_f, Mat p2p_p, Vec Q, const string inputDir){
	
	PetscErrorCode ierr = 0;
    
    // Load the matrix F_p acting on p
	ierr = MatSetType(F_p,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(F_p,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,Ny*Nx);
	ierr = LoadMatFromInputFile(F_p,inputDir,"F_p");CHKERRQ(ierr);
	ierr = MatSetFromOptions(F_p);CHKERRQ(ierr);


	// Load the matrix F_u acting on w
	ierr = MatSetType(F_u,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(F_u,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(F_u,inputDir,"F_u");CHKERRQ(ierr);
	ierr = MatSetFromOptions(F_u);CHKERRQ(ierr);

	// Load the boundary matrix B_f and create boundary data vector b_f
	ierr = MatSetType(B_f,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(B_f,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,Nt);
	ierr = LoadMatFromInputFile(B_f,inputDir,"B_f");CHKERRQ(ierr);
	ierr = MatSetFromOptions(B_f);CHKERRQ(ierr);


	// Load the matrix P, for transforming p_p to p.
	ierr = MatSetType(p2p_p,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(p2p_p,PETSC_DECIDE,PETSC_DECIDE,Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(p2p_p,inputDir,"p2p_p");CHKERRQ(ierr);
	ierr = MatSetFromOptions(p2p_p);CHKERRQ(ierr);

	// Load source term vector Q
	ierr = VecSetSizes(Q,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(Q,inputDir,"Q");CHKERRQ(ierr);
	ierr = VecSetFromOptions(Q);CHKERRQ(ierr);
	

	return ierr;
}

PetscErrorCode LoadMechanicalEquilibriumFromMatlab(PetscInt Nx, PetscInt Ny, PetscInt Nt, MatType matType, Mat M_u, Mat mM_p, Mat mB_m, const string inputDir){
	
	PetscErrorCode ierr = 0;

	// Load the LHS matrix M_u, acting on w.
	ierr = MatSetType(M_u,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(M_u,PETSC_DECIDE,PETSC_DECIDE,2*Ny*Nx,2*Ny*Nx);
	ierr = LoadMatFromInputFile(M_u,inputDir,"M_u");CHKERRQ(ierr);
	ierr = MatSetFromOptions(M_u);CHKERRQ(ierr);

	// Load the RHS matrix M_p, acting on p.
	ierr = MatSetType(mM_p,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(mM_p,PETSC_DECIDE,PETSC_DECIDE,2*Ny*Nx,Ny*Nx);
	ierr = LoadMatFromInputFile(mM_p,inputDir,"mM_p");CHKERRQ(ierr);
	ierr = MatSetFromOptions(mM_p);CHKERRQ(ierr);

	// Load the boundary data matrix B_m
	ierr = MatSetType(mB_m,matType);CHKERRQ(ierr);
	ierr = MatSetSizes(mB_m,PETSC_DECIDE,PETSC_DECIDE,2*Ny*Nx,Nt);
	ierr = LoadMatFromInputFile(mB_m,inputDir,"mB_m");CHKERRQ(ierr);
	ierr = MatSetFromOptions(mB_m);CHKERRQ(ierr);


	return ierr;

}


PetscErrorCode LoadAnalyticSolutionsFromMatlab(PetscInt Nx, PetscInt Ny, PetscInt Nt, Vec p_a, Vec u_a, Vec v_a, const string inputDir){
	
	PetscErrorCode ierr = 0;

	// Load analytic solution vector p_a
	ierr = VecSetSizes(p_a,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(p_a,inputDir,"p_a");CHKERRQ(ierr);
	ierr = VecSetFromOptions(p_a);CHKERRQ(ierr);

	// Load analytic solution vector u_a v_a
	ierr = VecSetSizes(u_a,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(u_a,inputDir,"u_a");CHKERRQ(ierr);
	ierr = VecSetFromOptions(u_a);CHKERRQ(ierr);

	// Load analytic solution vector v_a
	ierr = VecSetSizes(v_a,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = LoadVecFromInputFile(v_a,inputDir,"v_a");CHKERRQ(ierr);
	ierr = VecSetFromOptions(v_a);CHKERRQ(ierr);


	return ierr;

}