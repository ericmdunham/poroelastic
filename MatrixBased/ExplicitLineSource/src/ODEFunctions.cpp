 #include "ODEFunctions.hpp"

PetscErrorCode FluidDiffusion(Vec p_p, Vec w, Mat F_p, Mat F_u, Vec Q, Vec b_f, Vec f){
	
	PetscErrorCode ierr = 0;
 	//b_f = b_f + S
 	ierr = VecAXPY(b_f,1,Q);CHKERRQ(ierr);
	//b_f = F_p*p_p + b_f
	ierr = MatMultAdd(F_p,p_p,b_f,b_f);CHKERRQ(ierr); 
	//b_f = F_u*w_p + b_f
	ierr = MatMultAdd(F_u,w,b_f,b_f);
	//f = F_u_ts*w + b_f
	ierr = MatMultAdd(F_p,p_p,b_f,f);
	return ierr;
 }

 PetscErrorCode MechanicalEquilibrium(Vec p_p, Vec w, Mat mM_p, Vec b_m, KSP ksp){
 	PetscErrorCode ierr = 0;
 	//b_m = mM_p*p_p + b_m
 	ierr = MatMultAdd(mM_p,p_p,b_m,b_m);CHKERRQ(ierr);
 	//solve M_u*w = b_m
 	ierr = KSPSolve(ksp,b_m,w);CHKERRQ(ierr);
 	return ierr;
 }