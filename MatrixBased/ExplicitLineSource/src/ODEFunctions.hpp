#ifndef ODEFUNCTIONS_HPP
#define ODEFUNCTIONS_HPP

#include <petscts.h>

PetscErrorCode FluidDiffusion(Vec p_p, Vec w, Mat F_p, Mat F_u, Vec Q, Vec b_f, Vec f);

PetscErrorCode MechanicalEquilibrium(Vec p_p, Vec w, Mat mM_p, Vec b_m, KSP ksp);

#endif