#include <petscts.h>
#include <cmath>
#include "ODEFunctions.hpp"
#include "LoadFromMatlab.hpp"

int main(int argc,char *argv[])
{

    PetscErrorCode ierr;
    PetscMPIInt localRank, totalSize;
    PetscInt i;
	double timerTS, timerKSPSetUp;		
	string solver;

	
  	//Initialize PETSc
	PetscInitialize(&argc,&argv,NULL,NULL);
	MPI_Comm_rank(PETSC_COMM_WORLD,&localRank);
	MPI_Comm_size(PETSC_COMM_WORLD,&totalSize);
	ierr = 0;

	if (argc>1){ //Check if solver is defined from command line
	
			solver = argv[1];
	}
	else{ //Default is iterative solver
	
		solver = "iterative";
	}

	//Check if the matrices should be partitioned sequential or in parallel
	MatType matType;
	if (totalSize > 1) {matType = MATMPIAIJ;}
	else {matType = MATSEQAIJ;}
	
	/* --------------- Set up domain -------------------- */
	//Set input directory for data structures
	string inputDir = "../GenerateMatrices/DataStructs/";

	Vec x, y, t;
	PetscInt Nx, Ny, Nt;
	PetscReal dx, dy, dt;
	// Set up vectors over processors
	ierr = VecCreate(PETSC_COMM_WORLD,&x);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&y);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&t);CHKERRQ(ierr);
	// Load domain from binary files generated in Matlab
	ierr = LoadDomainFromMatlab(x, Nx, dx, y, Ny, dy, t, Nt, dt, inputDir);

	/* --------------- Set initial conditions -------------------- */
	Vec p_p, w;
	// Set up vectors over processors
	ierr = VecCreate(PETSC_COMM_WORLD,&p_p);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&w);CHKERRQ(ierr);
	// Load inital conditions from binary files generated in Matlab
	ierr = LoadInitialConditionsFromMatlab(Nx, Ny, p_p, w, inputDir);

	/* ------- Construct Fluid-Diffusion discretization ----------- */
	Mat F_p, F_u, B_f, p2p_p;
	Vec Q, b_f, p, f; 
	// Set up vectors and matrices over processors
	ierr = MatCreate(PETSC_COMM_WORLD,&F_p);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,&F_u);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,&B_f);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,&p2p_p);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&Q);CHKERRQ(ierr);
	// Load Fluid-Diffusion vectors and matrices from binary files generated in Matlab
	ierr = LoadFluidDiffusionFromMatlab(Nx, Ny, Nt, matType, F_p, F_u, B_f, p2p_p,Q, inputDir);
	// Create and allocate memory for boundary data vector b_f
	ierr = VecDuplicate(p_p,&b_f);CHKERRQ(ierr);
	ierr = VecSetFromOptions(b_f);CHKERRQ(ierr);
	// Create and allocate memory for pore pressure vector p
	ierr = VecDuplicate(p_p,&p);CHKERRQ(ierr);
	ierr = VecSetFromOptions(p);CHKERRQ(ierr);
	// Create and allocate memory Fluid-diffusion function vector f
	ierr = VecDuplicate(p_p,&f);CHKERRQ(ierr);
	ierr = VecSetFromOptions(b_f);CHKERRQ(ierr);


	/* ------ Construct Mechanical equilibrium discretization ------ */
	Mat M_u, mM_p, mB_m;
	Vec b_m, u, v;
	// Set up vectors and matrices over processors
	ierr = MatCreate(PETSC_COMM_WORLD,&M_u);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,&mM_p);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,&mB_m);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&b_m);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&u);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&v);CHKERRQ(ierr);
	// Load Mechanical equilibrium vectors and matrices from binary files generated in Matlab
	ierr = LoadMechanicalEquilibriumFromMatlab(Nx, Ny, Nt, matType, M_u, mM_p, mB_m, inputDir);
	// Allocate memory for boundary data vector b_m
	ierr = VecSetSizes(b_m,PETSC_DECIDE,2*Nx*Ny);CHKERRQ(ierr);
	ierr = VecSetFromOptions(b_m);CHKERRQ(ierr);
	// Allocate memory for horizontal displacement vector u
	ierr = VecSetSizes(u,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = VecSetFromOptions(u);CHKERRQ(ierr);
	// Allocate memory for vertical displacement vector v
	ierr = VecSetSizes(v,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = VecSetFromOptions(v);CHKERRQ(ierr);


	/* ------- Construct Analytical solution ------------ */
	Vec p_a, u_a, v_a;
	// Set up vectors over processors
	ierr = VecCreate(PETSC_COMM_WORLD,&v_a);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&p_a);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,&u_a);CHKERRQ(ierr);
	// Load analytic solutions from binary files generated in Matlab
	ierr = LoadAnalyticSolutionsFromMatlab(Nx,  Ny, Nt, p_a, u_a, v_a, inputDir);

	/* ------------------ Set up KSP ----------------------- */
	KSP ksp;
	PC pc;

	MPI_Barrier(PETSC_COMM_WORLD);
	if (localRank == 0) timerKSPSetUp = MPI_Wtime(); //Start clock for KSP set up
  	// Set up the Krylov subspace so you can use the different solvers:
	ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);

	// Set KSPType for direct solver, using direct LU from MUMPS
	if (solver == "direct"){
		ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
	}
	//set KSPType for iterative solver
	else if (solver == "iterative"){
		//Default is GMRES. Can be overwritten using command line option
		//ierr = KSPSetType(ksp,KSPGMRES);CHKERRQ(ierr);
		//PetscInt restart = 10;
		//ierr =  KSPGMRESSetRestart(ksp, restart);
		ierr = KSPSetType(ksp,KSPRICHARDSON);CHKERRQ(ierr);
		ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);CHKERRQ(ierr);
		
	}

	ierr = KSPSetOperators(ksp,M_u,M_u);CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE);CHKERRQ(ierr);
	ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);

	// Set KSPPC for direct solver, using direct LU from MUMPS
	if (solver == "direct"){
		ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
		ierr = PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS);CHKERRQ(ierr);
		ierr = PCFactorSetUpMatSolverPackage(pc);CHKERRQ(ierr);
		/* for Cholesky factorization from MUMPS
		ierr = PCSetType(pc,PCCHOLESKY);CHKERRQ(ierr);
  		*/
	}
	// Set KSPPC for iterative solver
	else if (solver == "iterative"){ 
		//Default is PCASM. Can be overwritten using command line option
		//ierr = PCSetType(pc,PCASM);CHKERRQ(ierr);
		ierr = PCSetType(pc,PCHYPRE);CHKERRQ(ierr);
    	ierr = PCHYPRESetType(pc,"boomeramg");CHKERRQ(ierr);
    	ierr = PCFactorSetLevels(pc,4);CHKERRQ(ierr);
	}

	// finish setting up KSP context using options defined above
	ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  	// perform computation of preconditioners now, rather than on first use
	ierr = KSPSetUp(ksp);CHKERRQ(ierr);

	MPI_Barrier(PETSC_COMM_WORLD);
   	   if (localRank == 0)
   	    {	timerKSPSetUp = MPI_Wtime()-timerKSPSetUp; //Stop clock for KSP set uP
   	   		timerTS = MPI_Wtime(); //Start clock for time stepping
   	   	}
  	/* --------------- Time integration -------------------- */
	for (i = 0; i<Nt-1; i++){

  		/* Time step fluid diffusion equation to t+dt. */ 
   	   	//Extract boundary data vector b_f = B_f(:,i)
		ierr = MatGetColumnVector(B_f,b_f,i);CHKERRQ(ierr);
		//b_f = b_f + Q
		ierr = FluidDiffusion(p_p,w,F_p,F_u,Q,b_f,f);
		//Forward Euler update
		ierr = VecAXPY(p_p,dt,f);CHKERRQ(ierr);

		/* Solve mechanical equilibrium equation at t+dt. */
		//Extract boundary data vector b_m = mB_m(:,i+1)
		ierr = MatGetColumnVector(mB_m,b_m,i+1);CHKERRQ(ierr);
		//Solve the mechanical equilibrium equation
		ierr = MechanicalEquilibrium(p_p, w, mM_p, b_m, ksp);

	}
	MPI_Barrier(PETSC_COMM_WORLD);
	if (localRank == 0) timerTS = MPI_Wtime()-timerTS;
	
	/* ------------------ Results  ----------------------- */
	//Compute pore pressure p = p_p - P*w
	ierr =  MatScale(p2p_p,-1);CHKERRQ(ierr);
	ierr = MatMultAdd(p2p_p,w,p_p,p);CHKERRQ(ierr);

	/* Extract u and v from w */
	PetscInt iLo, iHi, iEnd;
	PetscScalar val;
	ierr = VecGetOwnershipRange(w,&iLo,&iHi);
	
	//Check if proc should write to u.
	if (iLo<(Nx*Ny-1)){	
		iEnd = iHi;
		//Check if proc also should write to v. If so, adjust bounds for u
		if (Nx*Ny<iHi) iEnd = Nx*Ny;
		//Write to u
		for (i=iLo;i<iEnd;i++){
    		ierr = VecGetValues(w,1,&i,&val);CHKERRQ(ierr);
    		ierr = VecSetValue(u,i,val,INSERT_VALUES);CHKERRQ(ierr);
    	}
    	//Check if proc should write to v. If so, write to v
  		if (Nx*Ny<iHi){
  			for (i=Nx*Ny;i<iHi;i++){

  				ierr = VecGetValues(w,1,&i,&val);CHKERRQ(ierr);
  				ierr = VecSetValue(v,i-Nx*Ny,val,INSERT_VALUES);CHKERRQ(ierr);
  			}
  		}	
	}
	else{ //Proc should write to v.
	
		//Write to v.
		for (i=iLo;i<iHi;i++){	

    		ierr = VecGetValues(w,1,&i,&val);CHKERRQ(ierr);
    		ierr = VecSetValue(v,i-Nx*Ny,val,INSERT_VALUES);CHKERRQ(ierr);
    	}
    	
	}
	//Assemble u and v.
	ierr = VecAssemblyBegin(u);CHKERRQ(ierr);
  	ierr = VecAssemblyEnd(u);CHKERRQ(ierr);
  	ierr = VecAssemblyBegin(v);CHKERRQ(ierr);
  	ierr = VecAssemblyEnd(v);CHKERRQ(ierr);

	/* ----------------- Error calculation --------------------- */
	Vec e_p, e_u, e_v;
	//Set up error vectors
	ierr = VecCreate(PETSC_COMM_WORLD,&e_p);CHKERRQ(ierr);
	ierr = VecSetSizes(e_p,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = VecSetFromOptions(e_p);CHKERRQ(ierr);

	ierr = VecCreate(PETSC_COMM_WORLD,&e_u);CHKERRQ(ierr);
	ierr = VecSetSizes(e_u,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = VecSetFromOptions(e_u);CHKERRQ(ierr);

	ierr = VecCreate(PETSC_COMM_WORLD,&e_v);CHKERRQ(ierr);
	ierr = VecSetSizes(e_v,PETSC_DECIDE,Nx*Ny);CHKERRQ(ierr);
	ierr = VecSetFromOptions(e_v);CHKERRQ(ierr);

	//Calculate pointwise difference of numerical and analytical solution
	ierr =  VecWAXPY(e_p,-1,p,p_a);CHKERRQ(ierr);
	ierr =  VecWAXPY(e_u,-1,u,u_a);CHKERRQ(ierr);
	ierr =  VecWAXPY(e_v,-1,v,v_a);CHKERRQ(ierr);
	
	//Calculate max norm of difference.
	PetscReal norm_u, norm_v, norm_p;
	ierr =  VecNorm(e_p,NORM_2,&norm_p);CHKERRQ(ierr);
	norm_p = norm_p*sqrt(dy*dx);
	ierr =  VecNorm(e_u,NORM_2,&norm_u);CHKERRQ(ierr);
	norm_u = norm_u*sqrt(dy*dx);
	ierr =  VecNorm(e_v,NORM_2,&norm_v);CHKERRQ(ierr);
	norm_v = norm_v*sqrt(dy*dx);


	/* Print results */
   	PetscPrintf(PETSC_COMM_WORLD,"Results using %s solver running with %d processes:\n",solver.c_str(), totalSize);
   	PetscPrintf(PETSC_COMM_WORLD,"Required time for setting up KSP: %f\n",timerKSPSetUp);
   	PetscPrintf(PETSC_COMM_WORLD,"Required time for time stepping: %f\n",timerTS);
   	PetscPrintf(PETSC_COMM_WORLD,"L2 error p: %f\n",norm_p);
   	PetscPrintf(PETSC_COMM_WORLD,"L2 error u: %f\n",norm_u);
   	PetscPrintf(PETSC_COMM_WORLD,"L2 error v: %f\n",norm_v);


	
    //Write result to file
    ierr = WriteVec(p,"../GenerateMatrices/DataStructs/p");CHKERRQ(ierr);
	ierr = WriteVec(u,"../GenerateMatrices/DataStructs/u");CHKERRQ(ierr);
	ierr = WriteVec(v,"../GenerateMatrices/DataStructs/v");CHKERRQ(ierr);


 	/* --------------- Clean up memory -------------------- */

	//Ksp
 	ierr = KSPDestroy(&ksp);CHKERRQ(ierr);

 	//Domain
 	ierr = VecDestroy(&x);CHKERRQ(ierr);
 	ierr = VecDestroy(&y);CHKERRQ(ierr);
 	ierr = VecDestroy(&t);CHKERRQ(ierr);

 	//Fluid diffusion
 	ierr = VecDestroy(&p_p);CHKERRQ(ierr);
 	ierr = VecDestroy(&p);CHKERRQ(ierr);
 	ierr = VecDestroy(&p_a);CHKERRQ(ierr);
 	ierr = VecDestroy(&b_f);CHKERRQ(ierr);
 	ierr = VecDestroy(&Q);CHKERRQ(ierr);
 	ierr = MatDestroy(&B_f);CHKERRQ(ierr);
 	ierr = MatDestroy(&F_p);CHKERRQ(ierr);
 	ierr = MatDestroy(&F_u);CHKERRQ(ierr);
 	ierr = MatDestroy(&p2p_p);CHKERRQ(ierr);

 	//Mechanical equilibrium
 	ierr = VecDestroy(&w);CHKERRQ(ierr);
 	ierr = VecDestroy(&u);CHKERRQ(ierr);
 	ierr = VecDestroy(&v);CHKERRQ(ierr);
 	ierr = VecDestroy(&u_a);CHKERRQ(ierr);
 	ierr = VecDestroy(&v_a);CHKERRQ(ierr);
 	ierr = VecDestroy(&b_m);CHKERRQ(ierr);
 	ierr = MatDestroy(&mB_m);CHKERRQ(ierr);
 	ierr = MatDestroy(&mM_p);CHKERRQ(ierr);
 	ierr = MatDestroy(&M_u);CHKERRQ(ierr);

 	//Destroy error vectors
 	ierr = VecDestroy(&e_u);CHKERRQ(ierr);
 	ierr = VecDestroy(&e_v);CHKERRQ(ierr);
 	ierr = VecDestroy(&e_p);CHKERRQ(ierr);

 	PetscFinalize();
 	return ierr;
 }

  /* 
  // for Cholesky factorization from MUMPS
  ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,sbp._A,sbp._A);CHKERRQ(ierr);
  ierr = KPSSetReusePreconditioner(ksp,PETSC_TRUE);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  PCSetType(pc,PCCHOLESKY);
  PCFactorSetMatSolverPackage(pc,MATSOLVERMUMPS);
  PCFactorSetUpMatSolverPackage(pc);
  */

   /*
  // for conjugate gradient (requires your matrix to be symmetric)
  ierr = KSPSetType(ksp,KSPCG);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,_A,_A);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  PCSetType(pc,PCGAMG); // this seems to be the suggested default option
  */
