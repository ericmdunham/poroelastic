%% Simulation of the line source problem 
% Script that simulates the line source problem with the SBP-SAT technique.
% The script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and results can be presented in different ways using
% different helper plotting functions. With a coordinate transform.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/LineSource' ...
    '../Helpers/ImplicitDiscretization'

%% Define type of SBP operators
SBPStencil = 'n';	% Type of SBP-stencil. w - wide, n - narrow, r - russian
Order = 2;        	% order of SBP-operator.
DirichletOrder = 2; % Order of accuracy of the Dirichlet conditions
SaveMovie = 0;      % If a movie should be saved

% Dirac Delta parameters
ooa = 2;    % Order of accuracy
sc = 1;     % Smoothness conditions

% Fluid Injection parameters
q = 1;      % Rate of injection
rho = 1;    % Density of the injected fluid

%% Material parameters
a = 1;      % 10^3 m
b = 1;      % 10^3 m
G = 1;      % 10^9 Pa
kappa = 1;  % 10^-9 m^2/(Pa s)

% Dimensionless parameters
B = 0.8; 
nu = 0.2;
nu_u = 0.4;

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G, kappa, B, nu, nu_u);

%% Spatial discretization
xL = -a;                    % West boundary
xR = a;                     % East boundary
yL = -a;                    % South Boundary
yR = b;                     % North Boundary
% Use even points in line source
Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dy = (yR - yL)/(Ny - 1);    % Spatial step size, y-direction

% The numerical grid xi living on [-1, 1]
xi = linspace(-1, 1, Nx);
dxi = 2/(Nx - 1);

% Determine the coordinate transformation
beta = 1.2;
x = a*tanh(beta*xi)/tanh(beta);

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid 
% discharge velocity and traction) always have weight one, since that is 
% required for stability. The weight for the Dirichlet conditions 
% (pore pressure and displacement) can be specified as an arbitrary 
% positive number however. A Dirichlet condition can therefore be 
% approximated using Robin boundary conditions using a large 
% weight. A large number will give a more accurate solution, 
% but will increase stiffness of the system. Here follows the structure of 
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

% West boundary
% u = u_a, v = v_a, p = p_a
%
% East boundary
% u = u_a, v = v_a, p = p_a
%
% South boundary
% u = u_a, v = v_a, p = p_a
%
% North boundary
% u = u_a, v = v_a, p = p_a

BC = [1 1 1 1;
      1 1 1 1;
      1 1 1 1];

% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Robin conditions are set according to Weights.
BC = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC);

% Build the matrices used for the discretization of the system
[M, A, M_b, F_b] = SpatialOperators(Nx, dxi, Ny, dy, alpha*ones(Ny,Nx), ...
    K*ones(Ny,Nx), G*ones(Ny,Nx), kappa*ones(Ny,Nx), M*ones(Ny,Nx), ...
    SBPStencil, BC, Order, x', 'x');

%% Temporal discritization
StartTime = 0.01;
EndTime = 0.1;
Nt = 100;
Nts = 4*Nt - 3;
t = linspace(StartTime, EndTime, Nt);
t_stag = linspace(StartTime, EndTime, Nts);
dt = t_stag(2) - t_stag(1);

%% Create forcing function
addpath '../Helpers/OperatorsNarrow'
mats = SBP(Nx, dxi, 2);
J = diag(mats.D1int*x');
Jinv = inv(J);
Q = [sparse(2*Ny*Nx, 1); q*PointSource(Nx, x(Nx/2 + 1) - x(Nx/2), Ny, dy, ooa, sc, 0, 0)];
rmpath '../Helpers/OperatorsNarrow'

%% Compute analytical solutions
[u_a, v_a, p_a] = AnalyticalSolutions(...
    x, y, t_stag, q, rho, G, lambda, kappa, c, alpha);

%% Boundary data
% Data is specified component-wise for all times in a M-Nt matrix, where M
% is the number of grid points at the corresponding boundary. Boundary data 
% for Dirichelet condition need to be scaled by the corresponding BC.

% Boundary data for mechanical equilibrium equations.
gWu = squeeze(BC(1,1)*u_a(:, 1, :));
gWv = squeeze(BC(2,1)*v_a(:, 1, :));
gW = [gWu; gWv];

gEu = squeeze(BC(1,2)*u_a(:, Nx, :));
gEv = squeeze(BC(2,2)*v_a(:, Nx, :));
gE = [gEu; gEv];

gSu = squeeze(BC(1,3)*u_a(1, :, :));
gSv = squeeze(BC(2,3)*v_a(1, :, :));
gS = [gSu; gSv];

gNu = squeeze(BC(1,4)*u_a(Ny, :, :));
gNv = squeeze(BC(2,4)*v_a(Ny, :, :));
gN = [gNu; gNv];

% Boundary data for fluid diffusion equation
fW = squeeze(BC(3,1)*p_a(:, 1, :));
fE = squeeze(BC(3,2)*p_a(:, Nx, :));
fS = squeeze(BC(3,3)*p_a(1, :, :));
fN = squeeze(BC(3,4)*p_a(Ny, :, :));

% Build the boundary data matrix
B = [M_b.W*gW + M_b.E*gE + M_b.S*gS + M_b.N*gN;
    F_b.W*fW + F_b.E*fE + F_b.S*fS + F_b.N*fN];

% Impose analytical solutions at t = startTime as initial conditions.
V = [reshape(u_a(:, :, 1), Ny*Nx, 1);
    reshape(v_a(:, :, 1), Ny*Nx, 1);
    reshape(p_a(:, :, 1), Ny*Nx, 1)];

for i = 1 : Nt - 1
    % Perform an BDF4 time step
    V1 = (M - dt*A)\(M*V + dt*(B(:, 4*i - 2) + Q));
    V2 = (M - 2*dt/3*A)\(M*(4/3*V1 - 1/3*V) + 2*dt/3*(B(:, 4*i - 1) + Q));
    V3 = (M - 6*dt/11*A)\(M*(18/11*V2 - 9/11*V1 + 2/11*V) + 6*dt/11*(B(:, 4*i) + Q));
    V = (M - 12*dt/25*A)\(M*(48/25*V3 - 36/25*V2 + 16/25*V1 - 3/25*V) + 12*dt/25*(B(:, 4*i + 1) + Q));
    
    % Collect variable fields
    u = reshape(V(1:Ny*Nx), Ny, Nx);
    v = reshape(V(Ny*Nx + 1:2*Ny*Nx), Ny, Nx);
    p = reshape(V(2*Ny*Nx + 1:3*Ny*Nx), Ny, Nx);
    
    Plot1D(x, y, u, v, p, u_a(:, :, 4*i + 1), v_a(:, :, 4*i + 1), p_a(:, :, 4*i + 1));
end

% Save movie
if SaveMovie
    video = VideoWriter('simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/LineSource' ...
    '../Helpers/ImplicitDiscretization'