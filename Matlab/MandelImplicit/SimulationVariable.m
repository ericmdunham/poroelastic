%% Simulation of Mandel-similar problem with variable coefficients
% Script that simulates Mandel's problem with the SBP-SAT technique. The
% script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and plots in 1D or 2D can be presented by using different
% help functions.
%
% This code uses variable coefficients to demonstrate that the SBP-SAT
% discretization can handle that. Note that the problem is unrealistic
% though and that the analytical solution set as a boundary condition is
% not valid for spatially variable coefficients. This should therefore not
% be seen as a simulation of Mandel's problem, but rather as a simulation
% of a problem similar to Mandel's problem.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/Mandel' ...
    '../Helpers/ImplicitDiscretization'

%% Setup

% Define type of SBP operators
SBPStencil = 'n';	% Type of SBP-stencil. w - wide, n - narrow, r - russian
order = 2;        	% order of SBP-operator.
DirichletOrder = 1; % Order of accuracy of the Dirichlet condition
saveMovie = 0;      % If a movie should be saved

% Material parameters

% Parameters in SI units.
a_SI = 0.1;         % Length in x-direction
b_SI = 0.1;         % Length in y-direction
G_SI = 30e9;        % Shear modulus
kappa_SI = 1e-16;   % k/my, k-permiability, my fluid viscoscity
F_SI = 2e6;         % Compressive force

% Parameters in dimensionless scales
a = 1;                  % Length in x-direction
b = 1;                  % Length in y-direction
G = 1;                  % Shear modulus
kappa = 1;              % k/my, k-permiability, my fluid viscoscity
F = 1;                  % Compressive force

% Scales for going from unitless to SI units
x_SC = a_SI;
y_SC = b_SI;
u_SC = F_SI/G_SI;
v_SC = F_SI*b_SI/(G_SI*a_SI);
p_SC = F_SI/a_SI;
t_SC = a_SI^2/(kappa_SI*G_SI);

% Dimensionless parameters
B = 0.8;                % Skemptons paramater
nu = 0.2;               % Poissions ratio (drained)
nu_u = 0.4;             % Poissons ratio (undrained)

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G,kappa,B,nu,nu_u);

%% Spatial discretization

Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
xL = 0;                     % West boundary
xR = a;                     % East boundary
yL = 0;                     % South Boundary
yR = b;                     % North Boundary
x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction
dy = (yR - yL)/(Ny - 1);    % Spatial step size, y-direction

%% Temporal discritization

startTime = 0.01*a^2/c;                 % Start of simulation
endTime = 2*a^2/c;                      % End of simulation
Nt = 100;                               % Temporal resolution
t = linspace(startTime, endTime, Nt);   % Temporal grid
dt = (endTime - startTime)/(Nt - 1);    % Temporal step size

%% Initial condition
% Impose analytical solutions at t=startTime as initial conditions.

%Compute analytical solutions
r_max = 50; % Determines quality of analytic solution. 1 < r_max < Inf
[u_a, v_a, p_a] = ...
    AnalyticalSolutions(x, y, t, G, F, c, nu, nu_u, B, r_max);

v_init = [reshape(ones(Ny, 1)*u_a(:, 1)', Nx*Ny, 1);...
    reshape(v_a(:, 1)*ones(1, Nx), Nx*Ny, 1);...
    reshape(ones(Ny, 1)*p_a(:, 1)', Nx*Ny, 1)];

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid 
% discharge velocity and traction) always have weight one, since that is 
% required for stability. The weight for the Dirichlet conditions 
% (pore pressure and displacement) can be specified as an arbitrary 
% positive number however. A Dirichlet condition can therefore be 
% approximated using Robin boundary conditions using a large 
% weight. A large number will give a more accurate solution, 
% but will increase stiffness of the system. Here follows the structure of 
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

% West boundary
% u = 0, sigma_yx = 0, q_x = 0
%
% East boundary
% sigma_xx = 0, sigma_yx = 0, p = 0
%
% South boundary
% sigma_xy = 0, v = 0, q_y = 0
%
% North boundary
% sigma_xy = 0, v = v_a, q_y = 0

BC = [1 0 0 0;
      0 0 1 1;
      0 1 0 0];

% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Dirichlet conditions are imposed with the accuracy
% specified by DirichletOrder.
BC = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC);

% Temporal discritization
startTime = 1e-3;
endTime = startTime + 1e1;
Nt = 20;
Nts = 4*Nt - 3;
t = linspace(startTime, endTime, Nt);
t_stag = linspace(startTime, endTime, Nts);
dt = t_stag(2) - t_stag(1);

% Compute analytical solutions
r_max = 100; % Determines quality of analytic solution. 1 < r_max < Inf
[u_a, v_a, p_a, sigma_yy_a] = AnalyticalSolutions(x, y, t_stag, G, F, ...
    c, nu, nu_u, B, r_max);

% Here the variable coefficient matrices are built.
alpha = alpha*(0.1 + 0.3*sin(pi/(2*a)*x)'*cos(pi/(2*b)*y));
K = K*(1 + 0.5*cos(pi/(2*a)*x)'*cos(pi/(2*b)*y));
G = G*(0.1 + sin(pi/(2*a)*x)'*sin(pi/(2*b)*y));
kappa = 0.01*kappa*(1 + 0.5*sin(pi/(2*a)*x)'*cos(pi/(2*b)*y));
M = M*(0.2 + 0.8*cos(pi/(2*a)*x)'*cos(pi/(2*b)*y));

% Build the matrices used for the discretization of the system
[M, A, M_b, F_b, Sigma_xx, Sigma_xy, Sigma_yy] = SpatialOperators(...
    Nx, dx, Ny, dy, alpha, K, G, kappa, M, SBPStencil, BC, order);

%% Boundary data
% Data is specified component-wise for all times in a M-Nt matrix, where M
% is the number of grid points at the corresponding boundary.  

% Boundary data at north boundary
gNu = spalloc(Nx, Nts, 0);
gNv = ones(Nx, 1)*v_a(Ny, :);
fN = spalloc(Nx, Nts, 0);

% Build the boundary data matrix. Boundary data for Dirichelet condition 
% need to be scaled by the corresponding BC.
B = [M_b.N*[gNu; BC(2,4)*gNv]; 
    F_b.N*fN*BC(3,4)];

% Impose analytical solutions at t = startTime as initial conditions.
V = [reshape(ones(Ny, 1)*u_a(:, 1)', Ny*Nx, 1); ...
    reshape(v_a(:, 1)*ones(1, Nx), Ny*Nx, 1); ...
    reshape(ones(Ny, 1)*p_a(:, 1)', Ny*Nx, 1)];

for i = 1 : Nt-1
   % Perform an BDF4 time step
    V1 = (M - dt*A)\(M*V + dt*B(:, 4*i - 2));
    V2 = (M - 2*dt/3*A)\(M*(4/3*V1 - 1/3*V) + 2*dt/3*B(:, 4*i - 1));
    V3 = (M - 6*dt/11*A)\(M*(18/11*V2 - 9/11*V1 + 2/11*V) + 6*dt/11*B(:, 4*i));
    V = (M - 12*dt/25*A)\(M*(48/25*V3 - 36/25*V2 + 16/25*V1 - 3/25*V) + 12*dt/25*B(:, 4*i + 1));
    
    % Collect variable fields
    u = reshape(V(1:Ny*Nx), Ny, Nx);
    v = reshape(V(Ny*Nx + 1:2*Ny*Nx), Ny, Nx);
    p = reshape(V(2*Ny*Nx + 1:3*Ny*Nx), Ny, Nx);
    sigma_xx = reshape(Sigma_xx*V, Ny, Nx);
    sigma_xy = reshape(Sigma_xy*V, Ny, Nx);
    sigma_yy = reshape(Sigma_yy*V, Ny, Nx);
    
    % Use a plotting function
    Plot2D(x, y, t(i), u, v, p, sigma_xx, sigma_xy, sigma_yy);
%     PlotHeatmap(x*x_SC, y*y_SC, t(i)*t_SC, u*u_SC, v*v_SC, p*p_SC);
    
    if saveMovie
        %Give time to resize the window
        if i == 1
            pause(3);
        end
        % Save frame to save as movie
        frames(i) = getframe(gcf);
    end
end

% Save movie
if saveMovie
    video = VideoWriter('simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/Mandel' ...
    '../Helpers/ImplicitDiscretization'
