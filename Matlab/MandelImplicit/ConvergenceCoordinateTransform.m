%% Convergence study of Mandel's problem
% Script that calculates and presents convergence rates of Mandel's problem
% using the SBP-SAT technique with either wide operators, narrow operators
% or the Russian operators (indicated by a flag) to discretize the problem.
% The problem is simulated in 2D and then plots of the convergence rates
% are presented.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
% Add paths for helper functions
addpath '../Helpers/Mandel' '../Helpers' '../Helpers/ImplicitDiscretization'

% Define type of SBP operators
SBPStencil = 'n';       % Type of SBP-stencil. w - wide, n - narrow, r - Russian
Order = 4;              % order of SBP-operator.
DirichletOrder = 3;     % Order of accuracy of the Dirichlet condition
RefOrd = [2, 3];        % Comparison lines in the solution

% Temporal discretization
StartTime = 1e-3;
EndTime = StartTime + 1e-6;
TimeScaleFactor = 1;

% Spatial resolution and variables for storing the errors.
Res = [10, 20, 30, 40, 50, 60];
NormsU = zeros(1, length(Res));
NormsV = zeros(1, length(Res));
NormsP = zeros(1, length(Res));
NormsSigmaXX = zeros(1, length(Res));
NormsSigmaXY = zeros(1, length(Res));
NormsSigmaYY = zeros(1, length(Res));

%% Compute errors for each resolution
for l = 1:length(Res)
    %% Material parameters
    % a, b, G, kappa and F are dimensionless since we want to measure the
    % errors dimensionlessly, and we can always convert from dimensionless
    % units to SI units afterwards. Therefore a, b, G, kappa and F is never
    % stated in SI units for the convergence study.
    
    % Parameters in dimensionless scales
    a = 1;                  % Length in x-direction
    b = 1;                  % Length in y-direction
    G = 1;                  % Shear modulus
    kappa = 1;              % k/my, k - permeability, my - viscosity
    F = 1;                  % Compressive force
    
    % Dimensionless parameters
    B = 0.8;                % Skempton's parameter
    nu = 0.2;               % Poisson's ratio (drained)
    nu_u = 0.4;             % Poisson's ratio (undrained)
    
    % Compute other poroelastic material parameters;
    [K, alpha, M, c, lambda] = MaterialParameters(G,kappa,B,nu,nu_u);
    
    %% Spatial discretization
    xL = 0;                     % West boundary
    xR = a;                     % East boundary
    yL = 0;                     % South Boundary
    yR = b;                     % North Boundary
    Nx = Res(l);                % Resolution in x-direction
    Ny = Res(l);                % Resolution in y-direction
    y = linspace(yL, yR, Ny);   % Spatial grid, x-direction
    dy = (yR - yL)/(Ny - 1);    % Spatial step size, x-direction
    
    % The numerical grid xi living on [0, 1]
    xi = linspace(0, 1, Ny);
    dxi = 1/(Ny - 1);
    
    % Determine the coordinate transformation
    hmaxmin = 10;    % Determines h_max/h_min
    xf = @(beta) (tanh(beta*xi(end)) - tanh(beta*xi(end - 1)))/...
        (tanh(beta*xi(2)) - tanh(beta*xi(1))) - 1/hmaxmin;
    beta = fsolve(xf, 2, optimset('Display','off'));
    x = b*tanh(beta*(xi - 1))/tanh(beta) + b;
    
    %% Boundary conditions and boundary data
    %
    % The boundary conditions are specified as Robin boundary conditions in a
    % 3x4 matrix. The weight on the Neumann type boundary conditions (fluid
    % discharge velocity and traction) always have weight one, since that is
    % required for stability. The weight for the Dirichlet conditions
    % (pore pressure and displacement) can be specified as an arbitrary
    % positive number however. A Dirichlet condition can therefore be
    % approximated using Robin boundary conditions using a large
    % weight. A large number will give a more accurate solution,
    % but will increase stiffness of the system. Here follows the structure of
    % the matrix:
    %
    %          W E S N
    %          - - - -
    %       u |       |
    %  BC = v | | | | |
    %       p |       |
    %          - - - -
    %
    % Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
    %                            boundary
    %
    
    % West boundary
    % u = 0, sigma_yx = 0, q_x = 0
    %
    % East boundary
    % sigma_xx = 0, sigma_yx = 0, p = 0
    %
    % South boundary
    % sigma_xy = 0, v = 0, q_y = 0
    %
    % North boundary
    % sigma_xy = 0, v = v_a, q_y = 0
    
    BC = [1 0 0 0;
          0 0 1 1;
          0 1 0 0];
    
    % Scale the boundary conditions based on the material properties and grid
    % spacing, so that the Dirichlet conditions are imposed with the accuracy
    % specified by DirichletOrder.
    BC = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC);
    
    % Build the matrices used for the discretization of the system
    [M, A, M_b, F_b, Sigma_xx, Sigma_xy, Sigma_yy] = SpatialOperators(...
        Nx, dxi, Ny, dy, alpha*ones(Ny,Nx), K*ones(Ny,Nx), G*ones(Ny,Nx), ...
        kappa*ones(Ny,Nx), M*ones(Ny,Nx), SBPStencil, BC, Order, x', 'x');
    
    % Temporal discritization
    Nt = TimeScaleFactor*Nx;
    Nts = 4*Nt - 3;
    t = linspace(StartTime, EndTime, Nt);
    t_stag = linspace(StartTime, EndTime, Nts);
    dt = t_stag(2) - t_stag(1);
    
    % Compute analytical solutions
    r_max = 100; % Determines quality of analytic solution. 1 < r_max < Inf
    [u_a, v_a, p_a, sigma_yy_a] = AnalyticalSolutions(...
        x, y, t_stag, G, F, c, nu, nu_u, B, r_max);
    
    % Boundary data
    % Data is specified component-wise for all times in a M x Nt matrix, where M
    % is the number of grid points at the corresponding boundary.
    
    % Boundary data at north boundary
    gNu = spalloc(Nx, Nts, 0);
    gNv = ones(Nx, 1)*v_a(Ny, :);
    fN = spalloc(Nx, Nts, 0);
    
    % Build the boundary data matrix. Boundary data for Dirichlet condition
    % need to be scaled by the corresponding BC.
    B = [M_b.N*[gNu; BC(2,4)*gNv];
        F_b.N*fN*BC(3,4)];
    
    % Impose analytical solutions at t = startTime as initial conditions.
    V = [reshape(ones(Ny, 1)*u_a(:, 1)', Ny*Nx, 1);...
        reshape(v_a(:, 1)*ones(1, Nx), Ny*Nx, 1);...
        reshape(ones(Ny, 1)*p_a(:, 1)', Ny*Nx, 1)];
    
    for i = 1 : Nt - 1
        % Perform an BDF4 time step
        V1 = (M - dt*A)\(M*V + dt*B(:, 4*i - 2));
        V2 = (M - dt*2/3*A)\(M*(4/3*V1 - 1/3*V) + dt*2/3*B(:, 4*i - 1));
        V3 = (M - dt*6/11*A)\(M*(18/11*V2 - 9/11*V1 + 2/11*V) + dt*6/11*B(:, 4*i));
        V = (M - dt*12/25*A)\(M*(48/25*V3 - 36/25*V2 + 16/25*V1 - 3/25*V) + dt*12/25*B(:, 4*i + 1));
    end
    
    % Collect quantities from V and save the norm of the error.
    u = reshape(V(1:Ny*Nx), Ny, Nx);
    v = reshape(V(Ny*Nx + 1:2*Ny*Nx), Ny, Nx);
    p = reshape(V(2*Ny*Nx + 1:3*Ny*Nx), Ny, Nx);
    SigmaXX = reshape(Sigma_xx*V, Ny, Nx);
    SigmaXY = reshape(Sigma_xy*V, Ny, Nx);
    SigmaYY = reshape(Sigma_yy*V, Ny, Nx);
    
    uRef = ones(Ny, 1)*u_a(:, end)';
    vRef = v_a(:, end)*ones(1, Nx);
    pRef = ones(Ny, 1)*p_a(:, end)';
    SigmaYYRef = ones(Ny, 1)*sigma_yy_a(:, end)';
    
    uErr = reshape(u - uRef, Ny*Nx, 1);
    vErr = reshape(v - vRef, Ny*Nx, 1);
    pErr = reshape(p - pRef, Ny*Nx, 1);
    SigmaXXErr = reshape(SigmaXX, Ny*Nx, 1);
    SigmaXYErr = reshape(SigmaXY, Ny*Nx, 1);
    SigmaYYErr = reshape(SigmaYY - SigmaYYRef, Ny*Nx, 1);
    
    % Obtain the H and J matrix that are used for the error
    if SBPStencil == 'r'
        addpath '../Helpers/OperatorsRussian'
        [Dp, D, HI] = SBP(Ny, dxi, Order);
        rmpath '../Helpers/OperatorsRussian'
    else
        addpath '../Helpers/OperatorsWide'
        [D, HI] = SBP(Ny - 1, dxi, Order);
        rmpath '../Helpers/OperatorsWide'
    end
    H = inv(HI);
    J = spdiags(D*y', 0, Ny, Ny);
    J = kron(speye(Nx, Nx), J);
    H = kron(H, H);
    
    % Calculate the norms for the different fields
    NormsU(l) = sqrt(uErr'*J*H*uErr);
    NormsV(l) = sqrt(vErr'*J*H*vErr);
    NormsP(l) = sqrt(pErr'*J*H*pErr);
    NormsSigmaXX(l) = sqrt(SigmaXXErr'*J*H*SigmaXXErr);
    NormsSigmaXY(l) = sqrt(SigmaXYErr'*J*H*SigmaXYErr);
    NormsSigmaYY(l) = sqrt(SigmaYYErr'*J*H*SigmaYYErr);
    
    l % Print out the iteration number
end

% Save convergence data
save('ConvergenceData/Data.mat', 'NormsP', 'NormsU', 'NormsV', ...
    'NormsSigmaXX', 'NormsSigmaXY', 'NormsSigmaYY', 'Res', ...
    'RefOrd', 'StartTime', 'EndTime', 'TimeScaleFactor', ...
    'Order', 'DirichletOrder', 'SBPStencil');

% Remove paths for helper functions
rmpath '../Helpers/Mandel' '../Helpers' '../Helpers/ImplicitDiscretization'
