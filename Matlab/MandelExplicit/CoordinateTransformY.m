%% Simulation of Mandel's problem using the explicit scheme
% Script that simulates Mandel's problem with the SBP-SAT technique. The
% script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and plots in 1D or 2D can be presented by using different
% help functions. With a coordinate transform.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/Mandel' ...
    '../Helpers/ExplicitDiscretization'

%% Setup

% Define type of SBP operators
SBPStencil = 'n';	% Type of SBP-stencil. w - wide, n - narrow, r - russian
Order = 2;        	% order of SBP-operator.
DirichletOrder = 2; % Order of accuracy of the Dirichlet conditions
saveMovie = 0;      % If a movie should be saved

%% Material parameters

% Parameters in SI units.
a_SI = 0.1;         % Length in x-direction
b_SI = 0.1;         % Length in y-direction
G_SI = 30e9;        % Shear modulus
kappa_SI = 1e-9;    % k/my, k-permiability, my fluid viscoscity
F_SI = 2e6;         % Compressive force

% Parameters in dimensionless scales
a = 1;                  % Length in x-direction
b = 1;                  % Length in y-direction
G = 1;                  % Shear modulus
kappa = 1;              % k/my, k-permiability, my fluid viscoscity
F = 1;                  % Compressive force

% Scales for going from unitless to SI units
x_SC = a_SI;
y_SC = b_SI;
u_SC = F_SI/G_SI;
v_SC = F_SI*b_SI/(G_SI*a_SI);
p_SC = F_SI/a_SI;
t_SC = a_SI^2/(kappa_SI*G_SI);

% Dimensionless parameters
B = 0.8;                % Skemptons paramater
nu = 0.2;               % Poissions ratio (drained)
nu_u = 0.4;             % Poissons ratio (undrained)

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G, kappa, B, nu, nu_u);

%% Spatial discretization
Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
xL = 0;                     % West boundary
xR = a;                     % East boundary
yL = 0;                     % South Boundary
yR = b;                     % North Boundary
x = linspace(xL, xR, Nx);   % Spatial grid, x-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction

% The numerical grid xi living on [0, 1]
xi = linspace(0, 1, Ny);
dxi = 1/(Ny - 1);

% Determine the coordinate transformation
hmaxmin = 20;    % Determines h_max/h_min
xf = @(beta) (tanh(beta*xi(end)) - tanh(beta*xi(end - 1)))/...
    (tanh(beta*xi(2)) - tanh(beta*xi(1))) - 1/hmaxmin;
beta = fsolve(xf, 2, optimset('Display','off'));
y = a*tanh(beta*xi)/tanh(beta);
dy = min(diff(y)); % Used for stability condition

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (discharge
% velocity for the fluid flow equation and traction for the mechanical
% equilibrium equation) always have weight one, since that is required for
% stability. The weight for the Dirichlet conditions can be specified
% as an arbitrary positive number however. A Dirichlet condition can
% therefore be approximated using Robin boundary conditions using a large
% number in the Matrix. A large number will give a more accurate solution,
% but will increase stiffness of the system. Here follows the structure of
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

% West boundary
% u = 0, sigma_xy = 0, q_x = 0
%
% East boundary
% sigma_xx = 0, sigma_xy = 0, p = 0
%
% South boundary
% sigma_xy = 0, v = 0, q_y = 0
%
% North boundary
% sigma_xy = 0, v = v_a(Ny), q_y = 0

BC = [1 0 0 0;
      0 0 1 1;
      0 1 0 0];
  
% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Dirichlet conditions are imposed with the accuracy
% specified by DirichletOrder.
BC = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC);
  
% Obtain the stability condition from the boundary conditions and the interior
C = 0.5; % A numeric constant that is dependent of the problem and discretization
dt = C*min(dx^2/(M*kappa), ...          % From interior/Neumann in x-direction
    min(dy^2/(M*kappa), ...             % From interior/Neumann in y-direction
    min(dx/(M*BC(3, 1)), ...            % From Dirichlet at west boundary
    min(dx/(M*BC(3, 2)), ...            % From Dirichlet at east boundary
    min(dy/(hmaxmin*M*BC(3, 3)), ...    % From Dirichlet at south boundary
    min(dy/(hmaxmin*M*BC(3, 4))))))));  % From Dirichlet at north boundary

%% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dxi, alpha*ones(Ny,Nx), K*ones(Ny,Nx), ...
    G*ones(Ny,Nx), kappa*ones(Ny,Nx), M*ones(Ny,Nx), SBPStencil, ...
    BC, Order, y', 'y');

%% Time integration system
Starttime = 1e-3;
Endtime = Starttime + 1e-4;
t = Starttime:dt:Endtime;
t_stag = Starttime:0.5*dt:t(end);

% Make sure that we end up at endtime
if t(end) ~= Endtime
    t_stag(end + 1) = (Endtime + t(end))/2;
    t_stag(end + 1) = Endtime;
    t(end + 1) = Endtime;
end
Nt = length(t);

%% Compute analytical solutions
r_max = 100; % Determines quality of analytic solution. 1 < r_max < Inf
[u_a, v_a, p_a, sigma_yy_a] = AnalyticalSolutions(x, y, t_stag, G, F, ...
    c, nu, nu_u, B, r_max);

%% Boundary data
% Data is specified component-wise for all times in a M-Nt matrix, where M
% is the number of grid points at the corresponding boundary.
gNu = spalloc(Nx, 2*Nt - 1, 0);
gNv = ones(Nx, 1)*v_a(Ny, :);

% Non-zero boundary data for Dirichelet condition need to be scaled by the
% corresponding BC.
BM = M_b.N*[gNu; BC(2, 4)*gNv];

% Impose analytical solutions at t = startTime as initial conditions.
w = [reshape(ones(Ny, 1)*u_a(:, 1)', Ny*Nx, 1);...
    reshape(v_a(:, 1)*ones(1, Nx), Ny*Nx, 1)];
p_p = reshape(ones(Ny, 1)*p_a(:, 1)', Ny*Nx, 1) + P*w;

for i = 1:Nt - 1
    % Find the time step for the current iteration
    dt = t(i + 1) - t(i);
    
    % Perform a explicit RK4 time step
    % 1:st stage
    k1 = F_p*p_p + F_u*w;
    
    % 2:nd stage
    p_p2 = p_p + 0.5*dt*k1;
    w2 = -M_u\(M_p*p_p2 + BM(:, 2*i));
    k2 = F_p*p_p2 + F_u*w2;
    
    % 3:rd stage
    p_p3 = p_p + 0.5*dt*k2;
    w3 = -M_u\(M_p*p_p3 + BM(:, 2*i));
    k3 = F_p*p_p2 + F_u*w2;
    
    % 4:th stage
    p_p4 = p_p + dt*k3;
    w4 = -M_u\(M_p*p_p4 + BM(:, 2*i + 1));
    k4 = F_p*p_p4 + F_u*w4;
    
    % Time  step in the fluid diffusion equation
    p_p = p_p + dt*(k1/6 + k2/3 + k3/3 + k4/6);
    
    % Solve the mechanical equilibrium equation for u and v.
    w = -M_u\(M_p*p_p + BM(:, 2*i + 1));
    
    % Collect variable fields
    p = reshape(p_p - P*w, Ny, Nx);
    u = reshape(w(1:Ny*Nx),Ny,Nx);
    v = reshape(w(Nx*Ny + 1:2*Ny*Nx),Ny,Nx);
    sigma_xx = reshape(Sigma_xx*w - alpha*p_p,Ny,Nx);
    sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
    sigma_yy = reshape(Sigma_yy*w - alpha*p_p, Ny, Nx);
    
%     Use a plotting function
    Plot2D(x, y, t(i), u, v, p, sigma_xx, sigma_xy, sigma_yy);
%     Plot2DS(x*x_SC, y*y_SC, t(i)*t_SC, u*u_SC, v*v_SC, p*p_SC, ...
%         sigma_xx*p_SC, sigma_xy*p_SC, sigma_yy*p_SC);
%     PlotHeatmap(x*x_SC, y*y_SC, t(i)*t_SC, u*u_SC, v*v_SC, p*p_SC);
%     Plot1DS(x, y, t(i + 1), u, v, p, sigma_xx, sigma_xy, sigma_yy, ...
%         u_a(:, 2*i + 1), v_a(:, 2*i + 1), p_a(:, 2*i + 1), ...
%         sigma_yy_a(:, 2*i + 1));
%     Plot1D(x*x_SC, y*y_SC, t(i)*t_SC, u*u_SC, v*v_SC, p*p_SC, ...
%         u_SC*u_a(:, i), v_SC*v_a(:, i), p_SC*p_a(:, i));
%     Plot1DP(x*x_SC, Ny, t(i)*t_SC, p*p_SC, p_SC*p_a(:, i));
    
    
    if saveMovie
        %Give time to resize the window
        if i == 1
            pause(3);
        end
        % Save frame to save as movie
        frames(i) = getframe(gcf);
    end
end

% Save movie
if saveMovie
    video = VideoWriter('simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/Mandel' ...
    '../Helpers/ExplicitDiscretization'