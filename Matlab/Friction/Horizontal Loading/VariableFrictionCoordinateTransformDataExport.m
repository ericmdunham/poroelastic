%% Simulation using the explicit scheme with a fault on the south boundary
% Script that simulates the poroelastic problem with the SBP-SAT technique.
% The script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and results can be presented in different ways using
% different helper plotting functions. Here the friction law
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../../Helpers' '../../Helpers/Friction' '../../Helpers/LineSource' ...
    '../../Helpers/ExplicitDiscretization'

%% Setup

% Define type of SBP operators
SBPStencil = 'n';      % Type of SBP-stencil. w - wide, n - narrow, r - russian
Order = 2;             % order of SBP-operator.
DirichletOrder = 2;    % Order of accuracy of the Dirichlet conditions

% Choose how often data is stored
FaultExportFrequency = 10;
DomainExportFrequency = 100;

% Material parameters in self consistent units
Lx = 2;         % m
Ly = 1;         % m
G = 30;         % GPa
kappa = 1e-2;   % 1e-9 m^2/(Pa s)

% Dimensionless material parameters
B = 0.6;
nu = 0.2;
nu_u = 0.33;

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G, kappa, B, nu, nu_u);
lambda_u = lambda + alpha^2*M;

% Present the hydraulic diffusivity
fprintf('c = %f m^2/s\n', c);

% Friction parameters in self consistent units
f_0 = 0.6;                  % 1
a = 0.015;                  % 1
b = 0.02;                   % 1

sigma_0 = 50;               % MPa
L = 1e-2;                   % mm
tau_0 = 0.95*f_0*sigma_0;   % MPa
V_0 = 1e-3;                 % mm/s
c_c = 3.4e3;                % m/s
eta = G/(2*c_c);            % GPa/(m/s)

% Scales that has to be resolved
h_f = G*L*pi/(sigma_0*(b - a));
t_f = h_f/c_c;
h_d = sqrt(kappa*M*t_f);

% Error tolerance for the horisontal displacement at the fault
tol = 1e-5;

%% Spatial discretization
xL = -Lx;                   % West boundary
xR = Lx;                    % East boundary
yL = -Ly;                   % South Boundary
yR = Ly;                    % North Boundary

Nx = 40;                    % Resolution in x-direction
Ny = 40;                    % Resolution in y-direction
x = linspace(xL, xR, Nx);   % Spatial grid, x-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction

% The numerical grid xi living on [0, 1]
xi = linspace(0, 1, Nx);
dxi = 1/(Nx - 1);

% Determine the coordinate transformation
hmaxmin = 40;    % Determines h_max/h_min
xf = @(beta) (tanh(beta*xi(end)) - tanh(beta*xi(end - 1)))/...
    (tanh(beta*xi(2)) - tanh(beta*xi(1))) - 1/hmaxmin;
beta = fsolve(xf, 2, optimset('Display','off'));
y = 2*Ly*tanh(beta*xi)/tanh(beta) - Ly;
y = -fliplr(y);
dy = min(diff(y));

% Unitless friction fields
a = a*ones(Nx, 1);
b = b*ones(Nx, 1);

% Slip rate at the north boundary
du_N = 1e5*V_0*ones(Nx, 1)/2;

% Check if the spatial scales are resolved
fprintf('h_f/dx = %f\n', h_f/dx);
fprintf('h_d/dy = %f\n', h_d/dy);

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid
% discharge velocity and traction) always have weight one, since that is
% required for stability. The weight for the Dirichlet conditions
% (pore pressure and displacement) can be specified as an arbitrary
% positive number however. A Dirichlet condition can therefore be
% approximated using Robin boundary conditions using a large
% weight. A large number will give a more accurate solution,
% but will increase stiffness of the system. Here follows the structure of
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

BC = [0 0 1 1;
      1 1 0 0;
      0 0 1 0];
  
% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Dirichlet conditions are imposed with the accuracy
% specified by DirichletOrder.
BC = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC);
  
% Obtain the stability condition from the boundary conditions and the interior
C = 1; % A numeric constant that is dependent of the problem and discretization
maxdt = C*min(dx^2/(M*kappa), ...  % From interior/Neumann in x-direction
    min(dy^2/(M*kappa), ...     % From interior/Neumann in y-direction
    min(dx/(M*BC(3, 1)), ...    % From Dirichlet at west boundary
    min(dx/(M*BC(3, 2)), ...    % From Dirichlet at east boundary
    min(dy/(M*BC(3, 3)), ...    % From Dirichlet at south boundary
    min(dy/(M*BC(3, 4))))))));  % From Dirichlet at north boundary

%% Construct the spatial operators

% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dxi, alpha*ones(Ny,Nx), K*ones(Ny,Nx), ...
    G*ones(Ny,Nx), kappa*ones(Ny,Nx), M*ones(Ny,Nx), SBPStencil, ...
    BC, Order, y', 'y');

%% Temporal discritization
StartTime = 0;   	% s
EndTime = 7e-3;   	% s
t(1) = StartTime;
dt = min(maxdt, t_f);

% Present the number of time steps per characteristic time scale for
% stability
fprintf('t_f/dt = %f\n', t_f/maxdt);

%
% --- Set up inital values ---
%

% Set initial conditions (at time t = 0) for p_p
p_p = zeros(Ny*Nx, 1);

% Set initial conditions for fields living on the fault
psi = ones(Nx, 1)*f_0 - 1e-1*f_0*gaussmf(x, [0.2*h_f, 0])';
u_S = zeros(Nx, 1);
u_N = zeros(Nx, 1);

% --- Set the boundary data in the mechanical equation ---

B_S = M_b.S*[BC(1,3)*u_S; zeros(Nx, 1)];
B_N = M_b.N*[BC(1,4)*u_N; zeros(Nx, 1)];

% --- Solve the mechanical equilibrium equation for u and v ---

w = -M_u\(M_p*p_p + (B_S + B_N));

%
% --- Plot inital values ---
%

% Calculate values of fields in whole domain
p = reshape(p_p - P*w, Ny, Nx);
u = reshape(w(1:Ny*Nx),Ny,Nx);
v = reshape(w(Nx*Ny + 1:2*Ny*Nx),Ny,Nx);
sigma_xx = reshape(Sigma_xx*w - alpha*p_p,Ny,Nx);
sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
sigma_yy = reshape(Sigma_yy*w - alpha*p_p, Ny, Nx);

% Find tau_qs and sigma' at the fault
tau_qs = sigma_xy(1, :)';
sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';

% Compute the slip velocity at each point
V = ComputeSlip(Nx, sigma_p, a, V_0, psi, tau_0, tau_qs, eta);

% Collect variables for plotting
i = 1;
tausig(i) = (tau_0 + tau_qs(1) - eta*V(1))/sigma_p(1);
V_P(:, i) = V;
fss(i) = f_0 - (b(1) - a(1))*log(V(1)/V_0);

% Save initial data living on the whole domain
p_data(:, :, 1) = p;
u_data(:, :, 1) = u;
v_data(:, :, 1) = v;
sigma_xx_data(:, :, 1) = sigma_xx;
sigma_xy_data(:, :, 1) = zeros(Ny, Nx, 1);
sigma_yy_data(:, :, 1) = zeros(Ny, Nx, 1);

% Save initial data living on the fault
u_S_data(:, 1) = u_S;
V_data(:, 1) = V;
psi_data(:, 1) = psi;
tau_data(:, 1) = tau_qs;
sigma_data(:, 1) = sigma_p;

% Save initial data for the two time vectors
t_fault(1) = t(1);
t_domain(1) = t(1);

%% Take time steps
while t(i) < EndTime
    % Perform an adaptive Runge Kutta time step. Retries until a time step
    % with an appropriate size has been taken
    attempt = 1;
    while 1
        
        % Make sure that we don't overshoot past the endtime
        if t(i) + dt > EndTime
            dt = EndTime - t(i);
        end
        
        %
        % --- RK Stage 1 ---
        %
        
        % --- Calculate the rates of change du, dpsi and dp' ---
        
        du_S1 = V/2;
        dpsi1 = StateRate(a, b, V, V_0, psi, L, f_0);
        dp_p1 = F_p*p_p+ F_u*w;
        
        % --- Integrate fields up to time t + 0.5dt ---
        
        u_S2 = u_S + 0.5*dt*du_S1;
        u_N2 = u_N + 0.5*dt*du_N;
        psi2 = psi + 0.5*dt*dpsi1;
        p_p2 = p_p + 0.5*dt*dp_p1;
        
        % --- Set the boundary conditions in the mechanical equation ---
        
        B_S = M_b.S*[BC(1,3)*u_S2; zeros(Nx, 1)];
        B_N = M_b.N*[BC(1,4)*u_N2; zeros(Nx, 1)];
        
        % --- Solve the mechanical equilibrium equation for u and v ---
        
        w_temp = -M_u\(M_p*p_p2 + (B_N + B_S));
        
        % --- Find slip velocity at each point on the fault ---
        
        % Calculate values of p, sigma_xy and sigma_yy in whole domain
        p = reshape(p_p2 - P*w_temp, Ny, Nx);
        sigma_xy = reshape(Sigma_xy*w_temp, Ny, Nx);
        sigma_yy = reshape(Sigma_yy*w_temp - alpha*p_p2, Ny, Nx);
        
        % Find tau_qs and sigma' at the fault
        tau_qs = sigma_xy(1, :)';
        sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';
        
        % Compute the slip velocity at each point
        V_temp = ComputeSlip(Nx, sigma_p, a, V_0, psi2, tau_0, tau_qs, eta);
        
        %
        % --- RK Stage 2 ---
        %
        
        % --- Calculate the rates of change du, dpsi and dp' ---
        
        du_S2 = V_temp/2;
        dpsi2 = StateRate(a, b, V_temp, V_0, psi2, L, f_0);
        dp_p2 = F_p*p_p2 + F_u*w_temp;
        
        % --- Integrate fields up to time t + dt ---
        
        u_S3 = u_S + dt*(-du_S1 + 2*du_S2);
        u_N3 = u_N + dt*du_N;
        psi3 = psi + dt*(-dpsi1 + 2*dpsi2);
        p_p3 = p_p + dt*(-dp_p1 + 2*dp_p2);
        
        
        % --- Set the boundary conditions in the mechanical equation ---
        
        B_S = M_b.S*[BC(1,3)*u_S3; zeros(Nx, 1)];
        B_N = M_b.N*[BC(1,4)*u_N3; zeros(Nx, 1)];
        
        % --- Solve the mechanical equilibrium equation for u and v ---
        
        w_temp = -M_u\(M_p*p_p3 + (B_N + B_S));
        
        % --- Find slip velocity at each point on the fault ---
        
        % Calculate values of p, sigma_xy and sigma_yy in whole domain
        p = reshape(p_p3 - P*w_temp, Ny, Nx);
        sigma_xy = reshape(Sigma_xy*w_temp, Ny, Nx);
        sigma_yy = reshape(Sigma_yy*w_temp - alpha*p_p3, Ny, Nx);
        
        % Find tau_qs and sigma' at the fault
        tau_qs = sigma_xy(1, :)';
        sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';
        
        % Compute the slip velocity at each point
        V_temp = ComputeSlip(Nx, sigma_p, a, V_0, psi3, tau_0, tau_qs, eta);
        
        %
        % --- RK Stage 3 ---
        %
        
        % --- Calculate the rates of change du_S, dpsi and dp_p ---
        
        du_S3 = V_temp/2;
        dpsi3 = StateRate(a, b, V_temp, V_0, psi3, L, f_0);
        dp_p3 = F_p*p_p3 + F_u*w_temp;
        
        %
        % --- Error Control on Psi and slip ---
        %
        
        % Perform RK2 and RK3 to obtain u_S at t + dt and find error
        uRK2 = u_S + dt/2*(du_S1 + du_S3);
        uRK3 = u_S + dt/6*(du_S1 + 4*du_S2 + du_S3);
        psiRK2 = psi + dt/2*(dpsi1 + dpsi3);
        psiRK3 = psi + dt/6*(dpsi1 + 4*dpsi2 + dpsi3);
        
        % Calculate max norm of the state and slip rate at fault
        error = max(norm(uRK2 - uRK3, Inf), norm(psiRK2 - psiRK3, Inf));
        
        % --- For acceptable time step ---
        if error < tol
            
            % Update value of u
            u_S = uRK3;
            psi = psiRK3;
            
            % Update value of u_N and p_p
            u_N = u_N + dt*du_N;
            p_p = p_p + dt/6*(dp_p1 + 4*dp_p2 + dp_p3);
            
            % --- Set the boundary data in the mechanical equation ---
            
            B_S = M_b.S*[BC(1,3)*u_S; zeros(Nx, 1)];
            B_N = M_b.N*[BC(1,4)*u_N; zeros(Nx, 1)];
            
            % --- Solve the mechanical equilibrium equation for u and v ---
            
            w = -M_u\(M_p*p_p + (B_S + B_N));
            
            % --- Find slip velocity at each point on the fault ---
            
            % Calculate values of fields in whole domain
            p = reshape(p_p - P*w, Ny, Nx);
            u = reshape(w(1:Ny*Nx),Ny,Nx);
            v = reshape(w(Nx*Ny + 1:2*Ny*Nx),Ny,Nx);
            sigma_xx = reshape(Sigma_xx*w - alpha*p_p,Ny,Nx);
            sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
            
            % Find tau_qs and sigma' at the fault
            tau_qs = sigma_xy(1, :)';
            sigma_p = (sigma_0 - sigma_yy(1, :) - p(1, :))';
            
            % Compute the slip velocity at each point
            V = ComputeSlip(Nx, sigma_p, a, V_0, psi, tau_0, tau_qs, eta);
            
            % Update the number of time steps performed
            i = i + 1;
            
            % Update the time
            t(i) = t(i - 1) + dt;
            
            % Save data from fields in the domain
            if (mod(i, DomainExportFrequency) == 0)
                p_data(:, :, i/DomainExportFrequency) = p;
                u_data(:, :, i/DomainExportFrequency) = u;
                v_data(:, :, i/DomainExportFrequency) = v;
                sigma_xx_data(:, :, i/DomainExportFrequency) = sigma_xx;
                sigma_xy_data(:, :, i/DomainExportFrequency) = sigma_xy;
                sigma_yy_data(:, :, i/DomainExportFrequency) = sigma_yy;
                
                % Save the time for the domain.
                t_domain(i/DomainExportFrequency) = t(i);
                
                % Print time to follow progress.
                t(i)
            end
            
            % Save data from fields in on fault
            if (mod(i, FaultExportFrequency) == 0)
                u_S_data(:, i/FaultExportFrequency) = u_S;
                V_data(:, i/FaultExportFrequency) = V;
                psi_data(:, i/FaultExportFrequency) = psi;
                tau_data(:, i/FaultExportFrequency) = ...
                    tau_0 + tau_qs - eta*V;
                sigma_data(:, i/FaultExportFrequency) = sigma_p;
                t_fault(i/FaultExportFrequency) = t(i);
            end
            
            % Find suggestion for next time step length
            if error ~= 0
                dt = min(min(0.9*dt*(tol/error)^(1/3), maxdt), 5*dt);
            end
            
            % Exit while-loop and perform next time step
            break
        end
        
        % --- For unacceptable timestep ---
        
        % Notify user and update timestep
        disp(['Rejecting attempt ', num2str(attempt), ', error = ', ...
            num2str(error), ' with dt = ', num2str(dt)]);
        
        % Find suggestion for next time step length
        dt = min(0.9*dt*(tol/error)^(1/3), maxdt);
        
        attempt = attempt + 1;
    end
end

% Rename data varibles before exporting them
p = p_data;
u = u_data;
v = v_data;
sigma_xx = sigma_xx_data;
sigma_xy = sigma_xy_data;
sigma_yy = sigma_yy_data;

u_S = u_S_data;
V = V_data;
psi = psi_data;
tau = tau_data;
sigma = sigma_data;

% Export data to disc
save('data.mat',  'x', 'y', 't_domain', 't_fault', ...
    'p', 'u', 'v', 'sigma_xx', 'sigma_xy', 'sigma_yy', ...
    'u_S', 'V', 'psi', 'tau', 'sigma_p');

% Remove paths for helper functions
rmpath '../../Helpers' '../../Helpers/Friction' '../../Helpers/LineSource' ...
    '../../Helpers/ExplicitDiscretization'