%% Simulation using the explicit scheme with constant slip on north and
% south boundary.
%
% Script that simulates the poroelastic problem with the SBP-SAT technique.
% The script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and results can be presented in different ways using
% different helper plotting functions.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/Friction' '../Helpers/LineSource' ...
    '../Helpers/ExplicitDiscretization'

%% Setup

% Define type of SBP operators
SBPStencil = 'r';	% Type of SBP-stencil. w - wide, n - narrow, r - russian
order = 3;        	% order of SBP-operator.
saveMovie = 0;      % If a movie should be saved

% Material parameters in self consistent units
Lx = 1;     	% km
Ly = 1;     	% km

G = 30;         % GPa
kappa = 1e-13;  % 1e-3 m^2/(Pa s)

% Dimensionless material parameters
B = 0.6;
nu = 0.2;
nu_u = 0.33;

% Friction parameters in self consistent units
V_0 = 1e-6;   	% m/s

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G, kappa, B, nu, nu_u);
lambda_u = lambda + alpha^2*M;

%% Spatial discretization
xL = -Lx;                	% West boundary
xR = Lx;                	% East boundary
yL = -Ly;                 	% South Boundary
yR = Ly;                 	% North Boundary
Nx = 80;                	% Resolution in x-direction
Ny = 80;                	% Resolution in y-direction
x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction
dy = (yR - yL)/(Ny - 1);    % Spatial step size, y-direction

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid
% discharge velocity and traction) is always 1 stability for stability
% reasons. The weight for the Dirichlet conditions (pore pressure and
% displacement) can be specified as an arbitrary positive number however.
% A Dirichlet condition can therefore be approximated using Robin boundary
% conditions using a large weight. A large number will give a more
% accurate solution, but will increase stiffness of the system.
% Here follows the structure of
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

Weights = [0 0 1000 1000;
    1000 1000 0 0;
    0 0 100 0];

% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Robin conditions are set according to Weights.
BC = ScaleBC(lambda_u, G, kappa, dx, dy, Weights);

%% Construct the spatial operators

% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dy, alpha*ones(Ny,Nx), K*ones(Ny,Nx), ...
    G*ones(Ny,Nx), kappa*ones(Ny,Nx), M*ones(Ny,Nx), SBPStencil, BC, order);

%% Temporal discretization

% Time step for Dirichlet condition
if (sum(BC(3, :)) ~= 0)
    dt = 0.425/(M*(max(max(BC(3, 1:2)/dx), max(BC(3, 3:4)/dy))));
% Time step with only Neumann Condition
else
    dt = 0.25*min(dx^2, dy^2)/(kappa*M);
end
startTime = 0;
endTime = 3600*24*30; %One month
Nt = floor((endTime-startTime)/dt)+1;
while(Nt < 1)
    dt = dt/2;
    Nt = floor((endTime-startTime)/dt)+1;
end
disp(strcat('Number of time steps: ',num2str(Nt)));
t = zeros(1,Nt);
t(1:end-1) = startTime:dt:endTime-dt;
t(end) = endTime;

% Set initial conditions (at time t = 0) for fields living on whole domain
w = zeros(2*Ny*Nx, 1);
p_p = zeros(Ny*Nx, 1);
%p_p = reshape(sin(pi*y')*sin(pi*x),Nx*Ny,1);

%% Take time steps
for i = 1:Nt-1
    
    % --- Plotting ---
    p = reshape(p_p - P*w,Ny,Nx);
    u = reshape(w(1:Ny*Nx),Ny,Nx);
    v = reshape(w(Nx*Ny + 1:2*Ny*Nx),Ny,Nx);
    
    sigma_xx = reshape(Sigma_xx*w-alpha*p_p,Ny,Nx);
    sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
    sigma_yy = reshape(Sigma_yy*w-alpha*p_p, Ny, Nx);
    
    %u, v, p
    figure(1);
    subplot(1,3,1);
    pcolor(x, y, u);
    colorbar();
    xlabel('x [km]');
    ylabel('y [km]');
    title('u [m]');
    shading flat;
    subplot(1,3,2);
    pcolor(x, y, v);
    colorbar();
    xlabel('x [m]');
    ylabel('y [m]');
    title('v [m]');
    shading flat;
    subplot(1,3,3);
    pcolor(x, y, p);
    colorbar();
    xlabel('x [km]');
    ylabel('y [km]');
    title('p [MPa]');
    shading flat;
    suptitle(sprintf('t = %f [s]', t(i)));
    
    % sigma_xx, sigma_xy, sigma_yy
    figure(2);
    subplot(1,3,1);
    pcolor(x, y, sigma_xx);
    colorbar();
    xlabel('x [km]');
    ylabel('y [km]');
    title('\sigma_{xx} [MPa]');
    shading flat;
    subplot(1,3,2);
    pcolor(x, y, sigma_xy);
    colorbar();
    xlabel('x [m]');
    ylabel('y [m]');
    title('\sigma_{xy} [MPa]');
    shading flat;
    subplot(1,3,3);
    pcolor(x, y, sigma_yy);
    colorbar();
    xlabel('x [km]');
    ylabel('y [km]');
    title('\sigma_{yy} [MPa]');
    shading flat;
    suptitle(sprintf('t = %f [s]', t(i)));
    pause(0.001);
    
    
    % --- Set the non zero boundary data in the mech. eq. equation ---
    %OBS! t(i+1) gives the correct value of u_N at t = endTime. Look closer
    %at
    B_N = M_b.N*[ones(Nx, 1)*BC(1, 4)*1e3*V_0/2*t(i+1); zeros(Nx, 1)];
    
    % --- Solve the mechanical equilibrium equation for u and v ---
    
    w = M_u\(-M_p*p_p - B_N);
    
    % --- Perform an Euler Forward step in the Fluid Diffusion equation ---
    dp_p = F_p*p_p+ F_u*w;
    p_p = p_p + dt*dp_p;
    
    if saveMovie
        %Give time to resize the window
        if i == 1
            pause(3);
        end
        % Save frame to save as movie
        frames(i) = getframe(gcf);
    end
    
end

% --- Plot last update ---
p = reshape(p_p - P*w,Ny,Nx);
u = reshape(w(1:Ny*Nx),Ny,Nx);
v = reshape(w(Nx*Ny + 1:2*Ny*Nx),Ny,Nx);

sigma_xx = reshape(Sigma_xx*w-alpha*p_p,Ny,Nx);
sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
sigma_yy = reshape(Sigma_yy*w-alpha*p_p, Ny, Nx);

%u, v, p
figure(1);
subplot(1,3,1);
pcolor(x, y, u);
colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('u [m]');
shading flat;
subplot(1,3,2);
pcolor(x, y, v);
colorbar();
xlabel('x [m]');
ylabel('y [m]');
title('v [m]');
shading flat;
subplot(1,3,3);
pcolor(x, y, p);
colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('p [MPa]');
shading flat;
suptitle(sprintf('t = %f [s]', endTime));

% sigma_xx, sigma_xy, sigma_yy
figure(2);
subplot(1,3,1);
pcolor(x, y, sigma_xx);
colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('\sigma_{xx} [MPa]');
shading flat;
subplot(1,3,2);
pcolor(x, y, sigma_xy);
colorbar();
xlabel('x [m]');
ylabel('y [m]');
title('\sigma_{xy} [MPa]');
shading flat;
subplot(1,3,3);
pcolor(sigma_yy);
colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('\sigma_{yy} [MPa]');
shading flat;
suptitle(sprintf('t = %f [s]', endTime));

max_sigma_xx_W = max(max(sigma_xx(:,1)))
max_sigma_yy_S = max(max(sigma_yy(1,:)))

% Save movie
if saveMovie
    video = VideoWriter('simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/Friction' '../Helpers/LineSource' ...
    '../Helpers/ExplicitDiscretization'