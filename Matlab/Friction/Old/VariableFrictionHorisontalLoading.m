%% Simulation using the explicit scheme with a fault on the south boundary
% Script that simulates the poroelastic problem with the SBP-SAT technique.
% The script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and results can be presented in different ways using
% different helper plotting functions. Here the friction law
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/Friction' '../Helpers/LineSource' ...
    '../Helpers/ExplicitDiscretization'

%% Setup

% Define type of SBP operators
SBPStencil = 'n';   % Type of SBP-stencil. w - wide, n - narrow, r - russian
order = 2;          % order of SBP-operator.
saveMovie = 0;      % If a movie should be saved

% Dirac Delta parameters
ooa = 2;    % Order of accuracy
sc = 1;     % Smoothness conditions

% Material parameters in self consistent units
Lx = 1;         % km
Ly = 1;         % km
G = 30;         % GPa
kappa = 1e-6;   % 1e-3 m^2/(Pa s)

% Line source parameters
q = 1;
rho = 1;

% Dimensionless material parameters
B = 0.6;
nu = 0.2;
nu_u = 0.33;

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = ...
    MaterialParameters(G, kappa, B, nu, nu_u);
lambda_u = lambda + alpha^2*M;

% Friction parameters in self consistent units
f_0 = 0.6;                  % 1
a_par = 0.015;              % 1
b_par = 0.02;               % 1

sigma_0 = 50;               % MPa
L = 8e-3;                   % m
tau_0 = 0.95*f_0*sigma_0;   % MPa
V_0 = 1e-6;                 % m/s
c_c = 3.4;                  % km/s
eta = G/(2*c_c);            % MPa/(m/s)

% Scales that has to be resolved
h_f = pi*G*L/(sigma_0*(b_par - a_par));
t_f = h_f/c_c;
h_d = sqrt(kappa*M*t_f);

% Error tolerance for the horisontal displacement at the fault
tol = 1e-5;

%% Spatial discretization
xL = -Lx;                   % West boundary
xR = Lx;                    % East boundary
yL = -Ly;                   % South Boundary
yR = Ly;                    % North Boundary
% Use even points in line source
Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction
dy = (yR - yL)/(Ny - 1);    % Spatial step size, y-direction

% Unitless friction fields
Lc = 1.0; % Length of the velocity weakening region
a = a_par*ones(Nx, 1);
%b = b_par*ones(Nx, 1);
b = zeros(Nx, 1);
%b(x > -Lc/2 & x < Lc/2) = b_par;
b = b_par*gaussmf(x, [0.5, 0])';

% Slip rate at the north boundary
du_N = 1e3*V_0*ones(Nx, 1);

% Check if the spatial scales are resolved
fprintf('h_f/dx = %f\n', h_f/dx);
fprintf('h_d/dy = %f\n', h_d/dy);

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid
% discharge velocity and traction) always have weight one, since that is
% required for stability. The weight for the Dirichlet conditions
% (pore pressure and displacement) can be specified as an arbitrary
% positive number however. A Dirichlet condition can therefore be
% approximated using Robin boundary conditions using a large
% weight. A large number will give a more accurate solution,
% but will increase stiffness of the system. Here follows the structure of
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

Weights = [0 0 1e3 1e3;
           1e3 1e3 0 0;
           0 0 1e2 0];

% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Robin conditions are set according to Weights.
BC = ScaleBC(lambda_u, G, kappa, dx, dy, Weights);

%% Construct the spatial operators

% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dy, alpha*ones(Ny,Nx), K*ones(Ny,Nx), ...
    G*ones(Ny,Nx), kappa*ones(Ny,Nx), M*ones(Ny,Nx), SBPStencil, BC, order);

Q = 0;
% Q = M*PointSource(Nx, dx, Ny, dy, ooa, sc);

%% Temporal discritization
startTime = 0;       	% s
endTime = Ly^2*2e3; 	% s
t(1) = startTime;
dt = t_f;               % Start with a characteristic friction time step

% Stability conditions
if (sum(BC(3, :)) ~= 0)
    maxdt = 0.425/(M*(max(max(BC(3, 1:2)/dx), max(BC(3, 3:4)/dy))));
else
    maxdt = 0.25*min(dx^2, dy^2)/(kappa*M);
end

% Present the number of time steps per characteristic time scale for
% stability
fprintf('t_f/dt = %f\n', t_f/maxdt);

% Set initial conditions (at time t = 0) for fields living on whole domain
w = zeros(2*Ny*Nx, 1);
p_p = zeros(Ny*Nx, 1);

% Set initial conditions for fields living on the fault
psi = ones(Nx, 1)*f_0;
u_S = zeros(Nx, 1);
u_N = zeros(Nx, 1);

%% Take time steps
i = 1;
while t(i) < endTime
    % Perform an adaptive Runge Kutta time step. Retries until a time step
    % with an appropriate size has been taken
    attempt = 1;
    while 1
        
        % Make sure that we don't overshoot past the endtime
        if t(i) + dt > endTime
            dt = endTime - t(i);
        end
        
        %
        % --- RK Stage 1 ---
        %
        
        % --- Set the boundary data in the mechanical equation ---
        
        B_S = M_b.S*[BC(1,3)*u_S; zeros(Nx, 1)];
        B_N = M_b.N*[BC(1,4)*u_N; zeros(Nx, 1)];
        
        % --- Solve the mechanical equilibrium equation for u and v ---
        
        w = -M_u\(M_p*p_p + (B_S + B_N));
        
        % --- Find slip velocity at each point on the fault ---
        
        % Calculate values of fields in whole domain
        p = reshape(p_p - P*w, Ny, Nx);
        u = reshape(w(1:Ny*Nx),Ny,Nx);
        v = reshape(w(Nx*Ny + 1:2*Ny*Nx),Ny,Nx);
        sigma_xx = reshape(Sigma_xx*w - alpha*p_p,Ny,Nx);
        sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
        sigma_yy = reshape(Sigma_yy*w - alpha*p_p, Ny, Nx);
        
        % Find tau_qs and sigma' at the fault
        tau_qs = sigma_xy(1, :)';
        sigma_p = sigma_0 - sigma_yy(1, :)' - p(1, :)';
        
        % Compute the slip velocity at each point
        V = ComputeSlip(Nx, sigma_p, a, V_0, psi, tau_0, tau_qs, eta);
        
        % Collect variables for plotting
        tausig = (tau_0 + tau_qs - eta*V)./sigma_p;
        tausig_P(i) = tausig(ceil(Nx/2));
        V_P(i) = V(ceil(Nx/2));
        fss(i) = f_0 - (b_par - a_par)*log(V(ceil(Nx/2))/V_0);
        
        %
        % --- Plotting ---
        %
        
%         Plot2D(x, y, t(i), u, v, p, sigma_xx, sigma_xy, sigma_yy);
%         
%         figure(3)
%         subplot(1, 3, 1)
%         plot(x, sigma_yy(1, :))
%         xlabel('x [km]')
%         ylabel('\sigma_{yy} @ SB');
%         subplot(1, 3, 2)
%         plot(x, p(1, :))
%         xlabel('x [km]')
%         ylabel('p @ SB')
%         subplot(1, 3, 3)
%         plot(x, u(1, :))
%         xlabel('x [km]')
%         ylabel('u @ SB');
        
        % Plot slip and friction on fault
        subplot(2, 4, 1:2);
        semilogx(V_P, tausig_P, V_P, fss);
        xlabel('Slip rate V [m/s]');
        ylabel('f');
        
        subplot(2, 4, 5);
        semilogy(t, V_P);
        xlabel('t [s]');
        ylabel('Slip Rate V [m/s]');
        
        subplot(2, 4, 6);
        plot(t, tausig_P);
        xlabel('t [s]')
        ylabel('f');
        
        subplot(2, 4, 3);
        semilogy(x, V);
        xlabel('x [km]')
        ylabel('Slip rate V [m/s]');
        ylim([1e-8 1e1]);
        
        subplot(2, 4, 4);
        plot(x, u(1, :));
        xlabel('x [km]');
        ylabel('Slip u [m]');
        
        subplot(2, 4, 7);        
        plot(x, p(1, :));
        xlabel('x [km]');
        ylabel('Pressure p [MPa]');
        
        subplot(2, 4, 8);
        plot(x, tausig);
        xlabel('x [km]');
        ylabel('f');

        suptitle(strcat('t = ', num2str(t(i)), ' s'));
        pause(0.001);
        
        % --- Calculate the rates of change du, dpsi and dp' ---
        
        du_S1 = V/2;
        dpsi1 = StateRate(a, b, V, V_0, psi, L, f_0);
        dp_p1 = F_p*p_p+ F_u*w + Q;
        
        % --- Integrate fields up to time t + 0.5dt ---
        
        u_S2 = u_S + 0.5*dt*du_S1;
        u_N2 = u_N + 0.5*dt*du_N;
        psi2 = psi + 0.5*dt*dpsi1;
        p_p2 = p_p + 0.5*dt*dp_p1;
        
        %
        % --- RK Stage 2 ---
        %
        
        % --- Set the boundary conditions in the mechanical equation ---
        
        B_S = M_b.S*[BC(1,3)*u_S2; zeros(Nx, 1)];
        B_N = M_b.N*[BC(1,4)*u_N2; zeros(Nx, 1)];
        
        % --- Solve the mechanical equilibrium equation for u and v ---
        
        w = -M_u\(M_p*p_p2 + (B_N + B_S));
        
        % --- Find slip velocity at each point on the fault ---
        
        % Calculate values of p, sigma_xy and sigma_yy in whole domain
        p = reshape(p_p2 - P*w, Ny, Nx);
        sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
        sigma_yy = reshape(Sigma_yy*w - alpha*p_p2, Ny, Nx);
        
        % Find tau_qs and sigma' at the fault
        tau_qs = sigma_xy(1, :);
        sigma_p = sigma_0 - sigma_yy(1, :) - p(1, :);
        
        % Compute the slip velocity at each point
        V = ComputeSlip(Nx, sigma_p, a, V_0, psi2, tau_0, tau_qs, eta);
        
        % --- Calculate the rates of change du, dpsi and dp' ---
        
        du_S2 = V/2;
        dpsi2 = StateRate(a, b, V, V_0, psi2, L, f_0);
        dp_p2 = F_p*p_p2 + F_u*w+Q;
        
        % --- Integrate fields up to time t + dt ---
        
        u_S3 = u_S + dt*(-du_S1 + 2*du_S2);
        u_N3 = u_N + dt*du_N;
        psi3 = psi + dt*(-dpsi1 + 2*dpsi2);
        p_p3 = p_p + dt*(-dp_p1 + 2*dp_p2);
        
        %
        % --- RK Stage 3 ---
        %
        
        % --- Set the boundary conditions in the mechanical equation ---
        
        B_S = M_b.S*[BC(1,3)*u_S3; zeros(Nx, 1)];
        B_N = M_b.N*[BC(1,4)*u_N3; zeros(Nx, 1)];
        
        % --- Solve the mechanical equilibrium equation for u and v ---
        
        w = -M_u\(M_p*p_p3 + (B_N + B_S));
        
        % --- Find slip velocity at each point on the fault ---
        
        % Calculate values of p, sigma_xy and sigma_yy in whole domain
        p = reshape(p_p3 - P*w, Ny, Nx);
        sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
        sigma_yy = reshape(Sigma_yy*w - alpha*p_p3, Ny, Nx);
        
        % Find tau_qs and sigma' at the fault
        tau_qs = sigma_xy(1, :);
        sigma_p = sigma_0 - sigma_yy(1, :) - p(1, :);
        
        % Compute the slip velocity at each point
        V = ComputeSlip(Nx, sigma_p, a, V_0, psi3, tau_0, tau_qs, eta);
        
        % --- Calculate the rates of change du_S, dpsi and dp_p ---
        
        du_S3 = V/2;
        dpsi3 = StateRate(a, b, V, V_0, psi3, L, f_0);
        dp_p3 = F_p*p_p3 + F_u*w+Q;
        
        %
        % --- Error Control on Psi and slip ---
        %
        
        % Perform RK2 and RK3 to obtain u_S at t + dt and find error
        uRK2 = u_S + dt/2*(du_S1 + du_S3);
        uRK3 = u_S + dt/6*(du_S1 + 4*du_S2 + du_S3);
        psiRK2 = psi + dt/2*(dpsi1 + dpsi3);
        psiRK3 = psi + dt/6*(dpsi1 + 4*dpsi2 + dpsi3);
        
        % Calculate max norm of the state and slip rate at fault
        error = max(norm(uRK2 - uRK3, Inf), norm(psiRK2 - psiRK3, Inf));
        
        % --- For acceptable time step ---
        if error < tol
            % Update value of u
            u_S = uRK3;
            psi = psiRK3;
            
            % Update value of u_N and p_p
            u_N = u_N + dt*du_N;
            p_p = p_p + dt/6*(dp_p1 + 4*dp_p2 + dp_p3);
            
            % Update the number of time steps performed
            i = i + 1;
            
            % Update the time
            t(i) = t(i - 1) + dt;
            
            % Find suggestion for next time step length
            if error ~= 0
                dt = min(min(0.9*dt*(tol/error)^(1/3), maxdt), 5*dt);
            end
        
            % Exit while-loop and perform next time step
            break
        end
        
        % --- For unacceptable timestep ---
        
        % Notify user and update timestep
        disp(['Rejecting attempt ', num2str(attempt), ', error = ', ...
            num2str(error), ' with dt = ', num2str(dt)]);
        
        % Find suggestion for next time step length
        dt = min(0.9*dt*(tol/error)^(1/3), maxdt);
        
        attempt = attempt + 1;
    end
    
    if saveMovie
        %Give time to resize the window
        if mod(i,10) == 0
            pause(3);
        end
        % Save frame to save as movie
        frames(j) = getframe(gcf);
        j = j+1;
    end
end

% Save movie
if saveMovie
    video = VideoWriter('simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/Friction' '../Helpers/LineSource' ...
    '../Helpers/ExplicitDiscretization'