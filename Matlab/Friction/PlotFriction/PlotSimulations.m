%% Script for showing friction simulations

clear all
close all

addpath '../../Helpers/Friction'

%% Load data from file
file = '../Injection/SimulationData/InFlowNarrow40.mat';
load(file);

%% Plot fields in the domain
for i = 1:length(t_domain)
    
    % --- Plotting of u, v and p ---
    
    subplot(2, 3, 1);
    surf(x, y, u(:, :, i));
    xlabel('x [m]');
    ylabel('y [m]');
    title('u [mm]');
    
    subplot(2,3,2);
    surf(x, y, v(:, :, i));
    xlabel('x [m]');
    ylabel('y [m]');
    title('v [mm]');
    
    subplot(2,3,3);
    surf(x, y, p(:, :, i));
    xlabel('x [m]');
    ylabel('y [m]');
    title('p [MPa]');
    
    % --- Plotting of sigma_xx, sigma_xy and sigma_yy ---
    
    subplot(2, 3, 4);
    surf(x, y, sigma_xx(:, :, i));
    xlabel('x [m]');
    ylabel('y [m]');
    title('\sigma_{xx} [MPa]');
    
    subplot(2, 3, 5);
    surf(x, y, sigma_xy(:, :, i));
    xlabel('x [m]');
    ylabel('y [m]');
    title('\sigma_{xy} [MPa]');
    
    subplot(2, 3, 6);
    surf(x, y, sigma_yy(:, :, i));
    xlabel('x [m]');
    ylabel('y [m]');
    title('\sigma_{yy} [MPa]');
    
    suptitle(sprintf('t = %f [s]', t_domain(i)));
    drawnow;
end

%% Plot fields at the boundary
for i = 1:length(t_fault)
    VP = V(1, 1:i);
    tausig = tau(1, 1:i)./sigma(1, 1:i);
    fss = f_0 - (b - a)*log(V(1, 1:i)/V_0);
    
    % Plot slip and friction on fault
    subplot(2, 4, 1:2);
    semilogx(VP, tausig, VP, fss);
    xlabel('Slip rate V [mm/s]');
    ylabel('f');
    
    subplot(2, 4, 3);
    semilogy(x, V(:, i));
    xlabel('x [m]')
    ylabel('Slip rate V [mm/s]');
    
    subplot(2, 4, 4);
    plot(x, u_S(:, i));
    xlabel('x [m]');
    ylabel('Slip u [mm]');
    
    subplot(2, 4, 5);
    semilogy(t_fault(1:i), VP);
    xlabel('t [s]');
    ylabel('Slip Rate V [mm/s]');
    
    subplot(2, 4, 6);
    plot(t_fault(1:i), tausig);
    xlabel('t [s]')
    ylabel('f');
    
    subplot(2, 4, 7);
    plot(x, p_fault(:, i));
    xlabel('x [m]');
    ylabel('Pressure p [MPa]');
    
    subplot(2, 4, 8);
    plot(x, sigma_yy_fault(:, i));
    xlabel('x [m]');
    ylabel('\sigma_{yy} [MPa]');
    
    suptitle(strcat('t = ', num2str(t_fault(i)), ' s'));
    drawnow;
end

%% Remove path
rmpath '../../Helpers/Friction'
