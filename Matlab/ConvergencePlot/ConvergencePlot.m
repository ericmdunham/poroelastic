%% Presents a convergence plot
%
%   Input:
%   Vector p        - The errors in pore pressure p.
%   Vector u        - The errors in horisontal displacement u.
%   Vector v        - The errors in vertical displacement v.
%   Vector res      - The resolutions used in the computations.
%   Vector ref_ord  - Reference orders that should be plotted. Input of 
%                     [1 2 4] will present reference plots of order 1, 2
%                     and 4.
%   Boolean plot_u  - Flag determining whether or not plots of u and v
%                     should be presented.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = ConvergencePlot(p, u, v, res, ref_ord)

fontsize = 20;

% Plotting of p, u and v
loglog(res, p, '-*');
hold on;
loglog(res, u, '-*');
loglog(res, v, '-*');
legendary{1} = 'p';
legendary{2} = 'u';
legendary{3} = 'v';

% Plotting of reference convergence rates
for i = 1:length(ref_ord)
    temp = [res(end) res(1)].^ref_ord(i);
    ref = temp*(0.25*p(1)/temp(1));
    loglog([res(1) res(end)], ref, '--');
    legendary{i + 3} = ['Order ' num2str(ref_ord(i))];
end
hold off;
legend(legendary);
set(gca, 'FontSize', fontsize);
xlabel('N', 'FontSize', fontsize);
ylabel('$\| e \|_{L_2}$', 'Interpreter', 'latex', 'FontSize', fontsize + 5);
grid on;

end
