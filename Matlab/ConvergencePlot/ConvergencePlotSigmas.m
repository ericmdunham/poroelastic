%% Presents a convergence plot
%
%   Input:
%   Vector SigmaXX  - The errors in sigma component sigma_xx.
%   Vector SigmaXY  - The errors in sigma component sigma_xy.
%   Vector SigmaYY  - The errors in sigma component sigma_yy.
%   Vector res      - The resolutions used in the computations.
%   Vector ref_ord  - Reference orders that should be plotted. Input of 
%                     [1 2 4] will present reference plots of order 1, 2
%                     and 4.
%   Boolean plot_u  - Flag determining whether or not plots of u and v
%                     should be presented.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = ConvergencePlotSigmas(SigmaXX, SigmaXY, SigmaYY, Res, RefOrd)

fontsize = 20;

% Plotting of p, u and v
loglog(Res, SigmaXX, '-*');
hold on;
loglog(Res, SigmaXY, '-*');
loglog(Res, SigmaYY, '-*');
Legendary{1} = '\sigma_{xx}';
Legendary{2} = '\sigma_{xy}';
Legendary{3} = '\sigma_{yy}';

% Plotting of reference convergence rates
for i = 1:length(RefOrd)
    Temp = [Res(end) Res(1)].^RefOrd(i);
    Ref = Temp*(0.9*SigmaXX(1)/Temp(1));
    loglog([Res(1) Res(end)], Ref, '--');
    Legendary{i + 3} = ['Order ' num2str(RefOrd(i))];
end
hold off;
legend(Legendary);
set(gca, 'FontSize', fontsize);
xlabel('N', 'FontSize', fontsize);
ylabel('$\| e \|_{L_2}$', 'Interpreter', 'latex', 'FontSize', fontsize + 5);
grid on;

end
