%% Script for showing convergence data

clear all
close all

% Load data from file
file = '../MMSImplicit/ConvergenceData/Narrow4.mat';
load(file);

% Plot the convergence rates
ConvergencePlotSigmas(NormsSigmaXX, NormsSigmaXY, NormsSigmaYY, Res, RefOrd)

% Convergence measurement for all quantities
QSigmaXX = zeros(1,length(Res) - 1);
QSigmaXY = zeros(1,length(Res) - 1);
QSigmaYY = zeros(1,length(Res) - 1);
for i=1:length(QSigmaXX)
    N1 = Res(i)^2;
    N2 = Res(i + 1)^2;
    QSigmaXX(i) = log10(NormsSigmaXX(i)/NormsSigmaXX(i + 1))/log10((N2/N1)^(1/2));
    QSigmaXY(i) = log10(NormsSigmaXY(i)/NormsSigmaXY(i + 1))/log10((N2/N1)^(1/2));
    QSigmaYY(i) = log10(NormsSigmaYY(i)/NormsSigmaYY(i + 1))/log10((N2/N1)^(1/2));
end
