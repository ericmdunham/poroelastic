%% Script for showing convergence data

clear all
close all

% Load data from file
file = '../MandelImplicit/ConvergenceData/Narrow4.mat';
load(file);

% Plot the convergence rates
ConvergencePlot(NormsP, NormsU, NormsV, Res, [3])

% Convergence measurement for all quantities
QU = zeros(1,length(Res) - 1);
QV = zeros(1,length(Res) - 1);
QP = zeros(1,length(Res) - 1);
for i=1:length(QU)
    N1 = Res(i)^2;
    N2 = Res(i + 1)^2;
    QU(i) = log10(NormsU(i)/NormsU(i + 1))/log10((N2/N1)^(1/2));
    QV(i) = log10(NormsV(i)/NormsV(i + 1))/log10((N2/N1)^(1/2));
    QP(i) = log10(NormsP(i)/NormsP(i + 1))/log10((N2/N1)^(1/2));
end
