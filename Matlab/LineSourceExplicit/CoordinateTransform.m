%% Simulation of the line source problem using the explicit scheme
% Script that simulates the line source problem with the SBP-SAT technique.
% The script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and results can be presented in different ways using
% different helper plotting functions. With a coordinate transform.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/LineSource' ...
    '../Helpers/ExplicitDiscretization'

%% Define type of SBP operators
SBPStencil = 'n';	% Type of SBP-stencil. w - wide, n - narrow, r - russian
Order = 2;        	% order of SBP-operator.
DirichletOrder = 2; % Order of accuracy of the Dirichlet conditions
SaveMovie = 0;      % If a movie should be saved

% Dirac Delta parameters
ooa = 2;    % Order of accuracy
sc = 1;     % Smoothness conditions

% Fluid Injection parameters
q = 1;      % Rate of injection
rho = 1;    % Density of the injected fluid

%% Material parameters
a = 1;      % 10^3 m
b = 1;      % 10^3 m
G = 1;      % 10^9 Pa
kappa = 1;  % 10^-9 m^2/(Pa s)

% Dimensionless parameters
B = 0.8;
nu = 0.2;
nu_u = 0.4;

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G, kappa, B, nu, nu_u);

%% Spatial discretization
xL = -a;                    % West boundary
xR = a;                     % East boundary
yL = -a;                    % South Boundary
yR = b;                     % North Boundary
% Use even points in line source
Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
x = linspace(xL, xR, Nx);     % Spatial grid, x-direction
dx = (xR - xL)/(Nx - 1);    % Spatial step size, x-direction

% The numerical grid xi living on [-1, 1]
xi = linspace(-1, 1, Ny);
dxi = 2/(Ny - 1);

% Determine the coordinate transformation
para_coor = 0.90;
y = a*atanh(para_coor*xi)/atanh(para_coor);
dy = min(diff(y)); % Used for stability condition

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid 
% discharge velocity and traction) always have weight one, since that is 
% required for stability. The weight for the Dirichlet conditions 
% (pore pressure and displacement) can be specified as an arbitrary 
% positive number however. A Dirichlet condition can therefore be 
% approximated using Robin boundary conditions using a large 
% weight. A large number will give a more accurate solution, 
% but will increase stiffness of the system. Here follows the structure of 
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

% West boundary
% u = u_a, v = v_a, p = p_a
%
% East boundary
% u = u_a, v = v_a, p = p_a
%
% South boundary
% u = u_a, v = v_a, p = p_a
%
% North boundary
% u = u_a, v = v_a, p = p_a

BC = [1 1 1 1;
      1 1 1 1;
      1 1 1 1];
  
% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Dirichlet conditions are imposed with the accuracy
% specified by DirichletOrder.
BC = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC);
       
% Obtain the stability condition from the boundary conditions and the interior
C = 1.1; % A numeric constant that is dependent of the problem and discretization
dt = C*min(dx^2/(M*kappa), ...  % From interior/Neumann in x-direction
    min(dy^2/(M*kappa), ...     % From interior/Neumann in y-direction
    min(dx/(M*BC(3, 1)), ...    % From Dirichlet at west boundary
    min(dx/(M*BC(3, 2)), ...    % From Dirichlet at east boundary
    min(dy/(M*BC(3, 3)), ...    % From Dirichlet at south boundary
    min(dy/(M*BC(3, 4))))))));  % From Dirichlet at north boundary

%% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dxi, alpha*ones(Ny,Nx), K*ones(Ny,Nx), ...
    G*ones(Ny,Nx), kappa*ones(Ny,Nx), M*ones(Ny,Nx), SBPStencil, ...
    BC, Order, y', 'y');

%% Time integration system
StartTime = 0.5/c;
EndTime = 0.51/c;
t = StartTime:dt:EndTime;
t_stag = StartTime:0.5*dt:t(end);

% Make sure that we end up at endtime
if t(end) ~= EndTime
    t_stag(end + 1) = (EndTime + t(end))/2;
    t_stag(end + 1) = EndTime;
    t(end + 1) = EndTime;
end
Nt = length(t);

% Create forcing function. We need D1 for Q, but this is really inelegant.
addpath '../Helpers/OperatorsNarrow'
mats = SBP(Ny, dxi, 2);
J = diag(mats.D1int*y');
Jinv = inv(J);
Q = kron(speye(Nx, Nx), Jinv)*q*PointSource(Nx, dx, Ny, dxi, ooa, sc, 0, 0);
MQ = M*spdiags(ones(Ny*Nx, 1), 0, Ny*Nx, Ny*Nx)*Q;
rmpath '../Helpers/OperatorsNarrow'

%% Compute analytical solutions
[u_a, v_a, p_a] = AnalyticalSolutions(...
    x, y, t_stag, q, rho, G, lambda, kappa, c, alpha);

%% Boundary data
% Data is specified component-wise for all times in a M-Nt matrix, where M
% is the number of grid points at the corresponding boundary. Boundary data 
% for Dirichlet condition need to be scaled by the corresponding BC.

% Boundary data for mechanical equilibrium equations.
gWu = squeeze(BC(1,1)*u_a(:, 1, :));
gWv = squeeze(BC(2,1)*v_a(:, 1, :));
gW = [gWu; gWv];

gEu = squeeze(BC(1,2)*u_a(:, Nx, :));
gEv = squeeze(BC(2,2)*v_a(:, Nx, :));
gE = [gEu; gEv];

gSu = squeeze(BC(1,3)*u_a(1, :, :));
gSv = squeeze(BC(2,3)*v_a(1, :, :));
gS = [gSu; gSv];

gNu = squeeze(BC(1,4)*u_a(Ny, :, :));
gNv = squeeze(BC(2,4)*v_a(Ny, :, :));
gN = [gNu; gNv];

% Build the boundary data matrix for the mech. eq.
BM = M_b.W*gW + M_b.E*gE + M_b.S*gS + M_b.N*gN;

% Boundary data for fluid diffusion equation
fW = squeeze(BC(3,1)*p_a(:, 1, :));
fE = squeeze(BC(3,2)*p_a(:, Nx, :));
fS = squeeze(BC(3,3)*p_a(1, :, :));
fN = squeeze(BC(3,4)*p_a(Ny, :, :));

BF = F_b.W*fW + F_b.E*fE + F_b.S*fS + F_b.N*fN;

% Impose analytical solutions at t = startTime as initial conditions.
w = [reshape(u_a(:, :, 1), Ny*Nx, 1);
    reshape(v_a(:, :, 1), Ny*Nx, 1)];
p_p = reshape(p_a(:, :, 1), Ny*Nx, 1) + P*w;

for i = 1 : Nt - 1
    % Find the time step for the current iteration
    dt = t(i + 1) - t(i);
    
    % Perform a explicit RK4 time step
    % 1:st stage
    k1 = F_p*p_p + F_u*w + MQ + BF(: ,2*i - 1);
    
    % 2:nd stage
    p_p2 = p_p + 0.5*dt*k1;
    w2 = -M_u\(M_p*p_p2 + BM(:, 2*i));
    k2 = F_p*p_p2 + F_u*w2 + MQ + BF(:, 2*i);
    
    % 3:rd stage
    p_p3 = p_p + 0.5*dt*k2;
    w3 = -M_u\(M_p*p_p3 + BM(:, 2*i));
    k3 = F_p*p_p2 + F_u*w2 + MQ + BF(:, 2*i);
    
    % 4:th stage
    p_p4 = p_p + dt*k3;
    w4 = -M_u\(M_p*p_p4 + BM(:, 2*i + 1));
    k4 = F_p*p_p4 + F_u*w4 + MQ + BF(:, 2*i + 1);
    
    % Time  step in the fluid diffusion equation
    p_p = p_p + dt*(k1/6 + k2/3 + k3/3 + k4/6);
    
    % Solve the mechanical equilibrium equation for u and v.
    w = -M_u\(M_p*p_p + BM(:, 2*i + 1));
    
    % Collect variable fields
    p = reshape(p_p - P*w, Ny, Nx);
    u = reshape(w(1:Ny*Nx), Ny,Nx);
    v = reshape(w(Nx*Ny + 1:2*Ny*Nx), Ny, Nx);
    
    % Obtain non-dimensional fields and parameters
    l_d = sqrt(c*t(i + 1)); % Diffusion length
    x_n = x/l_d;
    y_n = y/l_d;
    u_n = u/l_d;
    v_n = v/l_d;
    
    % Use a plotting function
    %Plot2D(x, y, t(i), u, v, p);
    %PlotHeatmap(x, y, t(i), u, v, p, u_a(:, :, i), v_a(:, :, i), p_a(:, :, i));
    Plot1D(x_n, y_n, u_n, v_n, p, u_a(:, :, 2*i + 1)/l_d, v_a(:, :, 2*i + 1)/l_d, p_a(:, :, 2*i + 1));
end

% Save movie
if SaveMovie
    video = VideoWriter('Simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/LineSource' ...
    '../Helpers/ExplicitDiscretization'