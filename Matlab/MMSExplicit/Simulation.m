%% Simulation of the MMS problem
% Script that simulates the MMS problem with the SBP-SAT technique. The
% script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and plots in 1D or 2D can be presented by using different
% help functions.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ExplicitDiscretization' 

%% Setup

% Define type of SBP operators
SBPStencil = 'n';	% Type of SBP-stencil. w - wide, n - narrow, r - Russian
Order = 2;        	% order of SBP-operator.
DirichletOrder = 2; % Order of accuracy of the Dirichlet conditions
SaveMovie = 0;      % If a movie should be saved

% Material parameters

% Parameters in dimensionless scales
a = 1;                  % Length in x-direction
b = 1;                  % Length in y-direction

%% Spatial discretization
Nx = 20;                    % Resolution in x-direction
Ny = 2F_0;                    % Resolution in y-direction
xL = 0;                     % West boundary
xR = a;                     % East boundary
yL = 0;                     % South Boundary
yR = b;                     % North Boundary
x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dx = (xR-xL)/(Nx-1);        % Spatial step size, x-direction
dy = (yR-yL)/(Ny-1);        % Spatial step size, y-direction

% Obtain MMS forcing, solution fields and material properties
[UMMS, VMMS, PMMS, f1, f2, f3, SXXMMS, SXYMMS, SYYMMS, QXMMS, QYMMS, ...
    lambdaMMS, GMMS, alphaMMS, MMMS, kappaMMS] = MMSGeneration();

% Grid used for MMS
gridX = meshgrid(x, y);
xMMS = gridX(:);
gridY = meshgrid(y, x)';
yMMS = gridY(:);

% Obtain the spatially variable material parameters
lambda = reshape(lambdaMMS(xMMS, yMMS), Ny, Nx);
G = reshape(GMMS(xMMS, yMMS), Ny, Nx);
alpha = reshape(alphaMMS(xMMS, yMMS), Ny, Nx);
M = reshape(MMMS(xMMS, yMMS), Ny, Nx);
kappa = reshape(kappaMMS(xMMS, yMMS), Ny, Nx);
K = lambda + 2*G/3;

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid 
% discharge velocity and traction) always have weight one, since that is 
% required for stability. The weight for the Dirichlet conditions 
% (pore pressure and displacement) can be specified as an arbitrary 
% positive number however. A Dirichlet condition can therefore be 
% approximated using Robin boundary conditions using a large 
% weight. A large number will give a more accurate solution, 
% but will increase stiffness of the system. Here follows the structure of 
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

BC = [0 1 0 1;
      1 0 1 0;
      0 1 1 0];
  
% The maximum value is chosen for each parameter to ensure stability
% and that the suggested Dirichlet order is obtained. However, ideally
% one should use the material property at the corresponding grid point.
% For highly variable material properties the system might get
% unnecessarily stiff.
lambda_max = max(lambda(:));
G_max = max(G(:));
kappa_max = max(kappa(:));
M_max = max(M(:));

% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Dirichlet conditions are imposed with the accuracy
% specified by DirichletOrder.
BC = ScaleBC(lambda_max, G_max, kappa_max, x, y, Nx, Ny, DirichletOrder, BC);

% Obtain the stability condition from the boundary conditions and the interior
C = 0.6; % A numeric constant that is dependent of the problem and discretization
dt = C*min(dx^2/(M_max*kappa_max), ...  % From interior/Neumann in x-direction
    min(dy^2/(M_max*kappa_max), ...     % From interior/Neumann in y-direction
    min(dx/(M_max*BC(3, 1)), ...        % From Dirichlet at west boundary
    min(dx/(M_max*BC(3, 2)), ...        % From Dirichlet at east boundary
    min(dy/(M_max*BC(3, 3)), ...        % From Dirichlet at south boundary
    min(dy/(M_max*BC(3, 4))))))));      % From Dirichlet at north boundary

%% Build the matrices used for the discretization of the system
[F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dy, alpha, K, G, kappa, M, ...
        SBPStencil, BC, Order);

%% Time integration system
StartTime = 0;
EndTime = StartTime + 2e-3;
t = StartTime:dt:EndTime;
t_stag = StartTime:0.5*dt:t(end);

% Make sure that we end up at EndTime
if t(end) ~= EndTime
    t_stag(end + 1) = (EndTime + t(end))/2;
    t_stag(end + 1) = EndTime;
    t(end + 1) = EndTime;
end
Nt = length(t);

% Obtain forcing and boundary data for each time step
for i = 1:length(t_stag)
    % Interior forcing
    FIU(:, i) = f1(xMMS, yMMS, t_stag(i));
    FIV(:, i) = f2(xMMS, yMMS, t_stag(i));
    FIP(:, i) = f3(xMMS, yMMS, t_stag(i));
    
    % West boundary data
    BDWU(:, i) = -SXXMMS(0, y, t_stag(i));
    BDWV(:, i) = BC(2, 1)*VMMS(0, y, t_stag(i));
    BDWP(:, i) = -QXMMS(0, y, t_stag(i));
    
    % East boundary data
    BDEU(:, i) = BC(1, 2)*UMMS(1, y, t_stag(i));
    BDEV(:, i) = SXYMMS(1, y, t_stag(i));
    BDEP(:, i) = BC(3, 2)*PMMS(1, y, t_stag(i));
    
    % South boundary data
    BDSU(:, i) = -SXYMMS(x, 0, t_stag(i));
    BDSV(:, i) = BC(2, 3)*VMMS(x, 0, t_stag(i));
    BDSP(:, i) = BC(3, 3)*PMMS(x, 0, t_stag(i));
    
    % North boundary data
    BDNU(:, i) = BC(1, 4)*UMMS(x, 1, t_stag(i));
    BDNV(:, i) = SYYMMS(x, 1, t_stag(i));
    BDNP(:, i) = QYMMS(x, 1, t_stag(i));
end

% Build the forcing function in the interior for the mech eq
FIM = [FIU; FIV];

% Build the boundary data matrices
BM = M_b.W*[BDWU; BDWV] + M_b.E*[BDEU; BDEV] + ...
    M_b.S*[BDSU; BDSV] + M_b.N*[BDNU; BDNV];
BF = F_b.W*BDWP + F_b.E*BDEP + F_b.S*BDSP + F_b.N*BDNP;

% Obtain the total forcing function
FM = -FIM + BM;
FF = -spdiags(M(:), 0, Ny*Nx, Ny*Nx)*FIP + BF;

% Impose analytical solutions at t = startTime as initial conditions.
w = [UMMS(xMMS, yMMS, t(1)); VMMS(xMMS, yMMS, t(1))]; 
p_p = PMMS(xMMS, yMMS, t(1)) + P*w;

for i = 1 : Nt - 1
    % Find the time step for the current iteration
    dt = t(i + 1) - t(i);
    
    % Perform an explicit RK4 time step
    % 1:st stage
    k1 = F_p*p_p + F_u*w + FF(:, 2*i - 1);
    
    % 2:nd stage
    p_p2 = p_p + 0.5*dt*k1;
    w2 = -M_u\(M_p*p_p2 + FM(:, 2*i));
    k2 = F_p*p_p2 + F_u*w2 + FF(:, 2*i);
    
    % 3:rd stage
    p_p3 = p_p + 0.5*dt*k2;
    w3 = -M_u\(M_p*p_p3 + FM(:, 2*i));
    k3 = F_p*p_p2 + F_u*w2 + FF(:, 2*i);
    
    % 4:th stage
    p_p4 = p_p + dt*k3;
    w4 = -M_u\(M_p*p_p4 + FM(:, 2*i + 1));
    k4 = F_p*p_p4 + F_u*w4 + FF(:, 2*i + 1);
    
    % Time  step in the fluid diffusion equation
    p_p = p_p + dt*(k1/6 + k2/3 + k3/3 + k4/6);
    
    % Solve the mechanical equilibrium equation for u and v.
    w = -M_u\(M_p*p_p + FM(:, 2*i + 1));
    
    % Collect variable fields
    u = reshape(w(1:Ny*Nx), Ny, Nx);
    v = reshape(w(Ny*Nx + 1:2*Ny*Nx), Ny, Nx);
    p = reshape(p_p - P*w, Ny, Nx);
    sigma_xx = reshape(Sigma_xx*w - spdiags(alpha(:), 0, Ny*Nx, Ny*Nx)*p_p,Ny,Nx);
    sigma_xy = reshape(Sigma_xy*w, Ny, Nx);
    sigma_yy = reshape(Sigma_yy*w - spdiags(alpha(:), 0, Ny*Nx, Ny*Nx)*p_p, Ny, Nx);
    
    % Get analytical solutions
    u_a = reshape(UMMS(xMMS, yMMS, t_stag(2*i + 1)), Ny, Nx);
    v_a = reshape(VMMS(xMMS, yMMS, t_stag(2*i + 1)), Ny, Nx);
    p_a = reshape(PMMS(xMMS, yMMS, t_stag(2*i + 1)), Ny, Nx);
    sigma_xx_a = reshape(SXXMMS(xMMS, yMMS, t_stag(2*i + 1)), Ny, Nx);
    sigma_xy_a = reshape(SXYMMS(xMMS, yMMS, t_stag(2*i + 1)), Ny, Nx);
    sigma_yy_a = reshape(SYYMMS(xMMS, yMMS, t_stag(2*i + 1)), Ny, Nx);
    
    % Use a plotting function
%     PlotMMS(x, y, t(i), sigma_xx, sigma_xy, sigma_yy, ...
%         sigma_xx_a, sigma_xy_a, sigma_yy_a);
%     PlotMMS(x, y, t(i + 1), u, v, p, u_a, v_a, p_a);
%     Plot2D(x, y, t(i), u, v, p, sigma_xx, sigma_xy, sigma_yy);
%     PlotHeatmap(x, y, t(i + 1), u, v, p);
%     Plot1D(x, y, t(i + 1), u, v, p, ...
%         u_a(:, 4*i + 1), v_a(:, 4*i + 1), p_a(:, 4*i + 1));
%     Plot1DS(x, y, t(i + 1), u, v, p, sigma_xx, sigma_xy, sigma_yy, ...
%         u_a(:, 4*i + 1), v_a(:, 4*i + 1), p_a(:, 4*i + 1), ...
%         sigma_yy_a(:, 4*i + 1));
% 	  Plot1DP(x*x_SC, Ny, t(i)*t_SC, p*p_SC, p_SC*p_a(:, i));
    
    if SaveMovie
        %Give time to resize the window
        if i == 1
            pause(3);
        end
        % Save frame to save as movie
        frames(i) = getframe(gcf);
    end
    i
end

% Save movie
if SaveMovie
    video = VideoWriter('simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ExplicitDiscretization' 