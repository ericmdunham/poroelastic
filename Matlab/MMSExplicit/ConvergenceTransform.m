%% Convergence study of the MMS problem using the explicit scheme
% Script that calculates and presents convergence rates of the problem
% using the SBP-SAT technique with either wide operators, narrow operators
% or the Russian operators (indicated by a flag) to discretize the problem.
% The problem is simulated in 2D and then plots of the convergence rates
% are presented.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ExplicitDiscretization'

% Define type of SBP operators
SBPStencil = 'n';   % Type of SBP-stencil. w - wide, n - narrow, r - Russian
Order = 4;          % order of SBP-operator.
DirichletOrder = 3; % Order of accuracy of the Dirichlet conditions
RefOrd = [2 3];     % Comparison lines in the solution

% Temporal discritization
StartTime = 0;
EndTime = StartTime + 1e-5;

% Spatial resolution and variables for storing the errors.
Res = [10, 20, 30, 40, 50, 60, 70, 80];
NormsU = zeros(1, length(Res));
NormsV = zeros(1, length(Res));
NormsP = zeros(1, length(Res));
NormsSigmaXX = zeros(1, length(Res));
NormsSigmaXY = zeros(1, length(Res));
NormsSigmaYY = zeros(1, length(Res));

%% Compute errors for each resolution
for l = 1:length(Res)
    %% Material parameters
    
    % Parameters in dimensionless scales
    a = 1;                  % Length in x-direction
    b = 1;                  % Length in y-direction
    
    %% Spatial discretization
    xL = 0;                     % West boundary
    xR = a;                     % East boundary
    yL = 0;                     % South Boundary
    yR = b;                     % North Boundary
    Nx = Res(l);                % Resolution in x-direction
    Ny = Res(l);                % Resolution in y-direction
    y = linspace(yL, yR, Ny);   % Spatial grid, x-direction
    dy = (yR - yL)/(Ny - 1);    % Spatial step size, x-direction
    
    % The numerical grid xi living on [0, 1]
    xi = linspace(0, 1, Nx);
    dxi = 1/(Nx - 1);
    
    % Determine the coordinate transformation
    hmaxmin = 10;    % Determines h_max/h_min
    xf = @(beta) (tanh(beta*xi(end)) - tanh(beta*xi(end - 1)))/...
        (tanh(beta*xi(2)) - tanh(beta*xi(1))) - 1/hmaxmin;
    beta = fsolve(xf, 2, optimset('Display','off'));
    x = b*tanh(beta*(xi - 1))/tanh(beta) + b;
    dx = min(diff(x)); % Used for stability condition
    
    % Obtain MMS forcing, solution fields and material properties
    [UMMS, VMMS, PMMS, f1, f2, f3, SXXMMS, SXYMMS, SYYMMS, QXMMS, QYMMS, ...
        lambdaMMS, GMMS, alphaMMS, MMMS, kappaMMS] = MMSGeneration();
    
    % Grid used for MMS
    gridX = meshgrid(x, y);
    xMMS = gridX(:);
    gridY = meshgrid(y, x)';
    yMMS = gridY(:);
    
    % Obtain the spatially variable material parameters
    lambda = reshape(lambdaMMS(xMMS, yMMS), Ny, Nx);
    G = reshape(GMMS(xMMS, yMMS), Ny, Nx);
    alpha = reshape(alphaMMS(xMMS, yMMS), Ny, Nx);
    M = reshape(MMMS(xMMS, yMMS), Ny, Nx);
    kappa = reshape(kappaMMS(xMMS, yMMS), Ny, Nx);
    K = lambda + 2*G/3;
    
    %% Boundary conditions and boundary data
    %
    % The boundary conditions are specified as Robin boundary conditions in a
    % 3x4 matrix. The weight on the Neumann type boundary conditions (discharge
    % velocity for the fluid flow equation and traction for the mechanical
    % equilibrium equation) always have weight one, since that is required for
    % stability. The weight for the Dirichlet conditions can be specified
    % as an arbitrary positive number however. A Dirichlet condition can
    % therefore be approximated using Robin boundary conditions using a large
    % number in the Matrix. A large number will give a more accurate solution,
    % but will increase stiffness of the system. Here follows the structure of
    % the matrix:
    %
    %          W E S N
    %          - - - -
    %       u |       |
    %  BC = v | | | | |
    %       p |       |
    %          - - - -
    %
    % Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
    %                            boundary
    %
    
    BC = [0 1 0 1;
          1 0 1 0;
          0 1 1 0];
    
    % The maximum value is chosen for each parameter to ensure stability
    % and that the suggested Dirichlet order is obtained. However, ideally
    % one should use the material property at the corresponding grid point.
    % For highly variable material properties the system might get
    % unnecessarily stiff.
    lambda_max = max(lambda(:));
    G_max = max(G(:));
    kappa_max = max(kappa(:));
    M_max = max(M(:));
    
    % Scale the boundary conditions based on the material properties and grid
    % spacing, so that the Dirichlet conditions are imposed with the accuracy
    % specified by DirichletOrder.
    % The maximum value is chosen for each parameter to ensure stability
    % and that the suggested Dirichlet order is obtained. However, ideally
    % one should use the material property at the corresponding grid point.
    % For highly variable material properties the system might get
    % unnecessarily stiff.
    BC = ScaleBC(lambda_max, G_max, kappa_max, x, y, Nx, Ny, DirichletOrder, BC);
    
    % Obtain the stability condition from the boundary conditions and the interior
    C = 0.6; % A numeric constant that is dependent of the problem and discretization
    dt = C*min(dx^2/(M_max*kappa_max), ...  % From interior/Neumann in x-direction
        min(dy^2/(M_max*kappa_max), ...     % From interior/Neumann in y-direction
        min(dx/(M_max*BC(3, 1)), ...        % From Dirichlet at west boundary
        min(dx/(M_max*BC(3, 2)), ...        % From Dirichlet at east boundary
        min(dy/(M_max*BC(3, 3)), ...        % From Dirichlet at south boundary
        min(dy/(M_max*BC(3, 4))))))));      % From Dirichlet at north boundary
    
    %% Build the matrices used for the discretization of the system
    [F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
        SpatialOperators(Nx, dxi, Ny, dy, alpha, K, G, kappa, M, ...
        SBPStencil, BC, Order, x', 'x');
    
    %% Time integration system
    t = StartTime:dt:EndTime;
    t_stag = StartTime:0.5*dt:t(end);
    
    % Make sure that we end up at EndTime
    if t(end) ~= EndTime
        t_stag(end + 1) = (EndTime + t(end))/2;
        t_stag(end + 1) = EndTime;
        t(end + 1) = EndTime;
    end
    Nt = length(t)
    
    % Initialize forcing and boundary data
    FIU = zeros(Ny*Nx, length(t_stag));
    FIV = zeros(Ny*Nx, length(t_stag));
    FIP = zeros(Ny*Nx, length(t_stag));
    
    BDWU = zeros(Ny, length(t_stag));
    BDWV = zeros(Ny, length(t_stag));
    BDWP = zeros(Ny, length(t_stag));
    
    BDEU = zeros(Ny, length(t_stag));
    BDEV = zeros(Ny, length(t_stag));
    BDEP = zeros(Ny, length(t_stag));
    
    BDSU = zeros(Nx, length(t_stag));
    BDSV = zeros(Nx, length(t_stag));
    BDSP = zeros(Nx, length(t_stag));
    
    BDNU = zeros(Nx, length(t_stag));
    BDNV = zeros(Nx, length(t_stag));
    BDNP = zeros(Nx, length(t_stag));
    
    % Obtain forcing and boundary data for each time step
    for i = 1:length(t_stag)
        % Interior forcing
        FIU(:, i) = f1(xMMS, yMMS, t_stag(i));
        FIV(:, i) = f2(xMMS, yMMS, t_stag(i));
        FIP(:, i) = f3(xMMS, yMMS, t_stag(i));
        
        % West boundary data
        BDWU(:, i) = -SXXMMS(0, y, t_stag(i));
        BDWV(:, i) = BC(2, 1)*VMMS(0, y, t_stag(i));
        BDWP(:, i) = -QXMMS(0, y, t_stag(i));
        
        % East boundary data
        BDEU(:, i) = BC(1, 2)*UMMS(1, y, t_stag(i));
        BDEV(:, i) = SXYMMS(1, y, t_stag(i));
        BDEP(:, i) = BC(3, 2)*PMMS(1, y, t_stag(i));
        
        % South boundary data
        BDSU(:, i) = -SXYMMS(x, 0, t_stag(i));
        BDSV(:, i) = BC(2, 3)*VMMS(x, 0, t_stag(i));
        BDSP(:, i) = BC(3, 3)*PMMS(x, 0, t_stag(i));
        
        % North boundary data
        BDNU(:, i) = BC(1, 4)*UMMS(x, 1, t_stag(i));
        BDNV(:, i) = SYYMMS(x, 1, t_stag(i));
        BDNP(:, i) = QYMMS(x, 1, t_stag(i));
    end
    
    % Build the forcing function in the interior
    FIM = [FIU; FIV];
    
    % Build the boundary data matrices
    BM = M_b.W*[BDWU; BDWV] + M_b.E*[BDEU; BDEV] + ...
        M_b.S*[BDSU; BDSV] + M_b.N*[BDNU; BDNV];
    BF = F_b.W*BDWP + F_b.E*BDEP + F_b.S*BDSP + F_b.N*BDNP;
    
    % Obtain the total forcing functions
    FM = - FIM + BM;
    FF = -spdiags(M(:), 0, Ny*Nx, Ny*Nx)*FIP + BF;
    
    % Impose analytical solutions at t = startTime as initial conditions.
    w = [UMMS(xMMS, yMMS, t(1)); VMMS(xMMS, yMMS, t(1))];
    p_p = PMMS(xMMS, yMMS, t(1)) + P*w;
    
    for i = 1:Nt - 1
        % Find the time step for the current iteration
        dt = t(i + 1) - t(i);
        
        % Perform a explicit RK4 time step
        % 1:st stage
        k1 = F_p*p_p + F_u*w + FF(:, 2*i - 1);
        
        % 2:nd stage
        p_p2 = p_p + 0.5*dt*k1;
        w2 = -M_u\(M_p*p_p2 + FM(:, 2*i));
        k2 = F_p*p_p2 + F_u*w2 + FF(:, 2*i);
        
        % 3:rd stage
        p_p3 = p_p + 0.5*dt*k2;
        w3 = -M_u\(M_p*p_p3 + FM(:, 2*i));
        k3 = F_p*p_p2 + F_u*w2 + FF(:, 2*i);
        
        % 4:th stage
        p_p4 = p_p + dt*k3;
        w4 = -M_u\(M_p*p_p4 + FM(:, 2*i + 1));
        k4 = F_p*p_p4 + F_u*w4 + FF(:, 2*i + 1);
        
        % Time  step in the fluid diffusion equation
        p_p = p_p + dt*(k1/6 + k2/3 + k3/3 + k4/6);
        
        % Solve the mechanical equilibrium equation for u and v.
        w = -M_u\(M_p*p_p + FM(:, 2*i + 1));
    end
    
    % Collect quantities from V and save the norm of the error.
    u = reshape(w(1:Ny*Nx), Ny, Nx);
    v = reshape(w(Ny*Nx+1:2*Ny*Nx), Ny, Nx);
    p = reshape(p_p - P*w, Ny, Nx);
    SigmaXX = reshape(Sigma_xx*w - spdiags(alpha(:), 0, Ny*Nx, Ny*Nx)*p_p,Ny,Nx);
    SigmaXY = reshape(Sigma_xy*w, Ny, Nx);
    SigmaYY = reshape(Sigma_yy*w - spdiags(alpha(:), 0, Ny*Nx, Ny*Nx)*p_p, Ny, Nx);
    
    uRef = reshape(UMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    vRef = reshape(VMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    pRef = reshape(PMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    SigmaXXRef = reshape(SXXMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    SigmaXYRef = reshape(SXYMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    SigmaYYRef = reshape(SYYMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    
    uErr = reshape(u - uRef, Ny*Nx, 1);
    vErr = reshape(v - vRef, Ny*Nx, 1);
    pErr = reshape(p - pRef, Ny*Nx, 1);
    SigmaXXErr = reshape(SigmaXX - SigmaXXRef, Ny*Nx, 1);
    SigmaXYErr = reshape(SigmaXY - SigmaXYRef, Ny*Nx, 1);
    SigmaYYErr = reshape(SigmaYY - SigmaYYRef, Ny*Nx, 1);
    
    % Obtain the H matrix that is used for the calculation of the error
    if SBPStencil == 'r'
        addpath '../Helpers/OperatorsRussian'
        [Dp, Dm, HIx] = SBP(Nx, dx, Order);
        [Dp, Dm, HIy] = SBP(Ny, dy, Order);
        rmpath '../Helpers/OperatorsRussian'
    else
        addpath '../Helpers/OperatorsWide'
        [D, HIx] = SBP(Nx - 1, dx, Order);
        [D, HIy] = SBP(Ny - 1, dy, Order);
        rmpath '../Helpers/OperatorsWide'
    end
    Hx = inv(HIx);
    Hy = inv(HIy);
    H = kron(Hx, Hy);
    
    % Calculate the norms for the different fields
    NormsU(l) = sqrt(uErr'*H*uErr);
    NormsV(l) = sqrt(vErr'*H*vErr);
    NormsP(l) = sqrt(pErr'*H*pErr);
    NormsSigmaXX(l) = sqrt(SigmaXXErr'*H*SigmaXXErr);
    NormsSigmaXY(l) = sqrt(SigmaXYErr'*H*SigmaXYErr);
    NormsSigmaYY(l) = sqrt(SigmaYYErr'*H*SigmaYYErr);
    
    % Print the iteration number and the convergence rate
    l
    if l > 1
        log10(NormsP(l - 1)/NormsP(l))/log10((Res(l)^2/Res(l - 1)^2)^(1/2))
    end
end

% Save convergence data
save('ConvergenceData/Data.mat', 'NormsP', 'NormsU', 'NormsV', ...
    'NormsSigmaXX', 'NormsSigmaXY', 'NormsSigmaYY', 'Res', ...
    'RefOrd', 'StartTime', 'EndTime', 'Order', 'DirichletOrder', ...
    'SBPStencil');

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ExplicitDiscretization'
