%% Simulation of the MMS problem
% Script that simulates the MMS problem with the SBP-SAT technique. The
% script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and plots in 1D or 2D can be presented by using different
% help functions.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ImplicitDiscretization'

% Define type of SBP operators
SBPStencil = 'n';       % Type of SBP-stencil. w - wide, n - narrow, r - Russian
Order = 4;              % Order of SBP-operator.
DirichletOrder = 3;     % Order of accuracy of the Dirichlet condition
RefOrd = [2, 3, 4];     % Comparison lines in the solution

% Temporal discretization
StartTime = 0;
EndTime = StartTime + 1e-5;
TimeScaleFactor = 5;

% Spatial resolution and variables for storing the errors.
Res = [10, 20, 30, 40, 50, 60, 70, 80];
NormsU = zeros(1, length(Res));
NormsV = zeros(1, length(Res));
NormsP = zeros(1, length(Res));
NormsSigmaXX = zeros(1, length(Res));
NormsSigmaXY = zeros(1, length(Res));
NormsSigmaYY = zeros(1, length(Res));

%% Compute errors for each resolution
for l = 1:length(Res)
    
    %% Material parameters
    
    % Parameters in dimensionless scales
    a = 1;                  % Length in x-direction
    b = 1;                  % Length in y-direction
    
    %% Spatial discretization
    Nx = Res(l);                % Resolution in x-direction
    Ny = Res(l);                % Resolution in y-direction
    xL = 0;                     % West boundary
    xR = a;                     % East boundary
    yL = 0;                     % South Boundary
    yR = b;                     % North Boundary
    y = linspace(yL, yR, Ny);   % Spatial grid, x-direction
    dy = (yR - yL)/(Ny - 1);    % Spatial step size, x-direction
    
    % The numerical grid xi living on [0, 1]
    xi = linspace(0, 1, Ny);
    dxi = 1/(Ny - 1);
    
    % Determine the coordinate transformation
    hmaxmin = 10;    % Determines h_max/h_min
    xf = @(beta) (tanh(beta*xi(end)) - tanh(beta*xi(end - 1)))/...
        (tanh(beta*xi(2)) - tanh(beta*xi(1))) - 1/hmaxmin;
    beta = fsolve(xf, 2, optimset('Display','off'));
    x = b*tanh(beta*(xi - 1))/tanh(beta) + b;
    dx  = min(diff(x));
    
    %% Obtain MMS forcing, solution fields and material properties
    [UMMS, VMMS, PMMS, f1, f2, f3, SXXMMS, SXYMMS, SYYMMS, QXMMS, QYMMS, ...
        lambdaMMS, GMMS, alphaMMS, MMMS, kappaMMS] = MMSGeneration();
    
    % Grid used for MMS
    gridX = meshgrid(x, y);
    xMMS = gridX(:);
    gridY = meshgrid(y, x)';
    yMMS = gridY(:);
    
    % Obtain the spatially variable material parameters
    lambda = reshape(lambdaMMS(xMMS, yMMS), Ny, Nx);
    G = reshape(GMMS(xMMS, yMMS), Ny, Nx);
    alpha = reshape(alphaMMS(xMMS, yMMS), Ny, Nx);
    M = reshape(MMMS(xMMS, yMMS), Ny, Nx);
    kappa = reshape(kappaMMS(xMMS, yMMS), Ny, Nx);
    K = lambda + 2*G/3;
    
    %% Boundary conditions and boundary data
    %
    % The boundary conditions are specified as Robin boundary conditions in a
    % 3x4 matrix. The weight on the Neumann type boundary conditions (fluid
    % discharge velocity and traction) always have weight one, since that is
    % required for stability. The weight for the Dirichlet conditions
    % (pore pressure and displacement) can be specified as an arbitrary
    % positive number however. A Dirichlet condition can therefore be
    % approximated using Robin boundary conditions using a large
    % weight. A large number will give a more accurate solution,
    % but will increase stiffness of the system. Here follows the structure of
    % the matrix:
    %
    %          W E S N
    %          - - - -
    %       u |       |
    %  BC = v | | | | |
    %       p |       |
    %          - - - -
    %
    % Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
    %                            boundary
    %
    
    BC = [0 1 0 1;
          1 0 1 0;
          0 1 1 0];
    
    % Scale the boundary conditions based on the material properties and grid
    % spacing, so that the Dirichlet conditions are imposed with the accuracy
    % specified by DirichletOrder.
    % The maximum value is chosen for each parameter to ensure stability
    % and that the suggested Dirichlet order is obtained. However, ideally
    % one should use the material property at the corresponding grid point.
    % For highly variable material properties the system might get
    % unnecessarily stiff.
    BC = ScaleBC(max(lambda(:)), max(G(:)), max(kappa(:)), x, y, ...
        Nx, Ny, DirichletOrder, BC);
    
    % Build the matrices used for the discretization of the system
    [MSyst, ASyst, M_b, F_b, Sigma_xx, Sigma_xy, Sigma_yy] = SpatialOperators(...
        Nx, dxi, Ny, dy, alpha, K, G, kappa, M, SBPStencil, BC, Order, x', 'x');
    
    % Temporal discritization
    Nt = TimeScaleFactor*Nx;
    Nts = 4*Nt - 3;
    t = linspace(StartTime, EndTime, Nt);
    t_stag = linspace(StartTime, EndTime, Nts);
    dt = t_stag(2) - t_stag(1);
    
    % Initialize forcings and boundary data
    FIU = zeros(Ny*Nx, length(t_stag));
    FIV = zeros(Ny*Nx, length(t_stag));
    FIP = zeros(Ny*Nx, length(t_stag));
    
    BDWU = zeros(Ny, length(t_stag));
    BDWV = zeros(Ny, length(t_stag));
    BDWP = zeros(Ny, length(t_stag));
    
    BDEU = zeros(Ny, length(t_stag));
    BDEV = zeros(Ny, length(t_stag));
    BDEP = zeros(Ny, length(t_stag));
    
    BDSU = zeros(Nx, length(t_stag));
    BDSV = zeros(Nx, length(t_stag));
    BDSP = zeros(Nx, length(t_stag));
    
    BDNU = zeros(Nx, length(t_stag));
    BDNV = zeros(Nx, length(t_stag));
    BDNP = zeros(Nx, length(t_stag));
    
    % Obtain forcings and boundary data for each timestep
    for i = 1:length(t_stag)
        % Interior forcing
        FIU(:, i) = f1(xMMS, yMMS, t_stag(i));
        FIV(:, i) = f2(xMMS, yMMS, t_stag(i));
        FIP(:, i) = f3(xMMS, yMMS, t_stag(i));
        
        % West boundary data
        BDWU(:, i) = -SXXMMS(0, y, t_stag(i));
        BDWV(:, i) = BC(2, 1)*VMMS(0, y, t_stag(i));
        BDWP(:, i) = -QXMMS(0, y, t_stag(i));
        
        % East boundary data
        BDEU(:, i) = BC(1, 2)*UMMS(1, y, t_stag(i));
        BDEV(:, i) = SXYMMS(1, y, t_stag(i));
        BDEP(:, i) = BC(3, 2)*PMMS(1, y, t_stag(i));
        
        % South boundary data
        BDSU(:, i) = -SXYMMS(x, 0, t_stag(i));
        BDSV(:, i) = BC(2, 3)*VMMS(x, 0, t_stag(i));
        BDSP(:, i) = BC(3, 3)*PMMS(x, 0, t_stag(i));
        
        % North boundary data
        BDNU(:, i) = BC(1, 4)*UMMS(x, 1, t_stag(i));
        BDNV(:, i) = SYYMMS(x, 1, t_stag(i));
        BDNP(:, i) = QYMMS(x, 1, t_stag(i));
    end
    
    % Build the forcing function in the interior
    FI = [FIU; FIV; FIP];
    
    % Build the boundary data matrix
    B = [M_b.W*[BDWU; BDWV] + M_b.E*[BDEU; BDEV] + ...
        M_b.S*[BDSU; BDSV] + M_b.N*[BDNU; BDNV];
        F_b.W*BDWP + F_b.E*BDEP + F_b.S*BDSP + F_b.N*BDNP];
    
    % Obtain the total forcing function
    F = -FI + B;
    
    % Impose analytical solutions at t = startTime as initial conditions.
    V = [UMMS(xMMS, yMMS, t(1)); VMMS(xMMS, yMMS, t(1)); PMMS(xMMS, yMMS, t(1))];
    
    % Perform the time steps using BDF4
    for i = 1 : Nt - 1
        V1 = (MSyst - dt*ASyst)\(MSyst*V + dt*F(:, 4*i - 2));
        V2 = (MSyst - 2*dt/3*ASyst)\(MSyst*(4/3*V1 - 1/3*V) + 2*dt/3*F(:, 4*i - 1));
        V3 = (MSyst - 6*dt/11*ASyst)\(MSyst*(18/11*V2 - 9/11*V1 + 2/11*V) + 6*dt/11*F(:, 4*i));
        V = (MSyst - 12*dt/25*ASyst)\(MSyst*(48/25*V3 - 36/25*V2 + 16/25*V1 - 3/25*V) + 12*dt/25*F(:, 4*i + 1));
    end
    
    % Collect quantities
    u = reshape(V(1:Ny*Nx), Ny, Nx);
    v = reshape(V(Ny*Nx + 1:2*Ny*Nx), Ny, Nx);
    p = reshape(V(2*Ny*Nx + 1:3*Ny*Nx), Ny, Nx);
    SigmaXX = reshape(Sigma_xx*V, Ny, Nx);
    SigmaXY = reshape(Sigma_xy*V, Ny, Nx);
    SigmaYY = reshape(Sigma_yy*V, Ny, Nx);
    
    uRef = reshape(UMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    vRef = reshape(VMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    pRef = reshape(PMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    SigmaXXRef = reshape(SXXMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    SigmaXYRef = reshape(SXYMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    SigmaYYRef = reshape(SYYMMS(xMMS, yMMS, t_stag(end)), Ny, Nx);
    
    uErr = reshape(u - uRef, Ny*Nx, 1);
    vErr = reshape(v - vRef, Ny*Nx, 1);
    pErr = reshape(p - pRef, Ny*Nx, 1);
    SigmaXXErr = reshape(SigmaXX - SigmaXXRef, Ny*Nx, 1);
    SigmaXYErr = reshape(SigmaXY - SigmaXYRef, Ny*Nx, 1);
    SigmaYYErr = reshape(SigmaYY - SigmaYYRef, Ny*Nx, 1);
    
    % Obtain the H and J matrix that are used for the error
    if SBPStencil == 'r'
        addpath '../Helpers/OperatorsRussian'
        [Dp, D, HI] = SBP(Ny, dxi, Order);
        rmpath '../Helpers/OperatorsRussian'
    else
        addpath '../Helpers/OperatorsWide'
        [D, HI] = SBP(Ny - 1, dxi, Order);
        rmpath '../Helpers/OperatorsWide'
    end
    H = inv(HI);
    J = spdiags(D*y', 0, Ny, Ny);
    J = kron(speye(Nx, Nx), J);
    H = kron(H, H);
    
    % Calculate the norms for the different fields
    NormsU(l) = sqrt(uErr'*J*H*uErr);
    NormsV(l) = sqrt(vErr'*J*H*vErr);
    NormsP(l) = sqrt(pErr'*J*H*pErr);
    NormsSigmaXX(l) = sqrt(SigmaXXErr'*J*H*SigmaXXErr);
    NormsSigmaXY(l) = sqrt(SigmaXYErr'*J*H*SigmaXYErr);
    NormsSigmaYY(l) = sqrt(SigmaYYErr'*J*H*SigmaYYErr);
    
    % Print the iteration number and the convergence rate
    l
    if l > 1
        log10(NormsP(l - 1)/NormsP(l))/log10((Res(l)^2/Res(l - 1)^2)^(1/2))
    end
end

% Save convergence data
save('ConvergenceData/Data.mat', 'NormsP', 'NormsU', 'NormsV', ...
    'NormsSigmaXX', 'NormsSigmaXY', 'NormsSigmaYY', 'Res', ...
    'RefOrd', 'StartTime', 'EndTime', 'TimeScaleFactor', ...
    'Order', 'DirichletOrder', 'SBPStencil');

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ImplicitDiscretization'
