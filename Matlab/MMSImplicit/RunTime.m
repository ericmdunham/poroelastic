%% Simulation of the MMS problem
% Script that simulates the MMS problem with the SBP-SAT technique. The
% script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and plots in 1D or 2D can be presented by using different
% help functions.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ImplicitDiscretization'

% Define type of SBP operators
SBPStencil = 'n';   % Type of SBP-stencil. w - wide, n - narrow, r - Russian
Order = 2;          % order of SBP-operator.
DirichletOrder = 2; % Order of accuracy of the Dirichlet conditions

% Spatial resolution and variables for storing the errors.
Res = [10, 20, 30, 40, 50, 60, 70, 80];
Reruns = 5;
RunTimes = zeros(5,length(Res));

%% Compute run time for each resolution over several reruns
for k = 1:Reruns
    % Loop over resolutions
    for l = 1:length(Res)
        
        %% Material parameters
        
        % Parameters in dimensionless scales
        a = 1;                  % Length in x-direction
        b = 1;                  % Length in y-direction
        
        %% Spatial discretization
        Nx = Res(l);                % Resolution in x-direction
        Ny = Res(l);                % Resolution in y-direction
        xL = 0;                     % West boundary
        xR = a;                     % East boundary
        yL = 0;                     % South Boundary
        yR = b;                     % North Boundary
        x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
        y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
        dx = (xR-xL)/(Nx-1);        % Spatial step size, x-direction
        dy = (yR-yL)/(Ny-1);        % Spatial step size, y-direction
        
        %% Obtain MMS forcing, solution fields and material properties
        [UMMS, VMMS, PMMS, f1, f2, f3, SXXMMS, SXYMMS, SYYMMS, QXMMS, QYMMS, ...
            lambdaMMS, GMMS, alphaMMS, MMMS, kappaMMS] = MMSGeneration();
        
        % Grid used for MMS
        gridX = meshgrid(x, y);
        xMMS = gridX(:);
        gridY = meshgrid(y, x)';
        yMMS = gridY(:);
        
        % Obtain the spatially variable material parameters
        lambda = reshape(lambdaMMS(xMMS, yMMS), Ny, Nx);
        G = reshape(GMMS(xMMS, yMMS), Ny, Nx);
        alpha = reshape(alphaMMS(xMMS, yMMS), Ny, Nx);
        M = reshape(MMMS(xMMS, yMMS), Ny, Nx);
        kappa = reshape(kappaMMS(xMMS, yMMS), Ny, Nx);
        K = lambda + 2*G/3;
        
        %% Boundary conditions and boundary data
        %
        % The boundary conditions are specified as Robin boundary conditions in a
        % 3x4 matrix. The weight on the Neumann type boundary conditions (fluid
        % discharge velocity and traction) always have weight one, since that is
        % required for stability. The weight for the Dirichlet conditions
        % (pore pressure and displacement) can be specified as an arbitrary
        % positive number however. A Dirichlet condition can therefore be
        % approximated using Robin boundary conditions using a large
        % weight. A large number will give a more accurate solution,
        % but will increase stiffness of the system. Here follows the structure of
        % the matrix:
        %
        %          W E S N
        %          - - - -
        %       u |       |
        %  BC = v | | | | |
        %       p |       |
        %          - - - -
        %
        % Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
        %                            boundary
        %
        
        BC = [0 1 0 1;
            1 0 1 0;
            0 1 1 0];
        
        % The maximum value is chosen for each parameter to ensure stability
        % and that the suggested Dirichlet order is obtained. However, ideally
        % one should use the material property at the corresponding grid point.
        % For highly variable material properties the system might get
        % unnecessarily stiff.
        lambda_max = max(lambda(:));
        G_max = max(G(:));
        kappa_max = max(kappa(:));
        M_max = max(M(:));
        
        % Scale the boundary conditions based on the material properties and grid
        % spacing, so that the Dirichlet conditions are imposed with the accuracy
        % specified by DirichletOrder.
        % The maximum value is chosen for each parameter to ensure stability
        % and that the suggested Dirichlet order is obtained. However, ideally
        % one should use the material property at the corresponding grid point.
        % For highly variable material properties the system might get
        % unnecessarily stiff.
        BC = ScaleBC(lambda_max, G_max, kappa_max, x, y, Nx, Ny, DirichletOrder, BC);
        
        % Obtain the stability condition from the boundary conditions and the interior
        C = 0.6; % A numeric constant that is dependent of the problem and discretization
        dt = C*min(dx^2/(M_max*kappa_max), ...  % From interior/Neumann in x-direction
            min(dy^2/(M_max*kappa_max), ...     % From interior/Neumann in y-direction
            min(dx/(M_max*BC(3, 1)), ...        % From Dirichlet at west boundary
            min(dx/(M_max*BC(3, 2)), ...        % From Dirichlet at east boundary
            min(dy/(M_max*BC(3, 3)), ...        % From Dirichlet at south boundary
            min(dy/(M_max*BC(3, 4))))))));      % From Dirichlet at north boundary
        
        % Build the matrices used for the discretization of the system
        [MSyst, ASyst, M_b, F_b, Sigma_xx, Sigma_xy, Sigma_yy] = SpatialOperators(...
            Nx, dx, Ny, dy, alpha, K, G, kappa, M, SBPStencil, BC, Order);
        
        % Temporal discritization
        StartTime = 0;
        EndTime = StartTime + 100*dt;
        t = StartTime:dt:EndTime;
        Nt = length(t);
        Nts = 4*Nt - 3;
        t = linspace(StartTime, EndTime, Nt);
        t_stag = linspace(StartTime, EndTime, Nts);
        
        % Initialize forcings and boundary data
        FIU = zeros(Ny*Nx, length(t_stag));
        FIV = zeros(Ny*Nx, length(t_stag));
        FIP = zeros(Ny*Nx, length(t_stag));
        
        BDWU = zeros(Ny, length(t_stag));
        BDWV = zeros(Ny, length(t_stag));
        BDWP = zeros(Ny, length(t_stag));
        
        BDEU = zeros(Ny, length(t_stag));
        BDEV = zeros(Ny, length(t_stag));
        BDEP = zeros(Ny, length(t_stag));
        
        BDSU = zeros(Nx, length(t_stag));
        BDSV = zeros(Nx, length(t_stag));
        BDSP = zeros(Nx, length(t_stag));
        
        BDNU = zeros(Nx, length(t_stag));
        BDNV = zeros(Nx, length(t_stag));
        BDNP = zeros(Nx, length(t_stag));
        
        % Obtain forcings and boundary data for each timestep
        for i = 1:length(t_stag)
            % Interior forcing
            FIU(:, i) = f1(xMMS, yMMS, t_stag(i));
            FIV(:, i) = f2(xMMS, yMMS, t_stag(i));
            FIP(:, i) = f3(xMMS, yMMS, t_stag(i));
            
            % West boundary data
            BDWU(:, i) = -SXXMMS(0, y, t_stag(i));
            BDWV(:, i) = BC(2, 1)*VMMS(0, y, t_stag(i));
            BDWP(:, i) = -QXMMS(0, y, t_stag(i));
            
            % East boundary data
            BDEU(:, i) = BC(1, 2)*UMMS(1, y, t_stag(i));
            BDEV(:, i) = SXYMMS(1, y, t_stag(i));
            BDEP(:, i) = BC(3, 2)*PMMS(1, y, t_stag(i));
            
            % South boundary data
            BDSU(:, i) = -SXYMMS(x, 0, t_stag(i));
            BDSV(:, i) = BC(2, 3)*VMMS(x, 0, t_stag(i));
            BDSP(:, i) = BC(3, 3)*PMMS(x, 0, t_stag(i));
            
            % North boundary data
            BDNU(:, i) = BC(1, 4)*UMMS(x, 1, t_stag(i));
            BDNV(:, i) = SYYMMS(x, 1, t_stag(i));
            BDNP(:, i) = QYMMS(x, 1, t_stag(i));
        end
        
        % Build the forcing function in the interior
        FI = [FIU; FIV; FIP];
        
        % Build the boundary data matrix
        B = [M_b.W*[BDWU; BDWV] + M_b.E*[BDEU; BDEV] + ...
            M_b.S*[BDSU; BDSV] + M_b.N*[BDNU; BDNV];
            F_b.W*BDWP + F_b.E*BDEP + F_b.S*BDSP + F_b.N*BDNP];
        
        % Obtain the total forcing function
        F = -FI + B;
        
        % Impose analytical solutions at t = startTime as initial conditions.
        V = [UMMS(xMMS, yMMS, t(1)); VMMS(xMMS, yMMS, t(1)); PMMS(xMMS, yMMS, t(1))];
        
        %Start timer
        tStart = tic;
        % Perform the time steps using BDF4
        for i = 1 : Nt - 1
            V1 = (MSyst - dt*ASyst)\(MSyst*V + dt*F(:, 4*i - 2));
            V2 = (MSyst - 2*dt/3*ASyst)\(MSyst*(4/3*V1 - 1/3*V) + 2*dt/3*F(:, 4*i - 1));
            V3 = (MSyst - 6*dt/11*ASyst)\(MSyst*(18/11*V2 - 9/11*V1 + 2/11*V) + 6*dt/11*F(:, 4*i));
            V = (MSyst - 12*dt/25*ASyst)\(MSyst*(48/25*V3 - 36/25*V2 + 16/25*V1 - 3/25*V) + 12*dt/25*F(:, 4*i + 1));
        end
        %Stop timer
        tElapsed = toc(tStart);
        RunTimes(k,l) = tElapsed;
        
        l   % Print out the iteration number
    end
    %Print out the rerun number
    k
end

% Display and save run time data
MinRunTimes = min(RunTimes)
save('ImplicitRunTimes.mat','MinRunTimes');

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/MMS' ...
    '../Helpers/ImplicitDiscretization'
