%% Simulation of Mandel's problem
% Script that simulates Mandel's problem with the SBP-SAT technique. The
% script uses either wide operators, narrow operators or the Russian
% operators (indicated by a flag) to discretize the problem. The problem is
% solved in 2D and plots in 1D or 2D can be presented by using different
% help functions.
%
% Author: Kim Torberntsson, Vidar Stiernstrom

clear all
close all

% Add paths for helper functions
addpath '../Helpers' '../Helpers/Mandel' ...
    '../Helpers/ImplicitDiscretization' '../Helpers/MMS'

%% Setup

% Define type of SBP operators
SBPStencil = 'n';	% Type of SBP-stencil. w - wide, n - narrow, r - russian
Order = 4;        	% order of SBP-operator.
DirichletOrder = 3; % Order of accuracy of the Dirichlet conditions
SaveMovie = 0;      % If a movie should be saved

% Material parameters

% Parameters in dimensionless scales
a = 1;                  % Length in x-direction
b = 1;                  % Length in y-direction
G = 1;                  % Shear modulus
kappa = 1;              % k/my, k-permiability, my fluid viscoscity

% Dimensionless parameters
B = 0.8;                % Skemptons paramater
nu = 0.2;               % Poissions ratio (drained)
nu_u = 0.4;             % Poissons ratio (undrained)

% Compute other poroelastic material parameters;
[K, alpha, M, c, lambda] = MaterialParameters(G,kappa,B,nu,nu_u);

%% Spatial discretization

Nx = 20;                    % Resolution in x-direction
Ny = 20;                    % Resolution in y-direction
xL = 0;                     % West boundary
xR = a;                     % East boundary
yL = 0;                     % South Boundary
yR = b;                     % North Boundary
x = linspace(xL,xR,Nx);     % Spatial grid, x-direction
y = linspace(yL,yR,Ny);     % Spatial grid, y-direction
dx = (xR-xL)/(Nx-1);        % Spatial step size, x-direction
dy = (yR-yL)/(Ny-1);        % Spatial step size, y-direction

%% Boundary conditions and boundary data
%
% The boundary conditions are specified as Robin boundary conditions in a
% 3x4 matrix. The weight on the Neumann type boundary conditions (fluid 
% discharge velocity and traction) always have weight one, since that is 
% required for stability. The weight for the Dirichlet conditions 
% (pore pressure and displacement) can be specified as an arbitrary 
% positive number however. A Dirichlet condition can therefore be 
% approximated using Robin boundary conditions using a large 
% weight. A large number will give a more accurate solution, 
% but will increase stiffness of the system. Here follows the structure of 
% the matrix:
%
%          W E S N
%          - - - -
%       u |       |
%  BC = v | | | | |
%       p |       |
%          - - - -
%
% Example: BC(3, 2) = 0 <--> Condition on discharge velocity on East
%                            boundary
%

BC = [0 1 0 1;
      1 0 1 0;
      0 1 1 0];

% Scale the boundary conditions based on the material properties and grid
% spacing, so that the Dirichlet conditions are imposed with the accuracy
% specified by DirichletOrder.
BC = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC);

% Build the matrices used for the discretization of the system
[MSyst, ASyst, M_b, F_b, Sigma_xx, Sigma_xy, Sigma_yy] = SpatialOperators(...
    Nx, dx, Ny, dy, alpha*ones(Ny,Nx), K*ones(Ny,Nx), G*ones(Ny,Nx), ...
    kappa*ones(Ny,Nx), M*ones(Ny,Nx), SBPStencil, BC, Order);

%% Temporal discritization
StartTime = 0;
EndTime = StartTime + 2e-3;
Nt = 80;
Nts = 4*Nt - 3;
t = linspace(StartTime, EndTime, Nt);
t_stag = linspace(StartTime, EndTime, Nts);
dt = t_stag(2) - t_stag(1);

% Obtain MMS forcings and solution fields functions
[UMMS, VMMS, PMMS, f1, f2, f3, SXXMMS, SXYMMS, SYYMMS, QXMMS, QYMMS] = ...
    PoroelasticMMSHomogeneous(lambda, G, M, kappa, alpha);

% Grid used for MMS
gridX = meshgrid(x, y);
xMMS = gridX(:);
gridY = meshgrid(y, x)';
yMMS = gridY(:);

% Obtain forcings and boundary data for each timestep
for i = 1:length(t_stag)
    % Interior forcing
    FIU(:, i) = f1(xMMS, yMMS, t_stag(i));
    FIV(:, i) = f2(xMMS, yMMS, t_stag(i));
    FIP(:, i) = f3(xMMS, yMMS, t_stag(i));
    
    % West boundary data
    BDWU(:, i) = -SXXMMS(0, y, t_stag(i));
    BDWV(:, i) = BC(2, 1)*VMMS(0, y, t_stag(i));
    BDWP(:, i) = -QXMMS(0, y, t_stag(i));
    
    % East boundary data
    BDEU(:, i) = BC(1, 2)*UMMS(1, y, t_stag(i));
    BDEV(:, i) = SXYMMS(1, y, t_stag(i));
    BDEP(:, i) = BC(3, 2)*PMMS(1, y, t_stag(i));
    
    % South boundary data
    BDSU(:, i) = -SXYMMS(x, 0, t_stag(i));
    BDSV(:, i) = BC(2, 3)*VMMS(x, 0, t_stag(i));
    BDSP(:, i) = BC(3, 3)*PMMS(x, 0, t_stag(i));
    
    % North boundary data
    BDNU(:, i) = BC(1, 4)*UMMS(x, 1, t_stag(i));
    BDNV(:, i) = SYYMMS(x, 1, t_stag(i));
    BDNP(:, i) = QYMMS(x, 1, t_stag(i));
end

% Build the forcing function in the interior
FI = [FIU; FIV; FIP];

% Build the boundary data matrix
B = [M_b.W*[BDWU; BDWV] + M_b.E*[BDEU; BDEV] + ...
    M_b.S*[BDSU; BDSV] + M_b.N*[BDNU; BDNV];
    F_b.W*BDWP + F_b.E*BDEP + F_b.S*BDSP + F_b.N*BDNP];

% Obtain the total forcing function
F = -FI + B;

% Impose analytical solutions at t = startTime as initial conditions.
V = [UMMS(xMMS, yMMS, t(1)); VMMS(xMMS, yMMS, t(1)); PMMS(xMMS, yMMS, t(1))];

for i = 1 : Nt - 1
    % Perform an BDF4 time step
    V1 = (MSyst - dt*ASyst)\(MSyst*V + dt*F(:, 4*i - 2));
    V2 = (MSyst - 2*dt/3*ASyst)\(MSyst*(4/3*V1 - 1/3*V) + 2*dt/3*F(:, 4*i - 1));
    V3 = (MSyst - 6*dt/11*ASyst)\(MSyst*(18/11*V2 - 9/11*V1 + 2/11*V) + 6*dt/11*F(:, 4*i));
    V = (MSyst - 12*dt/25*ASyst)\(MSyst*(48/25*V3 - 36/25*V2 + 16/25*V1 - 3/25*V) + 12*dt/25*F(:, 4*i + 1));
    
    % Collect variable fields
    u = reshape(V(1:Ny*Nx), Ny, Nx);
    v = reshape(V(Ny*Nx + 1:2*Ny*Nx), Ny, Nx);
    p = reshape(V(2*Ny*Nx + 1:3*Ny*Nx), Ny, Nx);
    sigma_xx = reshape(Sigma_xx*V, Ny, Nx);
    sigma_xy = reshape(Sigma_xy*V, Ny, Nx);
    sigma_yy = reshape(Sigma_yy*V, Ny, Nx);
    
    % Get analytical solutions
    u_a = reshape(UMMS(xMMS, yMMS, t_stag(4*i + 1)), Ny, Nx);
    v_a = reshape(VMMS(xMMS, yMMS, t_stag(4*i + 1)), Ny, Nx);
    p_a = reshape(PMMS(xMMS, yMMS, t_stag(4*i + 1)), Ny, Nx);
    sigma_xx_a = reshape(SXXMMS(xMMS, yMMS, t_stag(4*i + 1)), Ny, Nx);
    sigma_xy_a = reshape(SXYMMS(xMMS, yMMS, t_stag(4*i + 1)), Ny, Nx);
    sigma_yy_a = reshape(SYYMMS(xMMS, yMMS, t_stag(4*i + 1)), Ny, Nx);
    
    % Use a plotting function
%     PlotMMS(x, y, t(i), sigma_xx, sigma_xy, sigma_yy, ...
%         sigma_xx_a, sigma_xy_a, sigma_yy_a);
    PlotMMS(x, y, t(i + 1), u, v, p, u_a, v_a, p_a);
%     Plot2D(x, y, t(i), u, v, p, sigma_xx, sigma_xy, sigma_yy);
%      PlotHeatmap(x, y, t(i + 1), u, v, p);
%     Plot1D(x, y, t(i + 1), u, v, p, ...
%         u_a(:, 4*i + 1), v_a(:, 4*i + 1), p_a(:, 4*i + 1));
%     Plot1DS(x, y, t(i + 1), u, v, p, sigma_xx, sigma_xy, sigma_yy, ...
%         u_a(:, 4*i + 1), v_a(:, 4*i + 1), p_a(:, 4*i + 1), ...
%         sigma_yy_a(:, 4*i + 1));
% 	  Plot1DP(x*x_SC, Ny, t(i)*t_SC, p*p_SC, p_SC*p_a(:, i));
    
    if SaveMovie
        %Give time to resize the window
        if i == 1
            pause(3);
        end
        % Save frame to save as movie
        frames(i) = getframe(gcf);
    end
end

% Save movie
if SaveMovie
    video = VideoWriter('simulation.avi');
    open(video);
    writeVideo(video, frames);
    close(video)
end

% Remove paths for helper functions
rmpath '../Helpers' '../Helpers/Mandel' ...
    '../Helpers/ImplicitDiscretization'