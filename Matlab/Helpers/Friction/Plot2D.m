%% 2D plot of u, v, p, sigma_xx, sigma_xy and sigma_yy in friction law
%
%   Input:
%   Vector x        - Spatial grid in x-direction
%   Vector y        - Spatial grid in y-direction
%   Scalar t        - Time point
%   Vector u        - Solution vector of the horisontal displacement
%   Vector v        - Solution vector of the vertical displacement
%   Vector p        - Solution vector of the pore pressure
%   Vector sigma_xx - Stress component
%   Vector sigma_xy - Stress component
%   Vector sigma_yy - Stress component
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = Plot2D(x, y, t, u, v, p, sigma_xx, sigma_xy, sigma_yy)

% --- Plotting of u, v and p ---

subplot(2,3,1);
surf(x, y, u);
%colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('u [m]');

subplot(2,3,2);
surf(x, y, v);
%colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('v [m]');

subplot(2,3,3);
surf(x, y, p);
%colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('p [MPa]');

% --- Plotting of sigma_xx, sigma_xy and sigma_yy ---

subplot(2, 3, 4);
surf(x, y, sigma_xx);
%colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('\sigma_{xx} [MPa]');

subplot(2, 3, 5);
surf(x, y, sigma_xy);
%colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('\sigma_{xy} [MPa]');

subplot(2, 3, 6);
surf(x, y, sigma_yy);
%colorbar();
xlabel('x [km]');
ylabel('y [km]');
title('\sigma_{yy} [MPa]');

suptitle(sprintf('t = %f [s]', t));
pause(0.001);

end