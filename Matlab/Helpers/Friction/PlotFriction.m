%% 2D plot of u, v, p, sigma_xx, sigma_xy and sigma_yy in friction law
%
%   Input:
%   Vector x            - Spatial grid in x-direction
%   Vector y            - Spatial grid in y-direction
%   Vector t            - Time vector
%   Vector V_P          - Slip rate as function of time
%   Vector tausig_P     - Effective strain over effective stress as 
%                       - function of time
%   Vector fss          - Steady state friction as function of time
%   Vector V            - Slip rate at fault
%   Vector u            - Horisontal displacement
%   Vector p            - Pore pressure at fault
%   Vector sigma_yy     - Stress component at fault
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = PlotFriction(x, y, t, V_P, tausig_P, fss, V, u, p, sigma_yy)

% Plot slip and friction on fault
subplot(2, 4, 1:2);
semilogx(V_P, tausig_P, V_P, fss);
xlabel('Slip rate V [mm/s]');
ylabel('f');

subplot(2, 4, 3);
semilogy(x, V);
xlabel('x [m]')
ylabel('Slip rate V [mm/s]');

subplot(2, 4, 4);
plot(x, u(:));
xlabel('x [m]');
ylabel('Slip u [mm]');

subplot(2, 4, 5);
semilogy(t, V_P);
xlabel('t [s]');
ylabel('Slip Rate V [mm/s]');

subplot(2, 4, 6);
plot(t, tausig_P);
xlabel('t [s]')
ylabel('f');

subplot(2, 4, 7);
plot(x, p);
xlabel('x [m]');
ylabel('Pressure p [MPa]');

subplot(2, 4, 8);
plot(x, sigma_yy);
xlabel('x [m]');
%ylabel('\sigma^{\prime} [MPa]');
ylabel('\sigma_{yy} [MPa]');

suptitle(strcat('t = ', num2str(t(end)), ' s'));
drawnow;

end