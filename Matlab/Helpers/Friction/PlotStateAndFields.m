%% 2D plot of the fields and plots of the earthquake nucleation
%
%   Input:
%   Vector x            - Spatial grid in x-direction
%   Vector y            - Spatial grid in y-direction
%   Vector t            - Time point
%   Vector u            - Horisontal displacement
%   Vector v            - Vertical displacement
%   Matrix p            - Stress component
%   Matrix sigma_xx     - Stress component
%   Matrix sigma_xy     - Stress component
%   Matrix sigma_yy     - Stress component
%   Vector V            - Slip rate at fault as function of time
%   Vector tausig       - Effective strain over effective stress as 
%                       - function of time
%   Vector fss          - Steady state friction as function of time
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = PlotStateAndFields(x, y, t, u, v, p, ...
    sigma_xx, sigma_xy, sigma_yy, V, tausig, fss)

subplot(3,3,1);
surf(x, y, u);
xlabel('x [km]');
ylabel('y [km]');
zlabel('Displacement u [m]');

subplot(3,3,2);
surf(x, y, v);
xlabel('x [km]');
ylabel('y [km]');
zlabel('Displacement v [m]');

subplot(3,3,3);
surf(x, y, p);
xlabel('x [km]');
ylabel('y [km]');
zlabel('Pressure p [MPa]');

subplot(3,3,4);
surf(x, y, sigma_xx);
xlabel('x [km]');
ylabel('y [km]');
zlabel('\sigma_{xx} [MPa]');

subplot(3,3,5);
surf(x, y, sigma_xy);
xlabel('x [km]');
ylabel('y [km]');
zlabel('\sigma_{xy} [MPa]');

subplot(3,3,6);
surf(x, y, sigma_yy);
xlabel('x [km]');
ylabel('y [km]');
zlabel('\sigma_{yy} [MPa]');

subplot(3, 3, 7)
plot(t, tausig);
xlabel('t [s]')
ylabel('f');

subplot(3, 3, 8)
semilogy(t, V);
xlabel('t [s]');
ylabel('Slip Rate V [m/s]');

subplot(3, 3, 9)
semilogx(V(1, :), tausig, V(1, :), fss);
xlabel('Slip Rate V [m/s]')
ylabel('f');

suptitle(strcat('t = ', num2str(t(end)), ' s'));
drawnow

end