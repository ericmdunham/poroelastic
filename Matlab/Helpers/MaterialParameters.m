%% Material Parameters
% Computes the material parameters K, alpha, M, c and lambda out of the 
% material parameters used for the problem setup
% 
% Input:
%   Scalar G        - Shear modulus
%   Scalar kappa    - Permeability
%   Scalar c        - Hydraulic diffusitivity
%   Scalar nu       - Poisson ratio
%   Scalar nu_u     - Poisson ratio (undrained)
%   Scalar B        - Skemptons parameter
%   
%
% Output:
%   Scalar K        - Drained bulk modulus
%   Scalar alpha    - Biot's constant
%   Scalar M        - Biot's Modulus
%   Scalar c        - Hydraulic diffusitivity
%   Scalar lambda   - 1st Lam� parameter
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function  [K, alpha, M, c, lambda] = MaterialParameters(...
    G, kappa, B, nu, nu_u)

K = 2*G.*(1 + nu)./(3*(1 - 2*nu));
alpha = 3*(nu_u - nu)./(B.*(1 - 2*nu).*(1 + nu_u));
M = 2*G.*(nu_u - nu)./(alpha.^2.*(1 - 2*nu_u).*(1 - 2*nu));
c = 2*kappa.*B.^2.*G.*(1 - nu).*(1 + nu_u).^2./(9*(1 - nu_u).*(nu_u - nu));
lambda = K - 2*G/3;

end
