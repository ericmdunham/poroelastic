%% Construct the spatial operators
%   Function for constructing the spatial operators used for solving the
%   system of equations MSyst*v_t = ASyst*v + BSyst. This is a wrapper for
%   choosing which type of operators should be used.
%
%   Input:
%   Vector Nx           - Spatial resolution in x-direction
%   Vector dx           - Spatial step size in x-direction
%   Vector Ny           - Spatial resolution in y-direction
%   Vector dy           - Spatial step size in y-direction
%   Matrix alpha        - Biot's coefficient
%   Matrix K            - Drained bulk modulus
%   Matrix G            - Shear modulus
%   Matrix kappa        - k/my, where k is permeability and my fluid 
%                       - viscosity
%   Matrix M            - Biot's modulus
%   Matrix BC           - Boundary condition matrix.
%   String SBP_stencil  - Flag for choosing SBP-operator.
%                         w - wide, n - narrow, r - russian
%   Scalar order        - Order of operators. Must be consistent with
%                         SBP_stencil
%
%   Optional Input:
%   x                   - The transformed grid. 
%   TransDir            - The direction of the matrix. Should be 'x' or 'y'
%
%   Output:
%   Matrix MSyst        - LHS matrix of linear system. 3*Ny*Nx-by-3*Ny*Nx
%   Matrix ASyst        - RHS matrix of linear system. 3*Ny*Nx-by-3*Ny*Nx
%   Object M_b          - Boundary data object for mech. equlibrium equation
%   Object F_b          - Boundary data object for fluid diffusion equation
%   Matrix Sigma_xx     - Used to calculate sigma_yy from [u, v, p]^T 
%   Matrix Sigma_xy     - Used to calculate sigma_xy from [u, v, p]^T.
%   Matrix Sigma_yy     - Used to calculate sigma_yy from [u, v, p]^T
%
%   Authors: Kim Torberntsson, Vidar Stiernstrom

function [MSyst, ASyst, M_b, F_b, Sigma_xx, Sigma_xy, Sigma_yy] ...
    = SpatialOperators(Nx, dx, Ny, dy, alpha, K, G, kappa, M, ...
    SBP_stencil, BC, order, x, TransDir)

% Construct matrices used in SBP-SAT discretization
[alpha, G, lambda, A, B, C, kappa, Minv] = ...
    MaterialMatrices(Nx, Ny, alpha, K, G, kappa, M);

% Find the right folder to add as path
superDir = strrep(mfilename('fullpath'), ...
    strcat('ImplicitDiscretization/', mfilename), '');

% Depending on the flag SBP_stencil, construct the SBP opertors and build
% the spatial operators.
switch SBP_stencil
    case 'w'
        addpath(strcat(superDir,'OperatorsWide'));
        % No coordinate transform
        if nargin < 13
            [Dx, Dy, MSyst, ASyst, M_b, F_b] = WideStencil(Nx, dx, Ny, dy, ...
                alpha, A, B, C, kappa, Minv, BC, order);
        % Coordinate transform
        else
            [Dx, Dy, MSyst, ASyst, M_b, F_b] = WideStencil(Nx, dx, Ny, dy, ...
                alpha, A, B, C, kappa, Minv, BC, order, x, TransDir);
        end
        rmpath(strcat(superDir,'OperatorsWide'));
    case 'n'
        addpath(strcat(superDir,'OperatorsNarrow'));
        % No coordinate transform
        if nargin < 13
            [Dx, Dy, MSyst, ASyst, M_b, F_b] = NarrowStencil(Nx, dx, Ny, dy, ...
                alpha, A, B, C, kappa, Minv, BC, order);
        % Coordinate transform
        else
            [Dx, Dy, MSyst, ASyst, M_b, F_b] = NarrowStencil(Nx, dx, Ny, dy, ...
                alpha, A, B, C, kappa, Minv, BC, order, x, TransDir);
        end
        rmpath(strcat(superDir,'OperatorsNarrow'));
    case 'r'
        addpath(strcat(superDir,'OperatorsRussian'));
        % No coordinate transform
        if nargin < 13
            [Dx, Dy, MSyst, ASyst, M_b, F_b] = RussianStencil(Nx, dx, Ny, dy, ...
                alpha, A, B, C, kappa, Minv, BC, order);
        % Coordinate transform
        else
            [Dx, Dy, MSyst, ASyst, M_b, F_b] = RussianStencil(Nx, dx, Ny, dy, ...
                alpha, A, B, C, kappa, Minv, BC, order, x, TransDir);
        end
        rmpath(strcat(superDir,'OperatorsRussian'));
end

% Some matrices used for calculating variable fields.

% Matrix used to calculate sigma_xx from [u, v, p]^T
Sigma_xx = [(lambda + 2*G)*Dx lambda*Dy -alpha];

% Matrix used to calculate sigma_xy from [u, v, p]^T.
Sigma_xy = [G*Dy G*Dx spalloc(Ny*Nx, Ny*Nx, 0)];

% Matrix used to calculate sigma_yy from [u, v, p]^T 
Sigma_yy = [lambda*Dx (lambda + 2*G)*Dy -alpha];

end
