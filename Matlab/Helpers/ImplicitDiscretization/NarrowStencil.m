%% Spatial Discritization for the narrow operators
%  
% Script that discretizes the poroelastic problem using the fully
% compatible narrow stencil operators.
%
%   Input:
%   Scalar Nx           - Resolution in x-direction
%   Scalar dx           - Step size in x-direction
%   Scalar Ny           - Resolution in y-direction
%   Scalar dy           - Step size in y-direction
%   Vector lambda       - First Lame parameter
%   Vector G            - Shear modulus
%   Matrix alpha        - Biot's coefficient   
%   Matrix A            - A = diag(lambda+2G,G)  
%   Matrix B            - B = diag(G,lambda+2G)  
%   Matrix C            - [0, lambda; G, 0] 
%   Matrix kappa        - k/my, where k is permeability and my fluid 
%                         viscosity 
%   Matrix Minv         - Inverse matrix of Biot's modulus
%   Matrix BC           - Boundary condition matrix. 
%   Scalar order        - Order of operators. Should be 2 or 4 for the
%                         narrow operators.
%
%   Optional Input:
%   x                   - The transformed grid. 
%   TransDir            - Which coordinate axis is transformed. 
%                         Should be 'x' or 'y'
%
%   Output:
%   Matrix Dx           - First derivative operator in the x-direction.
%   Matrix Dy           - First derivative operator in the y-direction.
%   Matrix MSyst        - LHS matrix of linear system.
%   Matrix ASyst        - RHS matrix of linear system.
%   Object M_b          - Boundary data object for mech. equlibrium equation
%   Object F_b          - Boundary data object for fluid diffusion equation
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dx, Dy, MSyst, ASyst, M_b, F_b] = NarrowStencil(Nx, dx, Ny, dy, ...
    alpha, A, B, C, kappa, Minv, BC, order, x, TransDir)

%% SAT penalty terms and boundary vectors

% Stability conditions
sigmaW = eye(2);
sigmaE = eye(2);
sigmaS = eye(2);
sigmaN = eye(2);

betaW = 1;
betaE = 1;
betaS = 1;
betaN = 1;

% Extract boundary conditions
gammaW = [BC(1,1) 0; 0 BC(2,1)];
gammaE = [BC(1,2) 0; 0 BC(2,2)];
gammaS = [BC(1,3) 0; 0 BC(2,3)];
gammaN = [BC(1,4) 0; 0 BC(2,4)];

deltaW = BC(3,1);
deltaE = BC(3,2);
deltaS = BC(3,3);
deltaN = BC(3,4);

%% SBP operators
Im = speye(Ny);
In = speye(Nx);
I2 = speye(2);
xHat = [1; 0];
yHat = [0; 1];

% D1 in x-direction
mats_x = SBP(Nx, dx, order);
D1n = mats_x.D1int;
HIn = mats_x.Hinv;
e1n = zeros(Nx, 1); e1n(1) = 1;
en = zeros(Nx, 1); en(Nx) = 1;

% D1 in y-direction
mats_y = SBP(Ny, dy, order);
D1m = mats_y.D1int;
HIm = mats_y.Hinv;
e1m = zeros(Ny, 1); e1m(1) = 1;
em = zeros(Ny, 1); em(Ny) = 1;

% Pick out the material parameters needed for the second derivative
% operators in the x- and y-direction.
A1 = A(1:Ny*Nx, 1:Ny*Nx);
A2 = A(Ny*Nx + 1:2*Ny*Nx, Ny*Nx + 1:2*Ny*Nx);
B1 = B(1:Ny*Nx, 1:Ny*Nx);
B2 = B(Ny*Nx + 1:2*Ny*Nx, Ny*Nx + 1:2*Ny*Nx);
kappaX = kappa;
kappaY = kappa;

% Do the coordinate transform of the operators if specified
if nargin > 12
    if TransDir == 'x'
        J = spdiags(D1n*x, 0, Nx, Nx);
        Jinv = inv(J);
        D1n = Jinv*D1n;
        HIn = Jinv*HIn;
        Jinv2D = kron(Jinv, Im);
        % This is how the coordinate transformation is done for Dxx
        A1 = A1*Jinv2D;
        A2 = A2*Jinv2D;
        kappaX = kappaX*Jinv2D;
    else
        J = spdiags(D1m*x, 0, Ny, Ny);
        Jinv = inv(J);
        D1m = Jinv*D1m;
        HIm = Jinv*HIm;
        Jinv2D = kron(In, Jinv);
        % This is how the coordinate transformation is done for Dyy
        B1 = B1*Jinv2D;
        B2 = B2*Jinv2D;
        kappaY = kappaY*Jinv2D;
    end
end

% D2 with damping in x-direction
Dxx_A11 = NarrowXX(Nx, Ny, dx, A1, order);
Dxx_A22 = NarrowXX(Nx, Ny, dx, A2, order);
Dxx_A = blkdiag(Dxx_A11, Dxx_A22);
Dxx_kappa = NarrowXX(Nx, Ny, dx, kappaX, order);

% D2 with damping in y-direction
Dyy_B11 = NarrowYY(Nx, Ny, dy, B1, order);
Dyy_B22 = NarrowYY(Nx, Ny, dy, B2, order);
Dyy_B = blkdiag(Dyy_B11, Dyy_B22);
Dyy_kappa = NarrowYY(Nx, Ny, dy, kappaY, order);

% Do coordinate transform was given transform the second derivative
% operators again to get from psi to x.
if nargin > 12
    if TransDir == 'x'
        % This is how the coordinate transformation is done for Dxx
        Dxx_A = blkdiag(Jinv2D, Jinv2D)*Dxx_A;
        Dxx_kappa = Jinv2D*Dxx_kappa;
    else
        % This is how the coordinate transformation is done for Dyy
        Dyy_B = blkdiag(Jinv2D, Jinv2D)*Dyy_B;
        Dyy_kappa = Jinv2D*Dyy_kappa;
    end
end

% Pressure operators
Dx = kron(D1n, Im);
eN = kron(In, em);
eS = kron(In, e1m);
HIx = kron(HIn, Im);
Dy = kron(In, D1m);
eE = kron(en, Im);
eW = kron(e1n, Im);
HIy = kron(In, HIm);

% Displacement operators
Dx2 = kron(I2,Dx);
eN2 = kron(I2,eN);
eS2 = kron(I2,eS);
HIx2 = kron(I2,HIx);
Dy2 = kron(I2,Dy);
eE2 = kron(I2,eE);
eW2 = kron(I2,eW);
HIy2 = kron(I2,HIy);

%RHS Matrix
ASyst11 = Dxx_A + Dx2*C*Dy2 + Dy2*C'*Dx2 + Dyy_B + ...
    HIx2*eW2*(-kron(gammaW,eW') + kron(sigmaW,eW')*(A*Dx2+C*Dy2)) + ...
    HIx2*eE2*(-kron(gammaE,eE') - kron(sigmaE,eE')*(A*Dx2+C*Dy2)) + ...
    HIy2*eS2*(-kron(gammaS,eS') + kron(sigmaS,eS')*(B*Dy2+C'*Dx2)) + ...
    HIy2*eN2*(-kron(gammaN,eN') - kron(sigmaN,eN')*(B*Dy2+C'*Dx2));

ASyst12 = -kron(xHat,Dx*alpha) - kron(yHat,Dy*alpha) + ...
    +HIx2*eW2*(kron(sigmaW,eW')*-kron(xHat,alpha)) + ...
    +HIx2*eE2*(-kron(sigmaE,eE')*-kron(xHat,alpha)) + ...
    +HIy2*eS2*(kron(sigmaS,eS')*-kron(yHat,alpha)) + ...
    +HIy2*eN2*(-kron(sigmaN,eN')*-kron(yHat,alpha));

ASyst21 = spalloc(Ny*Nx,2*Ny*Nx, 0);

ASyst22 = Dxx_kappa + Dyy_kappa + ...
    HIx*eW*(-deltaW*eW' + betaW*eW'*kappa*Dx) + ...
    HIx*eE*(-deltaE*eE' - betaE*eE'*kappa*Dx) + ...
    HIy*eS*(-deltaS*eS' + betaS*eS'*kappa*Dy) + ...
    HIy*eN*(-deltaN*eN' - betaN*eN'*kappa*Dy);


ASyst = [ASyst11 ASyst12; ASyst21 ASyst22];

% LHS matrix
MSyst11 = spalloc(2*Ny*Nx, 2*Ny*Nx, 0);
MSyst12 = spalloc(2*Ny*Nx, Ny*Nx, 0);
MSyst21 = kron(xHat', alpha*Dx) + kron(yHat', alpha*Dy);
MSyst22 = Minv;
MSyst = [MSyst11 MSyst12; MSyst21 MSyst22];

% The boundary data for the whole system
M_b.W = HIx2*eW2;
M_b.E = HIx2*eE2;
M_b.S = HIy2*eS2;
M_b.N = HIy2*eN2;

F_b.W = HIx*eW;
F_b.E = HIx*eE;
F_b.S = HIy*eS;
F_b.N = HIy*eN;

end
