%% Material matrices used in SBP-SAT discretization
%   Function for constructing the material matrices used in the SBP-SAT
%   discretization from the corresponding input vectors.
%
%   Input:
%   Scalar Ny       - Resolution in x-direction
%   Scalar Nx       - Resolution in y-direction
%   Matrix alpha    - Biot's coefficient
%   Matrix K        - Drained bulk modulus
%   Matrix G        - Shear modulus
%   Matrix kappa    - k/my, where k is permeability and my fluid viscosity
%   Matrix M        - Biot's modulus

%   Output:
%   Matrix alpha    - Biot's coefficient
%   Matrix G        - Shear modulus
%   Matrix lambda_u - The first Lam� paramter
%   Matrix A        - A = diag(lambda+2G,G)   
%   Matrix B        - B = diag(G,lambda+2G)
%   Matrix C        - [0, lambda; G, 0] 
%   Matrix kappa    - k/my, where k is permeability and my fluid viscosity
%   Matrix Minv     - Inverse matrix of Biot's modulus
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [alpha, G, lambda, A, B, C, kappa, Minv] = ...
    MaterialMatrices(Nx, Ny, alpha, K, G, kappa, M)

% Matrix for coupling term
alpha = spdiags(reshape(alpha, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);

% Matrices for pressure
kappa = spdiags(reshape(kappa, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);
M = spdiags(reshape(M, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);
Minv = inv(M);

% Matrices for displacement
K = spdiags(reshape(K, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);
G = spdiags(reshape(G, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);
lambda = K - 2*G/3;

% Matrices for displacement
A = blkdiag(lambda + 2*G, G);
B = blkdiag(G, lambda + 2*G);
C = [spalloc(Ny*Nx, Ny*Nx, 0) lambda; G spalloc(Ny*Nx, Ny*Nx, 0)];

end
