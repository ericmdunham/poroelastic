%% Spatial Discritization for the Russian operators
%  
% Script that discretizes the poroelastic problem using the Russian
% operators
%
%   Input:
%   Scalar Nx           - Resolution in x-direction
%   Scalar dx           - Step size in x-direction
%   Scalar Ny           - Resolution in y-direction
%   Scalar dy           - Step size in y-direction
%   Matrix Alpha        - Biot's coefficient 
%   Matrix A            - A = diag(lambda+2G,G)  
%   Matrix B            - B = diag(G,lambda+2G)
%   Matrix C            - [0, lambda; G, 0]
%   Matrix Kappa        - k/my, where k is permeability and my fluid 
%                         viscosity 
%   Matrix Minv         - Inverse matrix of Biot's modulus. 
%   Matrix BC           - Boundary condition matrix.
%   Scalar order        - Order of the operators. Should be 3, 5, 7 or 9 
%                         for the Russian operators.
%
%   Optional Input:
%   x                   - The transformed grid. 
%   TransDir            - Which coordinate axis is transformed. 
%                         Should be 'x' or 'y'
%
%   Output:
%   Matrix Dmx          - First derivative downwind operator in the 
%                         x-direction.
%   Matrix Dmy          - First derivative downwind operator in the 
%                         y-direction.
%   Matrix MSyst        - LHS matrix of linear system.
%   Matrix ASyst        - RHS matrix of linear system.
%   Object M_b          - Boundary data object for mech. equlibrium equation
%   Object F_b          - Boundary data object for fluid diffusion equation
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dmx, Dmy, MSyst, ASyst, M_b, F_b] = RussianStencil(Nx, dx, ...
    Ny, dy, alpha, A, B, C, kappa, Minv, BC, order, x, TransDir)

%% SAT penalty terms and boundary vectors

% Stability conditions
sigmaW = eye(2);
sigmaE = eye(2);
sigmaS = eye(2);
sigmaN = eye(2);

betaW = 1;
betaE = 1;
betaS = 1;
betaN = 1;

% Extract boundary conditions
gammaW = [BC(1,1) 0; 0 BC(2,1)];
gammaE = [BC(1,2) 0; 0 BC(2,2)];
gammaS = [BC(1,3) 0; 0 BC(2,3)];
gammaN = [BC(1,4) 0; 0 BC(2,4)];

deltaW = BC(3,1);
deltaE = BC(3,2);
deltaS = BC(3,3);
deltaN = BC(3,4);

%% SBP operators
Im = speye(Ny);
In = speye(Nx);
I2 = speye(2);
xHat = [1; 0];
yHat = [0; 1];

% Construct SBP operators
[Dpn, Dmn, HIn, e1n, en] = SBP(Nx,dx,order);
[Dpm, Dmm, HIm, e1m, em] = SBP(Ny,dy,order);

% Do the coordinate transform of the operators if specified
if nargin > 12
    if TransDir == 'x'
        J = spdiags(Dmn*x, 0, Nx, Nx);
        Jinv = inv(J);
        Dpn = Jinv*Dpn;
        Dmn = Jinv*Dmn;
        HIn = Jinv*HIn;
    else
        J = spdiags(Dmm*x, 0, Ny, Ny);
        Jinv = inv(J);
        Dpm = Jinv*Dpm;
        Dmm = Jinv*Dmm;
        HIm = Jinv*HIm;
    end
end

% Spatial matrices

% Pressure operators
Dpx = kron(Dpn, Im);
Dmx = kron(Dmn, Im);
eN = kron(In, em);
eS = kron(In, e1m);
HIx = kron(HIn, Im);

Dpy = kron(In, Dpm);
Dmy = kron(In, Dmm);
eE = kron(en, Im);
eW = kron(e1n, Im);
HIy = kron(In, HIm);

% Displacement operators
Dpx2 = kron(I2,Dpx);
Dmx2 = kron(I2,Dmx);
eN2 = kron(I2,eN);
eS2 = kron(I2,eS);
HIx2 = kron(I2,HIx);

Dpy2 = kron(I2,Dpy);
Dmy2 = kron(I2,Dmy);
eE2 = kron(I2,eE);
eW2 = kron(I2,eW);
HIy2 = kron(I2,HIy);

%  RHS matrix
ASyst11 = Dpx2*A*Dmx2 + Dpx2*C*Dmy2 + Dpy2*C'*Dmx2 + Dpy2*B*Dmy2 + ...
    HIx2*eW2*(-kron(gammaW,eW') + kron(sigmaW,eW')*(A*Dmx2+C*Dmy2)) + ...
    HIx2*eE2*(-kron(gammaE,eE') - kron(sigmaE,eE')*(A*Dmx2+C*Dmy2)) + ...
    HIy2*eS2*(-kron(gammaS,eS') + kron(sigmaS,eS')*(B*Dmy2+C'*Dmx2)) + ...
    HIy2*eN2*(-kron(gammaN,eN') - kron(sigmaN,eN')*(B*Dmy2+C'*Dmx2));

ASyst12 = -kron(xHat,Dpx*alpha) - kron(yHat,Dpy*alpha) + ...
    +HIx2*eW2*(kron(sigmaW,eW')*-kron(xHat,alpha)) + ...
    +HIx2*eE2*(-kron(sigmaE,eE')*-kron(xHat,alpha)) + ...
    +HIy2*eS2*(kron(sigmaS,eS')*-kron(yHat,alpha)) + ...
    +HIy2*eN2*(-kron(sigmaN,eN')*-kron(yHat,alpha));

ASyst21 = spalloc(Ny*Nx,2*Ny*Nx, 0);

ASyst22 = Dpx*kappa*Dmx + Dpy*kappa*Dmy + ...
    HIx*eW*(-deltaW*eW' + betaW*eW'*kappa*Dmx) + ...
    HIx*eE*(-deltaE*eE' - betaE*eE'*kappa*Dmx) + ...
    HIy*eS*(-deltaS*eS' + betaS*eS'*kappa*Dmy) + ...
    HIy*eN*(-deltaN*eN' - betaN*eN'*kappa*Dmy);


ASyst = [ASyst11 ASyst12; ASyst21 ASyst22];

% LHS matrix
MSyst11 = spalloc(2*Ny*Nx, 2*Ny*Nx, 0);
MSyst12 = spalloc(2*Ny*Nx, Ny*Nx, 0);
MSyst21 = kron(xHat', alpha*Dmx) + kron(yHat', alpha*Dmy);
MSyst22 = Minv;
MSyst = [MSyst11 MSyst12; MSyst21 MSyst22];

% The boundary data for the whole system
M_b.W = HIx2*eW2;
M_b.E = HIx2*eE2;
M_b.S = HIy2*eS2;
M_b.N = HIy2*eN2;

F_b.W = HIx*eW;
F_b.E = HIx*eE;
F_b.S = HIy*eS;
F_b.N = HIy*eN;

end
