%% Second order narrow SBP operators in x-direction.
%   Constructs fully compatible second order narrow SBP operators with 
%   artifical viscosity that approximates d/dx(C d/dx), where C can have 
%   variable coefficients. The code is a rewrite of a code written by Kali
%   Allison, who made the code for handling variable coefficients.
%
%   Input:
%   Scalar Nx   	- Number of points in the x-direction
%   Scalar Ny   	- Number of points in the y-direction
%   Scalar dx   	- grid spacing in x-direction.
%   Matrix C        - Diagonal coefficient matrix
%   Scalar order 	- The order of the operators used. 2 or 4.
%
%   Optional Input:
%   C_r             - Coefficient Matrix used for the R term
%
%   Output:
%   Dxx             - The final matrix that approximates d/dx(C d/dx)
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dxx] = NarrowXX(Nx, Ny, dx, C, order, C_r)

Iy = speye(Ny,Ny);

% If no matrix is provided for the R term, then use C for that.
if nargin < 6
    C_r = C;
end

% 1D matrices
SBPObj = SBP(Nx,dx,order);

% Build the R term to add artificial viscosity and make the stencil narrow
if order == 2
    C2x = speye(Nx, Nx); C2x(1, 1) = 0; C2x(Nx, Nx) = 0;
    R = (1/4/dx)*kron(SBPObj.D2, Iy)'*kron(C2x, Iy)*C_r*kron(SBPObj.D2, Iy);
elseif order == 4
    CC = diag(C_r);
    B3 = 0.5*sparse(1:Nx*Ny, 1:Nx*Ny, CC, Nx*Ny, Nx*Ny)...
        + 0.5*sparse(1:Nx*Ny, 1:Nx*Ny, [CC(2:end); CC(end-1)], Nx*Ny, Nx*Ny);
    R = (1/18/dx).*kron(SBPObj.D3, Iy)'*kron(SBPObj.C3, Iy)*B3*kron(SBPObj.D3, Iy)...
        + (7/144/dx).*kron(SBPObj.D4, Iy)'*kron(SBPObj.C4, Iy)*C_r*kron(SBPObj.D4, Iy);
end

% Construct the M operator and lastly the Dxx operator
M = kron(SBPObj.D1int, Iy)'*C*kron(SBPObj.H, Iy)*kron(SBPObj.D1int, Iy);
Dxx = kron(SBPObj.Hinv, Iy)*(-M - R + C*kron(SBPObj.BS, Iy));

end
