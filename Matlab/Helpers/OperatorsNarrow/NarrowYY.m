%% Second order narrow SBP operators in y-direction.
%   Constructs fully compatible second order narrow SBP operators with 
%   artifical viscosity that approximates d/dy(C d/dy), where C can have 
%   variable coefficients. The code is a rewrite of a code written by Kali
%   Allison, who made the code for handling variable coefficients.
%
%   Input:
%   Scalar Nx   	- Number of points in the x-direction
%   Scalar Ny   	- Number of points in the y-direction
%   Scalar dy   	- grid spacing in y-direction.
%   Vector C        - Diagonal coefficient matrix
%   Scalar order 	- The order of the operators used. 2 or 4.
%
%   Optional Input:
%   C_r             - Coefficient Matrix used for the R term
%
%   Output:
%   Dyy             - The final matrix that approximates d/dy(C d/dy)
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dyy] = NarrowYY(Nx, Ny, dy, C, order, C_r)

Ix = speye(Nx,Nx);

% If no matrix is provided for the R term, then use C for that.
if nargin < 6
    C_r = C;
end

% 1D matrices
SBPObj = SBP(Ny,dy,order);

% Build the R term to add artificial viscosity and make the stencil narrow
if order == 2
    C2y = speye(Ny, Ny); C2y(1,1) = 0; C2y(Ny,Ny) = 0;
    R = (1/4/dy)*kron(Ix, SBPObj.D2)'*kron(Ix, C2y)*C_r*kron(Ix, SBPObj.D2);
elseif order == 4
    CC = diag(C_r);
    B3 = 0.5*sparse(1:Nx*Ny, 1:Nx*Ny, CC, Nx*Ny, Nx*Ny) ...
        + 0.5*sparse(1:Nx*Ny, 1:Nx*Ny, [CC(2:end); CC(end-1)], Nx*Ny,Nx*Ny);
    R = (1/18/dy).*kron(Ix, SBPObj.D3)'*kron(Ix, SBPObj.C3)*B3*kron(Ix, SBPObj.D3)...
        +(7/144/dy).*kron(Ix, SBPObj.D4)'*kron(Ix, SBPObj.C4)*C_r*kron(Ix, SBPObj.D4);
end

% Construct the M operator and lastly the Dxx operator
M = kron(Ix, SBPObj.D1int)'*C*kron(Ix, SBPObj.H)*kron(Ix, SBPObj.D1int);
Dyy = kron(Ix, SBPObj.Hinv)*(-M - R + C*kron(Ix, SBPObj.BS));

end
