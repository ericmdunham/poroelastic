%% Second order narrow SBP operators in y-direction.
%   Constructs compatible second order narrow SBP operators with artifical
%   viscosity that approximates d/dy(C d/dy), where C can have 
%   variable coefficients. The code is a rewrite of a code written by Kali
%   Allison, who made the code for handling variable coefficients.
%
%   Input:
%   Scalar Nx   	- Number of points in the x-direction
%   Scalar Ny   	- Number of points in the y-direction
%   Scalar dy   	- grid spacing in y-direction.
%   Vector cIn      - Diagonal elements of the C matrix
%   Scalar order 	- The order of the operators used. 2 or 4.
%
%   Output:
%   Dyy             - The final matrix that approximates d/dy(C d/dy)
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dyy] = NarrowYY(Nx, Ny, dy, cIn, order)

c = sparse(diag(cIn(:)));

Ix = speye(Nx,Nx);

% 1D matrices
yMats = SBP(Ny,dy,order);

% 2nd derivatives
if order==2
    C2y = speye(Ny,Ny);C2y(1,1)=0;C2y(Ny,Ny)=0;
    Rymu = kron(Ix,yMats.D2)'*kron(Ix,C2y)*c*kron(Ix,yMats.D2).*(1/4/dy);
elseif order==4
    cc = cIn(:);
    B3 = 0.5*sparse(1:Nx*Ny,1:Nx*Ny,cc,Nx*Ny,Nx*Ny)...
        + 0.5*sparse(1:Nx*Ny,1:Nx*Ny,[cc(2:end); cc(end-1)],Nx*Ny,Nx*Ny);
    Rymu = (1/18/dy).*kron(Ix,yMats.D3)'*kron(Ix,yMats.C3)*B3*kron(Ix,yMats.D3)...
        +(1/144/dy).*kron(Ix,yMats.D4)'*kron(Ix,yMats.C4)*c*kron(Ix,yMats.D4);
end
Mmuy = kron(Ix,yMats.D1int)'*c*kron(Ix,yMats.H)*kron(Ix,yMats.D1int) + Rymu;
Dyy = kron(Ix,yMats.Hinv)*(-Mmuy + c*kron(Ix,yMats.BS));
end
