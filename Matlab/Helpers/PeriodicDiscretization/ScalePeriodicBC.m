%% Scale the Robin boundary conditions 
%   The Robin conditions are scaled so that they are implemented according 
%   to the Weights. This scaling depends on the material properties of the
%   problem and the spacial discretization. The code works both for
%   transform and for no transforms, but right now only for constant
%   coefficients. This version of the code is for periodic boundary
%   conditions in the x-direction.
%
%   Input:
%   Scalar lambda   		- The first Lam� parameter
%   Scalar G        		- The shear modulus
%   Scalar kappa    		- The permeability
%   Vector y        		- Spatial grid in y-direction
%   Scalar Ny               - Resolution in y-direction
%   Scalar DirichletOrder 	- Order of accuracy of the Dirichlet conditions
%   Matrix BC       		- Matrix describing the boundary conditions
%
%   Output:
%   Matrix BC    			- The scaled boundary conditions matrix
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [BC] = ScalePeriodicBC(lambda, G, kappa, y, Ny, DirichletOrder, BC)

% South
BC(1,1) = Ny^(DirichletOrder - 1)*BC(1,1)*G/(y(2) - y(1));                      %u
BC(2,1) = Ny^(DirichletOrder - 1)*BC(2,1)*(lambda+2*G)/(y(2) - y(1));           %v
BC(3,1) = Ny^(DirichletOrder - 1)*BC(3,1)*kappa/(y(2) - y(1));                  %p

% North
BC(1,2) = Ny^(DirichletOrder - 1)*BC(1,2)*G/(y(end) - y(end - 1));              %u
BC(2,2) = Ny^(DirichletOrder - 1)*BC(2,2)*(lambda+2*G)/(y(end) - y(end - 1));   %v
BC(3,2) = Ny^(DirichletOrder - 1)*BC(3,2)*kappa/(y(end) - y(end - 1));          %p

end
