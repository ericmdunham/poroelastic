%% Second order narrow SBP operators in x-direction.
%   Constructs compatible second order narrow SBP operators with artifical
%   viscosity that approximates d/dx(C d/dx), where C can have 
%   variable coefficients. The code is a rewrite of a code written by Kali
%   Allison, who made the code for handling variable coefficients.
%
%   Input:
%   Scalar Nx   	- Number of points in the x-direction
%   Scalar Ny   	- Number of points in the y-direction
%   Scalar dx   	- grid spacing in x-direction.
%   Vector cIn      - Diagonal elements of the C matrix
%   Scalar order 	- The order of the operators used. 2 or 4.
%
%   Output:
%   Dxx             - The final matrix that approximates d/dx(C d/dx)
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dxx] = NarrowPeriodicXX(Nx, Ny, dx, cIn, order)

c = sparse(diag(cIn(:)));

Iy = speye(Ny,Ny);

% 1D matrices
xMats = SBPPeriodic(Nx, dx, order);

% 2nd derivative
if order == 2
    Rxmu = kron(xMats.D2,Iy)'*c*kron(xMats.D2,Iy).*(1/4/dx);
elseif order == 4
    cc = cIn(:);
    B3 = 0.5*sparse(1:Nx*Ny,1:Nx*Ny,cc,Nx*Ny,Nx*Ny)...
        + 0.5*sparse(1:Nx*Ny,1:Nx*Ny,[cc(2:end); cc(end-1)],Nx*Ny,Nx*Ny);
    Rxmu = (1/18/dx).*kron(xMats.D3,Iy)'*kron(xMats.C3,Iy)*B3*kron(xMats.D3,Iy)...
        + (1/144/dx).*kron(xMats.D4,Iy)'*kron(xMats.C4,Iy)*c*kron(xMats.D4,Iy);
end
Mmux = kron(xMats.D1,Iy)'*c*kron(xMats.H,Iy)*kron(xMats.D1,Iy) + Rxmu;
Dxx = kron(xMats.Hinv,Iy)*(-Mmux);
end
