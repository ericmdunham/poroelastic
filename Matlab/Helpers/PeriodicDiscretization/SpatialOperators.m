%% Spatial discritization with the narrow operators
%
% Script that discretizes the poroelastic problem with the explicit scheme
% using the narrow operators.
%
%   Input:
%   Scalar Nx           - Resolution in x-direction
%   Scalar dx           - Step size in x-direction
%   Scalar Ny           - Resolution in y-direction
%   Scalar dy           - Step size in y-direction
%   Matrix alpha        - Biot's coefficient
%   Matrix A_u          - A = diag(lambda_u + 2G,G)
%   Matrix B_u          - B = diag(G,lambda_u + 2G)
%   Matrix C_u          - [0, lambda_u; G, 0]
%   Matrix kappa        - k/my, where k is permeability and my fluid
%                         viscosity
%   Matrix M            - Biot's modulus.
%   Matrix BC           - Boundary condition matrix.
%   Scalar order        - Order of the operators. Should be 2 or 4 for the
%                         narrow operators.
%
%   Optional Input:
%   y                   - The transformed grid if a trandformation is used. 
%
%   Output:
%   Matrix F_u          - The matrix for the displacements u and v in the 
%                         fluid flow equation.
%   Matrix F_p          - The matrix for the pressure in the fluid flow 
%                         equation.
%   Object F_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the fluid flow equation.
%   Matrix M_u          - The matrix for the displacements u and v in the 
%                         mechanical equilibrium equation.
%   Matrix M_p          - The matrix for the pressure in the mechanical 
%                         equilibrium equation.
%   Object M_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the mechanical equilibrium 
%                         equation.
%   Matrix P            - M alpha ( xHat' kron Dx + yHat' kron Dy). Used
%                         for converting between p' and p. p' = p + Pw
%   Matrix Sigma_xx     - Matrix used to calculate sigma_yy from [u, v]^T 
%                         (p' is also needed due to poroelastic effects)
%   Matrix Sigma_xy     - Used to calculate sigma_xy from [u, v]^T.
%   Matrix Sigma_yy     - Matrix used to calculate sigma_yy from [u, v]^T 
%                         (p' is also needed due to poroelastic effects)
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] = ...
    SpatialOperators(Nx, dx, Ny, dy, alpha, K, G, kappa, M, ...
    BC, order, y)

% Construct matrices used in the SBP-SAT discretization
[alpha, G, lambda_u, A_u, B_u, C_u, kappa, M] = ...
    MaterialMatrices(Nx, Ny, alpha, K, G, kappa, M);

% Stability conditions
sigmaS = eye(2);
sigmaN = eye(2);

betaS = 1;
betaN = 1;

% Extract boundary conditions
gammaS = [BC(1, 1) 0; 0 BC(2, 1)];
gammaN = [BC(1, 2) 0; 0 BC(2, 2)];

deltaS = BC(3, 1);
deltaN = BC(3, 2);

%% SBP operators
Im = speye(Ny);
In = speye(Nx);
I2 = speye(2);
xHat = [1; 0];
yHat = [0; 1];

% D1 in x-direction
mats_x = SBPPeriodic(Nx, dx, order);
D1n = mats_x.D1;

% D1 in y-direction
mats_y = SBP(Ny, dy, order);
D1m = mats_y.D1int;
HIm = mats_y.Hinv;
e1m = zeros(Ny, 1); e1m(1) = 1;
em = zeros(Ny, 1); em(Ny) = 1;

% Pick out the material parameters needed for the second derivative
% operators in the x- and y-direction.
A1 = diag(A_u(1:Ny*Nx, 1:Ny*Nx));
A2 = diag(A_u(Ny*Nx + 1:2*Ny*Nx, Ny*Nx + 1:2*Ny*Nx));
B1 = diag(B_u(1:Ny*Nx, 1:Ny*Nx));
B2 = diag(B_u(Ny*Nx + 1:2*Ny*Nx, Ny*Nx + 1:2*Ny*Nx));
kappaY = kappa;

% Do the coordinate transform in the y-direction if transform specified
if nargin > 11
        J = spdiags(D1m*y, 0, Ny, Ny);
        Jinv = inv(J);
        D1m = Jinv*D1m;
        HIm = Jinv*HIm;
        Jinv2D = kron(In, Jinv);
        % This is how the coordinate transformation is done for Dyy
        B1 = B1.*diag(Jinv2D);
        B2 = B2.*diag(Jinv2D);
        kappaY = kappaY*Jinv2D;
end

% D2 with damping in x-direction
Dxx_A11 = NarrowPeriodicXX(Nx, Ny, dx, A1, order);
Dxx_A22 = NarrowPeriodicXX(Nx, Ny, dx, A2, order);
Dxx_A = blkdiag(Dxx_A11, Dxx_A22);
Dxx_kappa = NarrowPeriodicXX(Nx, Ny, dx, diag(kappa), order);

% D2 with damping in y-direction
Dyy_B11 = NarrowYY(Nx, Ny, dy, B1, order);
Dyy_B22 = NarrowYY(Nx, Ny, dy, B2, order);
Dyy_B = blkdiag(Dyy_B11, Dyy_B22);
Dyy_kappa = NarrowYY(Nx, Ny, dy, diag(kappaY), order);

% Do coordinate transform was given transform the second derivative
% operators again to get from psi to y.
if nargin > 11
    % This is how the coordinate transformation is done for Dyy
    Dyy_B = blkdiag(Jinv2D, Jinv2D)*Dyy_B;
    Dyy_kappa = Jinv2D*Dyy_kappa;
end

% Scalar operators
Dx = kron(D1n, Im);
eN = kron(In, em);
eS = kron(In, e1m);
Dy = kron(In, D1m);
HIy = kron(In, HIm);

% Vector (system) operators
Dx2 = kron(I2,Dx);
eN2 = kron(I2,eN);
eS2 = kron(I2,eS);
Dy2 = kron(I2,Dy);
HIy2 = kron(I2,HIy);

%% Fluid Flow Equation

% The matrix for the relative pore pressure p' in the fluid flow equation
F_p = M*(Dxx_kappa + Dyy_kappa + ...
    HIy*eS*(-deltaS*eS' + betaS*eS'*kappa*Dy) + ...
    HIy*eN*(-deltaN*eN' - betaN*eN'*kappa*Dy));

% The matrix for the displacements u and v in the fluid flow equation.
F_u = M*(-Dxx_kappa*alpha*M*(kron(xHat', Dx) + kron(yHat', Dy)) - ...
    Dyy_kappa*alpha*M*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIy*eS*(deltaS*eS' - betaS*eS'*kappa*Dy)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIy*eN*(deltaN*eN' + betaN*eN'*kappa*Dy)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)));

% A boundary data object with the matrices needed for imposing the boundary
% data in each time step.
F_b.S = M*HIy*eS;
F_b.N = M*HIy*eN;

%% Mechanical Equilibrium Equation

% The matrix for the displacements u and v in the mechanical equilibrium
% equations.
M_u = Dxx_A + Dx2*C_u*Dy2 + Dy2*C_u'*Dx2 + Dyy_B + ...
    HIy2*eS2*(-kron(gammaS,eS') + kron(sigmaS,eS')*(B_u*Dy2+C_u'*Dx2)) + ...
    HIy2*eN2*(-kron(gammaN,eN') - kron(sigmaN,eN')*(B_u*Dy2+C_u'*Dx2));

% The matrix for the relative pore pressure p' in the mechanical equilibrium
% equations.
M_p = -kron(xHat,Dx*alpha) - kron(yHat, Dy*alpha) ...
    - HIy2*eS2*(kron(sigmaS,eS')*kron(yHat,alpha)) ...
    + HIy2*eN2*(kron(sigmaN,eN')*kron(yHat,alpha));

% A boundary data object with the matrices needed for imposing the boundary
% data in each time step.
M_b.S = HIy2*eS2;
M_b.N = HIy2*eN2;

% Matrix used for converting between p' and p
P = M*alpha*(kron([1 0], Dx) + kron([0 1], Dy));

% Matrix used to calculate sigma_xx from [u, v]^T (p' is also needed due to
% poroelastic effects)
Sigma_xx = [(lambda_u + 2*G)*Dx lambda_u*Dy];

% Matrix used to calculate sigma_xy from [u, v]^T.
Sigma_xy = [G*Dy G*Dx];

% Matrix used to calculate sigma_yy from [u, v]^T (p' is also needed due to
% poroelastic effects)
Sigma_yy = [lambda_u*Dx (lambda_u + 2*G)*Dy];

end