%% Material matrices used in explicit SBP-SAT discretization
%   Function for constructing the material matrices used in the SBP-SAT
%   discretization from the corresponding input matrices.
%
%   Input:
%   Scalar Ny       - Resolution in x-direction
%   Scalar Nx       - Resolution in y-direction
%   Matrix alpha    - Biot's coefficient
%   Matrix K        - Drained bulk modulus
%   Matrix G        - Shear modulus
%   Matrix kappa    - k/my, where k is permeability and my fluid viscosity
%   Matrix M        - Biot's modulus. 

%   Output:   
%   Matrix A_u      - A = diag(lambda_u+2G,G)  
%   Matrix B_u      - B = diag(G,lambda_u+2G)
%   Matrix C_u      - [0, lambda; G, 0]
%   Matrix Kappa    - k/my, where k is permeability and my fluid viscosity
%   Matrix M        - Biot's modulus. 
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [alpha, G, lambda_u, A_u, B_u, C_u, kappa, M] = ...
    MaterialMatrices(Nx, Ny, alpha, K, G, kappa, M)

% Matrix for coupling term
alpha = spdiags(reshape(alpha, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);

% Matrices for pressure
kappa = spdiags(reshape(kappa, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);
M = spdiags(reshape(M, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);

% Matrices for displacement
K = spdiags(reshape(K, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);
G = spdiags(reshape(G, Ny*Nx, 1), 0, Ny*Nx, Ny*Nx);
lambda_u = K - 2*G/3 + alpha.^2.*M;

A_u = blkdiag(lambda_u + 2*G, G);
B_u = blkdiag(G, lambda_u + 2*G);
C_u = [spalloc(Ny*Nx, Ny*Nx, 0) lambda_u; G spalloc(Ny*Nx, Ny*Nx, 0)];

end
