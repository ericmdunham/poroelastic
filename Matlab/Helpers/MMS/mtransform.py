# Function for transforming mathematical expressions from python syntax
# to Matlab syntax. The function takes a line of code in the python expression
# and returns the transformed string in Matlab syntax.

def mtransform(text):
    text = str(text)
    text = text.replace('**','.^')
    text = text.replace('*','.*')
    text = text.replace('/','./')
    return text + ";"