%% 2D plot of Mandel's problem
%
%   Input:
%   Vector x            - Spatial grid in x-direction
%   Vector y            - Spatial grid in y-direction
%   Scalar t            - Time point
%   Vector u            - Solution vector of the horisontal displacement
%   Vector v            - Solution vector of the vertical displacement
%   Vector p            - Solution vector of the pore pressure
%   Vector u_a		    - Analytic solution to u
%   Vector v_a		    - Analytic solution to v
%   Vector p_a		    - Analytic solution to p
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = PlotMMS(x, y, t, u, v, p, u_a, v_a, p_a)

Nx = length(x);
Ny = length(y);

font_size = 20;

% Plot the displacement u
subplot(2, 3, 1);
surf(x, y, reshape(u, Ny, Nx));
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('Horisontal displacement u', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

% Plot the displacement v
subplot(2, 3, 2);
surf(x, y, reshape(v, Ny, Nx));
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('Vertical displacement v', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

% Plot the pressure
subplot(2, 3, 3)
surf(x, y, reshape(p, Ny, Nx));
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('Pore Pressure p', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

% Plot u_a
subplot(2, 3, 4)
surf(x, y, u_a);
xlabel('x [m]', 'FontSize', font_size);
ylabel('y [m]', 'FontSize', font_size);
zlabel('u_a', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

% Plot v_a
subplot(2, 3, 5)
surf(x, y, v_a);
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('v_a', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

% Plot the stress in the y-direction sigma_yy
subplot(2, 3, 6)
surf(x, y, p_a);
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('p_a', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

title_with_time = suptitle(strcat('t = ', num2str(t), ' s'));
set(title_with_time, 'FontSize', font_size);
pause(0.01);

end
