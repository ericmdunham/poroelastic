# Calculates MMS forcing functions for Mandel's problem in the implicit form. 
# Note that the solutions are only valid for a problem with constant coefficients
# in this case. We also assume that 0 < x < 1, 0 < y < 1.

from sympy import *
from mtransform import mtransform
from math import pi

# Define the filenames of the output files
m_filename = 'PoroelasticMMSHomogeneous.m'

# Define symbols for the 2D domain
x, y, t = symbols('x y t')

# Define symbols for the unknown fields
u, v, p = symbols('u v p')

# Define symbols for the material parameters
Lambda, G, alpha, M, kappa = symbols('Lambda G alpha M kappa')

# Define the functions u*, v* and p* used for the mms solutions.
u = sin(pi*x)*sin(pi*y)*cos(pi*1000*t)
v = sin(pi*x)*sin(pi*y)*cos(pi*1000*t)
p = sin(pi*x)*sin(pi*y)*cos(pi*1000*t)

#
# Calculate the forcing functions in the interior symbolically
#

f1 = (Lambda + 2*G)*diff(diff(u, x), x) + Lambda*diff(diff(v, y), x) + G*diff(diff(u, y), y) + G*diff(diff(v, x), y) - alpha*diff(p, x)
f2 = G*diff(diff(v, x), x) + G*diff(diff(u, y), x) + (Lambda + 2*G)*diff(diff(v, y), y) + Lambda*diff(diff(u, x), y) - alpha*diff(p, y)
f3 = -1/M*diff(p, t) + kappa*diff(diff(p, x), x) + kappa*diff(diff(p, y), y) - alpha*diff(diff(u, x), t) - alpha*diff(diff(v, y), t)

# Calculate stresses
sigma_xx = (Lambda + 2*G)*diff(u, x) + Lambda*diff(v, y) - alpha*p
sigma_xy = G*diff(v, x) + G*diff(u, y)
sigma_yy = (Lambda + 2*G)*diff(v, y) + Lambda*diff(u, x) - alpha*p

# Calculate discharge velocity
q_x = kappa*diff(p, x)
q_y = kappa*diff(p, y)

# Write MMS functions to MATLAB file
with open(m_filename, 'w') as file:
	file.write("%\n% This is a generated file from a python script.\n% It has MMS forcing functions for a poroelastic problem\n%\n\n")
	file.write("function [u, v, p, f1, f2, f3, sigma_xx, sigma_xy, sigma_yy, q_x, q_y, p_p] = PoroelasticMMSHomogeneous(Lambda, G, M, kappa, alpha)\n")
	file.write("\n% Unknowns:\n")
	file.write("u = @(x, y, t) " + mtransform(u) + "\n")
	file.write("v = @(x, y, t) " + mtransform(v) + "\n")
	file.write("p = @(x, y, t) " + mtransform(p) + "\n")
	file.write("\n% Interior Forcing functions:\n")
	file.write("f1 = @(x, y, t) " + mtransform(f1) + "\n")
	file.write("f2 = @(x, y, t) " + mtransform(f2) + "\n")
	file.write("f3 = @(x, y, t) " + mtransform(f3) + "\n")
	file.write("\n% Sigma fields:\n")
	file.write("sigma_xx = @(x, y, t) " + mtransform(sigma_xx) + "\n")
	file.write("sigma_xy = @(x, y, t) " + mtransform(sigma_xy) + "\n")
	file.write("sigma_yy = @(x, y, t) " + mtransform(sigma_yy) + "\n")
	file.write("\n% q fields:\n")
	file.write("q_x = @(x, y, t) " + mtransform(q_x) + "\n")
	file.write("q_y = @(x, y, t) " + mtransform(q_y) + "\n")
	file.write("\nend")
