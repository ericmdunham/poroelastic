# Calculates MMS forcing functions for Mandel's problem in the implicit form. 
# Note that the solutions are valid for a problem with variable coefficients
# It is assumed that 0 < x < 1, 0 < y < 1.

from sympy import *
from mtransform import mtransform
from math import pi

# Define the filenames of the output files
m_filename = 'MMSGeneration.m'

# Define symbols for the 2D domain
x, y, t = symbols('x y t')

# Define symbols for the unknown fields
u, v, p = symbols('u v p')

# Define symbols for the material parameters
Lambda, G, alpha, M, kappa = symbols('Lambda G alpha M kappa')

# Define the functions u, v and p used for the mms solutions.
u = sin(pi*x)*sin(pi*y)*cos(pi*1000*t)
v = sin(pi*x)*sin(pi*y)*cos(pi*1000*t)
p = sin(pi*x)*sin(pi*y)*cos(pi*1000*t)

# Define the material parameters
Lambda = 1 + 0.5*sin(pi*x)*cos(pi*y)
G = 1 + 0.5*cos(pi*x)*sin(pi*y)
alpha = 0.7 + 0.2*sin(pi*x)*cos(pi*y)
M = 1 + 0.8*sin(pi*x)*sin(pi*y)
kappa = 0.001 + 1*cos(0.5*pi*x)*cos(0.5*pi*y)


# B = 0.8 - 0.1*cos(0.5*pi*x)*sin(pi*y)
# nu = 0.2 + 0.05*sin(pi*x)*cos(pi*y)
# nu_u = 0.4 + 0.05*sin(pi*x)*cos(pi*y)

# G = 1 + 0.8*sin(pi*x)*cos(pi*y)
# kappa = 1 + 100*cos(0.5*pi*x)*cos(0.5*pi*y)

# K = 2*G*(1 + nu)/(3*(1 - 2*nu))
# Lambda = K - 2*G/3
# alpha = 3*(nu_u - nu)/(B*(1 - 2*nu)*(1 + nu_u))
# M = 2*G*(nu_u - nu)/(alpha**2*(1 - 2*nu_u)*(1 - 2*nu))

#
# Calculate the forcing functions in the interior symbolically
#

f1 = diff((Lambda + 2*G)*diff(u, x), x) + diff(Lambda*diff(v, y), x) + diff(G*diff(u, y), y) + diff(G*diff(v, x), y) - diff(alpha*p, x)
f2 = diff(G*diff(v, x), x) + diff(G*diff(u, y), x) + diff((Lambda + 2*G)*diff(v, y), y) + diff(Lambda*diff(u, x), y) - diff(alpha*p, y)
f3 = -1/M*diff(p, t) + kappa*diff(diff(p, x), x) + kappa*diff(diff(p, y), y) - alpha*diff(diff(u, x), t) - alpha*diff(diff(v, y), t)

# Calculate stresses
sigma_xx = (Lambda + 2*G)*diff(u, x) + Lambda*diff(v, y) - alpha*p
sigma_xy = G*diff(v, x) + G*diff(u, y)
sigma_yy = (Lambda + 2*G)*diff(v, y) + Lambda*diff(u, x) - alpha*p

# Calculate discharge velocity
q_x = kappa*diff(p, x)
q_y = kappa*diff(p, y)

# Write MMS functions to MATLAB file
with open(m_filename, 'w') as file:
	file.write("%\n% This is a generated file from a python script.\n% It has MMS forcing functions for a poroelastic problem\n%\n\n")
	file.write("function [u, v, p, f1, f2, f3, sigma_xx, sigma_xy, sigma_yy, q_x, q_y, lambda, G, alpha, M, kappa] = MMSGeneration()\n")
	file.write("\n% Unknowns:\n")
	file.write("u = @(x, y, t) " + mtransform(u) + "\n")
	file.write("v = @(x, y, t) " + mtransform(v) + "\n")
	file.write("p = @(x, y, t) " + mtransform(p) + "\n")
	file.write("\n% Interior Forcing functions:\n")
	file.write("f1 = @(x, y, t) " + mtransform(f1) + "\n")
	file.write("f2 = @(x, y, t) " + mtransform(f2) + "\n")
	file.write("f3 = @(x, y, t) " + mtransform(f3) + "\n")
	file.write("\n% Sigma fields:\n")
	file.write("sigma_xx = @(x, y, t) " + mtransform(sigma_xx) + "\n")
	file.write("sigma_xy = @(x, y, t) " + mtransform(sigma_xy) + "\n")
	file.write("sigma_yy = @(x, y, t) " + mtransform(sigma_yy) + "\n")
	file.write("\n% q fields:\n")
	file.write("q_x = @(x, y, t) " + mtransform(q_x) + "\n")
	file.write("q_y = @(x, y, t) " + mtransform(q_y) + "\n")
	file.write("\n% q material parameters:\n")
	file.write("lambda = @(x, y) " + mtransform(Lambda) + "\n")
	file.write("G = @(x, y) " + mtransform(G) + "\n")
	file.write("alpha = @(x, y) " + mtransform(alpha) + "\n")
	file.write("M = @(x, y) " + mtransform(M) + "\n")
	file.write("kappa = @(x, y) " + mtransform(kappa) + "\n")
	file.write("\nend")
