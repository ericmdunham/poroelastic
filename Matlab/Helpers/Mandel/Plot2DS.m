%% 2D plot of Mandel's problem
%
%   Input:
%   Vector x        - Spatial grid in x-direction
%   Vector y        - Spatial grid in y-direction
%   Scalar t        - Time point
%   Vector u        - Solution vector of the horisontal displacement
%   Vector v        - Solution vector of the vertical displacement
%   Vector p        - Solution vector of the pore pressure
%   Vector sigma_xx - Solution vector of the stress sigma_xx
%   Vector sigma_xy - Solution vector of the strain sigma_xy
%   Vector sigma_yy - Solution vector of the stress sigma_yy
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = Plot2DS(x, y, t, u, v, p, sigma_xx, sigma_xy, sigma_yy)

Nx = length(x);
Ny = length(y);

font_size = 15;

% Plot the displacement u
subplot(2, 3, 1);
surf(x, y, reshape(u, Ny, Nx));
xlabel('x [m]', 'FontSize', font_size);
ylabel('y [m]', 'FontSize', font_size);
zlabel('Horisontal displacement u [m]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-37.5, 15);

% Plot the displacement v
subplot(2, 3, 2);
surf(x, y, reshape(v, Ny, Nx));
xlabel('x [m]', 'FontSize', font_size);
ylabel('y [m]', 'FontSize', font_size);
zlabel('Vertical displacement v [m]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-37.5, 15);

% Plot the pressure
subplot(2, 3, 3)
surf(x, y, reshape(p, Ny, Nx));
xlabel('x [m]', 'FontSize', font_size);
ylabel('y [m]', 'FontSize', font_size);
zlabel('Pore Pressure p [Pa]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-37.5, 15);

% Plot the stress sigma_xx
subplot(2, 3, 4);
surf(x, y, reshape(sigma_xx, Ny, Nx));
xlabel('x [m]', 'FontSize', font_size);
ylabel('y [m]', 'FontSize', font_size);
zlabel('Stress in x-direction \sigma_{xx} [Pa]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-37.5, 15);

% Plot the starin sigma_xy
subplot(2, 3, 5);
surf(x, y, reshape(sigma_xy, Ny, Nx));
xlabel('x [m]', 'FontSize', font_size);
ylabel('y [m]', 'FontSize', font_size);
zlabel('Strain \sigma_{xy} [Pa]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-37.5, 15);

% Plot the stress sigma_yy
subplot(2, 3, 6)
surf(x, y, reshape(sigma_yy, Ny, Nx));
xlabel('x [m]', 'FontSize', font_size);
ylabel('y [m]', 'FontSize', font_size);
zlabel('Stress in y-direction \sigma_{yy} [Pa]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-37.5, 15);

title_with_time = suptitle(strcat('t = ', num2str(t), ' s'));
set(title_with_time, 'FontSize', font_size);
pause(0.01);

end
