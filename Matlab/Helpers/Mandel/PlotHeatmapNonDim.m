%% 2D heatmap plot of Mandel's problem
%
%   Input:
%   Vector x    - Spatial grid in x-direction
%   Vector y    - Spatial grid in y-direction
%   Scalar t    - Time point
%   Vector u    - Solution vector of the horisontal displacement
%   Vector v    - Solution vector of the vertical displacement
%   Vector p    - Solution vector of the pore pressure
%   Vector Nx   - Number of points in the x-direction
%   Vector Ny   - Number of points in the y-direction
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = PlotHeatmapNonDim(x, y, t, u, v, p)

Nx = length(x);
Ny = length(y);

FontSize = 20;

% Plot the displacement u
subplot(1, 3, 1);
imagesc(x, y, reshape(u, Ny, Nx));
colorbar;
xlabel('x^*', 'FontSize', FontSize);
ylabel('y^*', 'FontSize', FontSize);
title('Horisontal displacement u^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

% Plot the displacement v
subplot(1, 3, 2);
imagesc(x, y, reshape(v, Ny, Nx));
colorbar;
xlabel('x^*', 'FontSize', FontSize);
ylabel('y^*', 'FontSize', FontSize);
title('Vertical displacement v^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

% Plot the pressure
subplot(1, 3, 3)
imagesc(x, y, reshape(p, Ny, Nx));
colorbar;
xlabel('x^*', 'FontSize', FontSize);
ylabel('y^*', 'FontSize', FontSize);
title('Pore Pressure p^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

title_with_time = suptitle(strcat('t = ', num2str(t), ' s'));
set(title_with_time, 'FontSize', FontSize);
pause(0.01);

end
