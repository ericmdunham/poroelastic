%% Analytical Solutions
% Function for computing the analytical solutions of displacements and
% pore pressure for Mandel's problem. Solutions were derived by Cheng
% et al (1988)
%
% Input:
%   Vector x            - Spatial grid in x-direction
%   Vector y            - Spatial grid in y-direction
%   Vector t            - Temporal grid
%   Scalar G            - Shear modulus
%   Scalar F            - Compressive force
%   Scalar c            - Hydraulic diffusitivity
%   Scalar nu           - Poisson ratio
%   Scalar nu_u         - Poisson ratio (undrained)
%   Scalar B            - Skemptons parameter
%   Scalar r_max        - Maximal value of root to solutions of
%                         tan(r) = (1 - nu)/(nu_u - nu)*r
%
% Output:
%   Vector u_a          - Horizontal displacement
%   Vector v_a          - Vertical displacement
%   Vector p_a          - Pore pressure
%   Vector sigma_yy_a   - Stress in y-direction
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [u_a, v_a, p_a, sigma_yy_a] = ...
    AnalyticalSolutions(x, y, t, G, F, c, nu, nu_u, B, r_max)

% Find roots r of the equation:
% tan(r) = (1 - nu)/(nu_u - nu)*r
% (there are an infinite amount of solutions)
syms r;
eq = tan(r) == (1 - nu)/(nu_u - nu)*r;
s = feval(symengine, 'numeric::realroots', eq, ...
    strcat('r=0.001..',num2str(r_max)), 1e-4);
r_i = zeros(1, length(s));
for i = 1:length(s)
    r_i(i) = vpasolve(eq, r, s(i));
end
% Extract grid points
Nx = length(x);
Ny = length(y);
Nt = length(t);

% Extract length a in spatial direction
a = x(end)-x(1);

% Analytical solution for the displacement u, (depends on x only)
u_a = zeros(Nx, Nt);
for i = 1:Nx
    for j = 1:Nt
        u_a(i, j) = (F*nu/(2*G*a) - F*nu_u/(G*a)...
            *sum(sin(r_i).*cos(r_i)...
            ./(r_i - sin(r_i).*cos(r_i))...
            .*exp(-r_i.^2*c*t(j)/a^2)))*x(i)...
            +F/G*sum(cos(r_i)./(r_i - sin(r_i).*cos(r_i))...
            .*sin(r_i*x(i)/a).*exp(-r_i.^2*c*t(j)/a^2));
    end
end

% Analytical solution for the displacement v, (depends on y only)
if nargout > 1
    v_a = zeros(Ny, Nt);
    for i = 1:Nt
        v_a(:, i) = (-F*(1 - nu)/(2*G*a) + F*(1 - nu_u)/(G*a)...
            *sum(sin(r_i).*cos(r_i)./(r_i - sin(r_i).*cos(r_i))...
            .*exp(-r_i.^2*c*t(i)/a^2)))*y;
    end
end

% Analytic solution for the pressure p, (depends on x only)
if nargout > 2
    p_a = zeros(Nx, Nt);
    for i = 1:Nx
        for j = 1:Nt
            p_a(i, j) = 2*F*B*(1 + nu_u)/(3*a)...
                *sum(sin(r_i)./(r_i - sin(r_i).*cos(r_i))...
                .*(cos(r_i*x(i)/a) - cos(r_i))...
                .*exp(-r_i.^2*c*t(j)/a^2));
        end
    end
end

% Analytical solution for stress sigma_yy, (depends on x only)
if nargout > 3
    sigma_yy_a = zeros(Nx, Nt);
    for i = 1:Nx
        for j = 1:Nt
            sigma_yy_a(i, j) = -F/a - 2*F*(nu_u - nu)/(a*(1 - nu))*...
                sum(sin(r_i)./(r_i - sin(r_i).*cos(r_i)).*cos(r_i*x(i)/a).*...
                exp(-r_i.^2*c*t(j)/a^2)) + ...
                2*F/a*sum(sin(r_i).*cos(r_i)./(r_i - sin(r_i).*cos(r_i)).*...
                exp(-r_i.^2*c*t(j)/a^2));
        end
    end
end

end
