%% 1D plot of the pressure in Mandel's problem
%
%   Input:
%   Vector x    - Spatial grid in x-direction
%   Scalar Ny   - The resolution in the y-direction
%   Scalar t    - Time point
%   Vector p    - Solution vector of the pore pressure
%   Vector p_a  - The analytical solution to p
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = Plot1DP(x, Ny, t, p, p_a)

% Pick out the 1D dependencies of the solution
Nx = length(x);
p = reshape(p, Ny, Nx);
p = p(1, :);

% Plotting
font_size = 20;

% Plot the pressure
plot(x, p, '*', x, p_a);
ylabel('Pore pressure p', 'FontSize', font_size);
xlabel('x [m]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

title_with_time = title(strcat('t = ', num2str(t), ' s'));
set(title_with_time, 'FontSize', font_size);
pause(0.01);

end
