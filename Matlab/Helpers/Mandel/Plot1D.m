%% 1D plot of Mandel's problem
%
%   Input:
%   Vector x            - Spatial grid in x-direction
%   Vector y            - Spatial grid in y-direction
%   Scalar t            - Time point
%   Vector u            - Solution vector of the horisontal displacement
%   Vector v            - Solution vector of the vertical displacement
%   Vector p            - Solution vector of the pore pressure
%   Vector u_a          - The analytical solution to u
%   Vector v_a          - The analytical solution to v
%   Vector p_a          - The analytical solution to p
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = Plot1D(x, y, t, u, v, p, u_a, v_a, p_a)

% Pick out the 1D dependencies of the solutions
Nx = length(x);
Ny = length(y);

u = reshape(u, Ny, Nx);
u = u(1, :);

v = reshape(v, Ny, Nx);
v = v(:, 1);

p = reshape(p, Ny, Nx);
p = p(1, :);

% Plotting
font_size = 20;

% Plot the displacement u
subplot(1, 3, 1)
plot(x, u, '*', x, u_a);
ylabel('Horizontal displacement u', 'FontSize', font_size);
xlabel('x [m]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

% Plot the displacement v
subplot(1, 3, 2)
plot(y, v, '*', y, v_a);
ylabel('Vertical displacement v', 'FontSize', font_size);
xlabel('y [m]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

% Plot the pressure
subplot(1, 3, 3)
plot(x, p, '*', x, p_a);
ylabel('Pore pressure p', 'FontSize', font_size);
xlabel('x [m]', 'FontSize', font_size);
set(gca, 'FontSize', font_size);

title_with_time = suptitle(strcat('t = ', num2str(t), ' s'));
set(title_with_time, 'FontSize', font_size);
pause(0.01);

end
