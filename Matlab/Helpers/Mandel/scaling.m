%% Scaling of Mandel's problem
% The function takes the unitless fields and the parameters needed to
% rescale the fields to SI units for presentation in a plot. It returns the
% scaled fields.
%
%   Input:
%   Vector x        - Spatial grid in x-direction
%   Vector y        - Spatial grid in y-direction
%   Scalar t        - Time point
%   Vector u        - Solution vector of the horisontal displacement
%   Vector v        - Solution vector of the vertical displacement
%   Vector p        - Solution vector of the pore pressure
%   Scalar a        - Horizontal side length of problem setup
%   Scalar b        - Vertical side length of problem setup
%   Scalar F        - The compressive force
%   Scalar G        - Shear Modulus
%   Scalar kappa    - k/my, where k is permeability and my fluid viscosity
%   
%   Ouput variables should be clear from input.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [x, y, t, u, v, p] = Scaling(x, y, t, u, v, p, a, b, F, G, kappa)

x = a*x;
y = b*y;
u = F/G*u;
v = F*b/(G*a)*v;
p = F/a*p;
t = a^2/(kappa*G)*t;

end
