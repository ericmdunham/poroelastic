%% Wrapper for the Russian operators 
% Generates SBP matrices D+, D- and H of internal order
% 3, 5 or 7 on equidistant grids with m+1 points. The SBP 
% scripts that the wrapper uses were written by Ken Mattsson.
%
%   Input:
%   Scalar m        - Number of points in the x-direction
%   Scalar h     	- Number of points in the y-direction
%   Scalar order 	- The order of the operators used. 3, 5, 7 or 9.
%
%   Output:
%   Matrix Dp              - D+ upwind matrix.
%   Matrix Dm              - D- downwind matrix.
%   Matrix HI              - Inverse of the H matrix.
%   Vector e_1             - Left boundary vector. Used for SAT.
%   Vector e_m             - Right boundary vector. Used for SAT.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dp, Dm, HI, e_1, e_m] = SBP(m, h, order)
    if order == 3
        SBP33Upwind;
    elseif order == 5;
        SBP54Upwind;
    elseif order == 7
        SBP7Upwind;
    elseif order == 9
        SBP9Upwind;
    else
        error('Order not compatible with the Russian Operators');
    end
end

