%% Spatial discritization with the narrow operators
%
% Script that discretizes the poroelastic problem with the explicit scheme
% using the narrow operators.
%
%   Input:
%   Scalar Nx           - Resolution in x-direction
%   Scalar dx           - Step size in x-direction
%   Scalar Ny           - Resolution in y-direction
%   Scalar dy           - Step size in y-direction
%   Matrix alpha        - Biot's coefficient
%   Matrix A_u          - A = diag(lambda_u + 2G,G)
%   Matrix B_u          - B = diag(G,lambda_u + 2G)
%   Matrix C_u          - [0, lambda_u; G, 0]
%   Matrix kappa        - k/my, where k is permeability and my fluid
%                         viscosity
%   Matrix M            - Biot's modulus.
%   Matrix BC           - Boundary condition matrix.
%   Scalar order        - Order of the operators. Should be 2 or 4 for the
%                         narrow operators.
%
%   Optional Input:
%   x                   - The transformed grid. 
%   TransDir            - Which coordinate axis is transformed. 
%                         Should be 'x' or 'y'
%
%   Output:
%   Matrix Dx           - First derivative operator in the x-direction.
%   Matrix Dy           - First derivative operator in the y-direction.
%   Matrix M_u          - The matrix for the displacements u and v in the
%                         mechanical equilibrium equation.
%   Matrix M_p          - The matrix for the pressure in the mechanical
%                         equilibrium equation.
%   Object M_b          - A boundary data object with the matrices and
%                         vectors needed for imposing the boundary data in
%                         each time step for the mechanical equilibrium
%                         equation.
%   Matrix F_u          - The matrix for the displacements u and v in the
%                         fluid flow equation.
%   Matrix F_p          - The matrix for the pressure in the fluid flow
%                         equation.
%   Object F_b          - A boundary data object with the matrices and
%                         vectors needed for imposing the boundary data in
%                         each time step for the fluid flow equation.
%   Matrix P            - M alpha ( xHat' kron Dx + yHat' kron Dy). Used
%                         for converting between p' and p.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = NarrowStencil(...
    Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order, x, TransDir)

% Stability conditions
sigmaW = eye(2);
sigmaE = eye(2);
sigmaS = eye(2);
sigmaN = eye(2);

betaW = 1;
betaE = 1;
betaS = 1;
betaN = 1;

% Extract boundary conditions
gammaW = [BC(1,1) 0; 0 BC(2,1)];
gammaE = [BC(1,2) 0; 0 BC(2,2)];
gammaS = [BC(1,3) 0; 0 BC(2,3)];
gammaN = [BC(1,4) 0; 0 BC(2,4)];

deltaW = BC(3,1);
deltaE = BC(3,2);
deltaS = BC(3,3);
deltaN = BC(3,4);

%% SBP operators
Im = speye(Ny);
In = speye(Nx);
I2 = speye(2);
xHat = [1; 0];
yHat = [0; 1];

% D1 in x-direction
mats_x = SBP(Nx, dx, order);
D1n = mats_x.D1int;
HIn = mats_x.Hinv;
e1n = zeros(Nx, 1); e1n(1) = 1;
en = zeros(Nx, 1); en(Nx) = 1;

% D1 in y-direction
mats_y = SBP(Ny, dy, order);
D1m = mats_y.D1int;
HIm = mats_y.Hinv;
e1m = zeros(Ny, 1); e1m(1) = 1;
em = zeros(Ny, 1); em(Ny) = 1;

% Pick out the material parameters needed for the second derivative
% operators in the x- and y-direction.
Au1 = A_u(1:Ny*Nx, 1:Ny*Nx);
Au2 = A_u(Ny*Nx + 1:2*Ny*Nx, Ny*Nx + 1:2*Ny*Nx);
Bu1 = B_u(1:Ny*Nx, 1:Ny*Nx);
Bu2 = B_u(Ny*Nx + 1:2*Ny*Nx, Ny*Nx + 1:2*Ny*Nx);
kappaX = kappa;
kappaY = kappa;

% Obtain the drained parameters used for the R-term in the narrow stencil
A1 = Au1 - alpha^2*M;
B2 = Bu2 - alpha^2*M;

% Do the coordinate transform of the operators if specified
if nargin > 12
    if TransDir == 'x'
        J = spdiags(D1n*x, 0, Nx, Nx);
        Jinv = inv(J);
        D1n = Jinv*D1n;
        HIn = Jinv*HIn;
        Jinv2D = kron(Jinv, Im);
        % This is how the coordinate transformation is done for Dxx
        Au1 = Au1*Jinv2D;
        Au2 = Au2*Jinv2D;
        A1 = A1*Jinv2D;
        kappaX = kappaX*Jinv2D;
    else
        J = spdiags(D1m*x, 0, Ny, Ny);
        Jinv = inv(J);
        D1m = Jinv*D1m;
        HIm = Jinv*HIm;
        Jinv2D = kron(In, Jinv);
        % This is how the coordinate transformation is done for Dyy
        Bu1 = Bu1*Jinv2D;
        Bu2 = Bu2*Jinv2D;
        B2 = B2*Jinv2D;
        kappaY = kappaY*Jinv2D;
    end
end

% D2 with damping in x-direction
Dxx_A11 = NarrowXX(Nx, Ny, dx, Au1, order, A1);
Dxx_A22 = NarrowXX(Nx, Ny, dx, Au2, order);
Dxx_A = blkdiag(Dxx_A11, Dxx_A22);
Dxx_kappa = NarrowXX(Nx, Ny, dx, kappaX, order);

% D2 with damping in y-direction
Dyy_B11 = NarrowYY(Nx, Ny, dy, Bu1, order);
Dyy_B22 = NarrowYY(Nx, Ny, dy, Bu2, order, B2);
Dyy_B = blkdiag(Dyy_B11, Dyy_B22);
Dyy_kappa = NarrowYY(Nx, Ny, dy, kappaY, order);

% Do coordinate transform was given transform the second derivative
% operators again to get from psi to x.
if nargin > 12
    if TransDir == 'x'
        % This is how the coordinate transformation is done for Dxx
        Dxx_A = blkdiag(Jinv2D, Jinv2D)*Dxx_A;
        Dxx_kappa = Jinv2D*Dxx_kappa;
    else
        % This is how the coordinate transformation is done for Dyy
        Dyy_B = blkdiag(Jinv2D, Jinv2D)*Dyy_B;
        Dyy_kappa = Jinv2D*Dyy_kappa;
    end
end

% Scalar operators
Dx = kron(D1n, Im);
eN = kron(In, em);
eS = kron(In, e1m);
HIx = kron(HIn, Im);
Dy = kron(In, D1m);
eE = kron(en, Im);
eW = kron(e1n, Im);
HIy = kron(In, HIm);

% Vector (system) operators
Dx2 = kron(I2,Dx);
eN2 = kron(I2,eN);
eS2 = kron(I2,eS);
HIx2 = kron(I2,HIx);
Dy2 = kron(I2,Dy);
eE2 = kron(I2,eE);
eW2 = kron(I2,eW);
HIy2 = kron(I2,HIy);

%% Fluid Flow Equation

% The matrix for the relative pore pressure p' in the fluid flow equation
F_p = M*(Dxx_kappa + Dyy_kappa + ...
    HIx*eW*(-deltaW*eW' + betaW*eW'*kappa*Dx) + ...
    HIx*eE*(-deltaE*eE' - betaE*eE'*kappa*Dx) + ...
    HIy*eS*(-deltaS*eS' + betaS*eS'*kappa*Dy) + ...
    HIy*eN*(-deltaN*eN' - betaN*eN'*kappa*Dy));

% The matrix for the displacements u and v in the fluid flow equation.
F_u = M*(-Dxx_kappa*alpha*M*(kron(xHat', Dx) + kron(yHat', Dy)) - ...
    Dyy_kappa*alpha*M*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIx*eW*(deltaW*eW' - betaW*eW'*kappa*Dx)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIx*eE*(deltaE*eE' + betaE*eE'*kappa*Dx)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIy*eS*(deltaS*eS' - betaS*eS'*kappa*Dy)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIy*eN*(deltaN*eN' + betaN*eN'*kappa*Dy)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)));

% A boundary data object with the matrices needed for imposing the boundary
% data in each time step.
F_b.W = M*HIx*eW;
F_b.E = M*HIx*eE;
F_b.S = M*HIy*eS;
F_b.N = M*HIy*eN;

%% Mechanical Equilibrium Equation

% The matrix for the displacements u and v in the mechanical equilibrium
% equations.
M_u = Dxx_A + Dx2*C_u*Dy2 + Dy2*C_u'*Dx2 + Dyy_B + ...
    HIx2*eW2*(-kron(gammaW,eW') + kron(sigmaW,eW')*(A_u*Dx2 + C_u*Dy2)) + ...
    HIx2*eE2*(-kron(gammaE,eE') - kron(sigmaE,eE')*(A_u*Dx2 + C_u*Dy2)) + ...
    HIy2*eS2*(-kron(gammaS,eS') + kron(sigmaS,eS')*(B_u*Dy2 + C_u'*Dx2)) + ...
    HIy2*eN2*(-kron(gammaN,eN') - kron(sigmaN,eN')*(B_u*Dy2 + C_u'*Dx2));

% The matrix for the relative pore pressure p' in the mechanical equilibrium
% equations.
M_p = -kron(xHat,Dx*alpha) - kron(yHat, Dy*alpha) ...
    - HIx2*eW2*(kron(sigmaW,eW')*kron(xHat,alpha)) ...
    + HIx2*eE2*(kron(sigmaE,eE')*kron(xHat,alpha)) ...
    - HIy2*eS2*(kron(sigmaS,eS')*kron(yHat,alpha)) ...
    + HIy2*eN2*(kron(sigmaN,eN')*kron(yHat,alpha));

% A boundary data object with the matrices needed for imposing the boundary
% data in each time step.
M_b.W = HIx2*eW2;
M_b.E = HIx2*eE2;
M_b.S = HIy2*eS2;
M_b.N = HIy2*eN2;

end