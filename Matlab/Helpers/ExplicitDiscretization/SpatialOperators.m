%% Construct the spatial operators for the exlicit scheme
%   Function for constructing the spatial operators used for solving the
%   linear poroelastic equations in an explicit form. This is a wrapper for
%   choosing which type of operators should be used.
%
%   Input:
%   Vector Nx           - Spatial resolution in x-direction
%   Vector dx           - Spatial step size in x-direction
%   Vector Ny           - Spatial resolution in y-direction
%   Vector dy           - Spatial step size in y-direction
%   Matrix alpha        - Biot's coefficient
%   Matrix K            - Drained bulk modulus
%   Matrix G            - Shear modulus
%   Matrix kappa        - k/my, where k is permeability and my fluid 
%                         viscosity,
%   Matrix M            - Biot's modulus. 
%   Matrix BC           - Boundary condition matrix.
%   String SBP_stencil  - Flag for choosing SBP-operator.
%                         w - wide, n - narrow, r - russian
%   Scalar order        - Order of operators. Must be consistent with
%                         SBP_stencil
%
%   Optional Input:
%   x                   - The transformed grid. 
%   TransDir            - The direction of the matrix. Should be 'x' or 'y'
%
%   Output:
%   Matrix F_u          - The matrix for the displacements u and v in the 
%                         fluid flow equation.
%   Matrix F_p          - The matrix for the pressure in the fluid flow 
%                         equation.
%   Object F_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the fluid flow equation.
%   Matrix M_u          - The matrix for the displacements u and v in the 
%                         mechanical equilibrium equation.
%   Matrix M_p          - The matrix for the pressure in the mechanical 
%                         equilibrium equation.
%   Object M_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the mechanical equilibrium 
%                         equation.
%   Matrix P            - M alpha ( xHat' kron Dx + yHat' kron Dy). Used
%                         for converting between p' and p. p' = p + Pw
%   Matrix Sigma_xx     - Used to calculate sigma_yy from [u, v]^T 
%                         (p' is also needed due to poroelastic effects)
%   Matrix Sigma_xy     - Used to calculate sigma_xy from [u, v]^T.
%   Matrix Sigma_yy     - Used to calculate sigma_yy from [u, v]^T 
%                         (p' is also needed due to poroelastic effects)
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [F_u, F_p, F_b, M_u, M_p, M_b, P, Sigma_xx, Sigma_xy, Sigma_yy] ...
    = SpatialOperators(Nx, dx, Ny, dy, alpha, K, G, kappa, M, ...
    SBP_stencil, BC, order, x, TransDir)

% Construct matrices used in the SBP-SAT discretization
[alpha, G, lambda_u, A_u, B_u, C_u, kappa, M] = ...
    MaterialMatrices(Nx, Ny, alpha, K, G, kappa, M);

% Find the right folder to add as path
superDir = strrep(mfilename('fullpath'), ...
    strcat('ExplicitDiscretization/', mfilename), '');

% Depending on the flag SBP_stencil, construct the SBP operators and build
% the spatial operators.
switch SBP_stencil
    case 'w'
        addpath(strcat(superDir,'OperatorsWide'));
        % No coordinate transform
        if nargin < 13
            [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = WideStencil...
                (Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order);
        % Coordinate transform
        else
            [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = WideStencil...
                (Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order, x, TransDir);
        end
        rmpath(strcat(superDir,'OperatorsWide'));
    case 'n'
        addpath(strcat(superDir,'OperatorsNarrow'));
        % No coordinate transform
        if nargin < 13
            [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = NarrowStencil...
                (Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order);
        % Coordinate transform
        else
            [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = NarrowStencil...
                (Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order, x, TransDir);
        end
        rmpath(strcat(superDir,'OperatorsNarrow'));
    case 'r'
        addpath(strcat(superDir,'OperatorsRussian'));
        % No coordinate transform
        if nargin < 13
            [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = RussianStencil...
                (Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order);
        % Coordinate transform
        else 
            [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = RussianStencil...
                (Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order, x, TransDir);
        end
        rmpath(strcat(superDir,'OperatorsRussian'));
    case 'c'
        addpath(strcat(superDir,'OperatorsCompatible'));
        [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = CompatibleStencil...
                (Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order);
        rmpath(strcat(superDir,'OperatorsCompatible'));
end

% Some matrices used for calculating variable fields.

% Matrix used for converting between p' and p
P = M*alpha*(kron([1 0], Dx) + kron([0 1], Dy));

% Matrix used to calculate sigma_xx from [u, v]^T (p' is also needed due to
% poroelastic effects)
Sigma_xx = [(lambda_u + 2*G)*Dx lambda_u*Dy];

% Matrix used to calculate sigma_xy from [u, v]^T.
Sigma_xy = [G*Dy G*Dx];

% Matrix used to calculate sigma_yy from [u, v]^T (p' is also needed due to
% poroelastic effects)
Sigma_yy = [lambda_u*Dx (lambda_u + 2*G)*Dy];

end
