%% Spatial discritization with the wide operators
%  
% Script that discretizes the poroelastic problem with the explicit scheme
% using the wide operators.
%
%   Input:
%   Scalar Nx           - Resolution in x-direction
%   Scalar dx           - Step size in x-direction
%   Scalar Ny           - Resolution in y-direction
%   Scalar dy           - Step size in y-direction
%   Matrix alpha        - Biot's coefficient
%   Matrix A_u          - A = diag(lambda_u + 2G,G)   
%   Matrix B_u          - B = diag(G,lambda_u + 2G) 
%   Matrix C_u          - [0, lambda_u; G, 0]  
%   Matrix kappa        - k/my, where k is permeability and my fluid
%                         viscosity
%   Matrix M            - Biot's modulus. 
%   Matrix BC           - Boundary condition matrix.
%   Scalar order        - Order of the operators. Should be 2, 4 or 6
%                         for the wide operators.
%
%   Optional Input:
%   x                   - The transformed grid. 
%   TransDir            - Which coordinate axis is transformed. 
%                         Should be 'x' or 'y'
%
%   Output:
%   Matrix Dx           - First derivative operator in the x-direction.
%   Matrix Dy           - First derivative operator in the y-direction.
%   Matrix F_u          - The matrix for the displacements u and v in the 
%                         fluid flow equation.
%   Matrix F_p          - The matrix for the pressure in the fluid flow 
%                         equation.
%   Object F_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the fluid flow equation.
%   Matrix M_u          - The matrix for the displacements u and v in the 
%                         mechanical equilibrium equation.
%   Matrix M_p          - The matrix for the pressure in the mechanical 
%                         equilibrium equation.
%   Object M_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the mechanical equilibrium 
%                         equation.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dx, Dy, F_u, F_p, F_b, M_u, M_p, M_b] = WideStencil(...
    Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order, x, TransDir)

% Stability conditions
sigmaW = eye(2);
sigmaE = eye(2);
sigmaS = eye(2);
sigmaN = eye(2);

betaW = 1;
betaE = 1;
betaS = 1;
betaN = 1;

% Extract boundary conditions
gammaW = [BC(1,1) 0; 0 BC(2,1)];
gammaE = [BC(1,2) 0; 0 BC(2,2)];
gammaS = [BC(1,3) 0; 0 BC(2,3)];
gammaN = [BC(1,4) 0; 0 BC(2,4)];

deltaW = BC(3,1);
deltaE = BC(3,2);
deltaS = BC(3,3);
deltaN = BC(3,4);

%% SBP operators
Im = speye(Ny);
In = speye(Nx);
I2 = speye(2);
xHat = [1; 0];
yHat = [0; 1];

% Construct SBP operators
[Dn, HIn, e1n, en] = SBP(Nx - 1, dx, order);
[Dm, HIm, e1m, em] = SBP(Ny - 1, dy, order);

% Do the coordinate transform of the operators if specified
if nargin > 12
    if TransDir == 'x'
        J = spdiags(Dn*x, 0, Nx, Nx);
        Jinv = inv(J);
        Dn = Jinv*Dn;
        HIn = Jinv*HIn;
    else
        J = spdiags(Dm*x, 0, Ny, Ny);
        Jinv = inv(J);
        Dm = Jinv*Dm;
        HIm = Jinv*HIm;
    end
end

% Scalar operators
Dx = kron(Dn, Im);
eN = kron(In, em);
eS = kron(In, e1m);
HIx = kron(HIn, Im);
Dy = kron(In, Dm);
eE = kron(en, Im);
eW = kron(e1n, Im);
HIy = kron(In, HIm);

% Vector (system) operators
Dx2 = kron(I2,Dx);
eN2 = kron(I2,eN);
eS2 = kron(I2,eS);
HIx2 = kron(I2,HIx);
Dy2 = kron(I2,Dy);
eE2 = kron(I2,eE);
eW2 = kron(I2,eW);
HIy2 = kron(I2,HIy);

%% Fluid Flow Equation

% The matrix for the relative pore pressure p' in the fluid flow equation
F_p = M*(Dx*kappa*Dx + Dy*kappa*Dy + ...
    HIx*eW*(-deltaW*eW' + betaW*eW'*kappa*Dx) + ...
    HIx*eE*(-deltaE*eE' - betaE*eE'*kappa*Dx) + ...
    HIy*eS*(-deltaS*eS' + betaS*eS'*kappa*Dy) + ...
    HIy*eN*(-deltaN*eN' - betaN*eN'*kappa*Dy));

% The matrix for the displacements u and v in the fluid flow equation.
F_u = M*(-Dx*kappa*Dx*alpha*M*(kron(xHat', Dx) + kron(yHat', Dy)) - ...
    Dy*kappa*Dy*alpha*M*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIx*eW*(deltaW*eW' - betaW*eW'*kappa*Dx)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIx*eE*(deltaE*eE' + betaE*eE'*kappa*Dx)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIy*eS*(deltaS*eS' - betaS*eS'*kappa*Dy)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)) + ...
    HIy*eN*(deltaN*eN' + betaN*eN'*kappa*Dy)*...
    M*alpha*(kron(xHat', Dx) + kron(yHat', Dy)));

% A boundary data object with the matrices needed for imposing the boundary 
% data in each time step.
F_b.W = M*HIx*eW;
F_b.E = M*HIx*eE;
F_b.S = M*HIy*eS;
F_b.N = M*HIy*eN;

%% Mechanical Equilibrium Equation

% The matrix for the displacements u and v in the mechanical equilibrium
% equations.
M_u = Dx2*A_u*Dx2 + Dx2*C_u*Dy2 + Dy2*C_u'*Dx2 + Dy2*B_u*Dy2 + ...
    HIx2*eW2*(-kron(gammaW,eW') + kron(sigmaW,eW')*(A_u*Dx2+C_u*Dy2)) + ...
    HIx2*eE2*(-kron(gammaE,eE') - kron(sigmaE,eE')*(A_u*Dx2+C_u*Dy2)) + ...
    HIy2*eS2*(-kron(gammaS,eS') + kron(sigmaS,eS')*(B_u*Dy2+C_u'*Dx2)) + ...
    HIy2*eN2*(-kron(gammaN,eN') - kron(sigmaN,eN')*(B_u*Dy2+C_u'*Dx2));

% The matrix for the relative pore pressure p' in the mechanical equilibrium
% equations.
M_p = -kron(xHat,Dx*alpha) - kron(yHat, Dy*alpha) ...
    - HIx2*eW2*(kron(sigmaW,eW')*kron(xHat,alpha)) ...
    + HIx2*eE2*(kron(sigmaE,eE')*kron(xHat,alpha)) ...
    - HIy2*eS2*(kron(sigmaS,eS')*kron(yHat,alpha)) ...
    + HIy2*eN2*(kron(sigmaN,eN')*kron(yHat,alpha));

% A boundary data object with the matrices needed for imposing the boundary 
% data in each time step.
M_b.W = HIx2*eW2;
M_b.E = HIx2*eE2;
M_b.S = HIy2*eS2;
M_b.N = HIy2*eN2;

end