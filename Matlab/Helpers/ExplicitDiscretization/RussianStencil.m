%% Spatial discritization with the Russian operators
%  
% Script that discretizes the poroelastic problem with the explicit scheme
% using the Russian operators.
%
%   Input:
%   Scalar Nx           - Resolution in x-direction
%   Scalar dx           - Step size in x-direction
%   Scalar Ny           - Resolution in y-direction
%   Scalar dy           - Step size in y-direction
%   Matrix alpha        - Biot's coefficient
%   Matrix A_u          - A = diag(lambda_u + 2G,G)   
%   Matrix B_u          - B = diag(G,lambda_u + 2G) 
%   Matrix C_u          - [0, lambda_u; G, 0]  
%   Matrix kappa        - k/my, where k is permeability and my fluid
%                         viscosity
%   Matrix M            - Biot's modulus. 
%   Matrix BC           - Boundary condition matrix.
%   Scalar order        - Order of the operators. Should be 3, 5, 7 or 9
%                         for the Russian operators.
%
%   Optional Input:
%   x                   - The transformed grid. 
%   TransDir            - Which coordinate axis is transformed. 
%                         Should be 'x' or 'y'
%
%   Output:
%   Matrix Dmx          - First derivative downwind operator in the 
%                         x-direction.
%   Matrix Dmy          - First derivative downwind operator in the 
%                         y-direction.
%   Matrix F_u          - The matrix for the displacements u and v in the 
%                         fluid flow equation.
%   Matrix F_p          - The matrix for the pressure in the fluid flow 
%                         equation.
%   Object F_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the fluid flow equation.
%   Matrix M_u          - The matrix for the displacements u and v in the 
%                         mechanical equilibrium equation.
%   Matrix M_p          - The matrix for the pressure in the mechanical 
%                         equilibrium equation.
%   Object M_b          - A boundary data object with the matrices and 
%                         vectors needed for imposing the boundary data in 
%                         each time step for the mechanical equilibrium 
%                         equation.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dmx, Dmy, F_u, F_p, F_b, M_u, M_p, M_b] = RussianStencil(...
    Nx, dx, Ny, dy, alpha, A_u, B_u, C_u, kappa, M, BC, order, x, TransDir)

% Stability conditions
sigmaW = eye(2);
sigmaE = eye(2);
sigmaS = eye(2);
sigmaN = eye(2);

betaW = 1;
betaE = 1;
betaS = 1;
betaN = 1;

% Extract boundary conditions
gammaW = [BC(1,1) 0; 0 BC(2,1)];
gammaE = [BC(1,2) 0; 0 BC(2,2)];
gammaS = [BC(1,3) 0; 0 BC(2,3)];
gammaN = [BC(1,4) 0; 0 BC(2,4)];

deltaW = BC(3,1);
deltaE = BC(3,2);
deltaS = BC(3,3);
deltaN = BC(3,4);

%% SBP operators
Im = speye(Ny);
In = speye(Nx);
I2 = speye(2);
xHat = [1; 0];
yHat = [0; 1];

% Construct SBP operators
[Dpn, Dmn, HIn, e1n, en] = SBP(Nx, dx, order);
[Dpm, Dmm, HIm, e1m, em] = SBP(Ny, dy, order);

% Do the coordinate transform of the operators if specified
if nargin > 12
    if TransDir == 'x'
        J = spdiags(Dmn*x, 0, Nx, Nx);
        Jinv = inv(J);
        Dpn = Jinv*Dpn;
        Dmn = Jinv*Dmn;
        HIn = Jinv*HIn;
    else
        J = spdiags(Dmm*x, 0, Ny, Ny);
        Jinv = inv(J);
        Dpm = Jinv*Dpm;
        Dmm = Jinv*Dmm;
        HIm = Jinv*HIm;
    end
end

% Scalar operators
Dpx = kron(Dpn, Im);
Dmx = kron(Dmn, Im);
eN = kron(In, em);
eS = kron(In, e1m);
HIx = kron(HIn, Im);

Dpy = kron(In, Dpm);
Dmy = kron(In, Dmm);
eE = kron(en, Im);
eW = kron(e1n, Im);
HIy = kron(In, HIm);

% Vector (system) operators
Dpx2 = kron(I2,Dpx);
Dmx2 = kron(I2,Dmx);
eN2 = kron(I2,eN);
eS2 = kron(I2,eS);
HIx2 = kron(I2,HIx);

Dpy2 = kron(I2,Dpy);
Dmy2 = kron(I2,Dmy);
eE2 = kron(I2,eE);
eW2 = kron(I2,eW);
HIy2 = kron(I2,HIy);

%% Fluid Flow Equation

% The matrix for the relative pore pressure p' in the fluid flow equation
F_p = M*(Dpx*kappa*Dmx + Dpy*kappa*Dmy + ...
    HIx*eW*(-deltaW*eW' + betaW*eW'*kappa*Dmx) + ...
    HIx*eE*(-deltaE*eE' - betaE*eE'*kappa*Dmx) + ...
    HIy*eS*(-deltaS*eS' + betaS*eS'*kappa*Dmy) + ...
    HIy*eN*(-deltaN*eN' - betaN*eN'*kappa*Dmy));

% The matrix for the displacements u and v in the fluid flow equation.
F_u = M*(-Dpx*kappa*Dmx*alpha*M*(kron(xHat', Dmx) + kron(yHat', Dmy)) - ...
    Dpy*kappa*Dmy*alpha*M*(kron(xHat', Dmx) + kron(yHat', Dmy)) + ...
    HIx*eW*(deltaW*eW' - betaW*eW'*kappa*Dmx)*...
    M*alpha*(kron(xHat', Dmx) + kron(yHat', Dmy)) + ...
    HIx*eE*(deltaE*eE' + betaE*eE'*kappa*Dmx)*...
    M*alpha*(kron(xHat', Dmx) + kron(yHat', Dmy)) + ...
    HIy*eS*(deltaS*eS' - betaS*eS'*kappa*Dmy)*...
    M*alpha*(kron(xHat', Dmx) + kron(yHat', Dmy)) + ...
    HIy*eN*(deltaN*eN' + betaN*eN'*kappa*Dmy)*...
    M*alpha*(kron(xHat', Dmx) + kron(yHat', Dmy)));

% A boundary data object with the matrices needed for imposing the boundary 
% data in each time step.
F_b.W = M*HIx*eW;
F_b.E = M*HIx*eE;
F_b.S = M*HIy*eS;
F_b.N = M*HIy*eN;

%% Mechanical Equilibrium Equation

% The matrix for the displacements u and v in the mechanical equilibrium
% equations.
M_u = Dpx2*A_u*Dmx2 + Dpx2*C_u*Dmy2 + Dpy2*C_u'*Dmx2 + Dpy2*B_u*Dmy2 + ...
    HIx2*eW2*(-kron(gammaW,eW') + kron(sigmaW,eW')*(A_u*Dmx2 + C_u*Dmy2)) + ...
    HIx2*eE2*(-kron(gammaE,eE') - kron(sigmaE,eE')*(A_u*Dmx2 + C_u*Dmy2)) + ...
    HIy2*eS2*(-kron(gammaS,eS') + kron(sigmaS,eS')*(B_u*Dmy2 + C_u'*Dmx2)) + ...
    HIy2*eN2*(-kron(gammaN,eN') - kron(sigmaN,eN')*(B_u*Dmy2 + C_u'*Dmx2));

% The matrix for the relative pore pressure p' in the mechanical equilibrium
% equations.
M_p = -kron(xHat,Dpx*alpha) - kron(yHat, Dpy*alpha) ...
    - HIx2*eW2*(kron(sigmaW,eW')*kron(xHat,alpha)) ...
    + HIx2*eE2*(kron(sigmaE,eE')*kron(xHat,alpha)) ...
    - HIy2*eS2*(kron(sigmaS,eS')*kron(yHat,alpha)) ...
    + HIy2*eN2*(kron(sigmaN,eN')*kron(yHat,alpha));

% A boundary data object with the matrices needed for imposing the boundary 
% data in each time step.
M_b.W = HIx2*eW2;
M_b.E = HIx2*eE2;
M_b.S = HIy2*eS2;
M_b.N = HIy2*eN2;

end