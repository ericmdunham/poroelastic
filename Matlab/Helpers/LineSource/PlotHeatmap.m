 %% 2D heatmap plot of the line source problem
%
%   Input:
%   Vector x    - Spatial grid in x-direction
%   Vector y    - Spatial grid in y-direction
%   Scalar t    - Time point
%   Vector u    - Numerical solution vector of the horisontal displacement 
%                 Nx*Ny-by-1
%   Vector v    - Numerical solution vector of the vertical displacement
%                 Nx*Ny-by-1
%   Vector p    - Numerical solution vector of the pore pressure Nx*Ny-by-1
%   Vector u_a  - Analytical solution vector of the horisontal displacement
%                 Ny-by-Nx
%   Vector v_a  - Analytical solution vector of the vertical displacement
%                 Ny-by-Nx
%   Vector p_a  - Analytical solution vector of the pore pressure Ny-by-Nx
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = PlotHeatmap(x, y, t, u, v, p, u_a, v_a, p_a)

Nx = length(x);
Ny = length(y);
u = reshape(u, Ny, Nx);
v = reshape(v, Ny, Nx);
p = reshape(p, Ny, Nx);

FontSize = 20;

rows = 1;
if nargin > 6
    rows = 2;
end

% Plot the displacement u
subplot(rows, 3, 1);
imagesc(x, y, u);
colorbar;
xlabel('x^*', 'FontSize', FontSize);
ylabel('y^*', 'FontSize', FontSize);
title('u^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

% Plot the displacement v
subplot(rows, 3, 2);
imagesc(x, y, v);
colorbar;
xlabel('x^*', 'FontSize', FontSize);
ylabel('y^*', 'FontSize', FontSize);
title('v^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

% Plot the pressure
subplot(rows, 3, 3)
imagesc(x, y, p);
colorbar;
xlabel('x^*', 'FontSize', FontSize);
ylabel('y^*', 'FontSize', FontSize);
title('p^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

%if analytical solutions provided, plot heatmap of difference.
if nargin > 6
% Plot the difference in displacement u
subplot(rows, 3, 4);
imagesc(x, y, abs(u-u_a));
colorbar;
xlabel('x', 'FontSize', FontSize);
ylabel('y', 'FontSize', FontSize);
title('abs(u-u_a)', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

% Plot the difference in displacement v
subplot(rows, 3, 5);
imagesc(x, y, abs(v-v_a));
colorbar;
xlabel('x', 'FontSize', FontSize);
ylabel('y', 'FontSize', FontSize);
title('abs(v-v_a)', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')

% Plot the difference in pressure p
subplot(rows, 3, 6)
imagesc(x, y, abs(p-p_a));
colorbar;
xlabel('x', 'FontSize', FontSize);
ylabel('y', 'FontSize', FontSize);
title('abs(p-p_a)', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);
set(gca,'Ydir','Normal')
end

drawnow;

end
