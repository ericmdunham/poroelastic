%% 2D plot of the line source problem
% This is a wrapper for the Dirac Delta function written by Ossian O'Reilly
% that builds the Dirac Delta source for the point source problem.
%
%   Input:
%   Vector Nx  	- Spatial resolution in x-direction
%   Vector dx 	- Spatial step size in x-direction
%   Vector Ny  	- Spatial resolution in y-direction
%   Vector dy 	- Spatial step size in y-direction
%   Scalar ooa	- Order of accuracy
%   Scalar sc  	- Number of smoothness conditions
%   Scalar Sx   - Shifts the Dirac Delta Sx number of points in x-direction
%   Scalar Sy   - Shifts the Dirac Delta Sy number of points in y-direction
%
%   Output:
%   Matrix Q    - The discretized point source.
%
%   Note:
%   ooa + sc + 1 gives the number of points in the 1D point source
%   discretization. For a grid with the source in between the points i.e. 
%   a grid with an even number of grid points, we would want to have a even 
%   number of points. This means that ooa + sc should be odd.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [Dirac] = PointSource(Nx, dx, Ny, dy, ooa, sc, Sx, Sy)

nop = ooa + sc + 1; %The length of the Dirac Delta discretization

D_x = DiracDelta(ooa, sc, 1, ones(1, nop), nop/2 - 0.5)/dx;
D_y = DiracDelta(ooa, sc, 1, ones(1, nop), nop/2 - 0.5)/dy;

x_points = Nx/2 - nop/2 + 1: Nx/2 + nop/2;
y_points = Ny/2 - nop/2 + 1: Ny/2 + nop/2;

x_points = x_points + Sx;
y_points = y_points + Sy;

Dirac = sparse(Ny, Nx);
Dirac(y_points, x_points) = D_y*D_x';
Dirac = reshape(Dirac, Ny*Nx, 1);

end