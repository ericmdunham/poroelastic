%% 1D plot of the line source problem
% Plots a 1D line in the middle of the domain. This plot does not present
% the full 2D problem, but can visualize the errors in the solution more
% clearly.
%
%   Input:
%   Vector x    - Spatial grid in x-direction
%   Vector y    - Spatial grid in y-direction
%   Vector u    - Solution vector of the horisontal displacement
%   Vector v    - Solution vector of the vertical displacement
%   Vector p    - Solution vector of the pore pressure
%   Vector Nx   - Number of points in the x-direction
%   Vector Ny   - Number of points in the y-direction
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = Plot1D(x, y, u, v, p, u_a, v_a, p_a)

% Obtain the relevant variables for plotting
Nx = length(x);
Ny = length(y);

u = reshape(u, Ny, Nx);
v = reshape(v, Ny, Nx);
p = reshape(p, Ny, Nx);

FontSize = 20;

% Plot the displacement u
subplot(1, 3, 1);
plot(x, u(Ny/2, :), '*b', x, u_a(Ny/2, :), 'b');
xlabel('x^*', 'FontSize', FontSize);
ylabel('u^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);

% Plot the displacement v
subplot(1, 3, 2);
plot(y, v(:, Nx/2), '*b', y, v_a(:, Nx/2), 'b');
xlabel('y^*', 'FontSize', FontSize);
ylabel('v^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);

% Plot the pressure
subplot(1, 3, 3)
plot(x, p(Ny/2, :), '*b', x, p_a(Ny/2, :), 'b');
xlabel('x^*', 'FontSize', FontSize);
ylabel('p^*', 'FontSize', FontSize);
set(gca, 'FontSize', FontSize);

drawnow;

end
