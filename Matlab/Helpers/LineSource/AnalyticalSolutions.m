%% Analytical solutions to the Line Source problem
%   Function for computing the analytical solutions of displacements and
%   pore pressure for the line source problem in 2D plain strain. Solutions
%   were derived by John W. Rudnicki (1986).
%
%   Input:
%   Vector x        - Spatial grid in x-direction
%   Vector y        - Spatial grid in y-direction
%   Vector t        - Temporal grid
%   Scalar q        - Fluid mass injection rate per unit length
%   Scalar rho      - Mass density of homogeneous pore fluid
%   Scalar G        - Bulk modulus
%   Scalar lambda   - First Lam� parameter
%   Scalar kappa    - Permeability
%   Scalar c        - Hydraulic diffusitivity
%   Scalar alpha    - Biot's coefficient
%
%   Output:
%   Vector u_a      - Horizontal displacement
%   Vector v_a      - Vertical displacement
%   Vector p_a      - Pore pressure
%
%   Note:
%   The mass source is located at the middle of the domain
%   ((x(end) + x(1))/2, (y(end) + y(1))/2)
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [u_a, v_a, p_a] = AnalyticalSolutions...
    (x, y, t, q, rho, G, lambda, kappa, c, alpha)

Nx = length(x);
Ny = length(y);
Nt = length(t);

x_0 = (x(end) + x(1))/2;
y_0 = (y(end) + y(1))/2;

u_const = q*alpha/(8*pi*kappa*rho*(lambda + 2*G));
p_const = q/(4*pi*kappa*rho);

R = zeros(Ny, Nx);
for i = 1:Ny
    for j = 1:Nx
        R(i, j) = sqrt((x(j) - x_0)^2 + (y(i) - y_0)^2);
    end
end

u_a = zeros(Ny, Nx, Nt);
if nargout > 1
    v_a = zeros(Ny, Nx, Nt);
    if nargout > 2
        p_a = zeros(Ny, Nx, Nt);
    end
end
for i = 1:Ny
    for j = 1:Nx
        for k = 1:Nt
            t_char = 4*c*t(k)/R(i, j)^2;
            u_temp = u_const*(t_char*(1 - exp(-1/t_char)) + expint(1/t_char));
            u_a(i, j, k) = u_temp*(x(j) - x_0);
            if nargout > 1
                v_a(i, j, k) = u_temp*(y(i) - y_0);
                if nargout > 2
                    p_a(i, j, k) = p_const*expint(1/t_char);
                end
            end
        end
    end
end
end
