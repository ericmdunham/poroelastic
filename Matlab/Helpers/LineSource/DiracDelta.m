%% Author: Ossian O'Reilly
function [d k] = DiracDelta(p,s,k0,w,alpha)
% [d k] = construct_source_discretization(p,s,k0,w,alpha)
% Discretizes the Dirac delta distribution applied at arbitrary location x*.
% The discretization is accurate to order p and has s smooth derivatives in 
% frequency domain.
%
% Input:
%      p: Order of accuracy, or number of moment conditions - 1 
%         (degree of polynomial that must be integrated exactly)
%      s: Number of smoothness conditions, or number of derivatives of the
%         frequency spectrum to set to zero at the end points
%     k0: Index of the first grid point (from the left) in the source discretization
%      w: Weights for quadrature rule (must be equal to the number of grid points in
%         the source discretization)
%  alpha: Relative source location away from k0 such that if x* is the source
%         location on the grid then alpha = (x* - k0*h)/h, where h is the grid spacing
% Output:
%      d: (p+s+1) x 1 vector holding the values of the source discretization
%                     (unweighted)
%      k: (p+s+1) x 1 vector holding the indices of the source discretization
%
% Example: 
% This example features a two-point stencil at the boundary,
% and with the source location in the center of the stencil.
% p = 1;       % 2nd-order accuracy
% s = 0;       % No smoothness conditions
% k0 = 1;      % First point at the boundary
% w = [1/2 1]; % Weights of 2nd-order accurate quadrature rule at the boundary
% alpha = 1/2; % Source location in the center of the stencil
% [d k] = construct_source_discretization(p,s,k0,w,alpha)
% 
% d =
%  1.0
%  0.5
% 
% k =
%      1     2
%
% The source discretization above applied on a grid with N = 100 grid points,
% and grid spacing h:
% u = zeros(N,1); 
% u(k) = d/h;


% Stencil size
m = p + s + 1;
% Grid indices
k = k0 + [0:m-1];

% -- Moment conditions

% Construct Vandermonde matrix for the moment conditions
K = repmat(k,p+1,1);
powers = repmat((0:p)',1,m);
V = bsxfun(@power,K,powers);
% Construct matrix holding quadrature weights
w = w(:);
W = repmat(w(k)',p+1,1);
% Construct moment condition matrix
M = V.*W;
% Construct right-hand side, which equals x*^p/h^(p+1)
b1 = (k0 + alpha).^(0:p)';

% -- Smoothness conditions

powers = repmat((0:s-1)',1,m);

% Nyquist
nq = (-1).^k;
N = repmat(nq,s,1);
X = repmat(k,s,1);
X = bsxfun(@power,X,powers);
S = X.*N;

% Construct right-hand side, which is zero
b2 = zeros(s,1);


% Obtain solution
% Use quad-precision. Comment the line below to disable quad-precision.
M = vpa(M); S = vpa(S); b1 = vpa(b1); b2 = vpa(b2);
d = [M;S]\[b1;b2];

end