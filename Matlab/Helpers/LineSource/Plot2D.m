%% 2D plot of the line source problem
%
%   Input:
%   Vector x    - Spatial grid in x-direction
%   Vector y    - Spatial grid in y-direction
%   Scalar t    - Time point
%   Vector u    - Numerical solution vector of the horisontal displacement 
%                 Nx*Ny-by-1
%   Vector v    - Numerical solution vector of the vertical displacement
%                 Nx*Ny-by-1
%   Vector p    - Numerical solution vector of the pore pressure Nx*Ny-by-1
%   Vector u_a  - Analytical solution vector of the horisontal displacement
%                 Ny-by-Nx
%   Vector v_a  - Analytical solution vector of the vertical displacement
%                 Ny-by-Nx
%   Vector p_a  - Analytical solution vector of the pore pressure Ny-by-Nx
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [] = Plot2D(x, y, t, u, v, p, u_a, v_a, p_a)

Nx = length(x);
Ny = length(y);

font_size = 10;

rows = 1;
if nargin > 6
    rows = 2;
end

Nx = length(x);
Ny = length(y);
u = reshape(u, Ny, Nx);
v = reshape(v, Ny, Nx);
p = reshape(p, Ny, Nx);

% Plot the displacement u
subplot(rows, 3, 1);
surf(x, y, u);
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('Horisontal displacement u', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-37.5, 10);

% Plot the displacement v
subplot(rows, 3, 2);
surf(x, y, v);
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('Vertical displacement v', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(-127.5, 10);

% Plot the pressure
subplot(rows, 3, 3)
surf(x, y, p);
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
zlabel('Pore Pressure p', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
view(15, 10);

%if analytical solutions provided, plot heatmap of difference.
if nargin > 6
% Plot the difference in displacement u
subplot(rows, 3, 4);
imagesc(x, y, abs((u-u_a)));
colorbar;
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
title('abs(u-u_a)', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
set(gca,'Ydir','Normal')

% Plot the difference in displacement v
subplot(rows, 3, 5);
imagesc(x, y, abs((v-v_a)));
colorbar;
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
title('abs(v-v_a)', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
set(gca,'Ydir','Normal')

% Plot the difference in pressure p
subplot(rows, 3, 6)
imagesc(x, y, abs((p-p_a)));
colorbar;
xlabel('x', 'FontSize', font_size);
ylabel('y', 'FontSize', font_size);
title('abs(p-p_a)', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
set(gca,'Ydir','Normal')
end

title_with_time = suptitle(strcat('t = ', num2str(t)));
set(title_with_time, 'FontSize', font_size);
pause(0.01);

end
