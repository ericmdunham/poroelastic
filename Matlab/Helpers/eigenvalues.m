%% Presents the eigenvalues of a sparse Matrix in a plot
%
%   Input:
%   Matrix A - The sparse matrix, which eigenvalues are to be computed
%
%   Output:
%   Scalar m - The biggest real part of the eigenvalue of the matrix A.
%
% Authors: Kim Torberntsson, Vidar Stiernstrom
function [m] = Eigenvalues(A)

% If the matrix is sparse, make it full
A = full(A);

eigA = eig(A);
realA = real(eigA);
imagA = imag(eigA);
m = max(realA);

font_size = 20;
plot(realA, imagA, '*');
xlabel('Re(eig)', 'FontSize', font_size);
ylabel('Im(eig)', 'FontSize', font_size);
set(gca, 'FontSize', font_size);
grid on;
end
