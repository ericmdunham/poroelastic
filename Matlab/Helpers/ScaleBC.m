%% Scale the Robin boundary conditions 
%   The scaling of the Robin boundary conditions depends on 
% 	the material properties of the problem and the spacial 
% 	discretization. The order of accuracy of the Dirichlet
% 	conditions are determined from DirichletOrder.
%
%   Input:
%   Scalar lambda           - The first Lam� parameter
%   Scalar G                - The shear modulus
%   Scalar kappa            - The permeability
%   Vector x                - Spatial grid in x-direction
%   Vector y                - Spatial grid in y-direction
%   Scalar Nx               - Resolution in x-direction
%   Scalar Ny               - Resolution in y-direction
%   Scalar DirichletOrder 	- Order of accuracy of the Dirichlet conditions
%   Matrix BC               - Matrix describing the boundary conditions
%
%   Output:
%   Matrix BC    			- The scaled boundary conditions matrix
%
% Authors: Kim Torberntsson, Vidar Stiernstrom

function [BC] = ScaleBC(lambda, G, kappa, x, y, Nx, Ny, DirichletOrder, BC)

% West
BC(1,1) = Nx^(DirichletOrder - 1)*BC(1,1)*(lambda+2*G)/(x(2) - x(1));           %u
BC(2,1) = Nx^(DirichletOrder - 1)*BC(2,1)*G/(x(2) - x(1));                      %v
BC(3,1) = Nx^(DirichletOrder - 1)*BC(3,1)*kappa/(x(2) - x(1));                  %p

% East
BC(1,2) = Nx^(DirichletOrder - 1)*BC(1,2)*(lambda+2*G)/(x(end) - x(end - 1));   %u
BC(2,2) = Nx^(DirichletOrder - 1)*BC(2,2)*G/(x(end) - x(end - 1));           	%v
BC(3,2) = Nx^(DirichletOrder - 1)*BC(3,2)*kappa/(x(end) - x(end - 1));          %p

% South
BC(1,3) = Ny^(DirichletOrder - 1)*BC(1,3)*G/(y(2) - y(1));                      %u
BC(2,3) = Ny^(DirichletOrder - 1)*BC(2,3)*(lambda+2*G)/(y(2) - y(1));           %v
BC(3,3) = Ny^(DirichletOrder - 1)*BC(3,3)*kappa/(y(2) - y(1));                  %p

% North
BC(1,4) = Ny^(DirichletOrder - 1)*BC(1,4)*G/(y(end) - y(end - 1));              %u
BC(2,4) = Ny^(DirichletOrder - 1)*BC(2,4)*(lambda+2*G)/(y(end) - y(end - 1));   %v
BC(3,4) = Ny^(DirichletOrder - 1)*BC(3,4)*kappa/(y(end) - y(end - 1));          %p

end
