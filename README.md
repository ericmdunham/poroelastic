# Poroelasticity with SBP-SAT #

## Publications that use this code ##
------

1. Torberntsson, K., Stiernström, V., Mattsson, K. & Dunham, E. M. A finite difference method for earthquake sequences in poroelastic solids. Computat. Geosci. 22, 1351–1370 (2018).

2. Heimisson, E. R., Dunham, E. M., Almquist, M. Poroelastic effects destabilize mildly rate-strengthening friction to generate stable slow slip pulses. J. Mech. Phys. Solids 130, 262-279 (2019). *For information on how the code was used here, see section **Periodic Boundary Conditions** below.*


## Introduction ##
------

This project contains code used for solving the linear poroelastic equations in 2D using the SBP-SAT technique. The code was developed by [Vidar Stiernström](https://www.linkedin.com/in/vidar-stiernström-610381117) and [Kim Torberntsson](http://kimtorberntsson.com) supervised jointly by [Ken Mattsson](http://www.it.uu.se/katalog/ken) from Uppsala University and [Eric Dunham](https://pangea.stanford.edu/~edunham/) from Stanford University. The code was used for a project course called Project in Computational Science of (15 hp) and for our master thesis at Uppsala (30 hp). 

The 15 hp course was used to show well-posedness for the poroelastic equations (with a well-posed set of boundary conditions) and develop a discretization scheme using the SBP-SAT technique. Mandel's problem was used to verify the numerical scheme, since there are analytical solutions available for the unknown fields. The full report that we wrote for this project can be found [here](http://www.it.uu.se/edu/course/homepage/projektTDB/ht15/project08/Project08_Report.pdf).

For the master thesis work new operators that add artificial viscosity were used. This is important for being able to solve problems with sharp spatial gradients, without getting numerical oscillations in the unknown fields. Another problem called the line source problem was investigated and solved with the new operators. The numerical solutions were, just as with Mandel's problem, compared to analytical solutions.

The final problem considered was a setup with friction boundary conditions at a fault, with interesting applications for induced seismicity. The friction at the fault is described by a nonlinear model called rate and state friction, for which well-posedness was showed. In order to make more efficient computations an explicit formulation of the equations was derived. The time stepping for friction using the explicit formulation is performed using an adaptive Runge Kutta method. The full report for the master thesis work can be found [here](http://www.diva-portal.org/smash/record.jsf?dswid=-9569&pid=diva2%3A945945&c=3&searchType=SIMPLE&language=sv&query=kim+torberntsson&af=%5B%5D&aq=%5B%5B%5D%5D&aq2=%5B%5B%5D%5D&aqe=%5B%5D&noOfRows=50&sortOrder=author_sort_asc&onlyFullText=false&sf=all).

## Overview ##
------

The code is divided into two main parts, the code written in MATLAB and the code written in C using the high performance framework [PETSc](https://www.mcs.anl.gov/petsc/). The MATLAB code is used for simulations and quick development, while the C code is used for high performance and parallel computing. At this moment the C-based code uses matrices created in MATLAB to solve the problems in parallel, meaning that at this moment a MATLAB license is required to run the code and the systems are solved using a matrix approach.

## MATLAB code ##
------

In the MATLAB folder there are additional folders with different problem setups, such as the line source problem, Mandel's problem and problems setups with rate and state friction. In the folder called "Helpers" are additional functions and scripts that implement subroutines, such as plotting, creation of the spatial operators and unit conversions. 

The main philosophy behind this is that the scripts for the different problems mostly state the properties of the problem setup, while most of the logic and the functionality is located in the Helpers area. In this way a new problem setup can quickly be implemented and redundancy is reduced, also improving readability. It is important to be able to reuse code for different setups for development efficiency reasons.

## PETSc code ##
------

The high performance code is developed in C/C++ using the high performance framework PETSc and uses matrices and vectors exported from MATLAB to solve the problems efficiently in parallel using MPI. Because of the matrix approach the folder with this code is called "MatrixBased". Before running the code the data structures (problem parameters, matrices and vectors) defining the problem have to be exported from MATLAB to a binary format that PETSc can read. Once the data structures are built the calculations can be performed in parallel using PETSc.

### Problem setups ###
The implemented problem setups are all problems involving a poroelastic medium with a slipping fault region on the south boundary of the domain. The problem setups are divided in to the following two main catagories:

* **Horizontal loading problems**: These problems involve a constant loading force on the north boundary. Problem setups exist for either non-periodic boundary conditions, where one needs to specify boundary conditions on each boundary of the domain, or for periodic boundary conditions, where the domain is periodic in the x-direction and boundary conditions are specified at the north and south boundary. The main directory is located in MatrixBased/Friction/HorizontalLoading. Problem setup directories are located in MatrixBased/Friction/HorizontalLoading/NonPeriodicBC/ or MatrixBased/Friction/HorizontalLoading/PeriodicBC/.
* **Injection Problems**: Currently there are two implemented problem setups. The first involve a point source injecting fluid at a constant rate into the medium. The second problem involves a medium with a damage zone and constant influx of fluid at the west boundary. The main directory is located in MatrixBased/Friction/Injection. Problem setup directories are located in MatrixBased/Friction/HorizontalLoading/NonPeriodicBC/.

### Getting started ###
1. **Install PETSc** - Instructions are found [here](https://www.mcs.anl.gov/petsc/documentation/installation.html). 
2. **Clone the poroelastic git repository**.
3. **Compile the PETSc code**.

**Compile Horizontal loading problems**

From MatrixBased/Friction/HorizontalLoading/ compile using any of the following commands:

* make uniformFrictionSim - Setup with uniform friction properties across fault.
* make variableFrictionSim - Setup with variable friction properties across fault.
* make variableFrictionCTSim - Setup with variable friction properties across fault using coordinate transform to stretch grid in y-direction.
* make variableFrictionCTPeriodicSim -  Setup with variable friction properties across fault using coordinate transform to stretch grid in y-direction and periodic boundary conditions at the east and west boundary.

**Compile Injection problems**

From MatrixBased/Friction/Injection/ compile using any of the following commands:

* make pointInjectorSim - Setup with constant injection at a point located in the domain.
* make uniformInflowSim - Setup with uniform influx from the west boundary, with a damage zone resulting in higher flow along fault.

### Running the code ###

1. Start MATLAB from bash and run generateDataStructures.m to generate in-files for your problem setup. The in-files are problem parameters and binary files for matrices and vectors which are read in the PETSc code. These files are stored in the directory in/ located within the problem setup directory. genererateDataStructures.m also creates the file setup.mat which stores the relevant problem parameters, later used in saveResultToMat.m and plotResult.m.
2. Run the PETSc code with mpiexec -np N ./EXECUTABLE SOLVER -KSP_OPTIONS, where N is the number of processes, SOLVER is either 'direct' or 'iterative' and -KSP_OPTIONS are optional command line options for the linear solver found [here](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetFromOptions.html).
3. Results are written to the directory out/ located in the specific problem directory. To convert the PETSc binary files to .mat files run saveResultsToMat.m. A script plotResults.m for plotting and making movies has also been provided.

The code can be run in the two modes:

1. **Profiling mode** - Intended for time profiling different parts of the code. All computations are carried out, but no data is saved. At the end of execution a print out of the wall time (total, not for each process) spent in different parts of the code. The mode is set by compiling with the preprocessor flag DO_PROFILING 1 (currently has to be set in main.src).
2. **Simulation mode** - Intended for simulations. All computations are carried out, and data is saved to binary files in the out/ directory located in the specific problem directory. The binary files can then be imported into MATLAB for plotting and evaluation. The parameters faultWriteStride and bodyWriteStride (found in generateDataStructures.m in the specific problem directory) controls the frequency of the output. The mode is set by compiling with the preprocessor flag DO_PROFILING 0 (currently has to be set in main.src)

**Running on CEES ** 
To run on the CEES-cluster the .bashrc profile must be configured for PETSc. Talk to the administrator to set it up. Example job scripts for running on the CEES cluster can be found under MatrixBased/CEESJobScripts.

## Periodic Boundary Conditions ##
------

This sections describes how to generate simulations as in Heimisson et al. 2018 with periodic boundary conditions on sides and Dirichlet boundary condition at the fault/slip surface.

1. **Checkout the the version of the code used in the paper.** In a terminal, navigate to the poroelastic folder and type: `git checkout Heimisson2018`
2. **Clone the mercurial repository sbplib.** In a terminal, navigate to a suitable folder and use the `hg clone` command: `cd my/favorite/location` followed by `hg clone ssh://hg.werpers.com/repos/public/sbplib`
3. **Update to the version used in Heimisson2018** by typing `cd sbplib` followed by `hg update Heimisson2018`.
4. **Repeat steps 2-3 for the mercurial repository elastic-operators.** In a terminal: `cd my/favorite/location`, `hg clone http://hg.code.sf.net/p/elastic-operators/code elastic-operators`, `cd elastic-operators`, `hg update Heimisson2018`.
5. Follow the instructions above for Horizontal loading with periodic boundary conditions on how to compile and run the code.
6. Examples of usage are provided in a script: run\_genDataStruct\_Heimisson2018.m and function: generateDataStructures\_Heimisson2018.m located in poroelastic/MatrixBased/Friction/HorizontalLoading/PeriodicBC/VariableFrictionCoordinateTransform/. The paths in run\_genDataStruct\_Heimisson2018.m will need to be changed to my/favorite/location/sbplib and my/favorite/location/elastic_operators.
 

## Future Improvements ##
------

The code will not continue to be developed by us, but other researchers/developers might build on this project. These are some suggestions for interesting possible future directions and features we did not have time to add.

* Change the treatment of Dirichlet boundary conditions. Currently the Dirichlet conditions are implemented using the Robin technique, resulting in an unnecessary stiff system. This particularly affects the explicit fluid content simulation, for cases where Dirichlet conditions are imposed in the fluid diffusion equation, since the time step will be limited. Changing to the standard Dirichlet boundary treatment should result in a less stiff system. **Edit:** *This has been improved in the simulations with rate-and-state friction, horizontal loading and periodic boundary conditions performed in [Heimisson et al., 2018].*  
* Find a well-suited preconditioner. Currently, no effort has been made to find a preconditioner that is well-suited for the problems, and as a result the iterative solver performs poorly. An iterative solver will be crucial when studying problems where high resolution is needed, or if one wants to extend the code to 3D.
* Implementation of a stencil based code - The PETSc code is Matrix-based and therefore uses a lot of memory. For a 3D problem or a problem with lots of grid points, a stencil based code would be preferred for memory and efficiency reasons, since no matrices has to be stored. A stencil based code could be developed without the usage of MATLAB, which would remove this dependency as well.
* 3D Code - The real problems are of course in 3D and therefore simulations in 3D are very interesting. Developing such a code is a large effort though and was definitely beyond the scope of this project.
* Implement a time integration method that allows for implicit time stepping during interseismic periods and explicit time stepping during earthquake nucleation processes.
* Use 2 poroelastic blocks - Right now the interface conditions are implemented as boundary conditions. This can be justified for certain symmetric problems, but for more general problems an interface between 2 poroelastic blocks should be used instead. This would make it possible to look at more realistic problem setups in 2D.
* Implement MMS for rate and state friction - Since no analytical solutions have been derived for the problem setup with rate and state friction at the time of writing, we cannot compare the numerical solution to an analytical solution. One could use MMS - the method of manufactured solutions, to validate the simulations. However, we did not have time to implement it for this project.

License
------

The code can be used freely and without restriction.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Acknowledging the code
------
If the code is used for publication or presentation, please reference:

Torberntsson, K., Stiernström, V., Mattsson, K. & Dunham, E. M. A finite difference method for earthquake sequences in poroelastic solids. Computat. Geosci. 22, 1351–1370 (2018).

If simulations using periodic boundary conditions or Dirichlet implementation of boundary conditions are used the following paper should also be referenced:

Heimisson, E. R., Dunham, E. M., Almquist, M. Poroelastic effects destabilize mildly rate-strengthening friction to generate stable slow slip pulses. J. Mech. Phys. Solids 130, 262-279 (2019).



